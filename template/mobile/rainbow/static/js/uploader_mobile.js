// =======================
// 函数定义
// =======================

function dataURLToBlob(dataURL) {
    // dataURL 的格式为 “data:image/png;base64,****”
    var p = dataURL.indexOf(':');
    if (p <= 0) return null;
    dataURL = dataURL.substring(p + 1);
    p = dataURL.indexOf(';');
    if (p <= 0) return null;
    var mime = dataURL.substring(0, p);
    dataURL = dataURL.substring(p + 1);
    p = dataURL.indexOf(',');
    if (p <= 0) return null;
    var data = dataURL.substring(p + 1);
    data = window.atob(data);
    var ia = new Uint8Array(data.length);
    for (var i = 0; i < data.length; i++) {
      ia[i] = data.charCodeAt(i);
    };
    var blob = new Blob([ia], { type: mime });
    return blob;
  }

  //请在页面中指定上传网址：
  //window.MOB_UPLOADER_URL="{:U('Admin/Ueditor/imageUp',['savepath'=>'cloud_card','pictitle'=>'-','dir'=>'images'])}";
  function upload(fileName, dataURL, completeFunc, uploadURL) {
    var data = new FormData(),
      sucFn, errFn = function(r) {
        console.log(r, '报错1')
        alert(r.state);
        return true;
      };
    if (completeFunc != null) {
      if (typeof(completeFunc) == 'function') {
        sucFn = completeFunc;
      } else {
        if (typeof(completeFunc.suc) == 'function') sucFn = completeFunc.suc;
        if (typeof(completeFunc.err) == 'function') errFn = completeFunc.err;
      }
    }
    //底部对话框
    // var index=layer.open({content: window.MOB_UPLOADER_TITLE||'正在上传中，请稍候...' ,skin: 'footer', shadeClose: false});
    data.append('file', dataURLToBlob(dataURL), fileName);
    $.ajax({
      url: uploadURL || window.MOB_UPLOADER_URL || '/index.php/Admin/Ueditor/imageUp/savepath/head_pic/pictitle/notitle/dir/images.html',
      type: 'POST',
      data: data,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(r) {
        // layer.close(index);
        if (r.state == 'SUCCESS') {
          return sucFn != null ? sucFn(r) : true;
        }
        return errFn != null ? errFn(r) : true;
      },
      error: function(xhr, textStatus, errorThrown) {
        // layer.close(index);

        console.log(xhr, '报错2')
        console.log(textStatus, '报错2')
        console.log(errorThrown, '报错2')
        alert(textStatus);
      }
    });
  };

  // ===============================
  // 实施代码
  // ===============================
  var UPLOAD_MAX_SIZE = null;

  function mobUploader(fileElem, previewElem, completeFunc, uploadURL, maxSize) {
    var funcs = { before: null, after: null };
    if (typeof(completeFunc) == 'function') {
      funcs.after = completeFunc;
    } else if (completeFunc != null && typeof(completeFunc) == 'object') {
      funcs = $.extend({}, funcs, completeFunc);
    }
    var attachEvent = function(maxSize) {
      //选择图片
      $('body').on('change', fileElem, function() {
        if (typeof(funcs.before) == 'function') {
          if (!funcs.before(this)) return false;
        }
        var fileReader = new FileReader(),
          files = this.files,
          file, that = $(this);
        if (!files.length) return;
        that.data('filename', that.val());
        file = files[0];
        if (file.size > maxSize) { //用size属性判断文件大小不能超过1M
          console.log('传的文件太大了')
          alert("你上传的文件太大了！111");
          return;
        }
        if (/^image\/\w+$/.test(file.type)) {
          fileReader.readAsDataURL(file);
          fileReader.onload = function() {

            upload(that.val(), this.result, funcs.after, uploadURL);
            if (previewElem == null) return true;
            var $preview = $(previewElem);
            if ($preview[0].tagName.toUpperCase() == 'IMG') {
              $preview.attr('src', this.result);
            } else {
              $preview.html('<img src="' + this.result + '" />');
            }
          };
        }
      });
    }
    if (maxSize != null) return attachEvent(maxSize);
    return getUploadMaxSize(attachEvent);
  }

  function getUploadMaxSize(attachEvent) {
    if (attachEvent == null) return;
    if (UPLOAD_MAX_SIZE != null) return attachEvent(UPLOAD_MAX_SIZE);
    $.get('/index.php/Admin/Ueditor/maxSize', {}, function(r) {
      maxSize = parseInt(r);
      if (isNaN(maxSize)) maxSize = 1024 * 1024;
      attachEvent(maxSize);
    }, 'html');
  }