define({ "api": [
  {
    "type": "GET",
    "url": "api/api/getUserTeamsAll",
    "title": "获取用户所有的下级",
    "version": "1.0.0",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "admin",
    "name": "GetApiApiGetuserteamsall"
  },
  {
    "type": "POST",
    "url": "api/api/selectUser",
    "title": "手机号码搜索",
    "version": "1.0.0",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>非必需，手机号码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>非必需，用户id，多个用英文,分割</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "isAccurate",
            "description": "<p>是否精确搜索，0否1是，默认值为0</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "currPage",
            "description": "<p>页码，传0时返回所有记录</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalCount",
            "description": "<p>总条数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalPage",
            "description": "<p>总页数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.currPage",
            "description": "<p>当前页</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.list",
            "description": "<p>列表数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.trueName",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.levelName",
            "description": "<p>会员等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.firstLeaderMobile",
            "description": "<p>上级手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.underlingNumber",
            "description": "<p>下级人数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.loginNum",
            "description": "<p>登录次数</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.lastLogin",
            "description": "<p>最后登录时间</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.regTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.headPic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.isLock",
            "description": "<p>是否允许登录0允许1不允许</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "admin",
    "name": "PostApiApiSelectuser"
  },
  {
    "type": "POST",
    "url": "api/api/selectUserByNickname",
    "title": "昵称搜索",
    "version": "1.0.0",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>用户昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "isAccurate",
            "description": "<p>是否精确搜索，0否1是，默认值为0</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "currPage",
            "description": "<p>页码，传0时返回所有记录</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalCount",
            "description": "<p>总条数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalPage",
            "description": "<p>总页数</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "admin",
    "name": "PostApiApiSelectuserbynickname"
  },
  {
    "type": "GET",
    "url": "api/api/newsTypeList",
    "title": "拓客商学院文章导航",
    "version": "1.0.0",
    "group": "article",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.sortId",
            "description": "<p>导航id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.title",
            "description": "<p>导航标题</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.link",
            "description": "<p>跳转地址</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "article",
    "name": "GetApiApiNewstypelist"
  },
  {
    "type": "POST",
    "url": "api/api/newsDetail",
    "title": "拓客商学院文章详情",
    "version": "1.0.0",
    "group": "article",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "newsId",
            "description": "<p>文章id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.type",
            "description": "<p>类型1 视频 2 图文 3音频</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.desc",
            "description": "<p>文章摘要</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.author",
            "description": "<p>作者</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.newsId",
            "description": "<p>文章id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.readNum",
            "description": "<p>阅读数</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.tagList",
            "description": "<p>关键字数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.coverList",
            "description": "<p>缩略图数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.fileUrl",
            "description": "<p>附件地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.addTime",
            "description": "<p>添加时间</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.content",
            "description": "<p>文章内容</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.catId",
            "description": "<p>分类id</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "article",
    "name": "PostApiApiNewsdetail"
  },
  {
    "type": "POST",
    "url": "api/api/newsList",
    "title": "拓客商学院文章列表",
    "version": "1.0.0",
    "group": "article",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sortId",
            "description": "<p>类型id，10000精选，10001音频，10002视频，6新手必看，7操作指南，8高手进阶，11成功案例，12常见问题</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "currPage",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pageSize",
            "description": "<p>每页条数</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.currPage",
            "description": "<p>页码</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalCount",
            "description": "<p>总条数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.totalPage",
            "description": "<p>总页数</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.showType",
            "description": "<p>类型1 视频 2 图文 3音频</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.showStyle",
            "description": "<p>1 单图展示 2 三图展示</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.isTop",
            "description": "<p>是否置顶1是0否</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.desc",
            "description": "<p>文章摘要</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.author",
            "description": "<p>作者</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.newsId",
            "description": "<p>文章id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.readNum",
            "description": "<p>阅读数</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.list.tagList",
            "description": "<p>关键字数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.list.coverList",
            "description": "<p>缩略图数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.fileUrl",
            "description": "<p>附件地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.detailUrl",
            "description": "<p>详情地址</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "article",
    "name": "PostApiApiNewslist"
  },
  {
    "type": "POST",
    "url": "api/api/clickAddLockNum",
    "title": "分享点击增加临时锁定数量",
    "version": "1.0.0",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "shareId",
            "description": "<p>分享UID</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "shareCookiesId",
            "description": "<p>sessionId或cookiesId</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "null",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "order",
    "name": "PostApiApiClickaddlocknum"
  },
  {
    "type": "POST",
    "url": "api/api/twoConfirmGiveGift",
    "title": "订单审核二次确认",
    "version": "1.0.0",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "giveGiftId",
            "description": "<p>送礼表id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "null",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "order",
    "name": "PostApiApiTwoconfirmgivegift"
  },
  {
    "type": "POST",
    "url": "api/api/updateOrderStatus",
    "title": "订单审核通过",
    "version": "1.0.0",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "collectUserId",
            "description": "<p>店主的用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "null",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "order",
    "name": "PostApiApiUpdateorderstatus"
  },
  {
    "type": "GET",
    "url": "api/api/bottonNav",
    "title": "底部导航",
    "version": "1.0.0",
    "group": "other",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>上级数组</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.name",
            "description": "<p>中文名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.url",
            "description": "<p>H5跳转地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.image",
            "description": "<p>导航图片选中</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.imageUnselect",
            "description": "<p>导航未选中图片</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.nameEnglish",
            "description": "<p>英文名称</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "GetApiApiBottonnav"
  },
  {
    "type": "GET",
    "url": "api/api/saleIndex",
    "title": "首页",
    "version": "1.0.0",
    "group": "other",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.bannerList",
            "description": "<p>轮播数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bannerList.adName",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bannerList.images",
            "description": "<p>轮播图片</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bannerList.bgcolor",
            "description": "<p>轮播背景色</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bannerList.link",
            "description": "<p>链接</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.noticesList",
            "description": "<p>公告数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.noticesList.title",
            "description": "<p>公告标题</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.noticesList.link",
            "description": "<p>公告详情链接</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.levelList",
            "description": "<p>等级数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.levelList.name",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.levelList.icon",
            "description": "<p>等级图标</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.navList",
            "description": "<p>导航</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.navList.name",
            "description": "<p>中文名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.navList.url",
            "description": "<p>H5跳转地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.navList.image",
            "description": "<p>导航图片选中</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.navList.imageUnselect",
            "description": "<p>导航未选中图片</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.navList.nameEnglish",
            "description": "<p>英文名称</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.bgList",
            "description": "<p>背景图</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bgList.bgImgUrl",
            "description": "<p>首页背景图</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.bgList.bgColor",
            "description": "<p>颜色(暂时为空)</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.userData",
            "description": "<p>用户数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.headPic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.levelName",
            "description": "<p>等级名</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.directUserMobile",
            "description": "<p>推荐人</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.nickName",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.creditPoint",
            "description": "<p>信用分</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.userContribution",
            "description": "<p>我的贡献</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.teamContribution",
            "description": "<p>团队贡献</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.compensateNum",
            "description": "<p>可分配流量</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.connecterMobile",
            "description": "<p>接点人</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.publicQrCode",
            "description": "<p>公众号二维码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.des",
            "description": "<p>描述</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "GetApiApiSaleindex"
  },
  {
    "type": "GET",
    "url": "api/api/shareCode",
    "title": "分享拓客",
    "version": "1.0.0",
    "group": "other",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>上级数组</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.name",
            "description": "<p>名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.images",
            "description": "<p>背景图片地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.contact",
            "description": "<p>公司联系方式</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.mobile",
            "description": "<p>推荐人手机号码，为空时前端不显示该项</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.wxAccount",
            "description": "<p>推荐人微信号，为空时前端不显示该项</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.inviteCode",
            "description": "<p>邀请码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.qrCodeUrl",
            "description": "<p>二维码内容</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "GetApiApiSharecode"
  },
  {
    "type": "GET",
    "url": "api/api/teamSearch",
    "title": "行会大厅列表",
    "version": "1.0.0",
    "group": "other",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "currPage",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "scene",
            "description": "<p>场景1我的贡献2我的下级</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>搜索的关键词</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.headPic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.mobile",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.title",
            "description": "<p>标签</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.jiedian",
            "description": "<p>接点人，为空时不显示</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.tuijian",
            "description": "<p>推荐人，为空时不显示</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.levelName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.addTime",
            "description": "<p>加入时间</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.devote",
            "description": "<p>他的贡献</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "GetApiApiTeamsearch"
  },
  {
    "type": "GET",
    "url": "api/api/teamUserData",
    "title": "行会大厅用户信息",
    "version": "1.0.0",
    "group": "other",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.userData",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.headPic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userData.levelName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.connecterNum",
            "description": "<p>最多可接点</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.firstLeader",
            "description": "<p>我的贡献</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.teamNum",
            "description": "<p>行会信息</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userData.aboveNum",
            "description": "<p>第一关及以上人数</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "GetApiApiTeamuserdata"
  },
  {
    "type": "POST",
    "url": "api/api/levelList",
    "title": "等级列表",
    "version": "1.0.0",
    "group": "other",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.levelId",
            "description": "<p>等级id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.showName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.amount",
            "description": "<p>等级价格</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.levelName",
            "description": "<p>头衔名称</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "other",
    "name": "PostApiApiLevellist"
  },
  {
    "type": "POST",
    "url": "a1",
    "title": "公共参数说明",
    "version": "1.0.0",
    "group": "public",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apiVersion",
            "description": "<p>接口版本</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>终端类型，Android，iOS，H5，PHP</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "timeStamp",
            "description": "<p>时间戳，秒</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "privateSign",
            "description": "<p>验签串</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>APP当前版本</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>语言，zh中文en英文</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "deviceBrand",
            "description": "<p>非必需，终端名牌</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "systemVersion",
            "description": "<p>非必需，终端系统版本</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "deviceModel",
            "description": "<p>非必需，终端型号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "netType",
            "description": "<p>非必需，网络类型</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api.php",
    "groupTitle": "public",
    "name": "PostA1"
  },
  {
    "type": "POST",
    "url": "a2",
    "title": "验签规则",
    "version": "1.0.0",
    "group": "public",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "a",
            "description": "<p>签名生成算法：签名生成规则如下：参与签名的字段包括privateKey,privateSecret, timestamp（时间戳）,提交的请求参数。对所有待签名参数按照字段名的ASCII 码从小到大排序（字典序）后，使用URL键值对的格式（即key1=value1&amp;key2=value2…）拼接成字符串string1。。对string1作md5加密（小写），再截取10-20之前的值，做为验签的参数提交在最后，即 privateSign=md5(string1).subString（10，20）。</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "Android",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Android.privateKey",
            "description": "<p>2ED689591F83D4685B58815D67B4D6CD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Android.privateSecret",
            "description": "<p>Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "iOS",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "iOS.privateKey",
            "description": "<p>2ED689591F83D4685B58815D67B4D6CD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "iOS.privateSecret",
            "description": "<p>Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "PHP",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "PHP.privateKey",
            "description": "<p>2ED689591F83D4685B58815D67B4D6CD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "PHP.privateSecret",
            "description": "<p>Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "H5",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "H5.privateKey",
            "description": "<p>4B9E2AB213CB02BD839A771055F5CC43</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "H5.privateSecret",
            "description": "<p>75392D04934E787B28F6AB6A45BDFCAB</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api.php",
    "groupTitle": "public",
    "name": "PostA2"
  },
  {
    "type": "POST",
    "url": "a3",
    "title": "商城与会员系统关联点",
    "version": "1.0.0",
    "group": "public",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "session",
            "description": "<p>登录成功后，会存在一个session为session('user')，里面存有用户表的整行信息</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "users_hash",
            "description": "<p>存储有用户的信息，为一个redis hash，key为users_hash，如果想获取一个用户的信息用$redis-&gt;hget(&quot;users_hash&quot;, 1)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "users_hash.user_id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.login_token",
            "description": "<p>loginToken</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.head_pic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "users_hash.first_leader",
            "description": "<p>推荐人用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "users_hash.real_status",
            "description": "<p>实名认证状态1未验证2待审核3验证不通过4实名认证成功</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.true_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "users_hash.sex",
            "description": "<p>性别0 保密 1 男 2 女</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.mobile",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.first_leader_mobile",
            "description": "<p>推荐人手机号码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.levelName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.level",
            "description": "<p>等级</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "users_hash.paypwd",
            "description": "<p>支付密码</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api.php",
    "groupTitle": "public",
    "name": "PostA3"
  },
  {
    "type": "GET",
    "url": "api/api/batchesCity",
    "title": "分批获取城市",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "parentId",
            "description": "<p>城市id，获取省时为0</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.list.name",
            "description": "<p>名称</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "GetApiApiBatchescity"
  },
  {
    "type": "GET",
    "url": "api/api/cityAreas",
    "title": "城市地区数据",
    "version": "1.0.0",
    "group": "user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.province",
            "description": "<p>省名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.provinceId",
            "description": "<p>省id</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list.list",
            "description": "<p>城市数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.list.city",
            "description": "<p>城市名</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.list.cityId",
            "description": "<p>城市ID</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list.list.areaList",
            "description": "<p>地区数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.list.list.areaList.area",
            "description": "<p>地区名</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.list.list.areaList.areaId",
            "description": "<p>地区ID</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "GetApiApiCityareas"
  },
  {
    "type": "GET",
    "url": "api/api/imgCode",
    "title": "图形验证码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "scene",
            "description": "<p>1注册2登录密码3支付密码4修改绑定手机号5修改备用手机号6忘记密码7用户登录身份验证</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.sessionId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.imgCodeUrl",
            "description": "<p>图形验证码地址</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "GetApiApiImgcode"
  },
  {
    "type": "GET",
    "url": "api/api/userData",
    "title": "商城获取用户信息",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>非必需，二选一，token</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "userId",
            "description": "<p>非必需，二选一，用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result.data",
            "description": "<p>数据集</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.data.user_id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.login_token",
            "description": "<p>loginToken</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.head_pic",
            "description": "<p>头像</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.data.first_leader",
            "description": "<p>推荐人用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.data.real_status",
            "description": "<p>实名认证状态1未验证2待审核3验证不通过4实名认证成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.true_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.data.sex",
            "description": "<p>性别0 保密 1 男 2 女</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.mobile",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.first_leader_mobile",
            "description": "<p>推荐人手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.levelName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.level",
            "description": "<p>等级</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.data.paypwd",
            "description": "<p>支付密码</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "GetApiApiUserdata"
  },
  {
    "type": "POST",
    "url": "api/api/appUpdate",
    "title": "检测APP和城市数据库的版本更新",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>APP当前版本号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>客户端区分，Android，iOS。</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.newVersion",
            "description": "<p>新版本号</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.updateLog",
            "description": "<p>更新内容</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.updateLogEn",
            "description": "<p>英文版更新内容</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.forceUpdate",
            "description": "<p>是否（1/0）强制更新</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.showAppUpdate",
            "description": "<p>是否（1/0）显示更新  --ios用</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.isCanUpdate",
            "description": "<p>app是否（1/0）可以更新(是否有新版本)  ios用</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.backgroundImg",
            "description": "<p>UI更新图地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.backgroundImgEn",
            "description": "<p>英文版UI更新图地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.updateUrl",
            "description": "<p>app更新地址</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.cityVersion",
            "description": "<p>城市数据库版本号</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.updateType",
            "description": "<p>1.原生提供 2.UI提供更新图片</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiAppupdate"
  },
  {
    "type": "POST",
    "url": "api/api/balanceChangeStatus",
    "title": "是否开启用户余额转账功能",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "null",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiBalancechangestatus"
  },
  {
    "type": "POST",
    "url": "api/api/bindAccount",
    "title": "绑定支付宝或微信",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>类型 1支付宝 2微信</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>账号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiBindaccount"
  },
  {
    "type": "POST",
    "url": "api/api/changeMobile",
    "title": "修改绑定手机号，修改备用手机",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>类型 1修改绑定手机号，2修改备用手机</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiChangemobile"
  },
  {
    "type": "POST",
    "url": "api/api/changePassword",
    "title": "修改登录密码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiChangepassword"
  },
  {
    "type": "POST",
    "url": "api/api/changePayPassword",
    "title": "修改支付密码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passwordConfirm",
            "description": "<p>第二次密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiChangepaypassword"
  },
  {
    "type": "POST",
    "url": "api/api/changePayPasswordForMall",
    "title": "总商城后台修改支付密码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passwordConfirm",
            "description": "<p>第二次密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiChangepaypasswordformall"
  },
  {
    "type": "POST",
    "url": "api/api/editUser",
    "title": "修改个人资料",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>非必须，昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "headPic",
            "description": "<p>非必须，头像图片链接</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sex",
            "description": "<p>非必须，性别，1男2女</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>非必须，出生日期</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiEdituser"
  },
  {
    "type": "POST",
    "url": "api/api/forgetPwd",
    "title": "忘记密码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiForgetpwd"
  },
  {
    "type": "POST",
    "url": "api/api/getAgreement",
    "title": "获取协议",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>协议类型：1用户购买协议</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.title",
            "description": "<p>协议标题</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.content",
            "description": "<p>协议内容</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiGetagreement"
  },
  {
    "type": "POST",
    "url": "api/api/getBackground",
    "title": "获取背景图",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "scene",
            "description": "<p>场景 1登录页背景图</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.bgImgUrl",
            "description": "<p>背景图</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.bgColor",
            "description": "<p>背景颜色</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiGetbackground"
  },
  {
    "type": "POST",
    "url": "api/api/getGuideImg",
    "title": "获取导航指引",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guideId",
            "description": "<p>导航位置 1:首页 2:分享拓客 3:个人中心 4:拓客社交 5:拓客直播 6:拓客商学院 7:全网拓客 8:申请闯关 9:审核闯关 10:行会大厅 11.全国优品</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.guideImgs",
            "description": "<p>引导图片</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiGetguideimg"
  },
  {
    "type": "POST",
    "url": "api/api/getPopupAdList",
    "title": "获取首页弹窗广告",
    "version": "1.0.0",
    "group": "user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.popupAdList.adName",
            "description": "<p>广告名称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.popupAdList.adLink",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.popupAdList.adCode",
            "description": "<p>图片地址</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiGetpopupadlist"
  },
  {
    "type": "POST",
    "url": "api/api/getUserRegAgreementUrl",
    "title": "用户注册协议跳转链接",
    "version": "1.0.0",
    "group": "user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.url",
            "description": "<p>拓客新零售用户注册协议URL</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiGetuserregagreementurl"
  },
  {
    "type": "POST",
    "url": "api/api/logout",
    "title": "退出登录",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiLogout"
  },
  {
    "type": "POST",
    "url": "api/api/parentList",
    "title": "获取用户所有上级",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "userId",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "result.list",
            "description": "<p>上级数组</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiParentlist"
  },
  {
    "type": "POST",
    "url": "api/api/realName",
    "title": "实名认证",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "trueName",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardNumber",
            "description": "<p>身份证号码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardFrontPhoto",
            "description": "<p>身份证正面图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardBackPhoto",
            "description": "<p>身份证反面图片地址</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiRealname"
  },
  {
    "type": "POST",
    "url": "api/api/smsCode",
    "title": "获取短信验证码",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scene",
            "description": "<p>场景，1注册,2修改登录密码，3修改支付密码，4修改绑定手机号码，5修改备用手机号，6忘记密码 7用户短信注册</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sessionId",
            "description": "<p>非必需，图形验证码的会话ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "verifyCode",
            "description": "<p>非必需，图形验证码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.code",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.smsTimeOut",
            "description": "<p>短信验证码过期时间</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiSmscode"
  },
  {
    "type": "POST",
    "url": "api/api/temporaryBindForMobile",
    "title": "不注册 临时锁定第一个推荐人",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstLeaderMobile",
            "description": "<p>推荐人手机号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiTemporarybindformobile"
  },
  {
    "type": "POST",
    "url": "api/api/tokenGetSessionId",
    "title": "获取sessionId",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.sessionId",
            "description": "<p>session_id</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiTokengetsessionid"
  },
  {
    "type": "POST",
    "url": "api/api/userInfo",
    "title": "用户资料",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginToken",
            "description": "<p>token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.mobile",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.mobileBak",
            "description": "<p>备用号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.email",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.sex",
            "description": "<p>性别0 保密 1 男 2 女</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.birthday",
            "description": "<p>出生日期</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.userMoney",
            "description": "<p>用户金额</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.frozenMoney",
            "description": "<p>冻结金额</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.headPic",
            "description": "<p>头像url</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.level",
            "description": "<p>等级</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.levelName",
            "description": "<p>等级名称</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.ztjNum",
            "description": "<p>我的贡献人数</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.teamNum",
            "description": "<p>行会信息人数</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.firstLeaderMobile",
            "description": "<p>推荐人手机号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.realStatus",
            "description": "<p>实名认证状态1未验证2待审核3验证不通过4实名认证成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.trueName",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.cardNumber",
            "description": "<p>身份证号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.cardNumberStar",
            "description": "<p>打星的身份证号码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.cardFrontPhoto",
            "description": "<p>身份证正面照片</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.cardBackPhoto",
            "description": "<p>身份证反面照片</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.zfbAccount",
            "description": "<p>支付宝账号</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.wxAccount",
            "description": "<p>微信账号</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.userContribution",
            "description": "<p>我的贡献</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.teamContribution",
            "description": "<p>团队贡献</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.compensateNum",
            "description": "<p>可分配流量</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.connecterMobile",
            "description": "<p>接点人</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "result.temporaryLockingNum",
            "description": "<p>锁定人数</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiUserinfo"
  },
  {
    "type": "POST",
    "url": "api/api/userLogin",
    "title": "用户登录",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "loginType",
            "description": "<p>用户登录类型 1 短信验证码注册登录 2 账号密码登录</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobileCode",
            "description": "<p>短信验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sessionId",
            "description": "<p>sessionId</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.sessionId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.loginToken",
            "description": "<p>用户的token</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiUserlogin"
  },
  {
    "type": "POST",
    "url": "api/api/userReg",
    "title": "用户注册",
    "version": "1.0.0",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "invitePhone",
            "description": "<p>推荐人手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "paytypeZfb",
            "description": "<p>支付宝</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "paytypeWx",
            "description": "<p>微信</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "source",
            "description": "<p>非必需，注册来源</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "regType",
            "description": "<p>非必需，注册方式，1正常注册2为他人注册</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>返回码,10000为成功</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>返回消息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "result.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.sessionId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.loginToken",
            "description": "<p>用户的token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.activity_page_url",
            "description": "<p>用户注册后跳转活动页的地址</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/Api100.php",
    "groupTitle": "user",
    "name": "PostApiApiUserreg"
  }
] });
