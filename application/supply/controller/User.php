<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/9/16
 * Time: 10:13
 */
namespace app\supply\controller;
use app\common\logic\UserLogic;
use app\common\model\UserToken;

class User extends Base
{
    //获取用户id
    private function getUserId()
    {
        $mobile = input('mobile');
        if(empty($mobile)){
            api_response('90006','手机号不能为空');
        }
        $userId = model('users')->where('mobile', $mobile)->value('user_id');
        if(empty($userId)){
            api_response('90007','用户不存在');
        }
        return $userId;
    }

    //获取所有的下级
    public function getUserTeamsAll()
    {
        $userId = $this->getUserId();
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $pageSize = $pageSize > 200?200:$pageSize;

        $result = model('common/distribut/DistributTeam')->getUserTeamsAllForPage($userId,$page,$pageSize);
        if($result['list']) {
            $userIdStr = implode(',', $result['list']);
            $resultAccount = $result ? model('users')->where('user_id', 'in', $userIdStr)->order("field(user_id,{$userIdStr})")->column('mobile') : [];
        }else{
            $resultAccount = [];
        }
        $data = [
            'list'=>$resultAccount,
            'count' => $result['count'],
            'totalPage' => ceil($result['count']/$pageSize),
            'page'=>$page,
            'pageSize' => $pageSize
        ];
        api_response(10000, '请求成功', $data);
    }

    //获取所有的上级
    public function parentList()
    {
        $userId = $this->getUserId();

        $cacheKey = 'supply_parent_list_'.md5($userId);
        $resultAccount = cache($cacheKey);
        if(empty($resultAccount)) {
            $result = model('common/distribut/DistributTeam')->getFirstLeaderTeam($userId);
            $userIdStr = implode(',', $result);
            $resultAccount = $result ? model('users')->where('user_id', 'in', $result)->order("field(user_id,{$userIdStr})")->column('mobile') : [];
            cache($cacheKey, $resultAccount, 60);
        }
        api_response(10000, '请求成功',['list'=>$resultAccount]);
    }

    //获取用户信息
    public function getUserData()
    {
        $mobileStr = input('mobileStr','');
        if(empty($mobileStr)){
            api_response(90009, '手机号码不能为空');
        }
        $cacheKey = 'supply_user_'.md5($mobileStr);
        $list = cache($cacheKey);
        if(empty($list)) {
            $userIdArr = model('users')->where('mobile', 'in', $mobileStr)->column('user_id');
            $list = [];
            if ($userIdArr) {
                $list = model('Users')->supplyGetUserDataList($userIdArr);
            }
            cache($cacheKey, $list, 60);
        }
        api_response(10000, '请求成功', ['list'=>$list]);
    }

    //注册
    public function userReg()
    {
        if(empty(input('phone'))){
            api_response(90010, '手机号码不能为空');
        }
        /*if(empty(input('password'))){
            api_response(90011, '密码不能为空');
        }*/
        if(empty(input('invitePhone'))){
            api_response(90012, '邀请人号码不能为空');
        }
        if(empty(input('nickname'))){
            api_response(90013, '昵称不能为空');
        }
        if(empty(input('invite_code'))){
            api_response(90014, '注册人邀请码不能为空');
        }

        $param = request()->post();
        $param['password'] = substr($param['phone'], -6,6);
        $param['paytypeZfb'] = '';
        $param['paytypeWx'] = '';

        $result = (new UserLogic())->userReg($param,1);
        api_response(10000, '请求成功');
    }

}