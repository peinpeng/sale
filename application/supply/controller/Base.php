<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/9/16
 * Time: 10:14
 */
namespace app\supply\controller;
use app\common\logic\supply\SupplyLogic;

class Base
{
    public $supplyLogic;

    public function __construct()
    {
        $this->supplyLogic = new SupplyLogic();
        $this->supplyLogic->verify();
    }
}