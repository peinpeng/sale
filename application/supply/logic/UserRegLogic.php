<?php
/**
 * Created by PhpStorm.
 * User: jianbiao
 * Date: 2019/11/13
 * Time: 16:51
 */
namespace app\supply\logic;

class UserRegLogic
{

    //注册
    public function reg($mobile, $invitePhone)
    {
        $rc4_pwd = tpCache('supply.rc4_pwd');
        $default_invite_code = tpCache('supply.default_invite_code');

        if($invitePhone){
            $code = model('users')->where('mobile', $invitePhone)->value('invite_code');
            $code = $code?$code:$default_invite_code;
        }else{
            $code = $default_invite_code;
        }
        $data = [
            'mobile' => $mobile,
            'code' => $code,
        ];
        $encode = base64_encode($this->rc4(json_encode($data),$rc4_pwd));
        $result = httpRequest('https://tbk.cmfspay.com/bly/register', 'POST', $encode);
        $data = json_decode($this->rc4(base64_decode($result), $rc4_pwd), true);
        if($data['code'] == 0){
            $invite_code = $data['data']['invite_code'];
            $result = model('users')->where('mobile', $mobile)->update(['invite_code'=>$invite_code]);
            //print_r($data);
            if($result !== false){
                return true;
            }
        }
        return false;
    }

    /**
     * rc4加密算法,解密方法直接再一次加密就是解密
     * @param  [type] $data 要加密的数据
     * @param  [type] $pwd  加密使用的key
     * @return [type]       [description]
     */
    public function rc4($data, $pwd) {
        $key[]       = "";
        $box[]       = "";
        $pwd_length  = strlen($pwd);
        $data_length = strlen($data);
        $cipher      = '';
        for ($i = 0; $i < 256; $i++) {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }
        for ($j = $i = 0; $i < 256; $i++) {
            $j       = ($j + $box[$i] + $key[$i]) % 256;
            $tmp     = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for ($a = $j = $i = 0; $i < $data_length; $i++) {
            $a       = ($a + 1) % 256;
            $j       = ($j + $box[$a]) % 256;
            $tmp     = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ $k);
        }
        return $cipher;
    }
}