<?php
/**
 * kitnote
 * ============================================================================
 * 版权所有 2015-2027 株洲清拓科技有限公司，并保留所有权利。
 * 网站地址: http://www.mall.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用TP5助手函数可实现单字母函数M D U等,也可db::name方式,可双向兼容
 * ============================================================================
 * Author: pengbing yaodian
 * Date: 2018-08-02
 */

namespace app\admin\controller;
use think\Page;

class HotImg extends Base {

    /**
     * [热点编辑图]
     * @return [type] [description]
     */
    public function index()
    {
        // //App跳转类型
        // $jump_type = M('app_jump_type')->field('jump_id, name')->select();
        // $option = '';
        // foreach ($jump_type as $k => $v) {
        //     $option .= '<option value="' . $v['jump_id'] . '">' . $v['name'] . '</option>';
        // }
        $html = '<label for="sign_id">app跳转类型</label><select id="sign_id" class="sign_id"><option value="0">请选择</option></select>';
        $this->assign('html', $html);
        return $this->fetch();
    }




}
