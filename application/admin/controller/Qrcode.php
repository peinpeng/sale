<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: 当燃      
 * Date: 2015-09-09
 */
namespace app\admin\controller; 
use think\AjaxPage;
use think\Controller;
use think\Url;
use think\Config;
use think\Page;
use think\Verify;
use think\Db;
class Qrcode extends Base {

    /*二维码 qrcode*/

    public function qrcodeList(){
        $model =  model('Common/Qrcode'); 
        $res = $list = array();
        $p = empty($_REQUEST['p']) ? 1 : $_REQUEST['p'];
        $size = empty($_REQUEST['size']) ? 20 : $_REQUEST['size'];
        
        $where = "1=1";
        $keywords = trim(I('keywords'));
        $keywords && $where.=" and title like '%$keywords%' ";
        $list = $model->where($where)->order('sort desc,id desc')->page("$p,$size")->select();

        // foreach ($list as $k => $v) {
        //     $list[$k]['tk_type_ch'] = $v['tk_type_ch'];
        // }

        $count = $model->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count,$size);// 实例化分页类 传入总记录数和每页显示的记录数
        //$page = $pager->show();//分页显示输出
        //
        $this->assign('list',$list);// 赋值数据集
        $this->assign('pager',$pager);// 赋值分页输出        
        return $this->fetch();
    }

    public function qrcodeEdit(){
        $act = I('GET.act','add');
        $info = array();
        if(I('GET.id')){
           $id = I('GET.id');
           $info = model('Common/Qrcode')->where('id='.$id)->find();
        }
        $this->assign('act',$act);
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function qrcodeHandle()
    {
        $data = I('post.');

        if(!empty($data['tk_type'])){
            $data['tk_type'] = implode(',', $data['tk_type']);
        }else{
            $data['tk_type'] = '';

        }

        $result = $this->validate($data, 'Qrcode.'.$data['act'], [], true);
        if ($result !== true) {
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => $result]);
        }
    
        // p($data);
        if ($data['act'] == 'add') {
            $data['add_time'] = time(); 
            $r = M('qrcode')->add($data);
        } elseif ($data['act'] == 'edit') {
            $r = M('qrcode')->where('id='.$data['id'])->save($data);
        } elseif ($data['act'] == 'del') {
            $r = M('qrcode')->where('id='.$data['id'])->delete();  
        }
        
        if (!$r) {
            $this->ajaxReturn(['status' => -1, 'msg' => '操作失败,或没作更改']);
        }
            
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }

    /*分享页 shareView*/

    public function shareViewList(){
        $model =  model('Common/shareView'); 
        $res = $list = array();
        $p = empty($_REQUEST['p']) ? 1 : $_REQUEST['p'];
        $size = empty($_REQUEST['size']) ? 20 : $_REQUEST['size'];
        
        $where = "1=1";
        $keywords = trim(I('keywords'));
        if($keywords){
            $qrcode_id = M('Qrcode')->where(['title'=>['like',"%{$keywords}%"]])->value('id')?:-1;
            $where.=" and qrcode_id = {$qrcode_id}";
        }
        $list = $model->where($where)->order('sort desc,id desc')->page("$p,$size")->select();

        // foreach ($list as $k => $v) {
        //     $list[$k]['tk_type_ch'] = $v['tk_type_ch'];
        // }

        $count = $model->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count,$size);// 实例化分页类 传入总记录数和每页显示的记录数
        //$page = $pager->show();//分页显示输出
        //
        $this->assign('list',$list);// 赋值数据集
        $this->assign('pager',$pager);// 赋值分页输出        
        return $this->fetch();
    }

    public function shareViewEdit(){
        $act = I('GET.act','add');
        $info = array();
        if(I('GET.id')){
           $id = I('GET.id');
           $info = model('Common/shareView')->where('id='.$id)->find();
        }
        $this->assign('act',$act);
        $this->assign('info',$info);
        $this->assign('qrcodes',M('Qrcode')->where(['enabled'=>1])->select());
        return $this->fetch();
    }

    public function shareViewHandle()
    {
        $data = I('post.');

        if(!empty($data['tk_type'])){
            $data['tk_type'] = implode(',', $data['tk_type']);
        }

        // p($data);
        $result = $this->validate($data, 'shareView.'.$data['act'], [], true);
        if ($result !== true) {
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => $result]);
        }

        // 图片处理
        if ($data['act'] == 'add' || $data['act'] == 'edit') {
            array_pop($data['thumb']);
            if(count($data['thumb'])>10) {
                $this->ajaxReturn(['status' => 0, 'msg' => '超过上传图片数量上限']);
            }
            if ($data['thumb']) {
                $data['thumb'] = implode($data['thumb'], ',');
            } else {
                $data['thumb'] = '';
            }
            //非图文类型 删除图片显示类型(单图显示，三图显示)
            if($data['article_type'] != 2) unset($data['show_type']);

            if(isset($data['file_url1']) && !empty($data['file_url1'])){
                $data['file_url'] = $data['file_url1'];
                unset($data['file_url1']);
            }
            

        }
    
        // p($data);
        if ($data['act'] == 'add') {
            $data['add_time'] = time(); 
            $r = M('shareView')->add($data);
        } elseif ($data['act'] == 'edit') {
            $r = M('shareView')->where('id='.$data['id'])->save($data);
        } elseif ($data['act'] == 'del') {
            $r = M('shareView')->where('id='.$data['id'])->delete();  
        }
        
        if (!$r) {
            $this->ajaxReturn(['status' => -1, 'msg' => '操作失败,或没作更改']);
        }
            
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }


    /*客服 CustomerService*/

    public function CustomerServiceList(){
        $model =  model('Common/CustomerService'); 
        $res = $list = array();
        $p = empty($_REQUEST['p']) ? 1 : $_REQUEST['p'];
        $size = empty($_REQUEST['size']) ? 20 : $_REQUEST['size'];
        
        $where = "1=1";
        $keywords = trim(I('keywords'));
        $keywords && $where.=" and title like '%$keywords%' ";
        $list = $model->where($where)->order('sort desc,id asc')->page("$p,$size")->select();

        // foreach ($list as $k => $v) {
        //     $list[$k]['tk_type_ch'] = $v['tk_type_ch'];
        // }

        $count = $model->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count,$size);// 实例化分页类 传入总记录数和每页显示的记录数
        //$page = $pager->show();//分页显示输出
        //
        $this->assign('list',$list);// 赋值数据集
        $this->assign('pager',$pager);// 赋值分页输出        
        return $this->fetch();
    }

    public function CustomerServiceEdit(){
        $act = I('GET.act','add');
        $info = array();
        if(I('GET.id')){
           $id = I('GET.id');
           $info = model('Common/CustomerService')->where('id='.$id)->find();
        }
        $this->assign('act',$act);
        $this->assign('info',$info);
        $this->assign('default_title','平台客服'.(model('Common/CustomerService')->count()+1));
        return $this->fetch();
    }

    public function CustomerServiceHandle()
    {
        $data = I('post.');

        // p($data);
        $result = $this->validate($data, 'CustomerService.'.$data['act'], [], true);
        if ($result !== true) {
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => $result]);
        }
        // p($data);
        if ($data['act'] == 'add') {
            $data['add_time'] = time(); 
            $r = M('CustomerService')->add($data);
        } elseif ($data['act'] == 'edit') {
            $r = M('CustomerService')->where('id='.$data['id'])->save($data);
        } elseif ($data['act'] == 'del') {
            $r = M('CustomerService')->where('id='.$data['id'])->delete();  
        }
        
        if (!$r) {
            $this->ajaxReturn(['status' => -1, 'msg' => '操作失败,或没作更改']);
        }
            
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }

    /*公众号设置 WechatOfficialAccount*/

    public function WechatOfficialAccountList(){
        $model =  model('Common/WechatOfficialAccount'); 
        $res = $list = array();
        $p = empty($_REQUEST['p']) ? 1 : $_REQUEST['p'];
        $size = empty($_REQUEST['size']) ? 20 : $_REQUEST['size'];
        
        $where = "1=1";
        $keywords = trim(I('keywords'));
        $keywords && $where.=" and title like '%$keywords%' ";
        $list = $model->where($where)->order('sort desc,id asc')->page("$p,$size")->select();

        // foreach ($list as $k => $v) {
        //     $list[$k]['tk_type_ch'] = $v['tk_type_ch'];
        // }

        $count = $model->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count,$size);// 实例化分页类 传入总记录数和每页显示的记录数
        //$page = $pager->show();//分页显示输出
        //
        $this->assign('list',$list);// 赋值数据集
        $this->assign('pager',$pager);// 赋值分页输出        
        return $this->fetch();
    }

    public function WechatOfficialAccountEdit(){
        $act = I('GET.act','add');
        $info = array();
        if(I('GET.id')){
           $id = I('GET.id');
           $info = model('Common/WechatOfficialAccount')->where('id='.$id)->find();
        }
        $this->assign('act',$act);
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function WechatOfficialAccountHandle()
    {
        $data = I('post.');

        // p($data);
        $result = $this->validate($data, 'WechatOfficialAccount.'.$data['act'], [], true);
        if ($result !== true) {
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => $result]);
        }
        // p($data);
        if ($data['act'] == 'add') {
            $data['add_time'] = time(); 
            $r = M('WechatOfficialAccount')->add($data);
        } elseif ($data['act'] == 'edit') {
            if($data['enabled'] == 1){
                M('WechatOfficialAccount')->where('enabled=1')->save(['enabled'=>0]);//保证只有一个被启用
            }
            $r = M('WechatOfficialAccount')->where('id='.$data['id'])->save($data);
        } elseif ($data['act'] == 'del') {
            $r = M('WechatOfficialAccount')->where('id='.$data['id'])->delete();  
        }
        
        if (!$r) {
            $this->ajaxReturn(['status' => -1, 'msg' => '操作失败,或没作更改']);
        }
            
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }
   
    
}