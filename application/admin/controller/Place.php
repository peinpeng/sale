<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: 当燃      
 * Date: 2015-09-09
 */
namespace app\admin\controller; 
use think\AjaxPage;
use think\Controller;
use think\Url;
use think\Config;
use think\Page;
use think\Verify;
use think\Db;
use think\Loader;
class Place extends Base {

    public $group_list = [
        'wait'        => '等待占位',
        'success'     => '成功占位',
        'working'     => '闯关审核中',
        'unenable'     => '冻结状态',
    ];  

    /*占位 placeWaitingMatching*/

    public function placeWaitingMatchingList(){
        $model =  model('Common/place/placeWaitingMatching'); 
        $inc_type =  I('get.inc_type','wait');
        $res = $list = array();
        $p = empty($_REQUEST['p']) ? 1 : $_REQUEST['p'];
        $size = empty($_REQUEST['size']) ? 20 : $_REQUEST['size'];
        
        $where = ['enable'=>1];
        $orderBy = "lineup_time asc";
        $keywords = trim(I('keywords'));
        if($keywords){
            $user_id = M('Users')->where(['nickname|mobile'=>['like',"%{$keywords}%"]])->column('user_id')?:-1;
            $where['user_id'] = ['in',$user_id];
        }

        switch ($inc_type) {
            case 'wait':
                $where['state'] = ['in',[0,1]];
                $orderBy = 'state desc,lineup_time asc';
                break;
            case 'success':
                $where['state'] = 3;
                $orderBy = 'update_time desc';
                break;
            case 'working':
                $where['state'] = 2;
                $orderBy = 'update_time desc';
                break;
            case 'unenable':
                $where['enable'] = 0;
                $orderBy = 'lineup_time desc';
                break;    
            default:
                # code...
                break;
        }
        // p($where);
        $list = $model->with('user')->where($where)->order($orderBy)->page("$p,$size")->select();

        foreach ($list as $k => $v) {
           
        }

        $count = $model->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count,$size);// 实例化分页类 传入总记录数和每页显示的记录数
        //$page = $pager->show();//分页显示输出
        //
        $this->assign('list',$list);// 赋值数据集
        $this->assign('pager',$pager);// 赋值分页输出        
        $this->assign('group_list',$this->group_list);
        $this->assign('inc_type',$inc_type);        
        return $this->fetch();
    }

    public function placeWaitingMatchingEdit(){
        $act = I('GET.act','add');
        $info = array();
        if(I('GET.id')){
           $id = I('GET.id');
           $info = M('placeWaitingMatching')->where('id='.$id)->find();
        }
        $this->assign('act',$act);
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function placeWaitingMatchingHandle()
    {
        $data = I('post.');

        if(!empty($data['tk_type'])){
            $data['tk_type'] = implode(',', $data['tk_type']);
        }

        // p($data);
        $result = $this->validate($data, 'placeWaitingMatching.'.$data['act'], [], true);
        if ($result !== true) {
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => $result]);
        }
    
        // p($data);
        if ($data['act'] == 'add') {
            $data['add_time'] = time(); 
            $r = M('placeWaitingMatching')->add($data);
        } elseif ($data['act'] == 'edit') {
            $r = M('placeWaitingMatching')->where('id='.$data['id'])->save($data);
        } elseif ($data['act'] == 'del') {
            $r = M('placeWaitingMatching')->where('id='.$data['id'])->delete();  
        }
        
        if (!$r) {
            $this->ajaxReturn(['status' => -1, 'msg' => '操作失败,或没作更改']);
        }
            
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }

    /*流失补偿配置 placeLossCompensateConfig*/

    /**
     * [lossCompensateConfig 流失补偿等级配置表]
     * @return [type] [description]
     */
    public function placeLossCompensateConfig()
    {
        $LossCompensateConfigMdl  = model('common/place/placeLossCompensateConfig');
        $p = $this->request->param('p');
        $res = $LossCompensateConfigMdl->with('levelConfig')->order('id asc')->page($p . ',10')->select();
       
        $this->assign('list', $res);
        $count = $LossCompensateConfigMdl->with('levelConfig')->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }               

    /**
     * [placeLossCompensateConfigDetail 接点人等级滑落人数配置详情]
     * @return [type] [description]
     */
     public function placeLossCompensateConfigDetail()
    {
        $act = I('get.act', 'add');
        $this->assign('act', $act);
        $id = I('get.id');
        if ($id) {
            $level_info = D('PlaceLossCompensateConfig')->where('id=' . $id)->find();
            $this->assign('info', $level_info);
        }
        return $this->fetch();
    }


    /**
     * [placeLossCompensateConfigDetail 接点人等级滑落人数配置编辑及删除]
     */
    public function placeLossCompensateConfigHandle()
    {
        $data = I('post.');
        $userLevelValidate = Loader::validate('PlaceLossCompensateConfig');
        $return = ['status' => 0, 'msg' => '参数错误', 'result' => ''];//初始化返回信息
        if ($data['act'] == 'add') {
            if (!$userLevelValidate->batch()->check($data)) {
                // p($userLevelValidate->batch()->check($data));
                $return = ['status' => 0, 'msg' => '添加失败', 'result' => $userLevelValidate->getError()];
            } else {
                unset($data['id']);
                $data['add_time'] = time();
                $r = D('PlaceLossCompensateConfig')->add($data);
                if ($r !== false) {
                    $return = ['status' => 1, 'msg' => '添加成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '添加失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'edit') {
            if (!$userLevelValidate->scene('edit')->batch()->check($data)) {
                $return = ['status' => 0, 'msg' => '编辑失败', 'result' => $userLevelValidate->getError()];
            } else {
                unset($dta['id']);
                $data['update_time'] = time();
                $r = D('PlaceLossCompensateConfig')->where('id=' . $data['id'])->save($data);
                if ($r !== false) {
                    $return = ['status' => 1, 'msg' => '编辑成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '编辑失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'del') {
            $r = D('PlaceLossCompensateConfig')->where('id=' . $data['id'])->delete();
            if ($r !== false) {
                $return = ['status' => 1, 'msg' => '删除成功', 'result' => ''];
            } else {
                $return = ['status' => 0, 'msg' => '删除失败，数据库未响应', 'result' => ''];
            }
        }
        $this->ajaxReturn($return);
    }
   
    
}