<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: 当燃
 * Date: 2015-09-09
 */

namespace app\admin\controller;

use app\admin\logic\OrderLogic;
use app\common\logic\UserHashLogic;
use think\AjaxPage;
use think\console\command\make\Model;
use think\Page;
use think\Verify;
use think\Db;
use app\admin\logic\UsersLogic;
use app\common\model\Withdrawals;
use app\common\model\Users;
use think\Loader;
use app\common\logic\SlippingUsersLogic;
use app\common\logic\UsersLogic as UserLogicMdl;

class User extends Base
{

    public function index()
    {
        $team_user_id        = I('team_user_id','');
        $list = $team_user_id >0?$this->getUserLevelUpd($team_user_id):[];
        $this->assign('modules', $list);


        $levelMdl = model('Common/users/UserLevel');
        $this->assign('_levelList',$levelMdl->getLevelType());
        return $this->fetch();
    }

    /**
     * 会员列表
     */
    public function ajaxindex()
    {
        // 搜索条件
        $condition           = array();
        $nickname            = I('nickname');
        $first_leader_mobile = I('first_leader_mobile');        
        $account             = I('account');
        $user_id             = I('user_id');
        $level               = I('level');
        $team_user_id        = I('team_user_id','');
        // $is_slipping         = I('is_slipping','');

        // $slippingUsersMdl = model('Common/SlippingUsers');
        // $slippingCountMdl = model('Common/SlippingCount');
        $usersModel = model('Common/Users');

        $account ? $condition['p.mobile']    = ['like', "%$account%"] : false;
        $nickname ? $condition['p.nickname'] = ['like', "%$nickname%"] : false;
        $user_id ? $condition['p.user_id']   = $user_id: false;
        $level ? $condition['p.level']       = $level: false;

        //滑落筛选
        // if($is_slipping !== ''){
        //     $condition['s.is_slipping'] = $is_slipping?:null;
        // }
       
        I('first_leader') && ($condition['p.first_leader'] = I('first_leader')); // 查看一级下线人有哪些
        I('second_leader') && ($condition['p.second_leader'] = I('second_leader')); // 查看二级下线人有哪些
        I('third_leader') && ($condition['p.third_leader'] = I('third_leader')); // 查看三级下线人有哪些
        $sort_order = I('order_by') . ' ' . I('sort');
        trim($sort_order,' ') || $sort_order='p.user_id desc';

        if ($first_leader_mobile){
            $first_leader_uid =   $usersModel->getFisrtLeaderIdByMobile($first_leader_mobile);
            $condition['p.first_leader'] = $first_leader_uid;
        }
        if ($team_user_id){
            $teamnum = model('common/distribut/DistributTeam')->getUserTeamsAllCount($team_user_id);//团队人数
            $contribution_num = model('Common/Users')->getFisrtLeaderNumAttr($team_user_id,0);      //他的贡献
            $locking_num = db('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$this->user_id])->count();//锁定人数

            //层级
            $userIds = model('common/distribut/DistributTeam')->getUserTeamsAllForMobile($team_user_id);
            $list = $this->getUserLevelUpd($team_user_id);
            $this->assign('modules', $list);
            $this->assign('teamnum', $teamnum);
            $this->assign('contribution_num', $contribution_num);
            $this->assign('locking_num', $locking_num);
        }

        $count = $usersModel
                    ->alias('p')
                    // ->join('saas_slipping_users s','s.slipping_id = p.user_id' ,'left')
                    ->where($condition)
                    ->count();
                    // p($count);
        //他的团队人数为列表总数
        if($team_user_id) $count = $teamnum;

        $Page = new AjaxPage($count, 10);
        $limit = $Page->firstRow . ',' . $Page->listRows;

        //数组分页输出
        if($team_user_id) {
            $idList = array_slice($ids,$Page->firstRow,10);
            $condition = [];
            $condition['user_id'] = ['in',$idList];
            $limit = '0,'.$Page->listRows;
        }
        $userList = $usersModel
                    ->alias('p')
                    // ->join('saas_slipping_users s','s.slipping_id = p.user_id' ,'left')
                    // ->with('creditUser,slippingCount,slippingUsers,placeWaitingMatching')
                    ->with('creditUser,placeWaitingMatching')
                    ->where($condition)
                    ->order($sort_order)
                    ->limit($limit)
                    ->select();
        // p($condition);
        $user_id_arr = get_arr_column($userList, 'user_id');  
        // p($user_id_arr);
        if (!empty($user_id_arr)) {
            $first_leader = DB::query("select first_leader,count(1) as count  from __PREFIX__users where first_leader in(" . implode(',', $user_id_arr) . ")  group by first_leader");
            $first_leader = convert_arr_key($first_leader, 'first_leader');

            $second_leader = DB::query("select second_leader,count(1) as count  from __PREFIX__users where second_leader in(" . implode(',', $user_id_arr) . ")  group by second_leader");
            $second_leader = convert_arr_key($second_leader, 'second_leader');

            $third_leader = DB::query("select third_leader,count(1) as count  from __PREFIX__users where third_leader in(" . implode(',', $user_id_arr) . ")  group by third_leader");
            $third_leader = convert_arr_key($third_leader, 'third_leader');
        }
        // p(collection($levelMdl->getLevelType())->toArray());
        $this->assign('first_leader', $first_leader);
        $this->assign('second_leader', $second_leader);
        $this->assign('third_leader', $third_leader);
        $show = $Page->show();
        $this->assign('userList', $userList);
        $this->assign('level', M('user_level')->getField('level_id,level_name'));
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('pager', $Page);
        return $this->fetch();
    }

    /**
     * 会员列表
     */
    public function teamInfo()
    {
        // 搜索条件
        $condition           = array();
        $nickname            = I('nickname');
        $first_leader_mobile = I('first_leader_mobile');        
        $account             = I('account');
        $user_id             = I('user_id');
        $level               = I('level');
        $team_user_id        = I('team_user_id','');
        $p = $this->request->param('p');
        // p($p);
        // $is_slipping         = I('is_slipping','');

        // $slippingUsersMdl = model('Common/SlippingUsers');
        // $slippingCountMdl = model('Common/SlippingCount');
        $usersModel = model('Common/Users');

        $account ? $condition['p.mobile']    = ['like', "%$account%"] : false;
        $nickname ? $condition['p.nickname'] = ['like', "%$nickname%"] : false;
        $user_id ? $condition['p.user_id']   = $user_id: false;
        $level ? $condition['p.level']       = $level: false;

        //滑落筛选
        // if($is_slipping !== ''){
        //     $condition['s.is_slipping'] = $is_slipping?:null;
        // }
       
        // p($condition);

        I('first_leader') && ($condition['p.first_leader'] = I('first_leader')); // 查看一级下线人有哪些
        I('second_leader') && ($condition['p.second_leader'] = I('second_leader')); // 查看二级下线人有哪些
        I('third_leader') && ($condition['p.third_leader'] = I('third_leader')); // 查看三级下线人有哪些
        $sort_order = I('order_by') . ' ' . I('sort');
        trim($sort_order,' ') || $sort_order='p.user_id desc';

        if ($first_leader_mobile){
            $first_leader_uid =   $usersModel->getFisrtLeaderIdByMobile($first_leader_mobile);
            $condition['p.first_leader'] = $first_leader_uid;
        }
        if ($team_user_id){
            $teamnum = model('common/distribut/DistributTeam')->getUserTeamsAllCount($team_user_id);//团队人数
            $contribution_num = model('Common/Users')->getFisrtLeaderNumAttr($team_user_id,0)?:0;      //他的贡献
            $locking_num = db('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$this->user_id])->count();//锁定人数
            $ids = model('common/distribut/DistributTeam')->getUserTeamsAllForCJ($team_user_id,$p);
            // p($ids);

            //层级
            // $userIds = model('common/distribut/DistributTeam')->getUserTeamsAllForMobile($team_user_id);
            $list = $team_user_id >0?$this->getUserLevelUpListOne($team_user_id):[];
// p($list);
            $this->assign('modules', $list);
            $this->assign('teamnum', $teamnum);
            $this->assign('contribution_num', $contribution_num);
            $this->assign('locking_num', $locking_num);
        }

        $count = $usersModel
                    ->alias('p')
                    // ->join('saas_slipping_users s','s.slipping_id = p.user_id' ,'left')
                    ->where($condition)
                    ->count();
        //他的团队人数为列表总数
        if($team_user_id) $count = $teamnum;

        // $Page = new AjaxPage($count, 10);
        $Page = new Page($count, 10);
        $limit = $Page->firstRow . ',' . $Page->listRows;

        //数组分页输出
        if($team_user_id) {
            // $idList = array_slice($ids,$Page->firstRow,10);
            $condition = [];
            $condition['user_id'] = ['in',$ids];
            $limit = '0,'.$Page->listRows;
        }
        $userList = $usersModel
                    ->alias('p')
                    // ->join('saas_slipping_users s','s.slipping_id = p.user_id' ,'left')
                    // ->with('creditUser,slippingCount,slippingUsers,placeWaitingMatching')
                    ->with('creditUser,placeWaitingMatching')
                    ->where($condition)
                    ->order($sort_order)
                    ->limit($limit)
                    ->select();
        // p($userList);
        $user_id_arr = get_arr_column($userList, 'user_id');  
        // p($user_id_arr);
        if (!empty($user_id_arr)) {
            $first_leader = DB::query("select first_leader,count(1) as count  from __PREFIX__users where first_leader in(" . implode(',', $user_id_arr) . ")  group by first_leader");
            $first_leader = convert_arr_key($first_leader, 'first_leader');

            $second_leader = DB::query("select second_leader,count(1) as count  from __PREFIX__users where second_leader in(" . implode(',', $user_id_arr) . ")  group by second_leader");
            $second_leader = convert_arr_key($second_leader, 'second_leader');

            $third_leader = DB::query("select third_leader,count(1) as count  from __PREFIX__users where third_leader in(" . implode(',', $user_id_arr) . ")  group by third_leader");
            $third_leader = convert_arr_key($third_leader, 'third_leader');
        }
        $levelMdl = model('Common/users/UserLevel');
        $this->assign('_levelList',$levelMdl->getLevelType());
        // p(collection($levelMdl->getLevelType())->toArray());
        $this->assign('first_leader', $first_leader);
        $this->assign('second_leader', $second_leader);
        $this->assign('third_leader', $third_leader);
        $show = $Page->show();
        $this->assign('userList', $userList);
        $this->assign('level', M('user_level')->getField('level_id,level_name'));
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('pager', $Page);
        $this->assign('count', $count);
        return $this->fetch();
    }

    public function ajaxGetUserLevelUpListOne(){

        $user_id = I('user_id','','trim');
        // p($user_id);
        $data = $this->getUserLevelUpListOne($user_id);
        // p($data);
        return $this->ajaxReturn(array('status' => 1, 'msg' => '成功', 'data' => $data));
    }

    /**
     * 获取用户层级用户
     * @Author    LP
     * @DateTime  2019-02-26T17:37:20+0800
     * @version   [version]
     * @parameter String
     * @param     [type]                   $user_id [description]
     * @return    [type]                            [description]
     */
    public function getUserLevelUpListOne($user_id){

        if(empty($user_id)) return [];
        $data =  model('common/distribut/DistributTeam')->getUserLevelUpListOne($user_id);
        // p($data);
        if($data && count($data)){
            foreach ($data as $k => $v) {
                // p($v['users']['mobile']);
                $list[$k]['name'] = $v['users']['mobile'];
                $list[$k]['parent_id'] = $v['parent_id'];
                $list[$k]['id'] = $v['user_id'];
                $list[$k]['pid'] = 0;
                $list[$k]['is_lower'] = $v['is_lower'];
            }
        }
        // p($list);
        return $list;
    }

     
    /**
     * 获取用户层级用户
     * @Author    LP
     * @DateTime  2019-02-26T17:37:20+0800
     * @version   [version]
     * @parameter String
     * @param     [type]                   $user_id [description]
     * @return    [type]                            [description]
     */
    public function getUserLevelUpd($user_id,$data=[],$pid=0){

        $team = model('common/distribut/DistributTeam')->alias('a')
        ->join('Users u','a.user_id=u.user_id','LEFT')
        ->where(['parent_id'=>['in',$user_id]])
        ->column('a.parent_id,a.user_id,u.mobile','a.user_id');
// p(count($team));
        if($team && count($team)){
            foreach ($team as $k => $v) {
                $data[$k][0]['name'] = $v['mobile'];
                $data[$k][0]['parent_id'] = $v['parent_id'];
                $data[$k][0]['id'] = $k;
                $data[$k][0]['pid'] = $pid==0?0:$v['parent_id'];
            }
            $pid += 1;
            return $this->getUserLevelUpd(array_keys($team),$data,$pid);
        }
        return $data;
    }
    

    /**
     * 会员详细信息查看
     */
    public function detail()
    {
        $uid = I('get.id');
        $mobile = I('get.mobile');
        $user = model('Users')->whereOr(array('user_id' => $uid,'mobile'=>$mobile))->find();
        if (!$user)
            exit($this->error('会员不存在'));
        $uid = $user['user_id'];
        if (IS_POST) {
            //  会员信息编辑
            // p($_POST);
            $password = I('post.password');
            $paypwd = I('post.paypwd');
            $is_lock = I('post.is_lock');
            $userLevel = I('post.level');
            $total_point =  I('post.total_point');
            $first_leader_mobile =  I('post.first_leader_mobile');
            // $before_first_leader_mobile = I('post.before_first_leader_mobile');
            $is_balance_transfer = I('post.is_balance_transfer');
            // p($userLevel);
        
            $model = model('Common/credit/CreditUserLog');
            $userLevelMdl = model('Common/UserLevelLog');
            $changeFirstLeaderLogMdl = model('Common/ChangeFirstLeaderLog');

            if($user['is_lock'] != $is_lock){
                if($is_lock == 0){
                    $res = $model->thawAdmin($uid);//后台解冻
                }else{
                    $log_id = $model->addData($uid,\app\common\model\credit\CreditUserLog::LOCK_ADMIN, 0,0,"后台冻结用户");//后台主动冻结用户（永久）
                }
            }
            
            //修改信用分
            $userTotalPoint = D('CreditUser')->where(array('user_id' => $uid))->value('total_point');
            if ($total_point != $userTotalPoint) {                
                $differenceScore = $total_point-$userTotalPoint;
                $log_id = $model->addData($uid,\app\common\model\credit\CreditUserLog::ADDCREDITSCORE_ADMIN, $differenceScore,0,"后台修改信用分");
                unset($_POST['is_lock']);
            }

            if (!empty($password))  {
                $_POST['password'] = encrypt($_POST['password']);  
            }else{
                unset($_POST['password']);
            } 

            if(!empty($paypwd))  { 
                $_POST['paypwd'] = encrypt($_POST['paypwd']);
            }else{
                unset($_POST['paypwd']);
            }  
           
            if (!empty($_POST['email'])) {
                $email = trim($_POST['email']);
                $c = M('users')->where("user_id != $uid and email = '$email'")->count();
                $c && exit($this->error('邮箱不得和已有用户重复'));
            }

            if (!empty($_POST['wx_account'])) {
                $paytypeModel = model('common/Paytype');
                $beforePaytype = $paytypeModel->where(['user_id'=>$uid,'type'=>2])->value('account');
                $resWx  = $paytypeModel->addOrSave($uid, 2, ['account' => $_POST['wx_account'], 'type' => 2]);
                //添加修改记录
                model('commen/PaytypeChangeLog')->addLog($uid,2,$beforePaytype,$_POST['wx_account'],'后台修改用户微信账号');
            }
            if (!empty($_POST['zfb_account'])) {
                $paytypeModel = model('common/Paytype');
                $beforePaytype1 = $paytypeModel->where(['user_id'=>$uid,'type'=>1])->value('account');
                $resWx  = $paytypeModel->addOrSave($uid, 1, ['account' => $_POST['zfb_account'], 'type' => 1]);
                //添加修改记录
                model('commen/PaytypeChangeLog')->addLog($uid,1,$beforePaytype1,$_POST['zfb_account'],'后台修改用户支付宝账号');
            }

            //后台修改用户等级不能改为0星会员
            if ( $user['level']!=1 && $userLevel==1) {
                exit($this->error('等级不能调为0级'));
            }            
            //判断修改的上级是否符合条件（VIP1及以上）
            $first_leader_info = D('users')->where(['user_id'=>$user['first_leader']])->find();    //原上级
            $first_leader = D('users')->where(['mobile'=>$first_leader_mobile])->find();       //要更改的上级
            if (!empty($first_leader_mobile && $first_leader_info['mobile']!=$first_leader_mobile)) {
                if(empty($first_leader)) $this->error('上级账号不存在');
                if($first_leader['level'] < 2){
                    $this->error('上级必须为VIP1及以上等级');
                }
                //添加会员变更上级记录
                $before_first_leader_id = $first_leader_info['user_id'];   //原上级ID         
                $after_first_leader_id = $first_leader['user_id'];         //要更改的上级ID         
                $result = $changeFirstLeaderLogMdl->addData($uid,$before_first_leader_id,$after_first_leader_id,1);  //添加会员修改上级记录

                $dtMdl = model('Common/distribut/DistributTeam');
                $res = $dtMdl->updateUserParent($user['mobile'],$first_leader_mobile);//变更上级，更换上级链.
                if(!$res){
                    $this->error($dtMdl->getError());
                }

                // // 更改上级后的滑落规则操作
                // $slippingUsersLogic = new SlippingUsersLogic;
                // //修改滑落用户的接点人引荐人等数据,非滑落人的上级直推人数
                // $res =$slippingUsersLogic->slippingRule($uid,'changeFirstLeader',1);  
                
                // if($res['status'] == 0)   $this->error($res['msg']);
            }

            if (!empty($_POST['mobile'])) {
                // p($_POST['mobile']);
                $mobile = trim($_POST['mobile']);
                $c = M('users')->where("user_id != $uid and mobile = '$mobile'")->count();
                $c && exit($this->error('手机号不得和已有用户重复'));
            }
            $user_before = M('users')->where(['user_id'=>$uid])->find();

            $_POST['level']=$user['level'];
            $row = M('users')->where(array('user_id' => $uid))->save($_POST);
            //后台修改会员手机号,添加记录
            if (!empty($_POST['mobile']) && $user_before['mobile']!= $_POST['mobile']) {
                $mobile = trim($_POST['mobile']);
                model('commen/MobileChangeLog')->addLog($uid,1,$user_before['mobile'],$mobile,'后台修改用户手机号');
            }

            //后台修改会员等级,添加会员升级记录
            if ( $userLevel != $user['level']){
                $result = $userLevelMdl->addData($uid,$userLevel,\app\common\model\UserLevelLog::UPLEVEL_SOURCE_ADMIN_PASS);
                // if($result) {
                //     //等级修改后是否还满足滑落规则的接点人条件
                //     $slippingUsersLogic = new SlippingUsersLogic;
                //     // $slippingUsersMdl = model('Common/SlippingUsers');
                //     // $res = $slippingUsersMdl->changeUserToConnecter($uid,'changeLevelForPC');
                //     $res = $slippingUsersLogic->slippingRule($uid,'changeLevel');
                //     if($res['status']==0)  exit($this->error('更改成功,但更新接点人数据失败'));
                // }else{
                //     exit($userLevelMdl->getError());
                // }    
                if(!$result) exit($userLevelMdl->getError());
            }
            
            $scuData = ['total_point'=>$_POST['total_point'],'update_time'=>time()];
            $rowCredit = M('CreditUser')->save($scuData,['user_id'=>$uid]);
            if ( $rowCredit > 0 || $row || $result ){
                exit($this->success('修改成功',U('User/index')));
            }
            else{
                exit($this->error('未作内容修改或修改失败'));
            }
        }
        $user['first_leader_mobile']  = M('users')->where(['user_id'=>$user['first_leader']])->value('mobile');
        $user['total_point']  = M('CreditUser')->where("user_id = {$user['user_id']}")->value('total_point');
        $user['first_lower']  = M('users')->where("first_leader = {$user['user_id']}")->count();
        $user['second_lower'] = M('users')->where("second_leader = {$user['user_id']}")->count();
        $user['third_lower']  = M('users')->where("third_leader = {$user['user_id']}")->count();
        // p($user);
        $this->assign('user', $user);
        return $this->fetch();
    }

    public function add_user()
    {
        if (IS_POST) {
            $data = I('post.');
            $user_obj = new UsersLogic();
            $model = model('Common/credit/CreditUser');
            //判断上级是否有权限（VIP1及以上）
            $first_leader_info = D('users')->where(['mobile'=>$data['first_leader']])->find();
            if(empty($first_leader_info)) $this->error('上级账号不存在');
            if($first_leader_info['level'] < 2){
                $this->error('上级必须为VIP1及以上等级');
            }
            $data['first_leader'] = $first_leader_info['user_id'];
            $res = $user_obj->addUser($data);
            $res1 = $model->initData($res['user_id']);
            if ($res['status'] == 1 ) {

                $this->success('添加成功', U('User/index'));
                exit;
            } else {
                $this->error('添加失败,' . $res['msg'], U('User/index'));
            }
        }
        return $this->fetch();
    }

    public function export_user()
    {
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">会员ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">会员昵称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">会员等级</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">手机号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">邮箱</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">注册时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">最后登录</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">余额</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">积分</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">累计消费</td>';
        $strTable .= '</tr>';
        $user_ids = I('user_ids');
        if ($user_ids) {
            $condition['user_id'] = ['in', $user_ids];
        } else {
            $mobile = I('mobile');
            $email = I('email');
            $mobile ? $condition['mobile'] = $mobile : false;
            $email ? $condition['email'] = $email : false;
        };
        $count = DB::name('users')->where($condition)->count();
        $p = ceil($count / 5000);
        for ($i = 0; $i < $p; $i++) {
            $start = $i * 5000;
            $end = ($i + 1) * 5000;
            $userList = M('users')->where($condition)->order('user_id')->limit($start . ',' . $end)->select();
            if (is_array($userList)) {
                foreach ($userList as $k => $val) {
                    $strTable .= '<tr>';
                    $strTable .= '<td style="text-align:center;font-size:12px;">' . $val['user_id'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['nickname'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['level'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i', $val['reg_time']) . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i', $val['last_login']) . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_money'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_points'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';
                    $strTable .= '</tr>';
                }
                unset($userList);
            }
        }
        $strTable .= '</table>';
        downloadExcel($strTable, 'users_' . $i);
        exit();
    }

    /**
     * 用户收货地址查看
     */
    public function address()
    {
        $uid = I('get.id');
        $lists = D('user_address')->where(array('user_id' => $uid))->select();
        $regionList = get_region_list();
        $this->assign('regionList', $regionList);
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * 删除会员
     */
    public function delete()
    {
        $uid = I('get.id');

        //先删除ouath_users表的关联数据
        M('OuathUsers')->where(array('user_id' => $uid))->delete();
        $row = M('users')->where(array('user_id' => $uid))->delete();
        if ($row) {
            $this->success('成功删除会员');
        } else {
            $this->error('操作失败');
        }
    }

    /**
     * 删除会员
     */
    public function ajax_delete()
    {
        $uid = I('id');
        if ($uid) {
            $row = M('users')->where(array('user_id' => $uid))->delete();
            if ($row !== false) {
                //把关联的第三方账号删除
                M('OauthUsers')->where(array('user_id' => $uid))->delete();
                $this->ajaxReturn(array('status' => 1, 'msg' => '删除成功', 'data' => ''));
            } else {
                $this->ajaxReturn(array('status' => 0, 'msg' => '删除失败', 'data' => ''));
            }
        } else {
            $this->ajaxReturn(array('status' => 0, 'msg' => '参数错误', 'data' => ''));
        }
    }

    /**
     * 账户资金记录
     */
    public function account_log()
    {
        $user_id = I('get.id');
        //获取类型
        $type = I('get.type');
        //获取记录总数
        $count = M('account_log')->where(array('user_id' => $user_id))->count();
        $page = new Page($count);
        $lists = M('account_log')->where(array('user_id' => $user_id))->order('change_time desc')->limit($page->firstRow . ',' . $page->listRows)->select();

        $this->assign('user_id', $user_id);
        $this->assign('page', $page->show());
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * 账户资金调节
     */
    public function account_edit()
    {
        $user_id = I('user_id');
        if (!$user_id > 0) $this->ajaxReturn(['status' => 0, 'msg' => "参数有误"]);
        $user = M('users')->field('user_id,user_money,frozen_money,pay_points,is_lock')->where('user_id', $user_id)->find();
        if (IS_POST) {
            $desc = I('post.desc');
            if (!$desc)
                $this->ajaxReturn(['status' => 0, 'msg' => "请填写操作说明"]);
            //加减用户资金
            $m_op_type = I('post.money_act_type');
            $user_money = I('post.user_money/f');
            $user_money = $m_op_type ? $user_money : 0 - $user_money;
            if (($user['user_money'] + $user_money) < 0) {
                $this->ajaxReturn(['status' => 0, 'msg' => "用户剩余资金不足！！"]);
            }
            //加减用户积分
            $p_op_type = I('post.point_act_type');
            $pay_points = I('post.pay_points/d');
            $pay_points = $p_op_type ? $pay_points : 0 - $pay_points;
            if (($pay_points + $user['pay_points']) < 0) {
                $this->ajaxReturn(['status' => 0, 'msg' => '用户剩余积分不足！！']);
            }
            //加减冻结资金
            $f_op_type = I('post.frozen_act_type');
            $revision_frozen_money = I('post.frozen_money/f');
            if ($revision_frozen_money != 0) {    //有加减冻结资金的时候
                $frozen_money = $f_op_type ? $revision_frozen_money : 0 - $revision_frozen_money;
                $frozen_money = $user['frozen_money'] + $frozen_money;    //计算用户被冻结的资金
                if ($f_op_type == 1 && $revision_frozen_money > $user['user_money']) {
                    $this->ajaxReturn(['status' => 0, 'msg' => "用户剩余资金不足！！"]);
                }
                if ($f_op_type == 0 && $revision_frozen_money > $user['frozen_money']) {
                    $this->ajaxReturn(['status' => 0, 'msg' => "冻结的资金不足！！"]);
                }
                $user_money = $f_op_type ? 0 - $revision_frozen_money : $revision_frozen_money;    //计算用户剩余资金
                M('users')->where('user_id', $user_id)->update(['frozen_money' => $frozen_money]);
            }
            if (accountLog($user_id, $user_money, $pay_points, $desc, 0)) {
                $this->ajaxReturn(['status' => 1, 'msg' => "操作成功", 'url' => U("Admin/User/account_log", array('id' => $user_id))]);
            } else {
                $this->ajaxReturn(['status' => -1, 'msg' => "操作失败"]);
            }
            exit;
        }
        $this->assign('user_id', $user_id);
        $this->assign('user', $user);
        return $this->fetch();
    }

    public function recharge()
    {
        $timegap = urldecode(I('timegap'));
        $nickname = I('nickname');
        $map = array();
        if ($timegap) {
            $gap = explode(',', $timegap);
            $begin = $gap[0];
            $end = $gap[1];
            $map['ctime'] = array('between', array(strtotime($begin), strtotime($end)));
            $this->assign('begin', $begin);
            $this->assign('end', $end);
        }
        if ($nickname) {
            $map['nickname'] = array('like', "%$nickname%");
            $this->assign('nickname', $nickname);
        }
        $count = M('recharge')->where($map)->count();
        $page = new Page($count);
        $lists = M('recharge')->where($map)->order('ctime desc')->limit($page->firstRow . ',' . $page->listRows)->select();
        $this->assign('page', $page->show());
        $this->assign('pager', $page);
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    public function level()
    {
        $act = I('get.act', 'add');
        $this->assign('act', $act);
        $level_id = I('get.level_id');
        if ($level_id) {
            $level_info = D('user_level')->where('level_id=' . $level_id)->find();
            $this->assign('info', $level_info);
        }
        $crossLevelList = model('Common/users/UserLevel')->getCol('level_id,show_name');
        $this->assign('crossLevelList', $crossLevelList);
        return $this->fetch();
    }

    public function levelList()
    {
        $Ad = M('user_level');
        $p = $this->request->param('p');
        $res = $Ad->order('level_id')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $k => $val) {
                $list[$k] = $val;
                if ($k > 0) {
                    $list[$k - 1]['next'] = $val['level_name'];
                }
            }
            $list[$k]['next'] = "-";
        }
        $this->assign('list', $list);
        $count = $Ad->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }

    public function creditUserLog()
    {
        // $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        $action_type  = I('action_type', '');
        $creditUserLogMdl = model('Common/credit/CreditUserLog');
        $userMdl = model('Common/Users');
        $where   = [];

        if($search_key) {
            $user_id = $userMdl->getUserIdByMobile($search_key);
            if(!$user_id){
                $this->error('搜索不到该用户');
            }
            $where['user_id'] =$user_id;
        }

        if ($action_type) $where['action_type'] = $action_type;
        $p = $this->request->param('p',1);
        $res = $creditUserLogMdl->with('user')->where($where)->order('id desc')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $list[] = $val;
            }
        }
        $this->assign('list', $list);
        $count = $creditUserLogMdl->with('user')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        $this->assign('_actionType', $creditUserLogMdl->getActionType());
        return $this->fetch();
    }
    //会员升级记录
    public function upgradeLog()
    {
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        $UserLevelLogMdl = model('Common/UserLevelLog');
        $userMdl = model('Common/Users');
        $userLevelMdl = model('Common/UserLevel');

        $where   = [];
        // p($search_type);
        if($search_type =='mobile' && $search_key) {
            $user_id = $userMdl->getUserIdByMobile($search_key);
            if(!$user_id){
                $this->error('搜索不到该用户');
            }
            $where['user_id'] = $user_id;
        }
        
        if ($search_type =='user_id' && $search_key) $where['user_id'] = $search_key;
        $p = $this->request->param('p',1);
        $res = $UserLevelLogMdl->with('user')->where($where)->order('id desc')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $val['before_level_name'] = $userLevelMdl->where(['level_id' =>$val['before_level_id']])->value('show_name');
                $val['after_level_name']  = $userLevelMdl->where(['level_id' =>$val['level_id']])->value('show_name');
                $list[] = $val;
            }
        }
        // p(collection($list)->toArray());
        $this->assign('list', $list);
        $count = $UserLevelLogMdl->with('user')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        // $this->assign('_actionType', $UserLevelLogMdl->getActionType());
        return $this->fetch();
    }
    //会员更改上级记录
    public function changeFirstLeaderLog()
    {
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        $changModel = model('Common/ChangeFirstLeaderLog');
        $userMdl = model('Common/Users');
        $where   = [];
        if($search_type =='mobile' && $search_key) {
            $user_id = $userMdl->getUserIdByMobile($search_key);
            if(!$user_id){
                $this->error('搜索不到该用户');
            }
            $where['user_id'] = $user_id;
        }
        
        if ($search_type =='user_id' && $search_key) $where['user_id'] = $search_key;
        $p = $this->request->param('p',1);
        $res = $changModel->with('user')->where($where)->order('id desc')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $beforeUser = $userMdl->getUserInfo($val['before_first_leader_id']);
                $afterUser  =  $userMdl->getUserInfo($val['after_first_leader_id']);
                $val['before_first_leader_mobile'] = $beforeUser['mobile'];
                $val['after_first_leader_mobile']  = $afterUser['mobile'];
                $list[] = $val;
            }
        }
        $this->assign('list', $list);
        $count = $changModel->with('user')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        // $this->assign('_actionType', $UserLevelLogMdl->getActionType());
        return $this->fetch();
    }
    //会员更改手机记录
    public function changeMobileLog()
    {
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        // $type   = I('type', '');
        $changModel = model('Common/MobileChangeLog');
        $where   = [];

        if($search_type =='mobile' ) {
            if($search_key){
                $user_id = $changModel->where(['after_mobile'=>$search_key,'type'=>1])->value('user_id');
                if(!$user_id){
                    $this->error('搜索不到该记录');
                }
                $where['user_id'] = $user_id;
            }else{
                $where['type'] = 1;
            }
        }
        if($search_type =='mobile_bak' ) {
            if($search_key){
                $user_ids = $changModel->where(['after_mobile'=>$search_key,'type'=>2])->column('user_id');
                // p($user_ids);
                if(!$user_ids){
                    $this->error('搜索不到该记录');
                }
                $where['user_id'] = ['in',$user_ids];
                $where['type'] = 2;
            }else{
                $where['type'] = 2;
            }
        }
        // if ($type) $where['type'] = $type;
        
        if ($search_type =='user_id' && $search_key) $where['user_id'] = $search_key;
        $p = $this->request->param('p',1);
        $list = $changModel->where($where)->order('id desc')->page($p . ',10')->select();
        
        $this->assign('list', $list);
        $count = $changModel->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        $this->assign('count', $count);
        // $this->assign('_actionType', $UserLevelLogMdl->getActionType());
        return $this->fetch();
    }
    //会员微信支付宝更改记录
    public function paytypeChangeLog()
    {
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        // $type   = I('type', '');
        $changModel = model('Common/PaytypeChangeLog');
        $where   = [];

        if($search_type =='wx' ) {
            if($search_key){
                $where['after_account'] = $search_key ;
                $where['type'] = 2;
            }else{
                $where['type'] = 2;
            }
        }
        if($search_type =='zfb' ) {
            if($search_key){
                $where['after_account'] = $search_key;
                $where['type'] = 1;
            }else{
                $where['type'] = 1;
            }
        }
        // if ($type) $where['type'] = $type;
        
        if ($search_type =='user_id' && $search_key) $where['user_id'] = $search_key;
        $p = $this->request->param('p',1);
        $list = $changModel->where($where)->order('id desc')->page($p . ',10')->select();
        
        $this->assign('list', $list);
        $count = $changModel->where($where)->count();
        // p($count);
        $Page = new Page($count, 10);
        $show = $Page->show();
        // p($show);
        $this->assign('page', $show);
        $this->assign('count', $count);
        // $this->assign('_actionType', $UserLevelLogMdl->getActionType());
        return $this->fetch();
    }
    
    //送礼管理列表
    public function giveGift()
    {
        $search_type = I('search_type', '');
        $search_key = I('search_key', '');
        $status = I('status', '');


        $begin = $this->begin;
        $end = $this->end;

        $userMdl = model('Common/Users');

        $where = ['addtime' => ['between', "$begin,$end"]];

        if ($search_type && $search_key) {
            switch ($search_type) {
                case 'give_mobile':
                    $user_id = $userMdl->where(['mobile' => $search_key])->value('user_id');
                    $where['give_user_id'] = $user_id;
                    break;
                case 'collect_mobile':
                    $user_id = $userMdl->where(['mobile' => $search_key])->value('user_id');
                    $where['collect_user_id'] = $user_id;
                    break;
                default:
                    # code...
                    break;
            }
        }

        if ($status !== '') {
            $where['status'] = $status;
        }


        $model = model('GiveGift');
        $p = $this->request->param('p',1);
        $res = $model->with(['give_user', 'collect_user'])->where($where)->order('id desc')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $list[] = $val;
            }
        }
        $this->assign('list', $list);
        $count = $model->with(['give_user', 'collect_user'])->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        $this->assign('levelList', model("Common/users/UserLevel")->column('level_name', 'level_id'));
        $this->assign('_status', $model->_status);
        return $this->fetch();
    }
    //送礼管理详情
    public function giveGiftDetail()
    {
        $id = I('get.id');
        $userMdl = model('Common/Users');
        $give_gift_model = model('Common/GiveGift');
        $giveGiftInfo = $give_gift_model->where(array('id' => $id))->find();
        $giveUserInfo = $userMdl->getUserInfo($giveGiftInfo['give_user_id']);
        $collectUserInfo = $userMdl->getUserInfo($giveGiftInfo['collect_user_id']);
        if (!$giveGiftInfo)
            exit($this->error('参数错误'));
        if (IS_POST) {
            $act = I('act','');
            // //删除
            // if($act == 'del'){
            //     $res = $id?$give_gift_model->delData($id):ajaxReturn(['code'=>0,'msg'=>'参数有误']);
            //     if($res){
            //         ajaxReturn(['code'=>1,'msg'=>"操作成功",'url'=>U('giveGift')]);

            //     }else{
            //         ajaxReturn(['code'=>0,'msg'=>'操作失败']);
            //     }
            // }
            //审核
            if($act == 'edit'){
                $status = I('post.status/d');    
                $remark = I('post.remark');
                $res = $give_gift_model->changeStatus($id,$status,$remark,\app\common\model\UserLevelLog::UPLEVEL_SOURCE_APPLY_PASS);// 修改审核状态
                    
                if ($res) {
                    $this->success('操作成功',U('Admin/User/giveGift'));
                } else {
                    $this->error('操作失败');
                }
                exit;
            } 
        }
        $giveGiftInfo['give_mobile'] = $giveUserInfo['mobile'];
        $giveGiftInfo['give_user_name'] = $giveUserInfo['nickname']?$giveUserInfo['nickname']:$giveUserInfo['mobile'];
        $giveGiftInfo['collect_mobile'] = $collectUserInfo['mobile'];
        $giveGiftInfo['collect_user_name'] = $collectUserInfo['nickname']?$collectUserInfo['nickname']:$collectUserInfo['mobile'];
        $this->assign('giveGiftInfo', $giveGiftInfo);
        $this->assign('levelList', model("Common/users/UserLevel")->column('level_name', 'level_id'));
        $this->assign('_status', $model->_status);
        return $this->fetch();
    }


    /**
     * 会员等级添加编辑删除
     */
    public function levelHandle()
    {
        $data = I('post.');
        $userLevelValidate = Loader::validate('UserLevel');
        $return = ['status' => 0, 'msg' => '参数错误', 'result' => ''];//初始化返回信息
        if ($data['act'] == 'add') {
            if (!$userLevelValidate->batch()->check($data)) {
                $return = ['status' => 0, 'msg' => '添加失败', 'result' => $userLevelValidate->getError()];
            } else {
                $r = D('user_level')->add($data);
                if ($r !== false) {
                    $return = ['status' => 1, 'msg' => '添加成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '添加失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'edit') {
            if (!$userLevelValidate->scene('edit')->batch()->check($data)) {
                $return = ['status' => 0, 'msg' => '编辑失败', 'result' => $userLevelValidate->getError()];
            } else {
                $r_before = D('user_level')->where('level_id=' . $data['level_id'])->find();
                $r = D('user_level')->where('level_id=' . $data['level_id'])->save($data);
                if ($r !== false) {

                    //同步修改商城等级贡献值比例表
                    $change_level_name = $r_before['level_name'] != $data['level_name'] ? $data['level_name'] : '';
                    $change_show_name  = $r_before['show_name']  != $data['show_name']  ? $data['show_name']  : '';
                    if($change_show_name || $change_level_name) $result = $this->editMallLevelContribution($data['level_id'],$change_level_name,$change_show_name);
                    if(isset($result) && $result['code']!= 10000){
                        $return = ['status' => 0, 'msg' => $res['message'], 'result' => ''];
                        $this->ajaxReturn($return);
                    }

                    //若改变该等级的是否能余额转账状态 则该等级的所有用户是否能余额转账重置为该值
                    if($r_before['is_balance_transfer'] != $data['is_balance_transfer']){
                        D('users')->where(['level' => $data['level_id']])->save(['is_balance_transfer' => $data['is_balance_transfer']]);
                    }

                    $discount = $data['discount'] / 100;
                    D('users')->where(['level' => $data['level_id']])->save(['discount' => $discount]);
                    $return = ['status' => 1, 'msg' => '编辑成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '编辑失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'del') {
            $r = D('user_level')->where('level_id=' . $data['level_id'])->delete();
            if ($r !== false) {
                $return = ['status' => 1, 'msg' => '删除成功', 'result' => ''];
            } else {
                $return = ['status' => 0, 'msg' => '删除失败，数据库未响应', 'result' => ''];
            }
        }
        $this->ajaxReturn($return);
    }

    /**
     * 同步修改商城等级贡献值比例表
     */
    public function editMallLevelContribution($level_id,$show_name='',$level_name='')
    {   
        if( $level_id <= 0 ) return ['code' => 0, 'message' => '参数错误'];
        $url = tpCache('retail.api_link').'/api/store/editLevelContribution';
        $data = [];

        $result = httpRequest($url,'POST',['level_id'=>$level_id,'show_name'=>$show_name,'level_name'=>$level_name]);
        return json_decode($result,true);
    }

    /**
     * 搜索用户名
     */
    public function search_user()
    {
        $search_key = trim(I('search_key'));
        if ($search_key == '') $this->ajaxReturn(['status' => -1, 'msg' => '请按要求输入！！']);
        $list = M('users')->where(['nickname' => ['like', "%$search_key%"]])->select();
        if ($list) {
            $this->ajaxReturn(['status' => 1, 'msg' => '获取成功', 'result' => $list]);
        }
        $this->ajaxReturn(['status' => -1, 'msg' => '未查询到相应数据！！']);
    }

    /**
     * 分销树状关系
     */
    public function ajax_distribut_tree()
    {
        $list = M('users')->where("first_leader = 1")->select();
        return $this->fetch();
    }

    /**
     *
     * @time 2016/08/31
     * @author dyr
     * 发送站内信
     */
    public function sendMessage()
    {
        $user_id_array = I('get.user_id_array');
        $users = array();
        if (!empty($user_id_array)) {
            $users = M('users')->field('user_id,nickname')->where(array('user_id' => array('IN', $user_id_array)))->select();
        }
        $this->assign('users', $users);
        return $this->fetch();
    }

    /**
     * 发送系统消息
     * @author dyr
     * @time  2016/09/01
     */
    public function doSendMessage()
    {
        $call_back = I('call_back');//回调方法
        $text = I('post.text');//内容
        $type = I('post.type', 0);//个体or全体
        $admin_id = session('admin_id');
        $users = I('post.user/a');//个体id
        $message = array(
            'admin_id' => $admin_id,
            'message' => $text,
            'category' => 0,
            'send_time' => time()
        );

        if ($type == 1) {
            //全体用户系统消息
            $message['type'] = 1;
            M('Message')->add($message);
        } else {
            //个体消息
            $message['type'] = 0;
            if (!empty($users)) {
                $create_message_id = M('Message')->add($message);
                foreach ($users as $key) {
                    M('user_message')->add(array('user_id' => $key, 'message_id' => $create_message_id, 'status' => 0, 'category' => 0));
                }
            }
        }
        echo "<script>parent.{$call_back}(1);</script>";
        exit();
    }

    /**
     *
     * @time 2016/09/03
     * @author dyr
     * 发送邮件
     */
    public function sendMail()
    {
        $user_id_array = I('get.user_id_array');
        $users = array();
        if (!empty($user_id_array)) {
            $user_where = array(
                'user_id' => array('IN', $user_id_array),
                'email' => array('neq', '')
            );
            $users = M('users')->field('user_id,nickname,email')->where($user_where)->select();
        }
        $this->assign('smtp', tpCache('smtp'));
        $this->assign('users', $users);
        return $this->fetch();
    }

    /**
     * 发送邮箱
     * @author dyr
     * @time  2016/09/03
     */
    public function doSendMail()
    {
        $call_back = I('call_back');//回调方法
        $message = I('post.text');//内容
        $title = I('post.title');//标题
        $users = I('post.user/a');
        $email = I('post.email');
        if (!empty($users)) {
            $user_id_array = implode(',', $users);
            $users = M('users')->field('email')->where(array('user_id' => array('IN', $user_id_array)))->select();
            $to = array();
            foreach ($users as $user) {
                if (check_email($user['email'])) {
                    $to[] = $user['email'];
                }
            }
            $res = send_email($to, $title, $message);
            echo "<script>parent.{$call_back}({$res['status']});</script>";
            exit();
        }
        if ($email) {
            $res = send_email($email, $title, $message);
            echo "<script>parent.{$call_back}({$res['status']});</script>";
            exit();
        }
    }

    /**
     *  转账汇款记录
     */
    public function remittance()
    {
        $status = I('status', 1);
        $realname = I('realname');
        $bank_card = I('bank_card');
        $where['status'] = $status;
        $realname && $where['realname'] = array('like', '%' . $realname . '%');
        $bank_card && $where['bank_card'] = array('like', '%' . $bank_card . '%');

        $create_time = urldecode(I('create_time'));
        // echo urldecode($create_time);
        // echo $create_time;exit;
        // $create_time = str_replace('+', '', $create_time);

        $create_time = $create_time ? $create_time : date('Y-m-d H:i:s', strtotime('-1 year')) . ',' . date('Y-m-d H:i:s', strtotime('+1 day'));
        $create_time3 = explode(',', $create_time);
        $this->assign('start_time', $create_time3[0]);
        $this->assign('end_time', $create_time3[1]);
        if ($status == 2) {
            $time_name = 'pay_time';
            $export_time_name = '转账时间';
            $export_status = '已转账';
        } else {
            $time_name = 'check_time';
            $export_time_name = '审核时间';
            $export_status = '待转账';
        }
        $where[$time_name] = array(array('gt', strtotime($create_time3[0])), array('lt', strtotime($create_time3[1])));
        $withdrawalsModel = new Withdrawals();
        $count = $withdrawalsModel->where($where)->count();
        $Page = new page($count, C('PAGESIZE'));
        $list = $withdrawalsModel->where($where)->limit($Page->firstRow, $Page->listRows)->order("id desc")->select();
        if (I('export') == 1) {
            # code...导出记录
            $selected = I('selected');
            if (!empty($selected)) {
                $selected_arr = explode(',', $selected);
                $where['id'] = array('in', $selected_arr);
            }
            $list = $withdrawalsModel->where($where)->order("id desc")->select();
            $strTable = '<table width="500" border="1">';
            $strTable .= '<tr>';
            $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">用户昵称</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="100">银行机构名称</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">账户号码</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">账户开户名</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">申请金额</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">状态</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">' . $export_time_name . '</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">备注</td>';
            $strTable .= '</tr>';
            if (is_array($list)) {
                foreach ($list as $k => $val) {
                    $strTable .= '<tr>';
                    $strTable .= '<td style="text-align:center;font-size:12px;">' . $val['users']['nickname'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['bank_name'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['bank_card'] . '</td>';
                    $strTable .= '<td style="vnd.ms-excel.numberformat:@">' . $val['realname'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['money'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $export_status . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val[$time_name]) . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['remark'] . '</td>';
                    $strTable .= '</tr>';
                }
            }
            $strTable .= '</table>';
            unset($remittanceList);
            downloadExcel($strTable, 'remittance');
            exit();
        }

        $show = $Page->show();
        $this->assign('show', $show);
        $this->assign('status', $status);
        $this->assign('Page', $Page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 提现申请记录
     */
    public function withdrawals()
    {
        $this->get_withdrawals_list();
        $this->assign('withdraw_status', C('WITHDRAW_STATUS'));
        return $this->fetch();
    }

    public function get_withdrawals_list($status = '')
    {
        $id = I('selected/a');
        $user_id = I('user_id/d');
        $realname = I('realname');
        $bank_card = I('bank_card');
        $create_time = urldecode(I('create_time'));
        $create_time = $create_time ? $create_time : date('Y-m-d H:i:s', strtotime('-1 year')) . ',' . date('Y-m-d H:i:s', strtotime('+1 day'));
        $create_time3 = explode(',', $create_time);
        $this->assign('start_time', $create_time3[0]);
        $this->assign('end_time', $create_time3[1]);
        $where['w.create_time'] = array(array('gt', strtotime($create_time3[0])), array('lt', strtotime($create_time3[1])));

        $status = empty($status) ? I('status') : $status;
        if ($status !== '') {
            $where['w.status'] = $status;
        } else {
            $where['w.status'] = ['lt', 2];
        }
        if ($id) {
            $where['w.id'] = ['in', $id];
        }
        $user_id && $where['u.user_id'] = $user_id;
        $realname && $where['w.realname'] = array('like', '%' . $realname . '%');
        $bank_card && $where['w.bank_card'] = array('like', '%' . $bank_card . '%');
        $export = I('export');
        if ($export == 1) {
            $strTable = '<table width="500" border="1">';
            $strTable .= '<tr>';
            $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">申请人</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="100">提现金额</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">银行名称</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">银行账号</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">开户人姓名</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">申请时间</td>';
            $strTable .= '<td style="text-align:center;font-size:12px;" width="*">提现备注</td>';
            $strTable .= '</tr>';
            $remittanceList = Db::name('withdrawals')->alias('w')->field('w.*,u.nickname')->join('__USERS__ u', 'u.user_id = w.user_id', 'INNER')->where($where)->order("w.id desc")->select();
            if (is_array($remittanceList)) {
                foreach ($remittanceList as $k => $val) {
                    $strTable .= '<tr>';
                    $strTable .= '<td style="text-align:center;font-size:12px;">' . $val['nickname'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['money'] . ' </td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['bank_name'] . '</td>';
                    $strTable .= '<td style="vnd.ms-excel.numberformat:@">' . $val['bank_card'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['realname'] . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . '</td>';
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['remark'] . '</td>';
                    $strTable .= '</tr>';
                }
            }
            $strTable .= '</table>';
            unset($remittanceList);
            downloadExcel($strTable, 'remittance');
            exit();
        }
        $count = Db::name('withdrawals')->alias('w')->join('__USERS__ u', 'u.user_id = w.user_id', 'INNER')->where($where)->count();
        $Page = new Page($count, 20);
        $list = Db::name('withdrawals')->alias('w')->field('w.*,u.nickname')->join('__USERS__ u', 'u.user_id = w.user_id', 'INNER')->where($where)->order("w.id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        //$this->assign('create_time',$create_time2);
        $show = $Page->show();
        $this->assign('show', $show);
        $this->assign('list', $list);
        $this->assign('pager', $Page);
        C('TOKEN_ON', false);
    }

    /**
     * 删除申请记录
     */
    public function delWithdrawals()
    {
        $id = I('del_id/d');
        $res = Db::name("withdrawals")->where(['id' => $id])->delete();
        if ($res !== false) {
            $return_arr = ['status' => 1, 'msg' => '操作成功', 'data' => '',];
        } else {
            $return_arr = ['status' => -1, 'msg' => '删除失败', 'data' => '',];
        }
        $this->ajaxReturn($return_arr);
    }

    /**
     * 修改编辑 申请提现
     */
    public function editWithdrawals()
    {
        $id = I('id');
        $withdrawals = Db::name("withdrawals")->find($id);
        $user = M('users')->where(['user_id' => $withdrawals['user_id']])->find();
        if ($user['nickname'])
            $withdrawals['user_name'] = $user['nickname'];
        elseif ($user['email'])
            $withdrawals['user_name'] = $user['email'];
        elseif ($user['mobile'])
            $withdrawals['user_name'] = $user['mobile'];
        $status = $withdrawals['status'];
        $withdrawals['status_code'] = C('WITHDRAW_STATUS')["$status"];
        $this->assign('user', $user);
        $this->assign('data', $withdrawals);
        $type = I('type', '', 'trim');
        if ($type) {
            return $this->fetch('financeWithdrawals');
        }
        return $this->fetch();
    }

    /**
     *  处理会员提现申请
     */
    public function withdrawals_update()
    {
        $id = I('id/d');
        $status = I('status');
        $remark = I('remark');
        $admin_id = session('admin_id');
        $file = I('upload_pic', '', 'trim');
        $isRefuse = I('is_refuse', 2, 'intval');
        $withdrawLogic = model('common/user/WithdrawLogic', 'logic');
        $r = $withdrawLogic->checkWithdraw($id, $status, $remark, $admin_id, $file, $isRefuse);
        if ($r !== false) {
            $this->ajaxReturn(array('status' => 1, 'msg' => "操作成功"), 'JSON');
        } else {
            $this->ajaxReturn(array('status' => 0, 'msg' => $withdrawLogic->getError()), 'JSON');
        }
    }

    // 用户申请提现
    public function transfer()
    {
        $id = I('selected/a');
        if (empty($id)) $this->error('请至少选择一条记录');
        $atype = I('atype');
        if (is_array($id)) {
            $withdrawals = M('withdrawals')->where('id in (' . implode(',', $id) . ')')->select();
        } else {
            $withdrawals = M('withdrawals')->where(array('id' => $id))->select();
        }
        $alipay['batch_num'] = 0;
        $alipay['batch_fee'] = 0;
        foreach ($withdrawals as $val) {
            $user = M('users')->where(array('user_id' => $val['user_id']))->find();
            $oauthUsers = M("OauthUsers")->where(['user_id' => $user['user_id'], 'oauth_child' => 'mp'])->find();
            //获取用户绑定openId
            $user['openid'] = $oauthUsers['openid'];
            if ($user['user_money'] < $val['money']) {
                $data = array('status' => -2, 'remark' => '账户余额不足');
                M('withdrawals')->where(array('id' => $val['id']))->save($data);
                $this->error('账户余额不足');
            } else {
                $rdata = array('type' => 1, 'money' => $val['money'], 'log_type_id' => $val['id'], 'user_id' => $val['user_id']);
                if ($atype == 'online') {
                    header("Content-type: text/html; charset=utf-8");
                    exit("请联系TPshop官网客服购买高级版支持此功能");
                } else {
                    accountLog($val['user_id'], ($val['money'] * -1), 0, "管理员处理用户提现申请");//手动转账，默认视为已通过线下转方式处理了该笔提现申请
                    $r = M('withdrawals')->where(array('id' => $val['id']))->save(array('status' => 2, 'pay_time' => time()));
                    expenseLog($rdata);//支出记录日志

                }
            }
        }
        if ($alipay['batch_num'] > 0) {
            //支付宝在线批量付款
            include_once PLUGIN_PATH . "payment/alipay/alipay.class.php";
            $alipay_obj = new \alipay();
            $alipay_obj->transfer($alipay);
        }
        $this->success("操作成功!", U('remittance'), 3);
    }


    /**
     * 签到列表
     * @date 2017/09/28
     */
    public function signList()
    {
        header("Content-type: text/html; charset=utf-8");
        exit("请联系TPshop官网客服购买高级版支持此功能");
    }


    /**
     * 会员签到 ajax
     * @date 2017/09/28
     */
    public function ajaxsignList()
    {
        header("Content-type: text/html; charset=utf-8");
        exit("请联系TPshop官网客服购买高级版支持此功能");
    }

    /**
     * 签到规则设置
     * @date 2017/09/28
     */
    public function signRule()
    {
        header("Content-type: text/html; charset=utf-8");
        exit("请联系TPshop官网客服购买高级版支持此功能");
    }
    /**
     * [authentication 实名认证]
     * @return [type] [description]
     */
    public function authentication(){

        $keywords = I('keywords',false,'trim');

        if($keywords) $where['c.true_name|u.mobile'] = array('like','%'.$keywords.'%');

        $cardMdl  = model('common/credit/CreditIdentityCard');
        $where['c.process']   = 'success';
        $count    = $cardMdl->getCreditIdentityCardCount($where);
        $Page     = $pager = new Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show();
        $indentityList = $cardMdl->getCreditIdentityCardList($where,$Page,'c.*,u.mobile','c.create_time desc,c.id desc');
        
        $this->assign('idcardinfo',$indentityList);
        $this->assign('count',$count);
        $this->assign('page',$show);// 赋值分页输出
        return $this->fetch();
    }
    /**
     * [authenticationDetail 实名认证详情]
     * @return [type] [description]
     */
    public function authenticationDetail(){
        $user_id       = I('user_id',false,'trim');
        $preUrl  = $_SERVER['HTTP_REFERER'];
        $cardMdl  = model('common/credit/CreditIdentityCard');

        if(IS_POST){    
            $status = I('status');
            $id = I('post.user_id/d');
            if ($status == 'success') {
                $cert_info = $cardMdl->where(['user_id'=>$id])->find();
                $certified_status = $cardMdl->certified_status($cert_info['true_name'], $cert_info['card_number'], $cert_info['user_id']);
                if ($certified_status) {
                    ajaxReturn(['code'=>0,'msg'=>"操作失败: 此身份证实名认证的账号已认证或审核中",'url'=>U('authenticationCheck')]);
                }
            }
            $result = $cardMdl->setProcess($id,$status);
            if($result>0){
                ajaxReturn(['code'=>1,'msg'=>"操作成功",'url'=>U("Admin/User/authenticationVerify")]);
            }
            ajaxReturn(['code'=>0,'msg'=>"操作失败",'url'=>U('authenticationCheck')]);
        }
        $model = $cardMdl->getUserIdentityDetail($user_id);
        $this->assign('idcardinfo',$model);
        $this->assign('user_id',$user_id);
        $this->assign('preUrl',$preUrl);
        $processList=$cardMdl->processList();

        foreach ($processList as $k => $v) {
            if($k == 'none')unset($processList[$k]);
        }
        $this->assign('processList',$processList);
        return $this->fetch();
    }
    /**
     * [authentication 实名认证审核列表]
     * @return [type] [description]
     */
    public function authenticationVerify(){

        $keywords = I('keywords',false,'trim');
        $search_type = I('search_type','');


        $usersModel = model('Common/Users');
        if($keywords) $where['c.true_name|u.mobile'] = array('like','%'.$keywords.'%');
        $where['c.process'] = $search_type ? $search_type : ['in',['pending','invalid']];
        $cardMdl  = model('common/credit/CreditIdentityCard');
        $count    = $cardMdl->getCreditIdentityCardCount($where);
        $Page     = $pager = new Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show();// 分页显示输出
        $indentityList = $cardMdl->getCreditIdentityCardList($where,$Page,'*','c.create_time desc,c.id desc');
        
        $this->assign('idcardinfo',$indentityList);
        $this->assign('count',$count);
        $this->assign('page',$show);// 赋值分页输出
        return $this->fetch();
    }
    /**
     * [authentication 实名认证审核列表操作]
     * @return [type] [description]
     */
    public function authenticationVerifyHandle(){

        $cardMdl  = model('common/credit/CreditIdentityCard');
        if(IS_AJAX){
            $id = I('id/d');
            $act = I('act','');
            if($act == 'del'){
                $res = $id?$cardMdl->delData($id):ajaxReturn(['code'=>0,'msg'=>'参数有误']);
                if($res){
                    ajaxReturn(['code'=>1,'msg'=>"操作成功",'url'=>U('authenticationVerify')]);

                }else{
                    ajaxReturn(['code'=>0,'msg'=>'操作失败']);
                }
            }
        }
    }
      /**
     * [authentication 实名认证审核]
     * @return [type] [description]
     */
    public function authenticationCheck(){
        $user_id       = I('user_id',false,'trim');
        $preUrl  = $_SERVER['HTTP_REFERER'];
        $cardMdl  = model('common/credit/CreditIdentityCard');

        if(IS_POST){    
            $status = I('status');
            $id = I('post.user_id/d');
            if ($status == 'success') {
                $cert_info = $cardMdl->where(['user_id'=>$id])->find();
                $certified_status = $cardMdl->certified_status($cert_info['true_name'], $cert_info['card_number'], $cert_info['user_id']);
                if ($certified_status) {
                    ajaxReturn(['code'=>0,'msg'=>"操作失败: 此身份证实名认证的账号已认证或审核中",'url'=>U('authenticationCheck')]);
                }
            }
            $result = $cardMdl->setProcess($id,$status);
            if($result>0){
                ajaxReturn(['code'=>1,'msg'=>"操作成功",'url'=>U("Admin/User/authenticationVerify")]);
            }
            ajaxReturn(['code'=>0,'msg'=>"操作失败",'url'=>U('authenticationCheck')]);
        }
        $model = $cardMdl->getUserIdentityDetail($user_id);
        $this->assign('idcardinfo',$model);
        $this->assign('user_id',$user_id);
        $this->assign('preUrl',$preUrl);
        $processList=$cardMdl->processList();

        foreach ($processList as $k => $v) {
            if($k == 'none')unset($processList[$k]);
        }
        $this->assign('processList',$processList);
        return $this->fetch();
    }
    public function complaintList()
    {   
        $status = I('status','');
        $complaintMobile = I('complaintPhone');
        $respondentMobile = I('respondentPhone');
        // p($complaintMobile);
        $complaintList = model('Common/Complaint');
        $p = $this->request->param('p');
        $where = [];

        if ($status !== ''){
            $where['c.status'] = $status;
        } 
        if ($complaintMobile){
            $where['u.mobile'] = $complaintMobile;
        }
        if ($respondentMobile){
            $respondentId = M('Users')->where(['mobile'=>$respondentMobile])->value('user_id');
            // p($respondentId);
            $where['c.respondent_user_id'] = $respondentId;
        }
        $list = $complaintList
            ->field('c.*,u.mobile,u.name')
            ->alias('c')
            ->join('users u', 'c.user_id=u.user_id')
            ->where($where)
            ->order('c.complaint_id DESC')->page($p . ',10')->select();
        foreach ($list as $key => $value) {
            $list[$key]['respondent_user_name'] = M('Users')->where(['user_id'=>$value['respondent_user_id']])->field('name,mobile')->find();
            if (!empty($value['pics']))
                $list[$key]['pics'] = explode(',', $value['pics']); //多图时候分开
            // p( $list[$key]['pics'] );
            if (!empty($value['appeal_pics']))
                $list[$key]['appeal_pics'] = explode(',', $value['appeal_pics']); //多图时候分开
        }
        // p($list);
        $this->assign('list', $list);
        $count = $complaintList->alias('c')->join('users u', 'c.user_id=u.user_id')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }

    //投诉审核
    public function complaintDetail()
    {
        $complaintModel = model('Common/Complaint');
        if (IS_POST) {
            $complaint_id          = I('complaint_id');
            $data['status']        = $_POST['status'];
            $data['reply_content'] = $_POST['reply_content'];
            $res = $complaintModel->check($complaint_id,$data['reply_content'],$data['status']);

            if ($res) {
                $this->success('操作成功!',U("Admin/User/complaintList"));
            } else {
                $this->error($complaintModel->getError());
            }
            exit;

        }

        $id = I('get.id/d', 0);

        if ($id <= 0) $this->error("数据错误");
        // $info = $complaintModel->where(['complaint_id' => $id])->find();
        $info = $complaintModel
            ->field('c.*,u.mobile,u.name')
            ->alias('c')
            ->join('users u', 'c.user_id=u.user_id')
            ->where(['complaint_id' => $id])->find();
        $info['respondent_user_name'] = M('Users')->where(['user_id'=>$info['respondent_user_id']])->field('name,mobile')->find();
        $img_list = [];
        if (!empty($info['pics']))
            $img_list['pics'] = explode(',', $info['pics']); //多图时候分开

        if (!empty($info['appeal_pics']))
            $img_list['appeal'] = explode(',', $info['appeal_pics']); //多图时候分开
        $user = M('users')->where('user_id', $info['complaint_id'])->field('mobile,name')->find();
        $complaintMdl = Model('commen/complaint');
        $statusList = $complaintMdl->getStatusList();
        // 移除撤销选项
        foreach ($statusList as $key => $value) {
            if($key == 3 ) unset($statusList[$key]);
            
        }
        // p($statusList);
        $this->assign('user', $user); //会员信息
        $this->assign('statusList',$statusList); 
        $this->assign('info', $info);
        $this->assign('img_list', $img_list);

        return $this->fetch();
    }

    /**
     * 手机后台管理员列表
     */
    public function userMobileAdminList()
    {
        $userMobileAdminMdl = model('common/UserMobileAdmin');
        $p = I('p',1);
        $listRows = 10;
        $list = $userMobileAdminMdl->lists('',$p,$listRows,'','Users');
        // p(collection($list)->toArray());
        $Page = new Page($userMobileAdminMdl->count(),$listRows);
        $this->assign('pager',$Page);
        $this->assign('page',$Page->show());
        $this->assign('list',$list);
        return $this->fetch();
    }
    /**
     * 添加手机后台管理员 
     */
    public function addMobileAdmin()
    {   
        $id = I('id/d');
        $usersModel = model('Common/Users');
        $userMobileAdminMdl = model('common/UserMobileAdmin');
        if ($_POST){

            if(empty($_POST['mobile'])) $this->error("添加的用户不能为空");

            if(empty($_POST['role'])) $this->error("请选择管理员等级");
            
            //判断该用户是否存在
            $userId = $usersModel->getUserIdByMobile($_POST['mobile']);    
            if(!$userId) $this->error("添加的用户不存在");
            
            //判断用户是否已添加
            $userMobileAdminId = $userMobileAdminMdl->checkUserMobileAdmin($userId);
            if($userMobileAdminId) $this->error("该用户已添加，请勿重复添加");
            $_POST['user_id'] = $userId;
            $res = $userMobileAdminMdl->addAdmin($_POST);
            if($res){
                $this->success('操作成功!',U("Admin/User/userMobileAdminList"));
            }else{
                $this->error("操作失败");
            }
        }
        // $roleList = $userMobileAdminMdl->getRoleList();
        // $this->assign('roleList',$roleList);
        return $this->fetch();
    }
    /**
     * 删除手机后台管理员
     */
    public function delMobileAdmin()
    {
        // $usersModel = model('Common/Users');
        $id = I('id/d');
               
        if (empty($id))  ajaxReturn(['code'=>0,'msg'=>"参数有误"]);

        $userMobileAdminMdl = model('common/UserMobileAdmin');
        $res = $userMobileAdminMdl->delAdmin($id);
        if($res){
                ajaxReturn(['code'=>1,'msg'=>"操作成功"]);
        }else{
                ajaxReturn(['code'=>0,'msg'=>"操作失败"]);
        }
        return $this->fetch();
    }
    
     /*
     * [authentication 用户关系网查询]
     * @return [type] [description]
     */
    public function userRelation(){

        $search_type = I('search_type','mobile','trim');
        $keywords = I('keywords','','trim');
        $p = I('p/d',1);
        $pageSize = 20;
        $usersModel = model('Common/Users');
        $model = model('Common/distribut/DistributTeam');

        if ($keywords){
            $user_id = $keywords;
            $search_type='mobile' && $user_id = $usersModel->where([$search_type=>array('like','%'.$keywords.'%')])->value('user_id');
        }else{
            return $this->fetch();
        }
        
        $pids = $model->getUserParentIds($user_id);
        $where = ['user_id'=>['in',$pids]];
        $Page     = $pager = new Page($count,$pageSize);// 实例化分页类 传入总记录数和每页显示的记录数
        $count  = $usersModel->where($where)->count();
        $count  = count($pids);
        // $userList = $usersModel->where($where)->page($p.','.$pageSize)->select();
        $userList = [];
        foreach ($pids as $k => $v) {
            array_unshift($userList,$usersModel->where(['user_id'=>$v])->find());
        }
        // p(collection($userList)->toArray());
        $show = $Page->show();// 分页显示输出
        $this->assign('userList',$userList);
        $this->assign('count',$count);
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('level', M('user_level')->getField('level_id,level_name'));
        return $this->fetch();
    }
     //数据平移
    public function excelReg(){
        if(IS_POST){
            
            $data_str = I('data');
            $data_str = str_replace("\t", ' ', $data_str);
            $data_str = trim($data_str);
            $arr = explode("\n",  $data_str);
            $array = [];
            $reg_array = [];
            //格式化数据
            foreach ($arr as $key => $value) {
                $arr2 = explode(' ', trim($value));
                if(!check_mobile(trim($arr2[0])) || !check_mobile(trim($arr2[1])) || intval($arr2[2])<0 || intval($arr2[2])>13) exit(ajaxReturn(['code'=>0,'msg'=>'操作失败，数据有误:'.$value,'data'=>$value]));
                $array[$key]['mobile'] = trim($arr2[0]);                
                $array[$key]['first_mobile'] = trim($arr2[1]);                
                $array[$key]['level_id'] = intval($arr2[2])+1;                

            } 

            $logic = new UserLogicMdl();
            $userMdl = model('Common/Users');
            //上级已存在则注册，并添加数据和更改上级链
            foreach ($array as $k => $v) {
                $user_id = $userMdl->where(['mobile'=>$v['mobile']])->value('user_id');
                $first_leader = $userMdl->where(['mobile'=>$v['first_mobile']])->find();
                if($user_id ) exit(ajaxReturn(['code'=>0,'msg'=>'操作失败，用户:'.$v['mobile'].'已存在']));
                if( $first_leader){
                    $res = $logic->regExcel($v['first_mobile'],$v['mobile'],$v['level_id']);
                    if($res['status'] == 0) exit(ajaxReturn(['code'=>0,'msg'=>'操作失败，数据有误:'.$res['result']['mobile'],'data'=>$res['result']]));
                    if($res['status'] == -1) exit(ajaxReturn(['code'=>0,'msg'=>$res['msg']]));
                }else{
                    exit(ajaxReturn(['code'=>0,'msg'=>'操作失败,用户:'.$v['mobile'].'上级'.$v['first_mobile'].'不存在']));
                }
                
            }
            exit(ajaxReturn(['code'=>1,'msg'=>'操作成功']));
            
        }
        
        return $this->fetch();
    }

    //接点人记录列表
    public function connecterList(){
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');

        $userMdl = model('Common/Users');
        $slippingCountMdl = model('Common/SlippingCount');

        $where   = [];
        if ($search_type =='connecter_id' && $search_key) $where['connecter_id'] = $search_key;

        if($search_type =='mobile' && $search_key) {
            $user_id = $userMdl->getUserIdByMobile($search_key);
            if(!$user_id){
                $this->error('搜索不到该用户');
            }
            $where['connecter_id'] = $user_id;
        }

        $p     = $this->request->param('p',1);
        $list  = $slippingCountMdl->with('user')->where($where)->order('id desc')->page($p . ',10')->select();
        // p(collection($list)->toArray());
        $count = $slippingCountMdl->with('user')->where($where)->count();
        $Page  = new Page($count, 10);
        $show  = $Page->show();

        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $show);
        return $this->fetch();
    }    
    

     //滑落人数据平移
    public function excelForSlippingUser(){
        if(IS_POST){
            
            $data_str = I('data');
            $data_str = str_replace("\t", ' ', $data_str);
            $data_str = trim($data_str);
            $arr = explode("\n",  $data_str);
            $array = [];
            $reg_array = [];

            $userMdl = model('Common/Users');
            $slippingUserMdl = model('Common/SlippingUsers');
            $slippingConfigMdl  = model('common/SlippingConfig');
            $changeFirstLeaderLogMdl = model('Common/ChangeFirstLeaderLog');


            $slippingConfig  = $slippingConfigMdl->where(['enable'=>1])->find();


            //格式化数据
            foreach ($arr as $key => $value) {
                $arr2 = explode(' ', trim($value));
             
                if(strlen(trim($arr2[0])) != 11 || strlen(trim($arr2[1])) != 11 || strlen(trim($arr2[2])) != 11 ) 
                    exit(ajaxReturn(['code'=>0,'msg'=>'操作失败,电话号码有误:'.$value,'data'=>$value]));

                $referrer_id  = $userMdl->where(['mobile'=>trim($arr2[0])])->value('user_id');                
                $connecter_id = $userMdl->where(['mobile'=>trim($arr2[2]),'first_leader'=>$referrer_id])->value('user_id');
                $slipping_id  = $userMdl->where(['mobile'=>trim($arr2[1]),'first_leader'=>$connecter_id])->value('user_id');  
                
                if(!$referrer_id || !$connecter_id || !$slipping_id ) 
                    exit(ajaxReturn(['code'=>0,'msg'=>'操作失败,数据有误:'.$value,'data'=>$value]));

                trans();

                //添加会员变更上级记录
                $before_first_leader_id = $connecter_id;   //原上级ID         
                $after_first_leader_id = $referrer_id;         //要更改的上级ID         
                $result = $changeFirstLeaderLogMdl->addData($slipping_id,$before_first_leader_id,$after_first_leader_id,1);  //添加会员修改上级记录

                $dtMdl = model('Common/distribut/DistributTeam');
                $res = $dtMdl->updateUserParent(trim($arr2[1]),trim($arr2[0]));//变更上级，更换上级链.
                if(!$res){
                    trans('rollback');
                    exit(ajaxReturn(['code'=>0,'msg'=>'变更上级链失败']));
                }

                //如果用户等级level>=2  更改上级后的滑落规则操作
                $slippingUsersLogic = new SlippingUsersLogic;
                // $slippingConfig     =  model('common/SlippingConfig')->where(['enable'=>1])->find();
                //修改滑落用户的接点人引荐人等数据,非滑落人的上级直推人数
                // $res =$slippingUsersLogic->changeFirstLeaderForExcel($after_first_leader_id); 
                $res =$slippingUsersLogic->changeFirstLeader($after_first_leader_id,$before_first_leader_id,$user['user_id'],1);  
                if($res != true && $res['status'] == 0)   {
                    trans('rollback');
                    exit(ajaxReturn(['code'=>0,'msg'=>$res['msg']]));
                }
                trans('commit');
            }

            exit(ajaxReturn(['code'=>1,'msg'=>'操作成功']));
        }
        
        return $this->fetch();
    }

    /**
     * [slippingConfig 接点人等级滑落人数配置]lossCompensateConfig
     * @return [type] [description]
     */
    public function slippingConfig()
    {
        $slippingConfig  = model('common/SlippingConfig');
        $p = $this->request->param('p');
        $res = $slippingConfig->with('levelConfig')->order('level asc')->page($p . ',10')->select();
       
        $this->assign('list', $res);
        $count = $slippingConfig->with('levelConfig')->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }



    /**
     * [slippingConfigDetail 接点人等级滑落人数配置详情]
     * @return [type] [description]
     */
     public function slippingConfigDetail()
    {
        $act = I('get.act', 'add');
        $this->assign('act', $act);
        $id = I('get.id');
        if ($id) {
            $level_info = D('slipping_config')->where('id=' . $id)->find();
            $this->assign('info', $level_info);
        }
        return $this->fetch();
    }


    /**
     * [slippingConfigDetail 接点人等级滑落人数配置编辑及删除]
     */
    public function slippingConfigHandle()
    {
        $data = I('post.');
        $userLevelValidate = Loader::validate('SlippingConfig');
        $return = ['status' => 0, 'msg' => '参数错误', 'result' => ''];//初始化返回信息
        if ($data['act'] == 'add') {
            if (!$userLevelValidate->batch()->check($data)) {
                // p($userLevelValidate->batch()->check($data));
                $return = ['status' => 0, 'msg' => '添加失败', 'result' => $userLevelValidate->getError()];
            } else {
                unset($data['id']);
                $data['add_time'] = time();
                $r = D('slipping_config')->add($data);
                if ($r !== false) {
                    $return = ['status' => 1, 'msg' => '添加成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '添加失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'edit') {
            if (!$userLevelValidate->scene('edit')->batch()->check($data)) {
                $return = ['status' => 0, 'msg' => '编辑失败', 'result' => $userLevelValidate->getError()];
            } else {
                unset($dta['id']);
                $data['update_time'] = time();
                $r = D('slipping_config')->where('id=' . $data['id'])->save($data);
                if ($r !== false) {
                    $return = ['status' => 1, 'msg' => '编辑成功', 'result' => $userLevelValidate->getError()];
                } else {
                    $return = ['status' => 0, 'msg' => '编辑失败，数据库未响应', 'result' => ''];
                }
            }
        }
        if ($data['act'] == 'del') {
            $r = D('slipping_config')->where('id=' . $data['id'])->delete();
            if ($r !== false) {
                $return = ['status' => 1, 'msg' => '删除成功', 'result' => ''];
            } else {
                $return = ['status' => 0, 'msg' => '删除失败，数据库未响应', 'result' => ''];
            }
        }
        $this->ajaxReturn($return);
    }
    /**
    * ajax 修改接点人表数据指定字段  并作其他字段相关数据更改
    * table,id_name,id_value,field,value
    */
    public function changeTableValForSlipping(){ 

            $table = I('table'); // 表名
            $id_name = I('id_name'); // 表主键id名
            $id_value = I('id_value'); // 表主键id值
            $field  = I('field'); // 修改哪个字段
            $value  = I('value'); // 修改字段值 


            $info = M($table)->where([$id_name => $id_value])->find();
            $surplus_num = ($value - $info['slipping_num'])>0?$value - $info['slipping_num']:0;
            $data =[
                $field=>$value,
                'surplus_num'=>$surplus_num,
                'enable'=>$surplus_num>0?1:0,
                'update_time'=>time(),

            ];
            M($table)->where([$id_name => $id_value])->save($data); // 根据条件保存修改的数据
    }


    /**
     * [walletRecharge 余额充值]
     * @return [type] [description]
     */
    public function walletRecharge()
    {
        $mobile = I('mobile','','trim');
        $user_id = I('id','','trim');
        $data = [];

        if(!$mobile) exit(ajaxReturn(['code'=>0,'msg'=>'会员账号不能为空']));
        if(IS_POST){
            $data['mobile'] = I('mobile','','trim');
            $data['money'] = I('money','','trim');
            $data['desc'] = I('desc','','trim');
            $data['user_id'] = I('user_id','','trim');

            $user =  model('Common/Users')->where(['user_id'=>$data['user_id'],'mobile'=>$data['mobile']])->count();
            if(!$data['mobile'])     exit(ajaxReturn(['code'=>0,'msg'=>'会员账号不能为空']));
            if(!$data['user_id'])    exit(ajaxReturn(['code'=>0,'msg'=>'会员ID有误']));
            if($data['money'] <= 0)  exit(ajaxReturn(['code'=>0,'msg'=>'充值金额有误']));
            
            if(!$user)  exit(ajaxReturn(['code'=>0,'msg'=>'用户数据有误']));

            // $url = tpCache('retail.api_link').'/child/Thirdapi/getFormAndEshop';
            $url = tpCache('retail.api_link').'/child/Thirdapi/user_wallet_recharge';
            $res = json_decode(httpRequest($url,'POST',$data),true);
            if($res['code'] != 10000){
                $this->error($res['message']);
            }

            //添加充值记录
            $addData = [
                'user_id'=>$data['user_id'],
                'money'=>$data['money'],
                'user_id'=>$data['user_id'],
                'type'=>1,
                'desc'=>$data['desc'],
                'create_time'=>time(),
            ];

            M('recharge_money_log')->add($addData);
            exit(ajaxReturn(['code'=>1,'msg'=>$res['message']]));
        }
        $this->assign('user_id',$user_id);
        $this->assign('mobile',$mobile);
        return $this->fetch();
    }

    /**
     * [rechargeMoneyLog 会员充值记录]
     * @return [type] [description]
     */
    public function rechargeMoneyLog()
    {
        $search_type  = I('search_type', '');
        $search_key   = I('search_key', '');
        $start_time   = I('start_time');
        $end_time   = I('end_time');
        $moneyLogMdl = model('Common/RechargeMoneyLog');
        $userMdl = model('Common/Users');
        $where   = [];

        //手机号搜索
        if($search_key && $search_type =='mobile') {
            $user_id = $userMdl->getUserIdByMobile($search_key);
            if(!$user_id){
                $this->error('搜索不到该用户');
            }
            $where['user_id'] =$user_id;
        }
        //昵称搜索
        if($search_key && $search_type =='nickname') {
            $user_ids = $userMdl->getUserIdByNickname($search_key);
            if(!$user_ids){
                $this->error('搜索不到该用户');
            }
            $where['user_id'] =['in',$user_ids];
        }
        if($start_time && $end_time)  $where['create_time'] =['between',[strtotime($start_time),strtotime($end_time)]];
        if($start_time && empty($end_time))  $where['create_time'] =['gt',strtotime($start_time)];
        if($end_time   && empty($start_time))    $where['create_time'] =['lt',strtotime($end_time)];

        $p = $this->request->param('p',1);
        $res = $moneyLogMdl->with('user')->where($where)->order('id desc')->page($p . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $list[] = $val;
            }
        }

        $this->assign('list', $list);
        $count = $moneyLogMdl->with('user')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();

    }

    //获取用户团队链下级，下下级。。。。链
    public function getTeamAll()
    {
        $userId = I('user_id','','trim');
        //获取团队链
        $userIds = model('common/distribut/DistributTeam')->getUserTeamsAllForMobile($userId);
        // $id_array = $this->getUserLevelUpd();
        $this->assign('list', $id_array);

    }

    

}