<?php
namespace app\admin\validate;
use think\Validate;
class UserLevel extends Validate
{
    // 验证规则
    protected $rule = [
        ['level_name', 'require|unique:user_level'],

        ['tjnum','require|number|unique:user_level'],
        ['gift_amount','require|number|unique:user_level'],
        ['gift_amount','require|number|unique:user_level'],
        ['tjnum','require|number|user_level'],
    ];
    protected $message  = [
        'level_name.require'    => '名称必填',
        'level_name.unique'     => '已存在相同等级名称',
        'tjnum.require'     => '升级直推人数必填',
        'tjnum.number'     => '升级直推人数是数字',

        'gift_amount.require'        => '礼包金额必填',
        'gift_amount.number'         => '礼包金额必须是数字',
        'gift_amount.unique'         => '已存在相同礼包金额',
    ];
    //验证场景
    protected $scene = [
        'edit'  =>  [
            'level_name'    =>'require|unique:user_level,level_name^level_id',
            'tjnum'        =>'require|number',
            'gift_aount'    =>'require|between:1,100|unique:user_level,gift_aount^level_id',
        ],
    ];
}