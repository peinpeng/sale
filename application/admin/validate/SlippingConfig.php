<?php
namespace app\admin\validate;
use think\Validate;
class SlippingConfig extends Validate
{
    // 验证规则
    protected $rule = [
        ['level', 'require|number|unique:slipping_config'],

        ['num','require|number|unique:slipping_config'],
       
    ];
    protected $message  = [
        'level.require'    => '接点人等级要求必填',
        'level.unique'     => '已存在相同等级名称',
        'level.number'     => '接点人等级要求是数字',
        'num.require'     => '可接滑落人数必填',
        'num.number'     => '可接滑落人数是数字',

    ];
    //验证场景
    protected $scene = [
        'edit'  =>  [
            'level'    =>'require|number|unique:slipping_config,level^level',
            'tjnum'        =>'require|number',
            // 'gift_aount'    =>'require|between:1,100|unique:user_level,gift_aount^level_id',
        ],
    ];
}