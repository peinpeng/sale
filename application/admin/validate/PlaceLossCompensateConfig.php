<?php
namespace app\admin\validate;
use think\Validate;
class PlaceLossCompensateConfig extends Validate
{
    // 验证规则
    protected $rule = [
        ['level_id', 'require|number'],

        ['compensate_num','require'],
       
    ];
    protected $message  = [
        'level_id.require'    => '等级要求必填',
        'level_id.unique'     => '已存在相同等级名称',
        'level_id.number'     => '等级要求是数字',
        'compensate_num.require'     => '补偿数必填',
    ];
    //验证场景
    protected $scene = [
        'edit'  =>  [
            'level_id'    =>'require|number',
            'tjnum'        =>'require|number',
            // 'gift_aount'    =>'require|between:1,100|unique:user_level,gift_aount^level_id',
        ],
    ];
}