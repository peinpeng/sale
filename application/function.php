<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: IT宇宙人 2015-08-10 $
 */
 
 
/**
 * @param $arr
 * @param $key_name
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名 
 */
function convert_arr_key($arr, $key_name)
{
	$arr2 = array();
	foreach($arr as $key => $val){
		$arr2[$val[$key_name]] = $val;        
	}
	return $arr2;
}

function encrypt($str){
	return md5(C("AUTH_CODE").$str);
}
            
/**
 * 获取数组中的某一列
 * @param array $arr 数组
 * @param string $key_name  列名
 * @return array  返回那一列的数组
 */
function get_arr_column($arr, $key_name)
{
	$arr2 = array();
	foreach($arr as $key => $val){
		$arr2[] = $val[$key_name];        
	}
	return $arr2;
}


/**
 * 获取url 中的各个参数  类似于 pay_code=alipay&bank_code=ICBC-DEBIT
 * @param type $str
 * @return type
 */
function parse_url_param($str){
    $data = array();
    $str = explode('?',$str);
    $str = end($str);
    $parameter = explode('&',$str);
    foreach($parameter as $val){
        $tmp = explode('=',$val);
        $data[$tmp[0]] = $tmp[1];
    }
    return $data;
}


/**
 * 二维数组排序
 * @param $arr
 * @param $keys
 * @param string $type
 * @return array
 */
function array_sort($arr, $keys, $type = 'desc')
{
    $key_value = $new_array = array();
    foreach ($arr as $k => $v) {
        $key_value[$k] = $v[$keys];
    }
    if ($type == 'asc') {
        asort($key_value);
    } else {
        arsort($key_value);
    }
    reset($key_value);
    foreach ($key_value as $k => $v) {
        $new_array[$k] = $arr[$k];
    }
    return $new_array;
}


/**
 * 多维数组转化为一维数组
 * @param 多维数组
 * @return array 一维数组
 */
function array_multi2single($array)
{
    static $result_array = array();
    foreach ($array as $value) {
        if (is_array($value)) {
            array_multi2single($value);
        } else
            $result_array [] = $value;
    }
    return $result_array;
}

/**
 * 友好时间显示
 * @param $time
 * @return bool|string
 */
function friend_date($time)
{
    if (!$time)
        return false;
    $fdate = '';
    $d = time() - intval($time);
    $ld = $time - mktime(0, 0, 0, 0, 0, date('Y')); //得出年
    $md = $time - mktime(0, 0, 0, date('m'), 0, date('Y')); //得出月
    $byd = $time - mktime(0, 0, 0, date('m'), date('d') - 2, date('Y')); //前天
    $yd = $time - mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')); //昨天
    $dd = $time - mktime(0, 0, 0, date('m'), date('d'), date('Y')); //今天
    $td = $time - mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')); //明天
    $atd = $time - mktime(0, 0, 0, date('m'), date('d') + 2, date('Y')); //后天
    if ($d == 0) {
        $fdate = '刚刚';
    } else {
        switch ($d) {
            case $d < $atd:
                $fdate = date('Y年m月d日', $time);
                break;
            case $d < $td:
                $fdate = '后天' . date('H:i', $time);
                break;
            case $d < 0:
                $fdate = '明天' . date('H:i', $time);
                break;
            case $d < 60:
                $fdate = $d . '秒前';
                break;
            case $d < 3600:
                $fdate = floor($d / 60) . '分钟前';
                break;
            case $d < $dd:
                $fdate = floor($d / 3600) . '小时前';
                break;
            case $d < $yd:
                $fdate = '昨天' . date('H:i', $time);
                break;
            case $d < $byd:
                $fdate = '前天' . date('H:i', $time);
                break;
            case $d < $md:
                $fdate = date('m月d日 H:i', $time);
                break;
            case $d < $ld:
                $fdate = date('m月d日', $time);
                break;
            default:
                $fdate = date('Y年m月d日', $time);
                break;
        }
    }
    return $fdate;
}


/**
 * 返回状态和信息
 * @param $status
 * @param $info
 * @return array
 */
function arrayRes($status, $info, $url = "")
{
    return array("status" => $status, "info" => $info, "url" => $url);
}
       
/**
 * @param $arr
 * @param $key_name
  * @param $key_name2
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名 数组指定列为元素 的一个数组
 */
function get_id_val($arr, $key_name,$key_name2)
{
	$arr2 = array();
	foreach($arr as $key => $val){
		$arr2[$val[$key_name]] = $val[$key_name2];
	}
	return $arr2;
}

// 服务器端IP
 function serverIP(){   
  return gethostbyname($_SERVER["SERVER_NAME"]);   
 }  

 /**
  * 自定义函数递归的复制带有多级子目录的目录
  * 递归复制文件夹
  * @param type $src 原目录
  * @param type $dst 复制到的目录
  */                        
//参数说明：            
//自定义函数递归的复制带有多级子目录的目录
function recurse_copy($src, $dst)
{
	$now = time();
	$dir = opendir($src);
	@mkdir($dst);
	while (false !== $file = readdir($dir)) {
		if (($file != '.') && ($file != '..')) {
			if (is_dir($src . '/' . $file)) {
				recurse_copy($src . '/' . $file, $dst . '/' . $file);
			}
			else {
				if (file_exists($dst . DIRECTORY_SEPARATOR . $file)) {
					if (!is_writeable($dst . DIRECTORY_SEPARATOR . $file)) {
						exit($dst . DIRECTORY_SEPARATOR . $file . '不可写');
					}
					@unlink($dst . DIRECTORY_SEPARATOR . $file);
				}
				if (file_exists($dst . DIRECTORY_SEPARATOR . $file)) {
					@unlink($dst . DIRECTORY_SEPARATOR . $file);
				}
				$copyrt = copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
				if (!$copyrt) {
					echo 'copy ' . $dst . DIRECTORY_SEPARATOR . $file . ' failed<br>';
				}
			}
		}
	}
	closedir($dir);
}

// 递归删除文件夹
function delFile($path,$delDir = FALSE) {
    if(!is_dir($path))
                return FALSE;		
	$handle = @opendir($path);
	if ($handle) {
		while (false !== ( $item = readdir($handle) )) {
			if ($item != "." && $item != "..")
				is_dir("$path/$item") ? delFile("$path/$item", $delDir) : unlink("$path/$item");
		}
		closedir($handle);
		if ($delDir) return rmdir($path);
	}else {
		if (file_exists($path)) {
			return unlink($path);
		} else {
			return FALSE;
		}
	}
}

 
/**
 * 多个数组的笛卡尔积
*
* @param unknown_type $data
*/
function combineDika() {
	$data = func_get_args();
	$data = current($data);
	$cnt = count($data);
	$result = array();
    $arr1 = array_shift($data);
	foreach($arr1 as $key=>$item) 
	{
		$result[] = array($item);
	}		

	foreach($data as $key=>$item) 
	{                                
		$result = combineArray($result,$item);
	}
	return $result;
}


/**
 * 两个数组的笛卡尔积
 * @param unknown_type $arr1
 * @param unknown_type $arr2
*/
function combineArray($arr1,$arr2) {		 
	$result = array();
	foreach ($arr1 as $item1) 
	{
		foreach ($arr2 as $item2) 
		{
			$temp = $item1;
			$temp[] = $item2;
			$result[] = $temp;
		}
	}
	return $result;
}
/**
 * 将二维数组以元素的某个值作为键 并归类数组
 * array( array('name'=>'aa','type'=>'pay'), array('name'=>'cc','type'=>'pay') )
 * array('pay'=>array( array('name'=>'aa','type'=>'pay') , array('name'=>'cc','type'=>'pay') ))
 * @param $arr 数组
 * @param $key 分组值的key
 * @return array
 */
function group_same_key($arr,$key){
    $new_arr = array();
    foreach($arr as $k=>$v ){
        $new_arr[$v[$key]][] = $v;
    }
    return $new_arr;
}

/**
 * 获取随机字符串
 * @param int $randLength  长度
 * @param int $addtime  是否加入当前时间戳
 * @param int $includenumber   是否包含数字
 * @return string
 */
function get_rand_str($randLength=6,$addtime=1,$includenumber=0){
    if ($includenumber){
        $chars='abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQEST123456789';
    }else {
        $chars='abcdefghijklmnopqrstuvwxyz';
    }
    $len=strlen($chars);
    $randStr='';
    for ($i=0;$i<$randLength;$i++){
        $randStr.=$chars[rand(0,$len-1)];
    }
    $tokenvalue=$randStr;
    if ($addtime){
        $tokenvalue=$randStr.time();
    }
    return $tokenvalue;
}

/**
 * CURL请求
 * @param $url string 请求url地址
 * @param $method string 请求方法 get post
 * @param mixed $postfields post数据数组
 * @param array $headers 请求header信息
 * @param bool|false $debug  调试开启 默认false
 * @return mixed
 */
function httpRequest($url, $method="GET", $postfields = null, $headers = array(), $debug = false)
{
    $method = strtoupper($method);
    $ci = curl_init();
    /* Curl settings */
    curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ci, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 60); /* 在发起连接前等待的时间，如果设置为0，则无限等待 */
    curl_setopt($ci, CURLOPT_TIMEOUT, 7); /* 设置cURL允许执行的最长秒数 */
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
    switch ($method) {
        case "POST":
            curl_setopt($ci, CURLOPT_POST, true);
            if (!empty($postfields)) {
                $tmpdatastr = is_array($postfields) ? http_build_query($postfields) : $postfields;
                curl_setopt($ci, CURLOPT_POSTFIELDS, $tmpdatastr);
            }
            break;
        default:
            curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method); /* //设置请求方式 */
            break;
    }
    $ssl = preg_match('/^https:\/\//i', $url) ? TRUE : FALSE;
    curl_setopt($ci, CURLOPT_URL, $url);
    if ($ssl) {
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, FALSE); // 不从证书中检查SSL加密算法是否存在
    }
    //curl_setopt($ci, CURLOPT_HEADER, true); /*启用时会将头文件的信息作为数据流输出*/
    if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
    	curl_setopt($ci, CURLOPT_FOLLOWLOCATION, 1);
    }
    curl_setopt($ci, CURLOPT_MAXREDIRS, 2);/*指定最多的HTTP重定向的数量，这个选项是和CURLOPT_FOLLOWLOCATION一起使用的*/
    curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ci, CURLINFO_HEADER_OUT, true);
    /*curl_setopt($ci, CURLOPT_COOKIE, $Cookiestr); * *COOKIE带过去** */
    $response = curl_exec($ci);
    $requestinfo = curl_getinfo($ci);
    $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
    if ($debug) {
        echo "=====post data======\r\n";
        var_dump($postfields);
        echo "=====info===== \r\n";
        print_r($requestinfo);
        echo "=====response=====\r\n";
        print_r($response);
    }
    curl_close($ci);
    return $response;
    //return array($http_code, $response,$requestinfo);
}

/**
 * 过滤数组元素前后空格 (支持多维数组)
 * @param $array 要过滤的数组
 * @return array|string
 */
function trim_array_element($array){
    if(!is_array($array))
        return trim($array);
    return array_map('trim_array_element',$array);
}

/**
 * 检查手机号码格式
 * @param $mobile 手机号码
 */
function check_mobile($mobile)
{
    if (preg_match('/1[3456789]\d{9}$/', $mobile)) {
        return true;
    }
    return false;
}
/**
 * 检查手机号码格式 含港澳台
 * @param $mobile 手机号码
 */
function check_mobile_all($mobile)
{
    if (preg_match('/^[1][3-9]\d{9}$|^([6|9])\d{7}$|^[0][9]\d{8}$|^[6]([8|6])\d{5}$/', $mobile)) {
        return true;
    }
    return false;
}
/**
 * 港澳台加区号
 * @param $mobile 手机号码
 */
function addMobileAreaCode($mobile,$province)
{
    if(in_array($province, ['47502','47495','47493'])){

       switch ($province) {
        //香港特别行政区
            case '47502':
               $mobile = '00852'.$mobile;
               break;
        //澳门特别行政区
            case '47495':
               $mobile = '00853'.$mobile;
               break;
        //台湾省
            case '47493':
               $mobile = '00886'.$mobile;
               break;
            default:
               break;
       }
   }
   return $mobile;
    
}
/**
 * 获取港澳台省id
 * @param $mobile 手机号码
 */
function getProvinceForHMT()
{
    $arr = ['47502','47495','47493'];
   return $arr;
    
}
/**
 * 根据手机号添加港澳台区号
 * @param $mobile 手机号码
 */
function getAreaCodeForMobile($mobile)
{
    if (preg_match('/1[3456789]\d{9}$/', $mobile)) return $mobile;
    if (preg_match('/^([6|9])\d{7}$/', $mobile)) return '00852'.$mobile;
    if (preg_match('/^[0][9]\d{8}$/', $mobile)) return '00886'.$mobile;
    if (preg_match('/^[6]([8|6])\d{5}$/', $mobile)) return '00853'.$mobile;
   return $mobile;
}



/**
 * 检查固定电话
 * @param $mobile
 * @return bool
 */
function check_telephone($mobile){
    if(preg_match('/^([0-9]{3,4}-)?[0-9]{7,8}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查邮箱地址格式
 * @param $email 邮箱地址
 */
function check_email($email){
    if(filter_var($email,FILTER_VALIDATE_EMAIL))
        return true;
    return false;
}


/**
 *   实现中文字串截取无乱码的方法
 */
function getSubstr($string, $start, $length) {
      if(mb_strlen($string,'utf-8')>$length){
          $str = mb_substr($string, $start, $length,'utf-8');
          return $str.'...';
      }else{
          return $string;
      }
}


/**
 * 判断当前访问的用户是  PC端  还是 手机端  返回true 为手机端  false 为PC 端
 * @return boolean
 */
/**
　　* 是否移动端访问访问
　　*
　　* @return bool
　　*/
function isMobile()
{
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
    return true;

    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    {
    // 找不到为flase,否则为true
    return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT']))
    {
        $clientkeywords = array ('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile');
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            return true;
    }
        // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    {
    // 如果只支持wml并且不支持html那一定是移动设备
    // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        }
    }
            return false;
 }

function is_weixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } return false;
}
 

function is_qq() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'QQ') !== false) {
        return true;
    } return false;
}
function is_alipay() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
        return true;
    } return false;
}

//php获取中文字符拼音首字母
function getFirstCharter($str){
      if(empty($str))
      {
            return '';          
      }
      $fchar=ord($str{0});
      if($fchar>=ord('A')&&$fchar<=ord('z')) return strtoupper($str{0});
      $s1=iconv('UTF-8','gb2312//TRANSLIT//IGNORE',$str);
      $s2=iconv('gb2312','UTF-8//TRANSLIT//IGNORE',$s1);
      $s=$s2==$str?$s1:$str;
      $asc=ord($s{0})*256+ord($s{1})-65536;
     if($asc>=-20319&&$asc<=-20284) return 'A';
     if($asc>=-20283&&$asc<=-19776) return 'B';
     if($asc>=-19775&&$asc<=-19219) return 'C';
     if($asc>=-19218&&$asc<=-18711) return 'D';
     if($asc>=-18710&&$asc<=-18527) return 'E';
     if($asc>=-18526&&$asc<=-18240) return 'F';
     if($asc>=-18239&&$asc<=-17923) return 'G';
     if($asc>=-17922&&$asc<=-17418) return 'H';
     if($asc>=-17417&&$asc<=-16475) return 'J';
     if($asc>=-16474&&$asc<=-16213) return 'K';
     if($asc>=-16212&&$asc<=-15641) return 'L';
     if($asc>=-15640&&$asc<=-15166) return 'M';
     if($asc>=-15165&&$asc<=-14923) return 'N';
     if($asc>=-14922&&$asc<=-14915) return 'O';
     if($asc>=-14914&&$asc<=-14631) return 'P';
     if($asc>=-14630&&$asc<=-14150) return 'Q';
     if($asc>=-14149&&$asc<=-14091) return 'R';
     if($asc>=-14090&&$asc<=-13319) return 'S';
     if($asc>=-13318&&$asc<=-12839) return 'T';
     if($asc>=-12838&&$asc<=-12557) return 'W';
     if($asc>=-12556&&$asc<=-11848) return 'X';
     if($asc>=-11847&&$asc<=-11056) return 'Y';
     if($asc>=-11055&&$asc<=-10247) return 'Z';
     return null;
}

/**
 * 获取整条字符串汉字拼音首字母
 * @param $zh
 * @return string
 */
function pinyin_long($zh){
    $ret = "";
    $s1 = iconv("UTF-8","gb2312", $zh);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $zh){$zh = $s1;}
    for($i = 0; $i < strlen($zh); $i++){
        $s1 = substr($zh,$i,1);
        $p = ord($s1);
        if($p > 160){
            $s2 = substr($zh,$i++,2);
            $ret .= getFirstCharter($s2);
        }else{
            $ret .= $s1;
        }
    }
    return $ret;
}


function ajaxReturn($data)
{
    exit(json_encode($data, JSON_UNESCAPED_UNICODE));
}

function flash_sale_time_space()
{
    $now_day = date('Y-m-d');
    $now_time = date('H');
    if ($now_time % 2 == 0) {
        $flash_now_time = $now_time;
    } else {
        $flash_now_time = $now_time - 1;
    }
    $flash_sale_time = strtotime($now_day . " " . $flash_now_time . ":00:00");
    $space = 7200;
    $time_space = array(
        '1' => array('font' => date("H:i", $flash_sale_time), 'start_time' => $flash_sale_time, 'end_time' => $flash_sale_time + $space),
        '2' => array('font' => date("H:i", $flash_sale_time + $space), 'start_time' => $flash_sale_time + $space, 'end_time' => $flash_sale_time + 2 * $space),
        '3' => array('font' => date("H:i", $flash_sale_time + 2 * $space), 'start_time' => $flash_sale_time + 2 * $space, 'end_time' => $flash_sale_time + 3 * $space),
        '4' => array('font' => date("H:i", $flash_sale_time + 3 * $space), 'start_time' => $flash_sale_time + 3 * $space, 'end_time' => $flash_sale_time + 4 * $space),
        '5' => array('font' => date("H:i", $flash_sale_time + 4 * $space), 'start_time' => $flash_sale_time + 4 * $space, 'end_time' => $flash_sale_time + 5 * $space),
    );
    return $time_space;
}

/**
 * 验证码操作(不生成图片)
 * @param array $inconfig  配置
 * @param sring $id 要生成验证码的标识 
 * @param string $incode 验证码,若为null生成验证码,否则检验验证码
 */
function capache($inconfig = [], $id = '', $incode = null)
{  
    $config = array(
        'seKey'     =>  'ThinkPHP.CN',   // 验证码加密密钥
        'codeSet'   =>  '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY', // 验证码字符集合
        'expire'    =>  1800,            // 验证码过期时间（s）
        'useZh'     =>  false,           // 使用中文验证码 
        'zhSet'     =>  '们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借',              // 中文验证码字符串
        'length'    =>  4,               // 验证码位数
        'reset'     =>  true,           // 验证成功后是否重置
    );
    $config = array_merge($config, $inconfig);
    $authcode = function ($str) use ($config) {
        $key = substr(md5($config['seKey']), 5, 8);
        $str = substr(md5($str), 8, 10);
        return md5($key . $str);
    };

    /* 生成验证码 */
    if ($incode === null) {
        for ($i = 0; $i<$config['length']; $i++) {
            $code[$i] = $config['codeSet'][mt_rand(0, strlen($config['codeSet'])-1)];
        }
        // 保存验证码
        $code_str   =   implode('', $code);
        $key        =   $authcode($config['seKey']);
        $code       =   $authcode(strtoupper($code_str));
        $secode     =   array();
        $secode['verify_code'] = $code; // 把校验码保存到session
        $secode['verify_time'] = NOW_TIME;  // 验证码创建时间
        session($key.$id, $secode);
        return $code_str;
    } 

    /* 检验验证码 */
    if (is_string($incode)) {
        $key = $authcode($config['seKey']).$id;
        // 验证码不能为空
        $secode = session($key);
        if (empty($incode) || empty($secode)) {
            return false;
        }
        // session 过期
        if (NOW_TIME - $secode['verify_time'] > $config['expire']) {
            session($key, null);
            return false;
        }

        if ($authcode(strtoupper($incode)) == $secode['verify_code']) {
            $config['reset'] && session($key, null);
            return true;
        }
        return false;
    }

    return false;
}

function urlsafe_b64encode($string) 
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_',''),$data);
    return $data;
}

/**
 * 当前请求是否是https
 * @return type
 */
function is_https()
{
    return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off';
}
function mobile_hide($mobile){
    return substr_replace($mobile,'****',3,4);
}

/**
 * BMP 创建函数
 * @author simon
 * @param string $filename path of bmp file
 * @example who use,who knows
 * @return resource of GD
 */
function imagecreatefrombmp( $filename ){
    if ( !$f1 = fopen( $filename, "rb" ) )
        return FALSE;
    $FILE = unpack( "vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread( $f1, 14 ) );
    if ( $FILE['file_type'] != 19778 )
        return FALSE;
    $BMP = unpack( 'Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel' . '/Vcompression/Vsize_bitmap/Vhoriz_resolution' . '/Vvert_resolution/Vcolors_used/Vcolors_important', fread( $f1, 40 ) );
    $BMP['colors'] = pow( 2, $BMP['bits_per_pixel'] );
    if ( $BMP['size_bitmap'] == 0 )
        $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
    $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel'] / 8;
    $BMP['bytes_per_pixel2'] = ceil( $BMP['bytes_per_pixel'] );
    $BMP['decal'] = ($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
    $BMP['decal'] -= floor( $BMP['width'] * $BMP['bytes_per_pixel'] / 4 );
    $BMP['decal'] = 4 - (4 * $BMP['decal']);
    if ( $BMP['decal'] == 4 )
        $BMP['decal'] = 0;
    $PALETTE = array();
    if ( $BMP['colors'] < 16777216 ){
        $PALETTE = unpack( 'V' . $BMP['colors'], fread( $f1, $BMP['colors'] * 4 ) );
    }
    $IMG = fread( $f1, $BMP['size_bitmap'] );
    $VIDE = chr( 0 );
    $res = imagecreatetruecolor( $BMP['width'], $BMP['height'] );
    $P = 0;
    $Y = $BMP['height'] - 1;
    while( $Y >= 0 ){
        $X = 0;
        while( $X < $BMP['width'] ){
            if ( $BMP['bits_per_pixel'] == 32 ){
                $COLOR = unpack( "V", substr( $IMG, $P, 3 ) );
                $B = ord(substr($IMG, $P,1));
                $G = ord(substr($IMG, $P+1,1));
                $R = ord(substr($IMG, $P+2,1));
                $color = imagecolorexact( $res, $R, $G, $B );
                if ( $color == -1 )
                    $color = imagecolorallocate( $res, $R, $G, $B );
                $COLOR[0] = $R*256*256+$G*256+$B;
                $COLOR[1] = $color;
            }elseif ( $BMP['bits_per_pixel'] == 24 )
                $COLOR = unpack( "V", substr( $IMG, $P, 3 ) . $VIDE );
            elseif ( $BMP['bits_per_pixel'] == 16 ){
                $COLOR = unpack( "n", substr( $IMG, $P, 2 ) );
                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            }elseif ( $BMP['bits_per_pixel'] == 8 ){
                $COLOR = unpack( "n", $VIDE . substr( $IMG, $P, 1 ) );
                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            }elseif ( $BMP['bits_per_pixel'] == 4 ){
                $COLOR = unpack( "n", $VIDE . substr( $IMG, floor( $P ), 1 ) );
                if ( ($P * 2) % 2 == 0 )
                    $COLOR[1] = ($COLOR[1] >> 4);
                else
                    $COLOR[1] = ($COLOR[1] & 0x0F);
                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            }elseif ( $BMP['bits_per_pixel'] == 1 ){
                $COLOR = unpack( "n", $VIDE . substr( $IMG, floor( $P ), 1 ) );
                if ( ($P * 8) % 8 == 0 )
                    $COLOR[1] = $COLOR[1] >> 7;
                elseif ( ($P * 8) % 8 == 1 )
                    $COLOR[1] = ($COLOR[1] & 0x40) >> 6;
                elseif ( ($P * 8) % 8 == 2 )
                    $COLOR[1] = ($COLOR[1] & 0x20) >> 5;
                elseif ( ($P * 8) % 8 == 3 )
                    $COLOR[1] = ($COLOR[1] & 0x10) >> 4;
                elseif ( ($P * 8) % 8 == 4 )
                    $COLOR[1] = ($COLOR[1] & 0x8) >> 3;
                elseif ( ($P * 8) % 8 == 5 )
                    $COLOR[1] = ($COLOR[1] & 0x4) >> 2;
                elseif ( ($P * 8) % 8 == 6 )
                    $COLOR[1] = ($COLOR[1] & 0x2) >> 1;
                elseif ( ($P * 8) % 8 == 7 )
                    $COLOR[1] = ($COLOR[1] & 0x1);
                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            }else
                return FALSE;
            imagesetpixel( $res, $X, $Y, $COLOR[1] );
            $X++;
            $P += $BMP['bytes_per_pixel'];
        }
        $Y--;
        $P += $BMP['decal'];
    }
    fclose( $f1 );
    return $res;
}
/** 
* 创建bmp格式图片 
* 
* @author: legend(legendsky@hotmail.com) 
* @link: http://www.ugia.cn/?p=96 
* @description: create Bitmap-File with GD library 
* @version: 0.1 
* 
* @param resource $im          图像资源 
* @param string   $filename    如果要另存为文件，请指定文件名，为空则直接在浏览器输出 
* @param integer  $bit         图像质量(1、4、8、16、24、32位) 
* @param integer  $compression 压缩方式，0为不压缩，1使用RLE8压缩算法进行压缩 
* 
* @return integer 
*/ 
function imagebmp(&$im, $filename = '', $bit = 8, $compression = 0) 
{ 
    if (!in_array($bit, array(1, 4, 8, 16, 24, 32))) 
    { 
        $bit = 8; 
    } 
    else if ($bit == 32) // todo:32 bit 
    { 
        $bit = 24; 
    } 
    $bits = pow(2, $bit); 
    // 调整调色板 
    imagetruecolortopalette($im, true, $bits); 
    $width  = imagesx($im); 
    $height = imagesy($im); 
    $colors_num = imagecolorstotal($im); 
    if ($bit <= 8) 
    { 
        // 颜色索引 
        $rgb_quad = ''; 
        for ($i = 0; $i < $colors_num; $i ++) 
        { 
            $colors = imagecolorsforindex($im, $i); 
            $rgb_quad .= chr($colors['blue']) . chr($colors['green']) . chr($colors['red']) . "\0"; 
        } 
        // 位图数据 
        $bmp_data = ''; 
        // 非压缩 
        if ($compression == 0 || $bit < 8) 
        { 
            if (!in_array($bit, array(1, 4, 8))) 
            { 
                $bit = 8; 
            } 
            $compression = 0; 
            // 每行字节数必须为4的倍数，补齐。 
            $extra = ''; 
            $padding = 4 - ceil($width / (8 / $bit)) % 4; 
            if ($padding % 4 != 0) 
            { 
                $extra = str_repeat("\0", $padding); 
            } 
            for ($j = $height - 1; $j >= 0; $j --) 
            { 
                $i = 0; 
                while ($i < $width) 
                { 
                    $bin = 0; 
                    $limit = $width - $i < 8 / $bit ? (8 / $bit - $width + $i) * $bit : 0; 
                    for ($k = 8 - $bit; $k >= $limit; $k -= $bit) 
                    { 
                        $index = imagecolorat($im, $i, $j); 
                        $bin |= $index << $k; 
                        $i ++; 
                    } 
                    $bmp_data .= chr($bin); 
                } 
                $bmp_data .= $extra; 
            } 
        } 
        // RLE8 压缩 
        else if ($compression == 1 && $bit == 8) 
        { 
            for ($j = $height - 1; $j >= 0; $j --) 
            { 
                $last_index = "\0"; 
                $same_num   = 0; 
                for ($i = 0; $i <= $width; $i ++) 
                { 
                    $index = imagecolorat($im, $i, $j); 
                    if ($index !== $last_index || $same_num > 255) 
                    { 
                        if ($same_num != 0) 
                        { 
                            $bmp_data .= chr($same_num) . chr($last_index); 
                        } 
                        $last_index = $index; 
                        $same_num = 1; 
                    } 
                    else 
                    { 
                        $same_num ++; 
                    } 
                } 
                $bmp_data .= "\0\0"; 
            } 
            $bmp_data .= "\0\1"; 
        } 
        $size_quad = strlen($rgb_quad); 
        $size_data = strlen($bmp_data); 
    } 
    else 
    { 
        // 每行字节数必须为4的倍数，补齐。 
        $extra = ''; 
        $padding = 4 - ($width * ($bit / 8)) % 4; 
        if ($padding % 4 != 0) 
        { 
            $extra = str_repeat("\0", $padding); 
        } 
        // 位图数据 
        $bmp_data = ''; 
        for ($j = $height - 1; $j >= 0; $j --) 
        { 
            for ($i = 0; $i < $width; $i ++) 
            { 
                $index  = imagecolorat($im, $i, $j); 
                $colors = imagecolorsforindex($im, $index); 
                if ($bit == 16) 
                { 
                    $bin = 0 << $bit; 
                    $bin |= ($colors['red'] >> 3) << 10; 
                    $bin |= ($colors['green'] >> 3) << 5; 
                    $bin |= $colors['blue'] >> 3; 
                    $bmp_data .= pack("v", $bin); 
                } 
                else 
                { 
                    $bmp_data .= pack("c*", $colors['blue'], $colors['green'], $colors['red']); 
                } 
                // todo: 32bit; 
            } 
            $bmp_data .= $extra; 
        } 
        $size_quad = 0; 
        $size_data = strlen($bmp_data); 
        $colors_num = 0; 
    } 
    // 位图文件头 
    $file_header = "BM" . pack("V3", 54 + $size_quad + $size_data, 0, 54 + $size_quad); 
    // 位图信息头 
    $info_header = pack("V3v2V*", 0x28, $width, $height, 1, $bit, $compression, $size_data, 0, 0, $colors_num, 0); 
    // 写入文件 
    if ($filename != '') 
    { 
        $fp = fopen("test.bmp", "wb"); 
        fwrite($fp, $file_header); 
        fwrite($fp, $info_header); 
        fwrite($fp, $rgb_quad); 
        fwrite($fp, $bmp_data); 
        fclose($fp); 
        return 1; 
    } 
    // 浏览器输出 
    header("Content-Type: image/bmp"); 
    echo $file_header . $info_header; 
    echo $rgb_quad; 
    echo $bmp_data; 
    return 1; 
} 
/**
 * 	作用：array转xml
 */
function arrayToXml($arr)
{
    $xml = "<xml>";
    foreach ($arr as $key=>$val)
    {
        if (is_numeric($val))
        {
            $xml.="<".$key.">".$val."</".$key.">";

        }
        else
            $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
    }
    $xml.="</xml>";
    return $xml;
}

/**
 * 替换特殊字符
 * @param unknown 原始字符串
 * @param string 替换字符串
 * @return mixed
 */
function replaceSpecialStr($orignalStr , $replace=''){
    return preg_replace("/[^\x{4e00}-\x{9fa5}]/iu", $replace ,$orignalStr);
}

/**
 * 比较两个版本大小, $v1>v2:1 ; $v1=v2:0 ;$v1<v2:0
 * @param unknown $v1
 * @param unknown $v2
 * @return number
 */
function compareVersion($v1, $v2) {
    $v1 = explode(".",$v1);
    $v2 =  explode(".",$v2);
    $len = max(count($v1), count($v2));

    while(count($v1) < $len) {
        array_push($v1, 0);
    }

    while(count($v2) < $len) {
        array_push($v2, 0);
    }
    for($i = 0; $i < $len;$i++) {
        $num1 = intval($v1[$i]);
        $num2 = intval($v2[$i]);
        if ($num1 > $num2) {
            return 1;
        } else if ($num1 < $num2) {
            return -1;
        }
    }
    return 0;
}

/**
 * [p 打断输出]
 * @param  [array]  $data [待输出的数组]
 * @param  integer $type [0打断输出、1不打断]
 * @return [array]        
 */
function p($data = [],$type = 0)
{
    switch ($type) {
        case 0:
            halt($data);
            break;
        case 1:
            dump($data);
            break;
    }
    return ;
}

/**
 * 打印模型的最后一条sql
 * @param  [type] $model [description]
 * @return [type]        [description]
 */
function ps($model,$isInterrupt = 0)
{   if($isInterrupt == 0)
         p($model->getLastSql());
    dump($model->getLastSql());
    return ;
}


/**
 * [str_relace_line_space 去除空格换行]
 * @param  [type] $str [description]
 * @return [type]      [description]
 */
function str_relace_line_space($str)
{
   return str_replace(array("\r\n", "\r", "\n"), "", $str);
}


/**
 * [base64 base64加密解密]
 * @param  string  $str      [description]
 * @param  integer $isEncode [1 加密 2 解密]
 * @return [type]            [description]
 */
function base64($str = '', $code = 'encode')
{
    switch ($code) {
        case 'encode':
            $baseString = base64_encode($str);
            break;
        case 'decode':
            $baseString = base64_decode($str);
            break;
    }
    return $baseString;
}

/**
 * [is_idcard 身份证验证]
 * @param  [type]  $id [description]
 * @return boolean     [description]
 */
function is_idcard( $id )
{
  $id = strtoupper($id);
  $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
  $arr_split = array();
  if(!preg_match($regx, $id))
  {
    return FALSE;
  }
  if(15==strlen($id)) //检查15位
  {
    $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";

    @preg_match($regx, $id, $arr_split);
    //检查生日日期是否正确
    $dtm_birth = "19".$arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
    if(!strtotime($dtm_birth))
    {
      return FALSE;
    } else {
      return TRUE;
    }
  }
  else      //检查18位
  {
    $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
    @preg_match($regx, $id, $arr_split);
    $dtm_birth = $arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
    if(!strtotime($dtm_birth)) //检查生日日期是否正确
    {
      return FALSE;
    }
    else
    {
      //检验18位身份证的校验码是否正确。
      //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
      $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
      $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
      $sign = 0;
      for ( $i = 0; $i < 17; $i++ )
      {
        $b = (int) $id{$i};
        $w = $arr_int[$i];
        $sign += $b * $w;
      }
      $n = $sign % 11;
      $val_num = $arr_ch[$n];
      if ($val_num != substr($id,17, 1))
      {
        return FALSE;
      } //phpfensi.com
      else
      {
        return TRUE;
      }
    }
  }

}

/**
 * [is_idcard_tw 台湾身份证验证]
 * @param  [type]  $id [description]
 * @return boolean     [description]
 */
  function is_idcard_tw($id) {

    $id=strtoupper($id);

    $d1=substr($id,0,1);

    if(strlen($id)!=10) {return FALSE;}

    if(stristr('ABCDEFGHJKLMNPQRSTUVXYWZIO',$d1)===FALSE) {return FALSE;}

    if(substr($id,1,1)!='1' && substr($id,1,1)!='2') {return FALSE;}

    if(!is_numeric(substr($id,1,9))) {return FALSE;}

 

    $num=array(

        'A'=>'10','B'=>'11','C'=>'12','D'=>'13','E'=>'14','F'=>'15','G'=>'16','H'=>'17','J'=>'18','K'=>'19','L'=>'20','M'=>'21','N'=>'22',

        'P'=>'23','Q'=>'24','R'=>'25','S'=>'26','T'=>'27','U'=>'28','V'=>'29','X'=>'30','Y'=>'31','W'=>'32','Z'=>'33','I'=>'34','O'=>'35',

    );

    $n1=substr($num[$d1],0,1)+(substr($num[$d1],1,1)*9);

    unset($num,$d1);

    $n2=0;

    for($j=1;$j<9;$j++) {

        $d4=substr($id,$j,1);

        $n2=$n2+$d4*(9-$j);

    }

    $n3=$n1+$n2+substr($id,9,1);

    if(($n3 % 10)!=0){return FALSE;}

    return TRUE;

}

/**
     * 香港身份证校验
     * @param string $number 香港身份证号码
     * @author mosishu
     * @return array 正确时返回true状态和身份证号码，错误时返回false状态
     */
     function checkHKIdCard($number){
           //香港身份证首字母对应的数字
     $letterNum = array(
        'A'=>1,
        'B'=>2,
        'C'=>3,
        'D'=>4,
        'E'=>5,
        'F'=>6,
        'G'=>7,
        'H'=>8,
        'I'=>9,
        'J'=>10,
        'K'=>11,
        'L'=>12,
        'M'=>13,
        'N'=>14,
        'O'=>15,
        'P'=>16,
        'Q'=>17,
        'R'=>18,
        'S'=>19,
        'T'=>20,
        'U'=>21,
        'V'=>22,
        'W'=>23,
        'X'=>24,
        'Y'=>25,
        'Z'=>26,
        );
    	//将中文括号全部替换成英文括号
		if (strpos($number,"（") || strpos($number,"）")){
			$number = str_replace('（', '(', $number);
			$number = str_replace('）', ')', $number);
		}
		if(strlen($number)!=10){
		    return array('status'=>false);
		}
    	$one = substr($number,0,1);//取首字母
    	$two = substr($number,1,1);//取第二位
    	$three = substr($number,2,1);//取第三位
    	$four = substr($number,3,1);//取第四位
    	$five = substr($number,4,1);//取第五位
    	$six = substr($number,5,1);//取第六位
    	$seven = substr($number,6,1);//取第七位
    	$checkCode = substr($number,-2,1);//取括号内的校验码
    	$sixNum = substr($number, 1,6);//取中间六位数字
    	if (preg_match('/^[A-Z]+$/', $one)){//第一位要是大写字母
    		if ( is_numeric($sixNum)){//中间六位要是数字
    			$sum =$letterNum[$one]*8+$two*7+$three*6+$four*5+$five*4+$six*3+$seven*2;
    			$residue = $sum%11;
    			if ($residue==1){
    				$checkIdCode='A';
    			}
    			elseif($residue==0){
    				$checkIdCode=0;
    			}
    			else{
    				$checkIdCode=11-$residue;
    			}
    			if ($checkCode==$checkIdCode){
    				return array('status'=>true,'sfz_id'=>$number);
    			}else{
    				return array('status'=>false);
    			}
    		}
    		else{
    			return array('status'=>false);
    		}
    	}
    	else{
    		return array('status'=>false);
    	}
    }
    
 
 


/**
 * 生成带下一步网址参数的网址
 *
 * @param string $url
 * @param string $nextURL
 * @return void
 * 用法举例: withNextURL(U('User/login'),$_SERVER['REQUEST_URI'])
 * @author Wenhui Shen <swh@admpub.com>
 */
function withNextURL($url,$nextURL='',$paramName='next'){
    if(!$nextURL) $nextURL=$_SERVER['REQUEST_URI'];
    if(strpos($url,'?')!==false) return $url.'&'.$paramName.'='.urlencode($nextURL);
    return $url.'?'.$paramName.'='.urlencode($nextURL);
}

/**
 * User: 聂风 Function:输出api信息
 * @param string $code
 * @param string $msg
 * @param null $data
 * @return string
 */
function  api_response($code = "", $msg = "", $data = null)
{
    echo json_encode([
        'code'=>$code,
        'message' => $msg,
        'result'=>$data,
    ], JSON_UNESCAPED_UNICODE);
    exit;
}

function  api_ajaxReturn($code = "", $msg = "", $data = null)
{
    if (session('token')) {
        $code=999;
    }
    return json_encode(array('code'=>$code,'message'=>$msg,'result'=>$data));
}

/**
 * User: 聂风 Function:生成token
 * @return string
 */
function create_token()
{
    return md5(time().mt_rand(1,999999999));
}

/**
 * User: 聂风 Function:处理图片
 * @param $url 地址
 * @param bool $priex 是否添加网址头
 * @return string
 */
function image_handle($url, $priex = true)
{
    if(empty($url)){
        return $url;
    }
    if(preg_match('/^(?:http[s]?:)?\/\//i',$url)){
        return $url;
    }
    if($priex){
        return SITE_URL . $url;
    }
    return $url;
}

/**
 * User: 聂风 Function: 跳转第三方时的拼接
 * @param $url
 */
function jump_stitch($url)
{
    if(empty($url)){
        return $url;
    }
    $arr = [];
    $userId = session('user')['user_id'];
    $token = model('common/UserToken')->where('user_id', $userId)->value('token');
    $arr['token'] = $token;
    $shareId = cookie('shareId');
    if($shareId){
        $arr['shareId'] = base64_encode($shareId);
    }
    $param = http_build_query($arr);

    if(strpos($url, '?') === false){
        $url = $url.'?'.$param;
    }else{
        $url = $url.'&'.$param;
    }
    return $url;
}
/**
 * User: 聂风 Function: app跳转第三方时的拼接
 * @param $url
 */
function jump_stitch_app($url)
{
    if(empty($url)){
        return $url;
    }
    $arr = [];
    $arr['token'] = trim(I('loginToken'));
    $shareId = cookie('shareId');
    if($shareId){
        $arr['shareId'] = base64_encode($shareId);
    }
    $param = http_build_query($arr);

    if(strpos($url, '?') === false){
        $url = $url.'?'.$param;
    }else{
        $url = $url.'&'.$param;
    }
    return $url;
}
/**
 * 判断当前的请求是否是我们的App端口
 * @param string $apiVersion 请求的API版本号。例如:
 * >1.3 表示版本号大于1.3；
 * >=1.3 表示版本号大于或等于1.3；
 * <1.3 表示版本号小于1.3；
 * <=1.3 表示版本号小于或等于1.3；
 * !1.3 表示版本号不等于1.3；
 * 1.3表示版本号等于1.3
 * @param string $client 客户端类型：Android 或 Ios
 */
//$_SERVER['HTTP_USER_AGENT']='Mozilla/5.0 (Linux; Android 6.0; F100S Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36KitNote_APP_Android/1.2.0.test ApiVersion/1.2';
//var_dump(judge_app('1.2','Android'));exit;
function judge_app($apiVersion = '',$client = ''){
    static $_checked=[];
    $key=$apiVersion.'|'.$client;
    if(isset($_checked[$key])) return $_checked[$key];
    if( $client == 'Android' ){

        $client = 'APP_Android_Tookeen';
    }elseif( $client == 'Ios' ){
        $client = 'APP_IOS_Tookeen';
    }
    $versionRight=false;
    if($apiVersion && preg_match('/ApiVersion\/([\d.]+)$/',$_SERVER['HTTP_USER_AGENT'],$match)){
        switch($apiVersion{0}){
            case '>':
            $versionRight=$apiVersion{1}=='='?version_compare($match[1],substr($apiVersion,2),'>='):version_compare($match[1],substr($apiVersion,1),'>');
            break;
            case '<':
            $versionRight=$apiVersion{1}=='='?version_compare($match[1],substr($apiVersion,2),'<='):version_compare($match[1],substr($apiVersion,1),'<');
            break;
            case '!':
            $versionRight=$apiVersion{1}=='='?version_compare($match[1],substr($apiVersion,2),'!='):version_compare($match[1],substr($apiVersion,1),'!=');
            break;
            default:
            $versionRight=version_compare($match[1],$apiVersion,'=');
        }
    }
    if($apiVersion && $client){
        $_checked[$key]=strchr($_SERVER['HTTP_USER_AGENT'], $client) && $versionRight;
    }elseif($client){
        $_checked[$key]=strchr($_SERVER['HTTP_USER_AGENT'], $client);
    }elseif($apiVersion){
        $_checked[$key]=(strchr($_SERVER['HTTP_USER_AGENT'], 'APP_Android_Tookeen') || strchr($_SERVER['HTTP_USER_AGENT'], 'APP_IOS_Tookeen')) && $versionRight;
    }else{
        $_checked[$key]=strchr($_SERVER['HTTP_USER_AGENT'], 'APP_Android_Tookeen') || strchr($_SERVER['HTTP_USER_AGENT'], 'APP_IOS_Tookeen') || strchr($_SERVER['HTTP_USER_AGENT'], 'APP-iOS');
    }
    return $_checked[$key];
}

/**
 * [judge_client 判断app端访问h5端设备类型]
 * @return [type] [description]
 */
function judge_client()
{
    static $client=null;
    if(!is_null($client)) return $client;
    if(strchr($_SERVER['HTTP_USER_AGENT'], 'APP_Android_Tookeen')){
        $client = 'Android';
    }elseif (strchr($_SERVER['HTTP_USER_AGENT'], 'APP_IOS_Tookeen')) {
        $client = 'IOS';
    }else{
        $client = 'Other';
    }
    return $client;
}

/**
 * [func_is_base64 判断是否已经base64]
 * @return [type] [description]
 */
function func_is_base64($str)
{
    return $str == base64_encode(base64_decode($str)) ? true : false;
}
