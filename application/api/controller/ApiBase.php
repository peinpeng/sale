<?php
/**
 * kitnote
 * ============================================================================
 * * 版权所有 2015-2027 株洲清拓科技有限公司，并保留所有权利。
 * 网站地址: http://www.mall.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用TP5助手函数可实现单字母函数M D U等,也可db::name方式,可双向兼容
 * ============================================================================
 * $Author: IT宇宙人 2016-08-10 $
 */
namespace app\api\controller;
use think\Controller;
use think\Session;
use think\Db;
use think\Verify;

define('API_CACHE_TIME', 86400);

class ApiBase extends Controller {
    public $session_id;
    /*
     * 初始化操作
     */
    public function _initialize() {

        Session::start();
        header("Cache-control: private");
        $this->session_id = session_id(); // 当前的 session_id
        define('SESSION_ID',$this->session_id); //将当前的session_id保存为常量，供其它方法调用
        define('CLIENT', I('client')); //客户端类型名
        define('UPUSHREGISTRATIONID',I('upushRegistrationId') ?: ""); //将当前的session_id保存为常量，供其它方法调用
        // 判断当前用户是否手机
        if(isMobile())
            cookie('is_mobile','1',3600);
        else
            cookie('is_mobile','0',3600);
    }

    public function _empty()
    {
      exit(api_response(0, '方法未定义'));
    }

    //图形验证码
    public function verify()
    {
        //验证码类型
        $type = I('get.type') ? I('get.type') : 'user_reg';
        $imageH = I('get.imgCodeH') ? I('get.imgCodeH') : 60;
        $imageW = I('get.imgCodeW') ? I('get.imgCodeW') : 300;
        $imageW=max($imageW,200);
        $sessionId=I('get.sessionId','');
        if($sessionId) session_id($sessionId);

        $config = array(
            'fontSize' => 30,
            'length' => 4,
            'imageH' =>  $imageH,
            'imageW' =>  $imageW,
            'fontttf' => '5.ttf',
            'useCurve' => false,
            'useNoise' => false,
            'codeSet'=>'1234567890',
        );
        $Verify = new Verify($config);
        $Verify->entry($type);
        exit();
    }

}
