<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/20
 * Time: 14:50
 */
namespace app\api\controller;

class Api0 extends Api
{
    protected $beforeActionList = [];

    /**
     * 资讯内容页格式化
     */
    protected function formatForNewsList($article_list){
        // p($article_list);description
        $list = [];
        foreach ($article_list as $k => $v){
            $list[$k]['showType'] = $v['article_type'];
            $list[$k]['showStyle'] = $v['show_type'];
            $list[$k]['isTop'] = $v['is_top'];
            $list[$k]['desc'] = $v['description'];
            $list[$k]['title'] = $v['title'];
            $list[$k]['author'] = $v['author'];
            $list[$k]['newsId'] = $v['article_id'];
            $click = $v['click'] + $v['click_base'];
            $click = $click >= 10000 ? round($click/10000, 1) .'万' : $click;
            $list[$k]['readNum'] = $click;
            // $list[$k]['detailUrl'] = SITE_URL.U('/Mobile/Article/news_detail', ['newsId' => $v['article_id']]);
            $list[$k]['detailUrl'] = U('/mobile/business_college/detail','',true,true) . '?newsId=' . $v['article_id'];
            $keywords_arr = [];
            if ($v['keywords']) {
                $keywords = explode(',', $v['keywords']);
                foreach ($keywords as $key => $value) {
                    // $keywords_arr[$key]['name'] = $value;
                    $keywords_arr[$key] = $value;
                }
            }
            $list[$k]['tagList'] = $keywords_arr;
            $thumb_arr = [];
            if ($v['thumb']) {
                $thumb = explode(',', $v['thumb']);
                foreach ($thumb as $key => $value) {
                    if(empty($value)){
                        continue;
                    }
                    // $thumb_arr[$key]['imgUrl'] = SITE_URL.$value;
                    $thumb_arr[$key] = SITE_URL.$value;
                }
            }
            $list[$k]['coverList'] = $thumb_arr;
            $list[$k]['fileUrl'] = $v['file_url'] ? SITE_URL.$v['file_url'] : '';
        }
        return $list;
    }

    /**
     * User: 聂风 Function:文章详情格式化
     * @param $article
     * @return array
     */
    protected function newsDetailFormat($article)
    {
        $data = [];
        $data['newsId'] = $article['article_id'];
        $data['catId'] = $article['cat_id'];
        $data['title'] = $article['title'];
        $data['author'] = $article['author'];
        $data['type'] = $article['article_type'];
        $data['fileUrl'] = $article['file_url'] ? SITE_URL.$article['file_url'] : '';
        $data['desc'] = $article['description'];
        $data['tagList'] = $article['keywords'] ? explode(',', $article['keywords']) : [];
        $thumb_arr = [];
        if ($article['thumb']) {
            $thumb = explode(',', $article['thumb']);
            foreach ($thumb as $key => $value) {
                if(empty($value)){
                    continue;
                }
                $thumb_arr[$key] = SITE_URL.$value;
            }
        }

        $data['coverList'] = $thumb_arr;
        $click = $article['click'] + $article['click_base'];
        $data['readNum'] = $click >= 10000 ? round($click/10000, 1) . '万' : $click ;
        $data['addTime'] = date('Y-m-d H:i:s', $article['add_time']);
        $data['content'] = htmlspecialchars_decode($article['content']);
        return $data;
    }

    //首页数据格式化
    protected function saleIndexFormat($noticesTemp, $levelListTemp, $navList)
    {
        //轮播
        // $bannerList = [];
        // foreach ($adListTemp as $k => $v) {
        //     $bannerList[] = [
        //         'adName' => $v['ad_name'],
        //         'images' => image_handle($v['ad_code']),
        //         'bgcolor' => $v['bgcolor'],
        //         'link'=> $v['ad_link'],
        //     ];
        // }
        //公告
        $notices = [];
        foreach ($noticesTemp as $k => $v) {
            $notices[] = [
                'title' => $v['title'],
                'link'=> url('mobile/Announcement/detail', ['id'=>$v['article_id']], true, true),
            ];
        }
        //等级列表
        $levelList = [];
        foreach ($levelListTemp as $k => $v) {
            $levelList[] = [
                'name' => $v['show_name'],
                'icon' => image_handle($v['level_icon']),
            ];
        }

        return [
            // 'bannerList'=>$bannerList,
            'noticesList'=>$notices,
            'levelList'=>$levelList,
            'navList' => $navList,
        ];
    }
    //首页数据格式化
    protected function saleIndexFormatBannerList($adListTemp)
    {
        //轮播
        $bannerList = [];
        foreach ($adListTemp as $k => $v) {
            $bannerList[] = [
                'adName' => $v['ad_name'],
                'images' => image_handle($v['ad_code']),
                'bgcolor' => $v['bgcolor'],
                'link'=> $v['ad_link'],
            ];
        }
        return $bannerList;
    }

    //格式化首页用户数据
    protected function saleIndexUserDataFormat($userData, $slippingUserTemp)
    {
        $userData['connecterMobile'] = $slippingUserTemp['connecter_mobile']?$slippingUserTemp['connecter_mobile']:'';
        $userData['des'] = $slippingUserTemp?'滑落':'';
        return [
            'userData' => $userData,
        ];
    }

}