<?php

// +----------------------------------------------------------------------
// | Copyright (c) 2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | User: Simony <1362229026@qq.com>
// +----------------------------------------------------------------------

namespace app\api\controller;

use app\common\logic\BackUserLogic;
use app\common\logic\GiveGiftLogic;
use app\common\logic\HallLogic;
use app\common\logic\UserHashLogic;
use app\common\logic\UserLogic;
use app\common\logic\UtilLogic;
use app\common\logic\UsersLogic;
use app\common\model\UserToken;
use think\Cache;

class Api100 extends Api0
{
    /**
     * @api {POST} api/api/userReg 用户注册
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} phone 手机号
     * @apiParam {String} password 密码
     * @apiParam {String} invitePhone 推荐人手机号
     * @apiParam {String} paytypeZfb 支付宝
     * @apiParam {String} paytypeWx 微信
     * @apiParam {string} source 非必需，注册来源
     * @apiParam {int} regType 非必需，注册方式，1正常注册2为他人注册
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} msg  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.userId  用户id
     * @apiSuccess {String} result.sessionId
     * @apiSuccess {String} result.loginToken  用户的token
     * @apiSuccess {String} result.activity_page_url  用户注册后跳转活动页的地址
     */
    public function userReg()
    {
        //api_response(10001, '系统正在升级中，请稍候重试~');

        $url = cookie('activity_page_url');
        $result = (new UserLogic())->userReg(request()->post());
        $result['activity_page_url'] = '';
        if($url) $result['activity_page_url'] = $url;
        //第三方s
        model('SupplyLogic','logic\supply')->loginRedirect($result['userId']);
        //第三方e
        api_response(10000, '注册成功', $result);
    }

    /**
     * @api {POST} api/api/userLogin 用户登录
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} phone 手机号
     * @apiParam {String} password 密码
     * @apiParam {String} loginType 用户登录类型 1 短信验证码注册登录 2 账号密码登录 
     * @apiParam {String} mobileCode 短信验证码
     * @apiParam {String} sessionId sessionId
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.userId  用户id
     * @apiSuccess {String} result.sessionId
     * @apiSuccess {String} result.loginToken  用户的token
     */
    public function userLogin()
    {
        $phone = input('phone');
        $password = input('password');
        $verifyCode = input('verifyCode', null);
        $loginType = input('loginType',2);
        $mobileCode = input('mobileCode');
        $sessionId = input('sessionId');
        // api_response(10000, $loginType, $mobileCode);

        $result = (new UserLogic())->userLogin($phone, $password, $verifyCode, $loginType, $mobileCode,$sessionId);
        api_response(10000, '登录成功', $result);
    }

    /**
     * @api {POST} api/api/temporaryBindForMobile 不注册 临时锁定第一个推荐人
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} phone 手机号
     * @apiParam {String} firstLeaderMobile 推荐人手机号
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     */
    public function temporaryBindForMobile()
    {
        $phone = input('phone');
        // $firstLeaderMobile = input('firstLeaderMobile');
        $tk_id = cookie('shareId');
        // $result = model('common/place/PlaceTemporaryLocking')->temporaryBindForMobile($firstLeaderMobile,$phone,$tk_id);
        $result = model('common/place/PlaceTemporaryLocking')->temporaryBindForMobile($phone,$tk_id);
        api_response($result['code'], $result['msg'], $result['data']);
    }
    


    /**
     * @api {GET} api/api/imgCode 图形验证码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {int} scene 1注册2登录密码3支付密码4修改绑定手机号5修改备用手机号6忘记密码7用户登录身份验证
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {String} result.sessionId
     * @apiSuccess {String} result.imgCodeUrl  图形验证码地址
     */
    public function imgCode()
    {
        $scene = trim(I('scene', 1, 'intval'));// 1 注册

        switch ($scene) {
            case 1://注册
                $scene_code = 'user_reg';
                break;
            case 2://登录密码
                $scene_code = 'change_pas';
                break;
            case 3://支付密码
                $scene_code = 'change_pay';
                break;
            case 4://修改绑定手机号
                $scene_code = 'change_mobile';
                break;
            case 5://修改备用手机号
                $scene_code = 'change_mobile_bak';
                break;
            case 6://忘记密码
                $scene_code = 'forget_pwd';
                break;
            case 7://用户登录身份验证
                $scene_code = 'user_login';
                break;
            default:
                $scene_code = 'default';
                break;
        }
        //传回 如果用了session 就得把session 传过去
        $str = SITE_URL . "/api/api/verify?type=" . $scene_code . "&sessionId=" . session_id() . "&time=" . time();
        $data = [
            'sessionId' => session_id(),
            'imgCodeUrl' => $str,
        ];
        api_response(10000,'请求成功',$data);
    }

    /**
     * @api {GET} api/api/cityAreas 城市地区数据
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     * @apiSuccess {String} result.list.province  省名称
     * @apiSuccess {int} result.list.provinceId  省id
     * @apiSuccess {array} result.list.list  城市数据集
     * @apiSuccess {String} result.list.list.city  城市名
     * @apiSuccess {int} result.list.list.cityId  城市ID
     * @apiSuccess {array} result.list.list.areaList  地区数据集
     * @apiSuccess {String} result.list.list.areaList.area  地区名
     * @apiSuccess {int} result.list.list.areaList.areaId  地区ID
     */
    public function cityAreas()
    {
        $result = (new UtilLogic())->cityAreas();
        api_response($result['code'], $result['message'], $result['result']);
    }

    /**
     * @api {POST} api/api/tokenGetSessionId 获取sessionId
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.userId  用户id
     * @apiSuccess {String} result.sessionId  session_id
     */
    public function tokenGetSessionId()
    {
        $userId = $this->getUserId();
        $sessionId = (new UserToken())->getSessionId($userId);
        api_response(10000, '请求成功', ['userId'=>$userId,'sessionId'=>$sessionId]);
    }

    /**
     * @api {POST} api/api/newsList 拓客商学院文章列表
     * @apiVersion 1.0.0
     * @apiGroup article
     *
     * @apiParam {int} sortId 类型id，10000精选，10001音频，10002视频，6新手必看，7操作指南，8高手进阶，11成功案例，12常见问题
     * @apiParam {int} currPage 页码
     * @apiParam {int} pageSize 每页条数
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.currPage  页码
     * @apiSuccess {int} result.totalCount  总条数
     * @apiSuccess {int} result.totalPage  总页数
     * @apiSuccess {Object} result.list  数据集
     * @apiSuccess {int} result.list.showType  类型1 视频 2 图文 3音频
     * @apiSuccess {int} result.list.showStyle  1 单图展示 2 三图展示
     * @apiSuccess {int} result.list.isTop  是否置顶1是0否
     * @apiSuccess {String} result.list.desc  文章摘要
     * @apiSuccess {String} result.list.title  标题
     * @apiSuccess {String} result.list.author  作者
     * @apiSuccess {String} result.list.newsId  文章id
     * @apiSuccess {String} result.list.readNum  阅读数
     * @apiSuccess {Object} result.list.tagList  关键字数据集
     * @apiSuccess {Object} result.list.coverList  缩略图数据集
     * @apiSuccess {String} result.list.fileUrl  附件地址
     * @apiSuccess {String} result.list.detailUrl  详情地址
     */
    public function newsList() {
        $cat_id = intval(input('sortId', 0));//分类ID
        $cat_id = $cat_id ? $cat_id : 10000;
        $media_id  = intval(input('mediaId', 0));//音频或视频分类
        $curr_page = intval(input('currPage', 1));//当前页码
        $page_size = intval(input('pageSize', 10));//每页几条
        $data['currPage'] = $curr_page;
        $data['list'] = [];
        // 获取推荐资讯
        switch ($cat_id) {
            case 10000:
                $cat_name = '拓客商学院';
                $cat_id = M('ArticleCat')->where(['cat_name' => $cat_name, 'show_in_nav' => 1])->value('cat_id');
                // $cat_id = 1;//拓客商学院
                // 获取拓客商学院所有子类
                $sub_cats = M('ArticleCat')->where("parent_id = $cat_id")->column('cat_id');
                $map['is_recommend'] = 1;
                $map['is_open'] = 1;
                $map['cat_id'] = array('in', $sub_cats);
                $count = M('Article')->where($map)->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where($map)->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            case 10001:
                $count = M('Article')->where(array('article_type' => 3,'is_open' => 1))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['article_type' => 3, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            case 10002:
                $count = M('Article')->where(array('article_type' => 1,'is_open' => 1))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['article_type' => 1, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            default:
                $count = M('Article')->where(array('cat_id' => $cat_id))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['cat_id' => $cat_id, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
        }
        if (empty($data['list'])) {
            $code = 1;
            $msg = '没有更多了';
        } else {
            $code = 10000;
            $msg = '成功';
        }
        api_response($code,$msg,$data);
    }

    /**
     * @api {POST} api/api/newsDetail 拓客商学院文章详情
     * @apiVersion 1.0.0
     * @apiGroup article
     *
     * @apiParam {int} newsId 文章id
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.type  类型1 视频 2 图文 3音频
     * @apiSuccess {String} result.desc  文章摘要
     * @apiSuccess {String} result.title  标题
     * @apiSuccess {String} result.author  作者
     * @apiSuccess {String} result.newsId  文章id
     * @apiSuccess {String} result.readNum  阅读数
     * @apiSuccess {Object} result.tagList  关键字数据集
     * @apiSuccess {Object} result.coverList  缩略图数据集
     * @apiSuccess {String} result.fileUrl  附件地址
     * @apiSuccess {String} result.addTime  添加时间
     * @apiSuccess {String} result.content  文章内容
     * @apiSuccess {String} result.catId  分类id
     */
    public function newsDetail()
    {
        $article_id = intval(input('newsId', ''));//文章ID
        $article = M('Article')->where(['article_id' => $article_id])->field('article_id, cat_id, title, content, author, keywords, article_type, add_time, file_url, description, click, click_base, thumb')->find();
        $data = $this->newsDetailFormat($article);

        // 浏览次数+1
        M('Article')->where(['article_id' => $article_id])->setInc('click');
        api_response(10000,'请求成功',$data);
    }

    /**
     * @api {POST} api/api/smsCode 获取短信验证码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} phone  手机号
     * @apiParam {String} scene  场景，1注册,2修改登录密码，3修改支付密码，4修改绑定手机号码，5修改备用手机号，6忘记密码 7用户短信注册
     * @apiParam {String} sessionId  非必需，图形验证码的会话ID
     * @apiParam {String} verifyCode  非必需，图形验证码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {String} result.code  短信验证码
     * @apiSuccess {String} result.smsTimeOut  短信验证码过期时间
     */
    public function smsCode()
    {
        $scene = trim(I('scene')); //1:注册，
        // $scene = 7; //1:注册，
        $veriry_code = I("verifyCode"); //图形验证码
        $mobile = I('phone'); //手机号
        $type = I('type') ?: 1; //1.手机 2.邮箱
        // $province = I('province',''); 
        $session_id = I('sessionId', session_id());
        session("scene", $scene);
        // p($province);
        if (!$mobile) {
            api_response(0, '参数有误');
        }
        // if (!$province) {
        //     api_response(0, '参数有误');
        // }
        $scene_code = '';
        //是否验证图形验证码
        $openVeriryCode = false;
        switch ($scene) {
            case 1: //注册
                $scene_code = 'user_reg';
                $openVeriryCode = true;
                break;
            case 2: //修改登录密码
                $scene_code = 'change_pas';
                $openVeriryCode = true;
                break;
            case 3: //修改支付密码
                $scene_code = 'change_pay';
                break;
            case 4: //修改绑定手机号
                $scene_code = 'change_mobile';
                $openVeriryCode = true;
                break;
            case 5: //修改备用手机号
                $scene_code = 'change_mobile_bak';
                $openVeriryCode = true;
            case 6: //忘记密码
                $scene_code = 'forget_pwd';
                $openVeriryCode = true;
                break;
            case 7: //注册身份验证
                $scene_code = 'user_login';
                break;
            /*case 5: //找回密码
                $scene_code = 'find_pwd';
                break;
            case 6: //编辑支付账号
                $scene_code = 'edit_account';
                break;*/
            default:
                $scene_code = 'default';
                break;
        }
        $apiLogic = model('common/ApiLogic', 'logic');
        //验证图形验证码
        if ($openVeriryCode) {
            if (!$apiLogic->verifyHandle($scene_code)) {
                api_response(-1, '图像验证码错误');
            }
        }
        if ($scene_code == 'user_reg' || $scene_code == 'change_mobile' || $scene_code == 'user_login') {
            //判断用户是否已注册
            $result = $this->checkUserByMobile($mobile);
            if ($result['status'] != 1) {
                echo api_ajaxReturn($result['status'], $result['info']);
                exit;
            }
        }else if ($scene_code == 'change_pas' || $scene_code == 'change_pay') {
            $result = $this->checkUserByMobile($mobile);
            if ($result['status'] != 1003) {
                echo api_ajaxReturn($result['status'], $result['info']);
                exit;
            }
        }
        //发送短信验证码
        $res = checkEnableSendSms($scene);
        if ($res['status'] != 1) {
            echo api_ajaxReturn(0, $res['msg']);
            exit;
        }
        //判断是否存在验证码
        $data = M('sms_log')->where(array('mobile' => $mobile, 'session_id' => $session_id, 'status' => 1))->order('id DESC')->find();
        //获取时间配置
        $sms_time_out = tpCache('sms.sms_time_out');
        $sms_time_out = $sms_time_out ? $sms_time_out : 120;

        //120秒以内不可重复发送
        if ($data && (time() - $data['add_time']) < $sms_time_out) {
            exit(api_ajaxReturn(1004, $sms_time_out . '秒内不允许重复发送'));
        }

        //随机一个验证码
        $code = rand(100000, 999999);
        $parameters['code'] = $code;
        $parameters['province'] = $province;
        if (!I('test')) {
            //发送短信
            $resp = sendSms($scene, $mobile, $parameters, $session_id);
        } else {
            //模拟手机验证
            $resp = $apiLogic->sendSmsTest($scene, $mobile, $code, $session_id);
        }
        if ($resp['status'] == 1) {
            //发送成功, 修改发送状态位成功
            M('sms_log')->where(array('mobile' => $mobile, 'code' => $code, 'session_id' => $session_id, 'status' => 0))->save(array('status' => 1));
            $return_arr = array('code' => 10000, 'msg' => '发送成功,请注意查收', 'data' => $code);
        } else {
            $msg = stristr($resp['msg'],'触发天级流控') == false?'发送失败' . $resp['msg']:'发送失败,该手机号当天获取短信验证码次数到达上限！';
            $return_arr = array('code' => -1, 'msg' => $msg, 'data' => array('code' => $code, 'sessionId' => $session_id));
        }
        api_response($return_arr['code'], $return_arr['msg'], ['code' => '', 'smsTimeOut' => (int)$sms_time_out]);
    }

    /**
     * @api {POST} api/api/changePassword 修改登录密码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {String} phone  手机号
     * @apiParam {String} code  短信验证码
     * @apiParam {String} password  密码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function changePassword()
    {
        $userId     = $this->getUserId();
        $phone       = input('phone');
        $code       = input('code');
        $password   = input('password');
        $sessionId   = input('sessionId', session_id());
        $client       = input('client');

        $result = (new UserLogic())->changePassword($userId, $phone, $code, $password, $sessionId, $client);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/changePayPassword 修改支付密码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {String} phone  手机号
     * @apiParam {String} code  短信验证码
     * @apiParam {String} password  密码
     * @apiParam {String} passwordConfirm  第二次密码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function changePayPassword()
    {
        $userId     = $this->getUserId();
        $phone       = input('phone');
        $code       = input('code');
        $password   = input('password');
        $passwordConfirm   = input('passwordConfirm');
        $sessionId   = input('sessionId', session_id());
        $client       = input('client');

        $result = (new UserLogic())->changePayPassword($userId, $phone, $code, $password, $passwordConfirm, $sessionId, $client);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/changePayPasswordForMall 总商城后台修改支付密码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} userId  用户id
     * @apiParam {String} phone  手机号
     * @apiParam {String} password  密码
     * @apiParam {String} passwordConfirm  第二次密码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function changePayPasswordForMall()
    {
        $userId     = input('userId');
        $phone      = input('phone');
        $password   = input('password');
        $passwordConfirm   = input('passwordConfirm');

        $result = (new UserLogic())->changePayPasswordForMall($userId, $phone, $password, $passwordConfirm);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/bindAccount 绑定支付宝或微信
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {int} type  类型 1支付宝 2微信
     * @apiParam {String} account  账号
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function bindAccount()
    {
        $userId     = $this->getUserId();

        $paytypeModel = model('common/Paytype');
        $type = I('type',2,'intval');
        $account = I('post.account');
        if(empty($account)){
            api_response(2, '账号不能为空');
        }

        $res = $paytypeModel->addOrSave($userId, $type, ['account' => $account, 'type' => $type]);
        if (!$res) {
            api_response(2, '绑定失败');
        }
        api_response(10000, '绑定成功');
    }

    /**
     * @api {POST} api/api/changeMobile 修改绑定手机号，修改备用手机
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {int} type  类型 1修改绑定手机号，2修改备用手机
     * @apiParam {String} phone  手机号码
     * @apiParam {String} code  短信验证码
     * @apiParam {String} password  密码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function changeMobile()
    {
        $userId     = $this->getUserId();
        $phone       = input('phone');
        $code       = input('code');
        $password   = input('password');
        $sessionId   = input('sessionId', session_id());
        $client       = input('client');
        $type       = input('type', 1);

        $result = (new UserLogic())->changeMobile($userId, $phone, $code, $password, $sessionId, $client, $type);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/userInfo 用户资料
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.userId  用户id
     * @apiSuccess {String} result.mobile  手机号码
     * @apiSuccess {String} result.mobileBak  备用号码
     * @apiSuccess {String} result.email  邮箱
     * @apiSuccess {int} result.sex  性别0 保密 1 男 2 女
     * @apiSuccess {String} result.birthday  出生日期
     * @apiSuccess {String} result.userMoney  用户金额
     * @apiSuccess {String} result.frozenMoney  冻结金额
     * @apiSuccess {String} result.headPic  头像url
     * @apiSuccess {int} result.level  等级
     * @apiSuccess {String} result.levelName  等级名称
     * @apiSuccess {int} result.ztjNum  我的贡献人数
     * @apiSuccess {int} result.teamNum  行会信息人数
     * @apiSuccess {String} result.nickname  昵称
     * @apiSuccess {String} result.firstLeaderMobile  推荐人手机号码
     * @apiSuccess {String} result.realStatus  实名认证状态1未验证2待审核3验证不通过4实名认证成功
     * @apiSuccess {String} result.trueName  真实姓名
     * @apiSuccess {String} result.cardNumber  身份证号码
     * @apiSuccess {String} result.cardNumberStar  打星的身份证号码
     * @apiSuccess {String} result.cardFrontPhoto  身份证正面照片
     * @apiSuccess {String} result.cardBackPhoto  身份证反面照片
     * @apiSuccess {String} result.zfbAccount  支付宝账号
     * @apiSuccess {String} result.wxAccount  微信账号
     * @apiSuccess {string} result.userContribution 我的贡献
     * @apiSuccess {string} result.teamContribution 团队贡献
     * @apiSuccess {string} result.compensateNum 可分配流量
     * @apiSuccess {string} result.connecterMobile 接点人
     * @apiSuccess {string} result.temporaryLockingNum 锁定人数
     */
    public function userInfo()
    {
        $userId     = $this->getUserId();

        $result = (new UserLogic())->userInfo($userId);
        api_response(10000, '请求成功', $result);
    }

    /**
     * @api {POST} api/api/realName 实名认证
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {String} trueName  姓名
     * @apiParam {String} cardNumber 身份证号码
     * @apiParam {String} cardFrontPhoto  身份证正面图片地址
     * @apiParam {String} cardBackPhoto  身份证反面图片地址
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function realName()
    {
        $userId = $this->getUserId();

        $result = (new UserLogic())->realName($userId, request()->post());
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/editUser 修改个人资料
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     * @apiParam {String} nickname  非必须，昵称
     * @apiParam {String} headPic  非必须，头像图片链接
     * @apiParam {int} sex  非必须，性别，1男2女
     * @apiParam {String} birthday  非必须，出生日期
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function editUser()
    {
        $userId = $this->getUserId();

        $post = [];
        I('post.nickname') ? $post['nickname'] = I('post.nickname') : false; //昵称
        I('post.name') ? $post['name'] = I('post.name') : false; //真实姓名
        I('post.qq') ? $post['qq'] = I('post.qq') : false;  //QQ号码
        I('post.headPic') ? $post['head_pic'] = I('post.headPic') : false; //头像地址
        I('post.sex') ? $post['sex'] = I('post.sex') : false;  // 性别
        I('post.birthday') ? $post['birthday'] = I('post.birthday', 0, 'strtotime') : false;  // 生日
        I('post.province') ? $post['province'] = I('post.province') : false;  //省份
        I('post.city') ? $post['city'] = I('post.city') : false;  // 城市
        I('post.district') ? $post['district'] = I('post.district') : false;  //地区
        I('post.email') ? $post['email'] = I('post.email') : false; //邮箱
        I('post.mobile') ? $post['mobile'] = I('post.mobile') : false; //手机

        $result = (new UserLogic())->editUser($userId, $post, $_FILES['headPic']);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/selectUser 手机号码搜索
     * @apiVersion 1.0.0
     * @apiGroup admin
     *
     * @apiParam {String} mobile  非必需，手机号码
     * @apiParam {String} userId  非必需，用户id，多个用英文,分割
     * @apiParam {String} isAccurate  是否精确搜索，0否1是，默认值为0
     * @apiParam {int} currPage  页码，传0时返回所有记录
     * @apiParam {int} pageSize  条数
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.totalCount 总条数
     * @apiSuccess {int} result.totalPage 总页数
     * @apiSuccess {int} result.currPage 当前页
     * @apiSuccess {Object} result.list 列表数据集
     * @apiSuccess {int} result.list.userId  用户id
     * @apiSuccess {String} result.list.trueName  真实姓名
     * @apiSuccess {String} result.list.mobile  手机号
     * @apiSuccess {String} result.list.nickname  昵称
     * @apiSuccess {String} result.list.levelName  会员等级名称
     * @apiSuccess {String} result.list.firstLeaderMobile  上级手机号码
     * @apiSuccess {int} result.list.underlingNumber  下级人数
     * @apiSuccess {int} result.list.loginNum  登录次数
     * @apiSuccess {String} result.list.lastLogin  最后登录时间
     * @apiSuccess {String} result.list.regTime  注册时间
     * @apiSuccess {String} result.list.headPic  头像
     * @apiSuccess {int} result.list.isLock  是否允许登录0允许1不允许
     */
    public function selectUser()
    {
        $currPage = intval(input('currPage', 0));
        $pageSize = input('pageSize', 20);
        $mobile   = input('mobile', '');
        $userId   = input('userId', '');
        $isAccurate   = input('isAccurate',0);
        $userIdArr = $userId?explode(',', $userId):[];

        $result = (new BackUserLogic())->selectUser($mobile, $userIdArr, $currPage, $pageSize, $isAccurate);
        echo $result; exit;
    }

        /**
     * @api {POST} api/api/selectUserByNickname 昵称搜索
     * @apiVersion 1.0.0
     * @apiGroup admin
     *
     * @apiParam {String} nickname  用户昵称
     * @apiParam {String} isAccurate  是否精确搜索，0否1是，默认值为0
     * @apiParam {int} currPage  页码，传0时返回所有记录
     * @apiParam {int} pageSize  条数
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {int} result.totalCount 总条数
     * @apiSuccess {int} result.totalPage 总页数
     */
    public function selectUserByNickname()
    {
        $currPage = intval(input('currPage', 0));
        $pageSize = input('pageSize', 20);
        $nickname   = input('nickname');
        $isAccurate   = input('isAccurate',0);

        $result = (new BackUserLogic())->selectUserByNickname($nickname, $currPage, $pageSize, $isAccurate);
        echo $result; exit;
    }


    /**
     * @api {POST} api/api/parentList 获取用户所有上级
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {int} userId  用户id
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list 上级数组
     */
    public function parentList()
    {
        $userId = input('userId');

        $list = model('common/distribut/DistributTeam')->getFirstLeaderTeam($userId);
        api_response(10000, '请求成功', ['list'=>$list]);
    }

    /**
     * @api {GET} api/api/bottonNav 底部导航
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list 上级数组
     * @apiSuccess {String} result.list.name 中文名称
     * @apiSuccess {String} result.list.url H5跳转地址
     * @apiSuccess {String} result.list.image 导航图片选中
     * @apiSuccess {String} result.list.imageUnselect 导航未选中图片
     * @apiSuccess {String} result.list.nameEnglish 英文名称
     */
    public function bottonNav()
    {
        $list = model('common/nav/Navigation')->getNavigationByPosition(2);
        $result = model('common/MobileLogic', 'logic')->navFormat($list);
        api_response(10000, '请求成功', ['list'=>$result]);
    }

    /**
     * @api {GET} api/api/saleIndex 首页
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.bannerList 轮播数据集
     * @apiSuccess {string} result.bannerList.adName 轮播名称
     * @apiSuccess {string} result.bannerList.images 轮播图片
     * @apiSuccess {string} result.bannerList.bgcolor 轮播背景色
     * @apiSuccess {string} result.bannerList.link 链接
     * @apiSuccess {array} result.noticesList 公告数据集
     * @apiSuccess {string} result.noticesList.title 公告标题
     * @apiSuccess {string} result.noticesList.link 公告详情链接
     * @apiSuccess {array} result.levelList 等级数据集
     * @apiSuccess {string} result.levelList.name 等级名称
     * @apiSuccess {string} result.levelList.icon 等级图标
     * @apiSuccess {array} result.navList 导航
     * @apiSuccess {string} result.navList.name 中文名称
     * @apiSuccess {string} result.navList.url H5跳转地址
     * @apiSuccess {string} result.navList.image 导航图片选中
     * @apiSuccess {string} result.navList.imageUnselect 导航未选中图片 
     * @apiSuccess {string} result.navList.nameEnglish 英文名称
     * @apiSuccess {array} result.bgList 背景图
     * @apiSuccess {string} result.bgList.bgImgUrl 首页背景图
     * @apiSuccess {string} result.bgList.bgColor 颜色(暂时为空)
     * @apiSuccess {array} result.userData 用户数据集
     * @apiSuccess {string} result.userData.headPic 头像
     * @apiSuccess {string} result.userData.levelName 等级名
     * @apiSuccess {string} result.userData.directUserMobile 推荐人
     * @apiSuccess {int} result.userData.userId 用户id
     * @apiSuccess {string} result.userData.nickName 昵称
     * @apiSuccess {int} result.userData.creditPoint 信用分
     * @apiSuccess {int} result.userData.userContribution 我的贡献
     * @apiSuccess {int} result.userData.teamContribution 团队贡献
     * @apiSuccess {int} result.userData.compensateNum 可分配流量
     * @apiSuccess {string} result.userData.connecterMobile 接点人
     * @apiSuccess {string} result.userData.publicQrCode 公众号二维码
     * @apiSuccess {string} result.userData.des 描述
     */
    public function saleIndex()
    {
        $userId = $this->getUserId();

        $cacheKey = 'apiIndexCache';
        $mobileIndexCache = Cache::get($cacheKey);

        if(!empty($mobileIndexCache) && empty(I('clearCache'))) {
            $mobileIndexCache = unserialize($mobileIndexCache);
        }else{
          
            $articleModel = new \app\common\model\Article();
            $cat_ids =  M('ArticleCat')->where(['parent_id'=>1])->column('cat_id'); //获取公告分类下的子分类ids
            $cond = [
                // 'cat_id' => 1, //cat_id=1 //是公告
                'is_open' => 1,
            ];
            $cat_ids && $cond['cat_id'] = ['in',$cat_ids];

            $notices = $articleModel->getList($cond, [], false, [], 'add_time desc', 5); //最新的公告图片放到首页

            $levelList = model('Common/users/UserLevel')->getList([],[],false,[],'level_id desc');

            //首页导航
            $navList = model('common/nav/Navigation')->getNavigationByPosition(11);
            $navList = model('common/MobileLogic', 'logic')->navFormat($navList);

            //背景图
            $bgList =  model('Common/MobileLogic','logic')->getBackground(2);
            $data = $this->saleIndexFormat($notices, $levelList, $navList);
            $data['bgList'] = $bgList;
            $mobileIndexCache = $data;

            Cache::set($cacheKey, serialize($data),300);
        }
          $adList = model('common/nav/Navigation')->getAdLinkWithToken(); //轮播广告 pid是1
          $mobileIndexCache['bannerList'] = $this->saleIndexFormatBannerList($adList);

        //用户信息模块
        $slippingUser = model('Common/slippingUsers')->where(['slipping_id'=>$userId])->find();
        if($slippingUser){
            $referrer     = model('Common/Users')->getBy('user_id',$slippingUser['referrer_id']);
            $connecter    = model('Common/Users')->getBy('user_id',$slippingUser['connecter_id']);

            $slippingUser['referrer_mobile']  = $referrer  && !empty($referrer['nickname'])?$referrer['nickname']:$referrer['mobile'];
            $slippingUser['connecter_mobile'] = $connecter && !empty($connecter['nickname'])?$connecter['nickname']:$connecter['mobile'];
        }

        $user = M('users')->where('user_id', $userId)->find();
        $userData['headPic'] = image_handle($user['head_pic']);
        $userData['publicQrCode'] = model('common/WechatOfficialAccount')->getWechatImage();
        $userData['levelName'] = model('Common/users/UserLevel')->getBy('level_id',$user['level'],'show_name');
        $direcUser = model('Common/Users')->getBy('user_id',$user['first_leader']);

        $userData['directUserMobile'] = !empty($direcUser['nickname'])?$direcUser['nickname']:(isset($direcUser['mobile'])?$direcUser['mobile']:'');
        $userData['userId'] = $userId;
        $userData['nickName'] = $user['nickname']?$user['nickname']:$user['mobile'];
        $userData['creditPoint'] = (int)model('Common/credit/CreditUser')->getBy('user_id',$userId,'total_point');
        $userData['userContribution'] = (int)model('Common/Users')->getFisrtLeaderNumAttr($userId,0);
        $userData['teamContribution'] = (int)model('Common/distribut/DistributTeam')->teamNum($userId);
        $userData['compensateNum'] = (int)model('Common/place/PlaceWaitingMatching')->getBy('user_id',$userId,'compensate_num');

        $userData = $this->saleIndexUserDataFormat($userData, $slippingUser);

        $result = $mobileIndexCache + $userData;

        api_response(10000, '请求成功', $result);
    }
    

     /**
     * @api {POST} api/api/getBackground 获取背景图
     * @apiVersion 1.0.0
     * @apiGroup user
     * 
     * @apiParam {int} scene  场景 1登录页背景图
     * 
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {String} result.bgImgUrl  背景图
     * @apiSuccess {String} result.bgColor  背景颜色
     */
    public function getBackground()
    {
        $scene = I('scene');
        $result = model('Common/MobileLogic','logic')->getBackground($scene);
        api_response(10000, '请求成功', $result);
    }
    /**
     * @api {GET} api/api/shareCode 分享拓客
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list 上级数组
     * @apiSuccess {String} result.list.name 名称
     * @apiSuccess {String} result.list.images 背景图片地址
     * @apiSuccess {String} result.list.contact 公司联系方式
     * @apiSuccess {String} result.list.mobile 推荐人手机号码，为空时前端不显示该项
     * @apiSuccess {String} result.list.wxAccount 推荐人微信号，为空时前端不显示该项
     * @apiSuccess {String} result.list.inviteCode 邀请码
     * @apiSuccess {String} result.list.qrCodeUrl 二维码内容
     */
    public function shareCode()
    {
        $userId = $this->getUserId();

        $result = (new UserLogic())->shareCode($userId);
        api_response(10000, '请求成功', ['list'=>$result]);
    }

    /**
     * @api {POST} api/api/logout 退出登录
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function logout()
    {
        $token = input('loginToken');
        if(empty($token)){
            api_response(2, 'token不能为空');
        }

        $res = (new UserToken())->where('token', $token)->delete();
        if(!$res){
            api_response(2, '退出失败');
        }
        session(null);
        api_response(10000, '退出成功');
    }

    /**
     * @api {GET} api/api/newsTypeList 拓客商学院文章导航
     * @apiVersion 1.0.0
     * @apiGroup article
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     * @apiSuccess {int} result.list.sortId  导航id
     * @apiSuccess {string} result.list.title  导航标题
     * @apiSuccess {string} result.list.link  跳转地址
     */
    public function newsTypeList()
    {
        $arr = [
            [
                'cat_id'=> '10000',
                'cat_name' => '精选',
                'link' => '',
            ],
            [
                'cat_id'=> '10001',
                'cat_name' => '音频',
                'link' => '',
            ],
            [
                'cat_id'=> '10002',
                'cat_name' => '视频',
                'link' => '',
            ],
        ];
        $cat_name = '拓客商学院';
        $cat_id = M('ArticleCat')->where(['cat_name' => $cat_name, 'show_in_nav' => 1])->value('cat_id');

        //获取常见问题 成功案例 id
        $cat_name_learn = '学习中心';
        $cat_id_learn  = M('ArticleCat')->where(['cat_name' => $cat_name_learn, 'show_in_nav' => 1])->value('cat_id');

        $data = M('article_cat')->where(['show_in_nav'=>1, 'parent_id'=>['in',[$cat_id,$cat_id_learn]]])->field('cat_id,cat_name,link')->select();
        $data = $arr +$data;
        $result = [];
        foreach ($data as $k => $v) {
            $result[] = [
                'sortId' => $v['cat_id'],
                'title' => $v['cat_name'],
                'link' => $v['link'],
            ];
        }
        api_response(10000, '请求成功',['list'=>$result]);
    }

    /**
     * @api {GET} api/api/teamUserData 行会大厅用户信息
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {Object} result.userData  数据集
     * @apiSuccess {string} result.userData.headPic  头像
     * @apiSuccess {string} result.userData.mobile  手机号
     * @apiSuccess {string} result.userData.levelName  等级名称
     * @apiSuccess {int} result.userData.connecterNum  最多可接点
     * @apiSuccess {int} result.userData.firstLeader  我的贡献
     * @apiSuccess {int} result.userData.teamNum  行会信息
     * @apiSuccess {int} result.userData.aboveNum  第一关及以上人数
     */
    public function teamUserData()
    {
        $userId = $this->getUserId();

        $result = (new HallLogic())->teamUserData($userId);
        api_response(10000, '请求成功',['userData'=>$result]);
    }

    /**
     * @api {GET} api/api/teamSearch 行会大厅列表
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiParam {String} loginToken  token
     * @apiParam {int} currPage  页码
     * @apiParam {int} pageSize  条数
     * @apiParam {int} scene  场景1我的贡献2我的下级
     * @apiParam {String} keywords  搜索的关键词
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     * @apiSuccess {string} result.list.headPic  头像
     * @apiSuccess {string} result.list.mobile  手机号码
     * @apiSuccess {string} result.list.title  标签
     * @apiSuccess {string} result.list.jiedian  接点人，为空时不显示
     * @apiSuccess {string} result.list.tuijian  推荐人，为空时不显示
     * @apiSuccess {string} result.list.levelName  等级名称
     * @apiSuccess {string} result.list.addTime  加入时间
     * @apiSuccess {int} result.list.devote  他的贡献
     */
    public function teamSearch()
    {
        $userId = $this->getUserId();
        $currPage = input('currPage',0);
        $pageSize = input('pageSize', 30);
        $scene = input('scene', 1);
        $keywords = input('keywords', '', 'trim');

        $result = (new HallLogic())->teamSearch($userId, $scene, $keywords, $currPage, $pageSize);
        api_response(10000, '请求成功',['list'=>$result]);
    }

    /**
     * @api {GET} api/api/getUserTeamsAll 获取用户所有的下级
     * @apiVersion 1.0.0
     * @apiGroup admin
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     */
    public function getUserTeamsAll()
    {
        $userId = $this->getUserId();
        $result = model('common/distribut/DistributTeam')->getUserTeamsAll($userId);
        api_response(10000, '请求成功',['list'=>$result]);
    }

    /**
     * @api {GET} api/api/batchesCity 分批获取城市
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {int} parentId  城市id，获取省时为0
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     * @apiSuccess {int} result.list.id  id
     * @apiSuccess {string} result.list.name  名称
     */
    public function batchesCity()
    {
        $parentId = input('parentId', 0);

        $result = (new UtilLogic())->batchesCity($parentId);
        api_response(10000, '请求成功',['list'=>$result]);
    }

    /**
     * @api {POST} api/api/updateOrderStatus 订单审核通过
     * @apiVersion 1.0.0
     * @apiGroup order
     *
     * @apiParam {int} userId  用户id
     * @apiParam {int} collectUserId  店主的用户id
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {null} result  返回数据
     */
    public function updateOrderStatus()
    {
        $userId = input('userId');

        $collectUserId = input('collectUserId');

        $result = (new GiveGiftLogic())->updateOrderStatus($collectUserId, $userId, 1);
        echo $result;exit;
    }

    /**
     * @api {GET} api/api/userData 商城获取用户信息
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  非必需，二选一，token
     * @apiParam {int} userId  非必需，二选一，用户id
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {Object} result.data  数据集
     * @apiSuccess {int} result.data.user_id 用户id
     * @apiSuccess {String} result.data.nickname 昵称
     * @apiSuccess {String} result.data.login_token loginToken
     * @apiSuccess {String} result.data.head_pic 头像
     * @apiSuccess {int} result.data.first_leader 推荐人用户id
     * @apiSuccess {int} result.data.real_status 实名认证状态1未验证2待审核3验证不通过4实名认证成功
     * @apiSuccess {String} result.data.true_name 真实姓名
     * @apiSuccess {int} result.data.sex 性别0 保密 1 男 2 女
     * @apiSuccess {String} result.data.mobile 手机号码
     * @apiSuccess {String} result.data.first_leader_mobile 推荐人手机号码
     * @apiSuccess {String} result.data.levelName 等级名称
     * @apiSuccess {String} result.data.level 等级
     * @apiSuccess {String} result.data.paypwd 支付密码
     */
    public function userData()
    {
        $userId = input('loginToken')?$this->getUserId():input('userId','');
        $mobile = input('mobile', '');
        if(empty($userId) && empty($mobile)){
            api_response(2, '用户信息错误');
        }
        if($mobile){
            $userId = model('Users')->where('mobile', $mobile)->value('user_id');
            if(empty($userId)){
                api_response(2, '用户不存在');
            }
        }

        $result = (new UserHashLogic())->findUser($userId);
        api_response(10000, '请求成功',['data'=>$result]);
    }

    /**
     * @api {POST} api/api/forgetPwd 忘记密码
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} phone  手机号
     * @apiParam {String} code  短信验证码
     * @apiParam {String} password  密码
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {String} result  返回数据
     */
    public function forgetPwd()
    {
        $phone       = input('phone');
        $code       = input('code');
        $password   = input('password');
        // $passwordConfirm   = input('passwordConfirm');
        $passwordConfirm   = $password;
        $sessionId   = input('sessionId', session_id());
        $client       = input('client');
        
        $result = (new UserLogic())->forgetPwd($phone, $code, $password, $passwordConfirm, $sessionId, $client);
        echo $result; exit;
    }

    /**
     * @api {POST} api/api/levelList 等级列表
     * @apiVersion 1.0.0
     * @apiGroup other
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {Object} result  返回数据
     * @apiSuccess {array} result.list  数据集
     * @apiSuccess {int} result.list.levelId  等级id
     * @apiSuccess {string} result.list.showName  等级名称
     * @apiSuccess {string} result.list.amount  等级价格
     * @apiSuccess {string} result.list.levelName  头衔名称
     */
    public function levelList()
    {
        $list = model('common/users/UserLevel')->field('level_id,show_name,gift_amount,level_name')->select();

        $data = [];
        foreach ($list as $k => $v) {
            $data[] = [
                'levelId' => $v['level_id'],
                'showName' => $v['show_name'],
                'amount' => $v['gift_amount'],
                'levelName' => $v['level_name'],
            ];
        }
        api_response(10000, '请求成功',['list'=>$data]);
    }



    /**
    * @api {POST} api/api/getUserRegAgreementUrl 用户注册协议跳转链接
    * @apiVersion 1.0.0
    * @apiGroup user
    *
    * @apiSuccess {String} code 返回码,10000为成功
    * @apiSuccess {String} message  返回消息
    * @apiSuccess {Object} result  返回数据
    * @apiSuccess {string} result.url  拓客新零售用户注册协议URL
    * 
    */
    public function getUserRegAgreementUrl()
    {
        $result = model('common/Article')->getUserRegAgreementUrl();
        api_response(10000, '请求成功',['url'=>$result]);
    }


    /**
    * @api {POST} api/api/getAgreement 获取协议
    * @apiVersion 1.0.0
    * @apiGroup user
    *
    * @apiParam {String} type  协议类型：1用户购买协议
    * 
    * @apiSuccess {String} code 返回码,10000为成功
    * @apiSuccess {String} message  返回消息
    * @apiSuccess {Object} result  返回数据
    * @apiSuccess {string} result.title  协议标题
    * @apiSuccess {string} result.content  协议内容
    * 
    */
    public function getAgreement()
    {
        $type   = input('type');
        $result = model('common/Article')->getAgreement($type);
        api_response(10000, '请求成功',['data'=>$result]);
    }

    /**
    * @api {POST} api/api/getGuideImg 获取导航指引
    * @apiVersion 1.0.0
    * @apiGroup user
    *
    * @apiParam {String} guideId  导航位置 1:首页 2:分享拓客 3:个人中心 4:拓客社交 5:拓客直播 6:拓客商学院 7:全网拓客 8:申请闯关 9:审核闯关 10:行会大厅 11.全国优品
    * 
    * @apiSuccess {String} code 返回码,10000为成功
    * @apiSuccess {String} message  返回消息
    * @apiSuccess {Object} result  返回数据
    * @apiSuccess {array} result.guideImgs  引导图片
    * 
    */
    public function getGuideImg()
    {
        $guideId   = input('guideId');
        $guideImgs = model('common/nav/Navigation')->getNavGuideImg($guideId);
        api_response(10000, '请求成功',['guideImgs'=>$guideImgs]);
    }

    /**
    * @api {POST} api/api/getPopupAdList 获取首页弹窗广告
    * @apiVersion 1.0.0
    * @apiGroup user
    *
    * @apiSuccess {String} code 返回码,10000为成功
    * @apiSuccess {String} message  返回消息
    * @apiSuccess {Object} result  返回数据
    * @apiSuccess {String} result.popupAdList.adName  广告名称
    * @apiSuccess {String} result.popupAdList.adLink  链接地址
    * @apiSuccess {String} result.popupAdList.adCode  图片地址
    * 
    */
    public function getPopupAdList()
    {
        $popupAdList = model('common/Ad')->getIndexAd(1);
        api_response(10000, '请求成功',$popupAdList);
    }


     /**
    * @api {POST} api/api/appUpdate 检测APP和城市数据库的版本更新
    * @apiVersion 1.0.0
    * @apiGroup user
    *
    * @apiParam {String} version  APP当前版本号
    * @apiParam {String} client   客户端区分，Android，iOS。
    * 
    * @apiSuccess {String} code 返回码,10000为成功
    * @apiSuccess {String} message  返回消息
    * @apiSuccess {Object} result  返回数据
    * @apiSuccess {String} result.newVersion  新版本号
    * @apiSuccess {String} result.updateLog  更新内容
    * @apiSuccess {String} result.updateLogEn  英文版更新内容
    * @apiSuccess {int} result.forceUpdate  是否（1/0）强制更新  
    * @apiSuccess {int} result.showAppUpdate  是否（1/0）显示更新  --ios用
    * @apiSuccess {int} result.isCanUpdate  app是否（1/0）可以更新(是否有新版本)  ios用
    * @apiSuccess {String} result.backgroundImg  UI更新图地址
    * @apiSuccess {String} result.backgroundImgEn  英文版UI更新图地址
    * @apiSuccess {String} result.updateUrl  app更新地址
    * @apiSuccess {int} result.cityVersion  城市数据库版本号
    * @apiSuccess {int} result.updateType  1.原生提供 2.UI提供更新图片
    * 
    */
    public function appUpdate()
    {
        $loginToken = trim(I('loginToken')); //用户id

        // if (!empty($loginToken)) {
        //     $user_id = $this->getUserId();
        // }
        $version = trim(I('version'));
        $language = trim(I('language'));

        $client = I('client'); //1.iOS 2.android 3.H5

        if (empty($version) || empty($client)) {
            api_response(0, '请求失败,参数不全');
        }

        $data['appUpdate'] = M('AppConfig')->where(['name' => ucfirst(strtolower($client))])->find();
        if (isset($data['appUpdate'])) {
            if($language == 'en-us'){
                $data['appUpdate']['backgroundImg']   = $data['appUpdate']['uiUpdateImgEn'] ? SITE_URL.$data['appUpdate']['uiUpdateImgEn'] : '';
                $data['appUpdate']['updateLog']   = $data['appUpdate']['updateLogEn'] ? $data['appUpdate']['updateLogEn'] : '';
            }else{
                $data['appUpdate']['backgroundImg']   = $data['appUpdate']['uiUpdateImg'] ? SITE_URL.$data['appUpdate']['uiUpdateImg'] : '';
            }
            $data['appUpdate']['backgroundImgEn'] = $data['appUpdate']['uiUpdateImgEn'] ? SITE_URL.$data['appUpdate']['uiUpdateImgEn'] : '';
            $data['appUpdate']['updateType']      = $data['appUpdate']['updateDateType'] == 0 ? 1 : $data['appUpdate']['updateDateType'];
            $data['appUpdate']['cityVersion']     = intval($data['appUpdate']['cityAppUpdateVersion']);
            
        }

        $data['appUpdate']['isCanUpdate'] = 0;

        if (version_compare($version, $data['appUpdate']['newVersion']) == -1) {
            $data['appUpdate']['isCanUpdate'] = 1;
        } else {
            $data['appUpdate']['forceUpdate'] = 0;
        }

        if ($client == 'iOS') {
            // $data['appUpdate']['showAppUpdate'] = $data['appUpdate']['showAppUpdateVersion'] != $version ? 1 : $data['appUpdate']['showAppUpdate'];

            $status = version_compare($version, $data['appUpdate']['newVersion']) <0 ? 1 : 0;
            // dump($data['appUpdate']['newVersion']);
            // dump($version);
            // p(version_compare($version, $data['appUpdate']['newVersion']));
            // $data['appUpdate']['showAppUpdate'] = $data['appUpdate']['newVersion'] != $version ? 1 : $data['appUpdate']['showAppUpdate'];
            $data['appUpdate']['showAppUpdate'] = $data['appUpdate']['showAppUpdate']==0 ? 0 : $status;
            // p($data['appUpdate']['showAppUpdate']);
        }
        unset($data['appUpdate']['uiUpdateImg'],
                $data['appUpdate']['uiUpdateImgEn'],
                $data['appUpdate']['id'],
                $data['appUpdate']['isAuditing'],
                $data['appUpdate']['versionData'],
                $data['appUpdate']['cityAppUpdateVersion'],
                $data['appUpdate']['showAppUpdateVersion'],
                $data['appUpdate']['currVersion']
            );
        api_response(10000, '请求成功',$data['appUpdate']);
    }

    /**
     * @api {POST} api/api/twoConfirmGiveGift 订单审核二次确认
     * @apiVersion 1.0.0
     * @apiGroup order
     *
     * @apiParam {int} giveGiftId  送礼表id
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {null} result  返回数据
     */
    public function twoConfirmGiveGift()
    {
        $id = input('giveGiftId', 0);

        $result = model('GiveGift')->find($id);
        $userLevel = model('Users')->where('user_id', $result['give_user_id'])->value('level');
        $status = $userLevel > $result['give_user_level']?1:0;
        api_response(10000, '请求成功',['status'=>$status]);
    }

    /**
     * @api {POST} api/api/clickAddLockNum 分享点击增加临时锁定数量
     * @apiVersion 1.0.0
     * @apiGroup order
     *
     * @apiParam {int} shareId  分享UID
     * @apiParam {int} shareCookiesId  sessionId或cookiesId
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {null} result  返回数据
     */
    public function clickAddLockNum()
    {
        $shareId = input('shareId', 0);
        $shareCookiesId = input('shareCookiesId', 0);

        $result = model('Common/TemporaryClickLocking')->isAddLockNum($shareId,$shareCookiesId);
        if($result){
            api_response(10000, '请求成功',$result);
        }else{
            api_response(10001, '添加失败',$result);
        }
    }

    /**
     * @api {POST} api/api/balanceChangeStatus 是否开启用户余额转账功能
     * @apiVersion 1.0.0
     * @apiGroup user
     *
     * @apiParam {String} loginToken  token
     *
     * @apiSuccess {String} code 返回码,10000为成功
     * @apiSuccess {String} message  返回消息
     * @apiSuccess {null} result  返回数据
     */
    public function balanceChangeStatus()
    {
        $userId = $this->getUserId();
        $isBalanceTransfer = model('common/users')->where('user_id', $userId)->value('is_balance_transfer');
        api_response(10000, '请求成功',['isBalanceTransfer'=>$isBalanceTransfer]);
    }



}
