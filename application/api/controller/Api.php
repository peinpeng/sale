<?php

namespace app\api\controller;

use think\Cache;
use think\Db;
use think\Model;
use think\Page;
use think\Session;
use think\Verify;
use think\cache\driver\Predis as Redis;
use app\common\logic\ApiLogic;

class Api extends ApiBase
{
    //接口公共参数s
    /**
     * @api {POST} a1 公共参数说明
     * @apiVersion 1.0.0
     * @apiGroup public
     *
     * @apiParam {String} apiVersion 接口版本
     * @apiParam {String} client 终端类型，Android，iOS，H5，PHP
     * @apiParam {String} timeStamp 时间戳，秒
     * @apiParam {String} privateSign 验签串
     * @apiParam {String} version APP当前版本
     * @apiParam {String} language 语言，zh中文en英文
     * @apiParam {String} deviceBrand 非必需，终端名牌
     * @apiParam {String} systemVersion 非必需，终端系统版本
     * @apiParam {String} deviceModel 非必需，终端型号
     * @apiParam {String} netType 非必需，网络类型
     */

    /**
     * @api {POST} a2 验签规则
     * @apiVersion 1.0.0
     * @apiGroup public
     *
     * @apiParam {String} a 签名生成算法：签名生成规则如下：参与签名的字段包括privateKey,privateSecret, timestamp（时间戳）,提交的请求参数。对所有待签名参数按照字段名的ASCII 码从小到大排序（字典序）后，使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串string1。。对string1作md5加密（小写），再截取10-20之前的值，做为验签的参数提交在最后，即 privateSign=md5(string1).subString（10，20）。
     * @apiParam {Object} Android
     * @apiParam {String} Android.privateKey 2ED689591F83D4685B58815D67B4D6CD
     * @apiParam {String} Android.privateSecret Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg
     * @apiParam {Object} iOS
     * @apiParam {String} iOS.privateKey 2ED689591F83D4685B58815D67B4D6CD
     * @apiParam {String} iOS.privateSecret Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg
     * @apiParam {Object} PHP
     * @apiParam {String} PHP.privateKey 2ED689591F83D4685B58815D67B4D6CD
     * @apiParam {String} PHP.privateSecret Rlh4UkZwQ0RuMVpMZkdGdHFDa3RLVkg
     * @apiParam {Object} H5
     * @apiParam {String} H5.privateKey 4B9E2AB213CB02BD839A771055F5CC43
     * @apiParam {String} H5.privateSecret 75392D04934E787B28F6AB6A45BDFCAB
     */

    /**
     * @api {POST} a3 商城与会员系统关联点
     * @apiVersion 1.0.0
     * @apiGroup public
     *
     * @apiParam {String} session 登录成功后，会存在一个session为session('user')，里面存有用户表的整行信息
     * @apiParam {Object} users_hash 存储有用户的信息，为一个redis hash，key为users_hash，如果想获取一个用户的信息用$redis->hget("users_hash", 1)
     * @apiParam {int} users_hash.user_id 用户id
     * @apiParam {String} users_hash.nickname 昵称
     * @apiParam {String} users_hash.login_token loginToken
     * @apiParam {String} users_hash.head_pic 头像
     * @apiParam {int} users_hash.first_leader 推荐人用户id
     * @apiParam {int} users_hash.real_status 实名认证状态1未验证2待审核3验证不通过4实名认证成功
     * @apiParam {String} users_hash.true_name 真实姓名
     * @apiParam {int} users_hash.sex 性别0 保密 1 男 2 女
     * @apiParam {String} users_hash.mobile 手机号码
     * @apiParam {String} users_hash.first_leader_mobile 推荐人手机号码
     * @apiParam {String} users_hash.levelName 等级名称
     * @apiParam {String} users_hash.level 等级
     * @apiParam {String} users_hash.paypwd 支付密码
     */

    //接口公共参数e

    protected $beforeActionList = [
        '_checkVerSion',
    ];

    protected $method = null;

    //不验签的方法
    protected $unckeck = ['verify','smsCode'];

    public function _initialize()
    {
        header('Access-Control-Allow-Origin:*');
        if (!empty($_REQUEST['sessionId'])) {
            session_id($_REQUEST['sessionId']);
        }

        $this->method = IS_POST?'POST':'GET';//当前请求 post 还是 get

        Session::start();
        $privateSign = I('privateSign') ?: "";
        if ($privateSign != "debug" && !in_array(ACTION_NAME, $this->unckeck)) {
            $isUserArr = $this->_check();
            if ($isUserArr['status'] != 1) {
                echo api_ajaxReturn(0, $isUserArr['info']);exit;
            }
        }

        parent::_initialize();
    }

    public function _check(){
        $params = $_POST?$_POST:$_GET;
        $key=trim(I('privateSign'));
        $this->logic=new ApiLogic();
        $isUserful=$this->logic->checkInterfaceUseful($params,$key);
        return $isUserful;
    }

    protected function _checkVerSion()
    {
        $apiVersion = I('apiVersion')?(string) I('apiVersion'):'1.0.0';
        $apiVersion = str_replace('.', '',$apiVersion);
        while (true){
           $file_path=APP_PATH . 'api/controller/Api'.$apiVersion . '.php';
           if(!is_file($file_path) && $apiVersion>0){
               $apiVersion = $apiVersion - 1;
           }else{
               break;
           }
        }

        if( CONTROLLER_NAME != 'Api'. $apiVersion)
        { 
            $params = I($this->method.'.');
            $apiUrl = SITE_URL . '/api/Api' . $apiVersion . '/' . request()->action().'.html';
            request()->create($apiUrl,$this->method,$params);
            \think\App::run($request);
        }
    }

    /**
     * Function: 获取当前用户user_id
     * @param bool $isMust
     * @return array
     */
    protected function getUserId($isMust = true)
    {
        //return 10149;
        $loginToken = trim(I('loginToken')); //用户id
        //前端要求，0也算token，特殊处理
        if (!$loginToken && $loginToken != '0') {
            if ($isMust == false) return 0;
            exit(api_ajaxReturn(1998, '登录凭证不能为空！'));
        }
        if($loginToken == '0'){
            if ($isMust == false) return 0;
            exit(api_ajaxReturn(1999, '用户登录信息失效，请重新登录！'));
        }
        //获取到用户id
        $user_token = M('user_token')->order('add_time desc')->where('token', $loginToken)->find();

        if(empty($user_token)){
            if ($isMust == false) return 0;
            exit(api_ajaxReturn(1999, '用户登录信息失效，请重新登录！'));
        }

        if (time() - $user_token['add_time'] > 60 * 60 * 24 * 7) {
            M('user_token')->where('token', $loginToken)->delete();
            if ($isMust == false) return 0;
            exit(api_ajaxReturn(1999, '用户登录信息失效，请重新登录！'));
        }

        $user_id = $user_token['user_id'];
        if ($user_id['status'] === 0) {
            if ($isMust == false) return 0;
            exit(api_ajaxReturn(1999, '用户登录信息失效，请重新登录！'));
        } else {
            return $user_id;
        }
    }

    /**
     * 系统内部非接口检测用户是否存在
     * @param  [type] $mobile [description]
     * @return [type]         [description]
     */
    public function checkUserByMobile($mobile){
        $logic=new ApiLogic();
        if(!check_mobile_all($mobile)){
            return array('status'=>-1,'info'=>'手机号码格式不正确！','data'=>'');
        }
        if($logic->checkUser($mobile)){
            return array('status'=>1003,'info'=>'此手机已注册','data'=>'');
        }else{
            return array('status'=>1,'info'=>'此手机可注册','data'=>'');
        }
    }


    

}
