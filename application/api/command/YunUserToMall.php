<?php
/**
 * Created by PhpStorm.
 * User: jianbiao
 * Date: 2019/11/14
 * Time: 10:41
 */
namespace app\api\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

class YunUserToMall extends Command
{
    //protected $user_id = 17725628787;
    protected $user_id = 153605651;  //线上的
    protected $first_leader = 153480819;
    protected $second_leader = 153456718;

    protected function configure()
    {
        $this->setName('yun_user')->setDescription('yun user to mall');
    }

    protected function execute(Input $input, Output $output)
    {
        $yunUserDb = Db::table('cloud_users');

        $result = Db::query('SELECT mobile FROM cloud_users WHERE mobile not in (SELECT mobile FROM saas_users)');
        foreach ($result as $v) {
            $this->makeData($v['mobile'], $this->user_id,$this->first_leader,$this->second_leader);
        }

        $output->writeln("ok");
    }

    //处理数据，插入
    protected function makeData($mobile, $first_leader,$second_leader,$third_leader)
    {
        $password = encrypt(substr($mobile, -6,6));
        $time = time();
        $data = [
            'mobile_validated' => 1,
            'mobile' => $mobile,
            'nickname' => $mobile,
            'head_pic' => '/template/mobile/rainbow/static/images/user68.jpg',
            'password' => $password,
            'reg_time' => $time,
            'paypwd' => $password,
            'first_leader'=>$first_leader,
            'second_leader'=>$second_leader,
            'third_leader'=>$third_leader,
            'last_login' => $time,
        ];
        $users_copy_model = Db::name('users');
        $user_id = $users_copy_model->insertGetId($data);

        $users_copy_model->where(array('user_id' => $first_leader))->setInc('underling_number');
        $users_copy_model->where(array('user_id' => $second_leader))->setInc('underling_number');
        $users_copy_model->where(array('user_id' => $third_leader))->setInc('underling_number');
        //更新会员的团队
        $this->getParentIdStr($user_id);
    }

    public function getParentIdStr($user_id){
        $distributTeamModel = Db::name('distribut_team');

        $userTeam = $distributTeamModel->where(['user_id'=>$user_id])->find();
        if(!empty($userTeam)) return $userTeam['parent_id_str'];

        $parent_id = Db::name('users')->where(['user_id'=>$user_id])->value('first_leader');
        if(empty($parent_id)) return 'team';

        $parent_team = $distributTeamModel->where(['user_id'=>$parent_id])->find();

        if($parent_team){
            $p_parent_id_str = $parent_team['parent_id_str'];
        }else{
            $p_parent_id_str = $this->getParentIdStr($parent_id);
        }

        $parent_id_str = $p_parent_id_str.'_('.$parent_id.')';

        $distributTeamModel->insertGetId(['user_id'=>$user_id,'parent_id_str'=>$parent_id_str,'parent_id'=>$parent_id,'add_time'=>time()]);

        return $parent_id_str;
    }
}