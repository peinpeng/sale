<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/18
 * Time: 16:36
 */
namespace app\api\command;

use app\common\logic\UserHashLogic;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class UsersHash extends Command
{
    protected function configure()
    {
        $this->setName('users_hash')->setDescription('users data input redis hash table');
    }

    protected function execute(Input $input, Output $output)
    {
        (new UserHashLogic())->allTable();
        $output->writeln("ok");
    }
}