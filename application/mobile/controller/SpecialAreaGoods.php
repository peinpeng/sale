<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 圣火科技网络公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: wq   2018-12-20
 */ 
namespace app\mobile\controller;
use app\common\logic\GoodsLogic;
use app\common\logic\GoodsActivityLogic;
use app\common\model\FlashSale;
use app\common\model\GroupBuy;
use think\Db;
use think\Page;
use app\common\logic\ActivityLogic;

class SpecialAreaGoods extends MobileBase {
    public function index(){
        $p=I('p',1,'intval');
        $distributType=I('distribut_type','','trim');
        $where=[];
        $cat_id=I('cat_id',0,'intval');
        $cat_id && $where['cat_id']=$cat_id;
        $Info=model('common/goods/GoodsInfoLogic','logic')->getGoodsInfoByDisType($distributType,$this->user_id,$p,$where);
        $this->assign($Info);
        switch ($distributType){
            case 'vip': 
                $viewFile=IS_AJAX ?'ajax_vip_goods':'vip_goods'; 
            break;
            case 'trade': 
                $viewFile=IS_AJAX ?'ajax_shop_goods':'shop_goods'; 
             break;
        }
        return $this->fetch($viewFile);
    }
}