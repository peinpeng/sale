<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: 当燃 2016-01-09
 */

namespace app\mobile\controller;

use app\common\logic\JssdkLogic;
use app\common\logic\UserHashLogic;
use Think\Db;
use think\db\Query;
use app\common\logic\UsersLogic;
use app\common\logic\wechat\WechatUtil;
use app\common\logic\UserLogic;

class Index extends MobileBase
{

    /*
        * 初始化操作
        */
    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $session_user = session('user');
            $user = M('users')->where("user_id", $session_user['user_id'])->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
        } else {
            header("Location:" . U('Mobile/User/login'));
            exit;
        }

        

    }


    /**
     * User: Simony
     * Function:首页
     * @return mixed
     */
    public function index()
    {
        //用户被冻结
        if(model('Common/credit/CreditUserLog')->isLockState($this->user_id)){
            return $this->redirect(U('mobile/User/abnormal_unusual',['user_id'=>$this->user_id]));
            // return $this->error(model('Common/credit/CreditUserLog')->lock_des($this->user['user_id']),U('mobile/User/logout'));
        }
        //页面引导
        $is_guide = $this->isGuideIndex($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        // $mobileIndexCache = cache('mobileIndexCache_'.$this->user_id);
        $mobileIndexCache = cache('mobileIndexCache');

        if(!empty($mobileIndexCache) && empty(I('clearCache'))) {
        // if(0) {
            $mobileIndexCache = unserialize($mobileIndexCache);
        }else{

            $articleModel = new \app\common\model\Article();
            $cat_ids =  M('ArticleCat')->where(['parent_id'=>1])->column('cat_id'); //获取公告分类下的子分类ids
            $cond = [
                // 'cat_id' => 1, //cat_id=1 //是公告
                'is_open' => 1,
            ];
            $cat_ids && $cond['cat_id'] = ['in',$cat_ids];
            

            $notices = $articleModel->getList($cond, [], false, [], 'add_time desc', 5); //最新的公告图片放到首页

            //首页背景图
            $bgList =  model('Common/MobileLogic','logic')->getBackground();

            $config = tpCache('shop_info');

            // p($adList);
            $data = [
                'config' => $config,  //站点配置
                'notices' => $notices,  //公告
                'bgList' => $bgList,  //背景图
            ];
            $mobileIndexCache = $data;
            // cache('mobileIndexCache_'.$this->user_id,serialize($data),300);
            cache('mobileIndexCache',serialize($data),300);
        }

        //广告banner
        $adList = M('ad')->where(['pid' => 1,'enabled'=>1])->select(); //轮播广告 pid是1
        foreach ($adList as $k => $v) {
            if(session('user') && preg_match('/^(?:http[s]?:)?\/\//i',$v['ad_link'])){
                $adList[$k]['ad_link'] = jump_stitch($v['ad_link']);
            }
        }
        $mobileIndexCache['adList'] = $adList;

        $slippingUser = model('Common/slippingUsers')->where(['slipping_id'=>$this->user_id])->find();
        if($slippingUser){
            $referrer     = model('Common/Users')->getBy('user_id',$slippingUser['referrer_id']);
            $connecter    = model('Common/Users')->getBy('user_id',$slippingUser['connecter_id']);

            $slippingUser['referrer_mobile']  = $referrer  && !empty($referrer['nickname'])?$referrer['nickname']:$referrer['mobile'];
            $slippingUser['connecter_mobile'] = $connecter && !empty($connecter['nickname'])?$connecter['nickname']:$connecter['mobile'];
        }
        //用户资料
        $userData['levelName'] = model('Common/users/UserLevel')->getBy('level_id',$this->user['level'],'show_name');
        $direcUser = model('Common/Users')->getBy('user_id',$this->user['first_leader']);
        $userData['directUserMobile'] = !empty($direcUser['nickname'])?$direcUser['nickname']:(isset($direcUser['mobile'])?$direcUser['mobile']:'');
        $userData['userId'] = $this->user_id;
        $userData['nickName'] = $this->user['nickname']?$this->user['nickname']:$this->user['mobile'];
        $userData['creditPoint'] = (int)model('Common/credit/CreditUser')->getBy('user_id',$this->user_id,'total_point');
        $userData['userContribution'] = (int)model('Common/Users')->getFisrtLeaderNumAttr($this->user_id,0);
        $userData['teamContribution'] = (int)model('Common/distribut/DistributTeam')->teamNum($this->user_id);
        $userData['compensateNum'] = (int)model('Common/place/PlaceLossCount')->getBy('user_id',$this->user_id,'loss_num');
        $userData['head_pic'] = model('Common/Users')->where(['user_id'=>$this->user_id])->value('head_pic');

        //用户资料
        $levelList = model('Common/users/Userlevel')->getList([],[],false,[],'level_id asc');

        $credit_user = model('Common/credit/CreditUser')->getInfo($this->user_id);

        //弹窗广告
        $popupAdList =model('common/Ad')->getIndexAd();
        // p($popupAdList);
        $mobileIndexCache['slippingUser'] = $slippingUser;
        $mobileIndexCache['userData'] = $userData;
        $mobileIndexCache['levelList'] = $levelList;
        $mobileIndexCache['credit_user'] = $credit_user;
        $mobileIndexCache['guide_imgs'] = $guide_imgs;
        $mobileIndexCache['popupAdList'] = $popupAdList;
        // p( $mobileIndexCache['userData']);
        $this->assign($mobileIndexCache);

        return $this->fetch();
    }

    /**
     * 首页是否需要引导
     * @param $user_id  用户id 
     * @return string status   1 需要引导 0 不需要引导
     */
    public function isGuideIndex($user_id){

       $mdl = model('common/UserGuideLog');
       $navigation = model('common/nav/Navigation')->getInfo(['name_english'=>'Home']);

       //不需要引导
       if($navigation['is_guide'] == 0) return array('status'=>0 , 'msg'=>'','data'=>[]);

       $data = [];

       //引导图
       $data['guide_imgs'] = $navigation['guide_imgs'];

       $guideLog =$mdl->where(['user_id'=>$user_id])->find();
     
        //不存在引导记录，则添加
        if(empty($guideLog)){
            $addData = [
                'user_id'=>$user_id,
                'guide_ids'=>$navigation['id'],
            ];
            $res = $mdl->addData($addData);
            return array('status'=>1 , 'msg'=>'','data'=>$data);
        }

        //存在判断是否已引导
        $guide_ids = explode(',', $guideLog['guide_ids']);
        if(in_array($navigation['id'], $guide_ids)) {
            return array('status'=>0 , 'msg'=>'','data'=>$data);
        }
        //添加记录
        $guide_ids[] = $navigation['id'];
        $guide_ids_str = implode(',', $guide_ids);
        $mdl->where(['id'=>$guideLog['id']])->update(['guide_ids'=>$guide_ids_str]);
        return array('status'=>1 , 'msg'=>'','data'=>$data);
    }


//    //微信Jssdk 操作类 用分享朋友圈 JS
//    public function ajaxGetWxConfig()
//    {
//        $askUrl = input('askUrl');//分享URL
//        $askUrl = urldecode($askUrl);
//
//        $wechat = new WechatUtil;
//        $signPackage = $wechat->getSignPackage($askUrl);
//        if (!$signPackage) {
//            exit($wechat->getError());
//        }
//
//        $this->ajaxReturn($signPackage);
//    }


    /**
     * 我要闯关页面
     */
    public function gift_giving()
    {
        $usersLogic = model('Common/UsersLogic','logic');
        $userMdl = model('Common/Users');
        $userAddressMdl = model('Common/UserAddress');
        $regionMdl = model('Common/Region');
        $userLevelMdl = model('Common/users/UserLevel');
        

        $croosAddressId = I('croos_address_id/d');
        $addressId = I('address_id/d');

        if($userLevelMdl->getMaxLevel() == $this->user['level']) {
            $this->redirect( U('mobile/user/maxLevelNotice'));
        }

        $defaultAddress = $userAddressMdl->getAddress(0,$this->user_id);
       
        $detailUrl = U('Mobile/User/add_address',['preurl'=>base64($this->request->domain().$this->request->baseUrl())]);
      
        if(empty($defaultAddress)) $this->redirect($detailUrl);
        
        $defaultAddress['detailAddress'] = $regionMdl->getDetailAddress($defaultAddress['address_id']);
        $defaultAddress['detail_address_url'] = $detailUrl;

        $croosAddress = $address = [];
      
        if(!empty($croosAddressId)) {
            $croosAddress = $userAddressMdl->getByPk($croosAddressId);
            $croosAddress['detailAddress'] = $regionMdl->getDetailAddress($croosAddress['address_id']);
           
        }

        $croosAddress['cross_address_url'] = U('Mobile/User/address_list',['preurl'=>base64($this->request->domain().$this->request->baseUrl()),'type'=>'croos']);
          
       
        if(!empty($addressId)) {
            $address = $userAddressMdl->getByPk($addressId);
            $address['detailAddress'] = $regionMdl->getDetailAddress($address['address_id']);
        } 
        
        $address['address_url'] = U('Mobile/User/address_list',['preurl'=>base64($this->request->domain().$this->request->baseUrl()),'type'=>'address']);  

        /**
         * 闯关检查（原送礼）
         */
        $checkData = $this->_checkGiving();
        $isFulfilUpgradeData = $checkData['isFulfilUpgradeData'];
        $checkIsGift = $checkData['checkIsGift'];

        if(!empty($checkIsGift['isSendGift']) && !$checkIsGift['isNeedAudit']){
            $this->redirect(U('mobile/member_center/updataLevel'));
            // $this->error('对对对',U('mobile/member_center/updataLevel'));
        }

        /**
         * [$isCroos 跨级信息、跨级用户信息]
         */
        $isCroos = !empty($isFulfilUpgradeData['nextLevelInfo']['need_cross_level']) ? 1 : 0;
        $croosUserInfo = $collectUserInfo = [];
        $croos_user_id = $collect_user_id = 0;

        if(!empty($isCroos))
        {
            $croos_user_id = $usersLogic->getCollectUserId($this->user_id,1); 
            $croosUserInfo = $userMdl->getByPk($croos_user_id); 
            $croosUserInfo['paytype'] = model('Common/Paytype')->getCol('id,account,type',['user_id'=>$croos_user_id]);
        }

        /**
         * [$collect_user_id 收礼人编号]
         */
        $collect_user_id = $usersLogic->getCollectUserId($this->user_id,0);
        $collectUserInfo = $userMdl->getByPk($collect_user_id);
        $collectUserInfo['paytype'] = model('Common/Paytype')->getCol('id,account,type',['user_id'=>$collect_user_id]);  

        if(IS_POST)
        {
            $croos_user_id = I('croos_user_id/d');
            $collect_user_id = I('collect_user_id/d');
            $address = [
                'croos_collect_type' => I('croos_collect_type/d'),
                'collect_type' => I('collect_type/d'),
                'address_id' => I('address_id/d'),
                'cross_address_id' => I('cross_address_id/d')
            ];
            /**
             * 闯关处理
             */
            $this->_doGiving($croos_user_id,$collect_user_id,$address);
        }

        // p($croosUserInfo['user_id']);

        /**
         * [$user_shop_manage 用户个人商城管理]
         */
        $childShopLink = model('Common/UserShopManage')->getChildShopLink([$collect_user_id,$croos_user_id]);//子商城 or e店 or 第三方链接

        I('debug') == 1 && p($checkIsGift);
        $data  = [
            'collectUserInfo' => $collectUserInfo,
            'croosUserInfo' => $croosUserInfo,
            'croos_user_id' => $croos_user_id,
            'collect_user_id' => $collect_user_id,
            'isSendGift' => (bool)$checkIsGift['isSendGift'],
            'isAllAudit' => (bool)$checkIsGift['isAllAudit'], 
            'isNeedAudit' => (bool)$checkIsGift['isNeedAudit'], 
            'defaultAddress' => $defaultAddress,
            'croosAddress' => $croosAddress,
            'address' => $address,
            'childShopLink' => $childShopLink,
        ];
        $this->assign($data);

        return $this->fetch();
    }


    /**
     * [_checkGiving 闯关检查（原送礼）]
     * @return [type] [description]
     */
    protected function _checkGiving()
    {

        $usersLogic = model('Common/UsersLogic','logic');
        $giveGiftMdl = model('Common/GiveGift');
        /**
         * [$isFulfilUpgrade 查看是否升级]
         */
        $isFulfilUpgradeData = $usersLogic->isFulfilUpgrade($this->user_id);

        if( empty($isFulfilUpgradeData) || $isFulfilUpgradeData['isFulfilUpgrade'] === false ) $this->error('暂不满足闯关条件！');
        // if( empty($isFulfilUpgradeData) || $isFulfilUpgradeData['isFulfilUpgrade'] === false ) $this->error('暂不满足闯关条件！',U('mobile/MemberCenter/updataLevel'));

        /**
         * [$checkIsGift 闯关详情]
         */
        $checkIsGift = $giveGiftMdl->checkIsGift($this->user_id);

        if($checkIsGift['isAllAudit'] === true) $this->success('审核中，请耐心等待',U('mobile/MemberCenter/updataLevel'));

        return compact('isFulfilUpgradeData','checkIsGift');
    }



    /**
     * [_doGiving 闯关处理]
     * @param  [type] $croos_user_id   [description]
     * @param  [type] $collect_user_id [description]
     * @return [type]                  [description]
     */
    protected function _doGiving($croos_user_id,$collect_user_id,$address)
    {
        //判断
        $key = 'give_gift_'.$croos_user_id.'_'.$collect_user_id.'_'.$this->user_id;

        if(cache($key) === false){
            cache($key,'give_gift_is_add',3);
        }else{
            return $this->error('请勿重复提交确认闯关');
        }   
        
        $giveGiftMdl = model('Common/GiveGift');
        $logic = model('Common/place/PlaceMatchCompensateLogic','logic');
        $new_first_leader_id = M('Users')->where(['user_id'=>$this->user_id])->value('first_leader');
        $new_first_leader = M('Users')->where(['user_id'=>$new_first_leader_id])->find();

        //是占位规则匹配上级链的普通消费 确认闯关检查
        if( $this->user['level'] == 1 && $new_first_leader['level'] <= 1 && $new_first_leader['user_id'] != $collect_user_id){
            $res = $logic->checkLeaderIsChange($collect_user_id,$this->user_id);
            if($res ==false){
                $this->error('闯关匹配人名额已用完，请重新闯关！');
            }
        }

        trans();

        /**
         * 如果存在跨级
         */
        if(!empty($croos_user_id))
        {
            /**
             * [$croosGift 跨级审核通过数据]
             */
            $croosGift = $giveGiftMdl->where([
                'give_user_id'=>$this->user_id,
                'give_user_level'=>$this->user['level'],
                'collect_user_id'=>$croos_user_id,
                // 'status'=>['in',[0,1]],
                'status'=>0,
                'give_type'=>2
            ])->find();


            if(empty($croosGift)) {
               

                // $sql = 'select count(*) as count from saas_give_gift where give_user_id ='.$this->user_id.' and give_user_level ='.$this->user['level'].' and collect_user_id ='.$croos_user_id.' and give_type = 2 and status in (0,1) FOR UPDATE';

                // $res = Db::query($sql);
                // // p($res);
                // if($res[0]['count'] > 0){
                //     trans('rollback');
                //     $this->error('请勿重复添加跨级消费数据！');
                // }

                $crossAddressId = (int)$address['croos_collect_type'] == 2 ? $address['cross_address_id'] : 0;
                $res = $giveGiftMdl->addData($this->user_id,$croos_user_id,1,$crossAddressId);
                if(empty($res))
                {
                    trans('rollback');
                    $this->error('跨级闯关失败！');
                }

            } 
           
        }
        /**
         * [$collectGift 普通升级是否有审核通过数据]
         */
        $collectGift = $giveGiftMdl->where([
            'give_user_id'=>$this->user_id,
            'give_user_level'=>$this->user['level'],
            'collect_user_id'=>$collect_user_id,
            // 'status'=>['in',[0,1]],
            'status'=>0,
            'give_type' => 1
        ])->find();
        // p($collectGift);
        if(empty($collectGift)){
           $addressId = (int)$address['collect_type'] == 2 ? $address['address_id'] : 0;

            // $sql = 'select count(*) as count from saas_give_gift where give_user_id ='.$this->user_id.' and give_user_level ='.$this->user['level'].' and collect_user_id ='.$collect_user_id.' and give_type = 1 and status in (0,1) FOR UPDATE';

            // $res = Db::query($sql);
            // if($res[0]['count'] > 0){
            //     trans('rollback');
            //     $this->error('请勿重复添加普通消费数据！');
            // }

            $res = $giveGiftMdl->addData($this->user_id,$collect_user_id,0,$addressId);
            if(empty($res))
            {
               trans('rollback');
               $this->error('主动闯关失败！');
            }

            //是占位规则匹配的上级 普通消费
            if( $this->user['level'] == 1 && $new_first_leader['level_id'] <= 1 && $new_first_leader['user_id'] != $collect_user_id){
                $res = $logic->addPlaceMatchingLog($collect_user_id,$this->user_id);//[添加匹配/补偿记录]
                if($res['status'] ==0 ){
                    trans('rollback');
                   $this->error('添加记录失败!');
                }
            }

        }
        trans('commit');

        //刷新信息
        (new UserHashLogic())->refreshOne($this->user_id);
        $this->success('申请闯关成功！');
        return ;
    }



    public function getAccount()
    {
        $type = I('post.type');
        $account = M('paytype')->where(['user_id' => $this->user_id, 'type' => $type])->getField('account');
        $this->ajaxReturn(array('status' => 1, 'account' => $account));
    }

    /**
     * APP下载页
     */
    public function download(){
        return $this->fetch();
    }

    //微信Jssdk 操作类 用分享朋友圈 JS
    public function ajaxGetWxConfig(){
        $askUrl = I('askUrl');//分享URL
        $weixin_config = M('wx_user')->find(); //获取微信配置
        $jssdk = new JssdkLogic($weixin_config['appid'], $weixin_config['appsecret']);
        $signPackage = $jssdk->GetSignPackage(urldecode($askUrl));
        if($signPackage){
            $this->ajaxReturn($signPackage,'JSON');
        }else{
            return false;
        }
    }

}