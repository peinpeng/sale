<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: 当燃 2016-01-09
 */

namespace app\mobile\controller;

use Think\Db;
use think\db\Query;
use think\Page;
use think\Request;
use app\common\logic\SlippingUsersLogic;


class MobileAdmin extends MobileBase
{

    public $nav = [
        'level'                  => '上下级关系',
        'edit_pwd'               => '修改密码',
        'edit_level'             => '修改等级',
        'order_list'             => '闯关管理',
        'audit_certification'    => '实名认证审核',
        'complain_list'          => '投诉管理',
        'member_upgrade'         => '会员升级记录',
        'superior_revision'      => '会员上级修改记录',
    ];
    /*
        * 初始化操作
        */
    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $session_user = session('user');
            $user = M('users')->where("user_id", $session_user['user_id'])->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
            $this->remark = '来自管理员：'.$user['mobile'].'的操作';
            if(!model('Common/UserMobileAdmin')->isMobileAdminUser($user['user_id'])){
                header("Location:" . U('Mobile/index/index'));
            }
        } else {
            header("Location:" . U('Mobile/User/login'));
            exit;
        }
        $this->nav_type = I('nav_type','level');

        $this->assign('nav',$this->nav);
        $this->assign('current_nav_type',$this->nav_type);
    }


    /**
     * Function:首页
     * @return mixed
     */
    public function index()
    {
        $nav_type = $this->nav_type;
        $fun = 'index_'.$this->nav_type;
        if(method_exists($this,$fun)){
            return $this->$fun();
        }
        return $this->fetch($fun);
    }

    //上下级关系 页面
    public function index_level()
    {
        return $this->fetch(__FUNCTION__);
    }
    //修改密码 页面
    public function index_edit_pwd()
    {
        return $this->fetch(__FUNCTION__);
    }
    //修改等级 页面
    public function index_edit_level()
    {
        return $this->fetch(__FUNCTION__);
    }
    //闯关管理 页面
    public function index_order_list()
    {
        return $this->fetch(__FUNCTION__);
        // $index = controller('Order');
        // return $index->order_list();
    }
    //实名认证审核 页面
    public function index_Audit_certification()
    {
        return $this->fetch(__FUNCTION__);
    }
    //投诉管理 页面
    public function index_complain_list()
    {
        return $this->fetch(__FUNCTION__);
    }
    //投诉管理详情 页面
    public function index_complain_detail()
    {
        $id = I('get.id');
         $model = model('Common/Complaint');
         $complaint = $model->with(['user','respondent_user','give_gift'])->where(['complaint_id' => $id])->find();
         if (!$complaint) $this->error('无权查看改详情');
         $complaint['pics'] = explode(',', $complaint['pics']);
         $this->assign('complaint', $complaint);
        return $this->fetch(__FUNCTION__);
    }

    //会员升级记录 页面
    public function member_upgrade()
    { 
        
        return $this->fetch(__FUNCTION__);
    }

    //会员上级修改记录 页面
    public function superior_revision()
    {
        return $this->fetch(__FUNCTION__);
    }

    //操作
    public function handle()
    {
        $fun = 'handle_'.$this->nav_type;
        return $this->$fun();
    }

    //上下级关系 操作
    public function handle_level()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);    
        if(!$mobile) ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        switch ($act) {
            case 'find':
                # 查看
                if(!$userInfo) ajaxReturn(['status'=>0,'msg'=>'用户不存在']);            
                $pInfo = model('Common/Users')->getBy('user_id',$userInfo['first_leader']);
                $data['levelInfo'] = model('Common/UserLevel')->select();
                ajaxReturn(['status'=>1,'msg'=>'ok','data'=>$pInfo['mobile']]);
                break;
            case 'edit':
                # 修改
                $pmobile = I('pmobile','0');
                if(!$pmobile) ajaxReturn(['status'=>0,'msg'=>'上级手机号不能为空']);
                $dtMdl = model('Common/distribut/DistributTeam');
                $res = $dtMdl->updateUserParent($mobile,$pmobile);//变更上级，更换上级链
                if(!$res){
                    ajaxReturn(['status'=>0,'msg'=>$dtMdl->getError()]);
                }
                //添加更改上级记录
                $first_leader_info = model('Common/Users')->where(['user_id'=>$userInfo['first_leader']])->find();  
                $first_leader      = model('Common/Users')->where(['mobile'=>$pmobile])->find();    

                $changeFirstLeaderLogMdl = model('Common/ChangeFirstLeaderLog');
                $before_first_leader_id  = $first_leader_info['user_id'];   //原上级ID         
                $after_first_leader_id   = $first_leader['user_id'];         //要更改的上级ID  
                $result = $changeFirstLeaderLogMdl->addData($userInfo['user_id'],$before_first_leader_id,$after_first_leader_id,2);  //添加会员修改上级记录

                //如果用户等级level>=2  更改上级后的滑落规则操作
                // $slippingUsersLogic = new SlippingUsersLogic;
                // $res = $slippingUsersLogic->slippingRule($userInfo['user_id'],'changeFirstLeader',2);
                if(!$result){
                    ajaxReturn(['status'=>0,'msg'=>'更新成功，但添加会员修改上级记录失败']);

                }
                ajaxReturn(['status'=>1,'msg'=>'修改成功']);
                break;
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //修改密码 操作
    public function handle_edit_pwd()
    {
        $mobile = I('mobile','0');
        $pwd1 = I('pwd1','0');
        $pwd2 = I('pwd2','0');
        if(!$mobile || !$pwd1 || !$pwd2) ajaxReturn(['status'=>0,'msg'=>'参数错误']);
        if($pwd2 != $pwd1) ajaxReturn(['status'=>0,'msg'=>'两次密码不一致']);
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);    
        if(!$userInfo) ajaxReturn(['status'=>0,'msg'=>'用户不存在']);  

        $res = M('users')->where("mobile", $mobile)->save(array('password'=>encrypt($pwd1)));
        if($res===false){
            ajaxReturn(['status'=>0,'msg'=>'出错了！']);
        }
        ajaxReturn(['status'=>1,'msg'=>'修改成功']);
    }

    //修改等级 操作
    public function handle_edit_level()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        if(!$mobile) ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        switch ($act) {
            case 'find':
                # 查看
                $userInfo = model('Common/Users')->getBy('mobile',$mobile);
                if(!$userInfo) ajaxReturn(['status'=>0,'msg'=>'用户不存在']);
                $data['levelInfo'] = model('Common/UserLevel')->select();
                $data['level_name'] = model('Common/UserLevel')->where(['level_id'=>$userInfo['level']])->value('show_name');
                ajaxReturn(['status'=>1,'msg'=>'ok','data'=>$data]);
                break;
            case 'edit':
                # 修改
                $level = I('level','0');
                if(!$level) ajaxReturn(['status'=>0,'msg'=>'请选择等级']);
                $model = model('Common/Users');
                $userInfo = $model->getBy('mobile',$mobile);
                if(!$userInfo) ajaxReturn(['status'=>0,'msg'=>'用户不存在']);
                if($userInfo['level']==$level) ajaxReturn(['status'=>0,'msg'=>'用户已是该等级了']);
                $res = $model->updateUserLevel($userInfo['user_id'],$level,\app\common\model\UserLevelLog::UPLEVEL_SOURCE_MOBILE_PASS);//等级调整及添加修改等级记录

                //等级修改后是否还满足滑落规则的接点人条件
                // $slippingUsersLogic = new SlippingUsersLogic;
                // $result = $slippingUsersLogic->slippingRule($userInfo['user_id'],'changeLevel');  
                // if(!$res){
                //     ajaxReturn(['status'=>0,'msg'=>$model->getError()]);
                // }
                ajaxReturn(['status'=>1,'msg'=>'修改成功']);
                break;
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //闯关管理 操作
    public function handle_order_list()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        if(!$mobile) ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        switch ($act) {
            case 'find':
                # 查看
                return $this->index_order_list_ajax();
                break;
            // case 'edit':
            //     # 审核
            //     $status = I('status','0');//1 审核成功  -1 拒绝
            //     $id = I('id','0');//闯关id
            //     // $data = [
            //     //   "status" => $status,
            //     //   "remark" => $this->remark,
            //     // ];
            //     $model = model('GiveGift');
            //     // $userLevelLog = model('Common/UserLevelLog');\app\common\model\credit\CreditUserLog::ADDCREDITSCORE_ADMIN
            //     p(1111111111);
            //     // P(\app\common\model\UserLevelLog::UPLEVEL_SOURCE_APPLY_PASS);
            //     $res = $model->changeStatus($id,$status,$this->remark,\app\common\model\UserLevelLog::UPLEVEL_SOURCE_APPLY_PASS);//审核闯关
            //     break;
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //闯关管理 查看 （复制order/order_list）
    public function index_order_list_ajax()
    {
        $giveGiftMdl = model('Common/giveGift');
        $usersMdl = model('Common/Users');
        $userLevelMdl = model('Common/UserLevel');

        //条件搜索
        $type = I('type', 'give'); // give:送礼 collect:收礼
        $status = I('order_type','');
        $mobile = I('mobile','');
        $page = I('p','1','intval');

        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        if(!$userInfo){
            return $this->fetch(__FUNCTION__);
        }
        $uid = $userInfo['user_id'];

        $dataName = array(
            'giveTitle' => '闯关记录',
            'collectTitle' => '闯关审核记录',
            'giveTypeName' => '闯关',
            'collectTypeName' => '闯关',
        );
       
        $statusStr = [
            '0' => '待确认',
            '1' => '已确认',
            '-1' => '已拒绝',
        ];

        $title = $dataName[$type . 'Title'];
        $typeName = $dataName[$type . 'TypeName'];

        $field = $type . '_user_id';
        $where[$field] = $uid;
        if ($mobile) {
            $give_user_ids = $usersMdl->getUserIdByMobileOrNickname($mobile);
            $where['give_user_id'] = ['in',$give_user_ids];
        }
        $status == 1 && $where['status'] = 0;
        $status == 2 && $where['status'] = 1;
        $status == 3 && $where['status'] = -1;
        
        $count = $giveGiftMdl->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $order_str = "addtime DESC";
        $order_list = $giveGiftMdl->order($order_str)->where($where)->page($page . ',' . $Page->listRows)->select();
        $whereUserField = $type == 'give' ? 'collect_user_id' : 'give_user_id';
        
        foreach ($order_list as &$list) {
            $_user = $usersMdl->getByPk($list[$whereUserField]);
            // $list['name'] = $_user['name'] ? $_user['name'] : ($_user['nickname'] ? $_user['nickname'] : $_user['mobile']);
            $list['name'] = $_user['nickname'] ? $_user['nickname'] : $_user['mobile'];
            $list['mobile'] = $_user['mobile'];
            $list['head_pic'] = $_user['head_pic'];
            //要闯的关卡等级（用户要升的等级）
            $give_user_level_next = $list['give_user_level'] + 1;
            $list['checkpoint_level_name'] = $userLevelMdl->where(['level_id' => $give_user_level_next])->value('show_name');
        }
        $data = [
            'lists' => $order_list,
            'active' => 'order_list',
            'active_status' => I('type'),
            'title' => $title,
            'typeName' => $typeName,
            'statusStr' => $statusStr,
            'user_id'   => $uid,
        ];
        $this->assign($data);
        return $this->fetch(__FUNCTION__);
    }

    //实名认证审核 操作
    public function handle_audit_certification()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        if(!$mobile) ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        switch ($act) {
            case 'find':
                # 查看
                return $this->index_audit_certification_ajax();
                break;
            case 'edit':
                # 修改
                //cp admin/user/authenticationCheck
                $id = I('post.user_id/d');
                $status = I('post.status');
                $cardMdl  = model('Common/credit/CreditIdentityCard');
                if ($status == 'success') {
                    $cert_info = $cardMdl->where(['user_id'=>$id])->find();
                    $certified_status = $cardMdl->certified_status($cert_info['true_name'], $cert_info['card_number'], $cert_info['user_id']);
                    if ($certified_status) {
                        ajaxReturn(['status'=>0,'msg'=>"操作失败: 此身份证实名认证的账号已认证或审核中"]);
                    }
                }
                $result = $cardMdl->setProcess($id,$status);
                if($result){
                    ajaxReturn(['status'=>1,'msg'=>"操作成功"]);
                }
                ajaxReturn(['status'=>0,'msg'=>$cardMdl->getError()]);
                    break;
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //实名认证审核 查看 
    public function index_audit_certification_ajax()
    {
        $mobile = I('mobile', '0');
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        if(!$userInfo){
            return $this->fetch(__FUNCTION__);
        }
        $uid = $userInfo['user_id'];
        $cardMdl  = model('Common/credit/CreditIdentityCard');

        $cardInfo = $cardMdl->where(['user_id' => $uid])->order('id desc')->find();
        $this->assign('data', $cardInfo);
        $this->assign('processList', $cardMdl->processList());
        return $this->fetch(__FUNCTION__);
    }

    //投诉管理 操作
    public function handle_complain_list()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        switch ($act) {
            case 'find':
                # 查看
                return $this->index_complain_list_ajax();
                break;
            case 'edit':
                # 投诉审核
                $complaint_id=I('id');
                $data['reply_content'] = I('remark');
                $data['status'] = I('status')?1:2;
                
                $complaintModel = model('Common/Complaint');
                $res = $complaintModel->check($complaint_id,$data['reply_content'],$data['status']);
                
                if($res){
                    ajaxReturn(['status'=>1,'msg'=>"操作成功"]);
                }
                ajaxReturn(['status'=>0,'msg'=>"操作失败"]);
                break;
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //投诉管理 查看 （复制Complaint/complain_list）
    public function index_complain_list_ajax()
    {
        $model = model('Common/Complaint');
        $p = I('p', '1');
        $mobile = I('mobile', '0');
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        if(!$userInfo){
            return $this->fetch(__FUNCTION__);
        }
        $uid = $userInfo['user_id'];
        $type = I('type',0);// 0 我的投诉 1 投诉我的  

        $where = [];

        $type ? $where['respondent_user_id']=$uid : $where['user_id']=$uid;
     
        $lists = $model->with(['user','respondent_user','give_gift'])->where($where)->order('complaint_id desc')->page($p , '10')->select();
        // ps($model);

        $this->assign('lists', $lists);
        
        return $this->fetch(__FUNCTION__);
    }

    //会员升级记录表 操作
    public function handle_member_upgrade()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        switch ($act) {
            case 'find':
                # 查看
                return $this->index_member_upgrade_ajax();
                break;
            
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }


    //会员升级记录表 查看
    public function index_member_upgrade_ajax()
    {
        // $act = I('act','find');
        $mobile = I('mobile','');
        $page = I('p', '1');

        if ( !$mobile){ 
            ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        }else{

            $userLevelLogMdl = model('Common/UserLevelLog');
            $userMdl = model('Common/Users');
            $userLevelMdl = model('Common/UserLevel');

            $where   = [];
            $user_id = $userMdl->getUserIdByMobile($mobile);
            // if(!$user_id){
            //     ajaxReturn(['status'=>0,'msg'=>'搜索不到该用户']);
            // }

            $where['user_id'] = $user_id;

            $count = $userLevelLogMdl->where($where)->count();
            $Page = new Page($count, 10);
            $show = $Page->show();
            $res = $userLevelLogMdl->where($where)->order('id desc')->page($page . ',' . $Page->listRows)->select();
            // p(collection($res)->toArray());
            if ($res) {
                foreach ($res as $val) {
                    $val['before_level_name'] = $userLevelMdl->where(['level_id' =>$val['before_level_id']])->value('show_name');
                    $val['after_level_name']  = $userLevelMdl->where(['level_id' =>$val['level_id']])->value('show_name');
                    $list[] = $val;
                }
            }
            
            // p(collection($list)->toArray());
            $this->assign('list', $list);
        }
        return $this->fetch(__FUNCTION__);
    }

    //会员上级修改记录表 操作
    public function handle_superior_revision()
    {
        $act = I('act','find');
        $mobile = I('mobile','0');
        switch ($act) {
            case 'find':
                # 查看
                return $this->index_superior_revision_ajax();
                break;
            
            default:
                ajaxReturn(['status'=>0,'msg'=>'act类型不正确']);
                break;
        }
    }

    //会员上级修改记录 查看 
    public function index_superior_revision_ajax()
    {
        // $act = I('act','find');
        $mobile = I('mobile','');
        $page = I('p','1','intval');

        if ( !$mobile){ 
            ajaxReturn(['status'=>0,'msg'=>'手机号不能为空']);
        }else{

            $userLeaderLogMdl = model('Common/ChangeFirstLeaderLog');
            $userMdl = model('Common/Users');
            // $userLevelMdl = model('Common/UserLevel');

            $where   = [];
            $user_id = $userMdl->getUserIdByMobile($mobile);

            // if(!$user_id){
            //     ajaxReturn(['status'=>0,'msg'=>'搜索不到该用户']);
            // }

            $where['user_id'] = $user_id;

            $count = $userLeaderLogMdl->where($where)->count();
            $Page = new Page($count, 10);
            $show = $Page->show();
            $res = $userLeaderLogMdl->where($where)->order('id desc')->page($page . ',' . $Page->listRows)->select();
            if ($res) {
                foreach ($res as $val) {
                    $beforeUser = $userMdl->getUserInfo($val['before_first_leader_id']);
                    $afterUser  =  $userMdl->getUserInfo($val['after_first_leader_id']);
                    $val['before_first_leader_mobile'] = $beforeUser['mobile'];
                    $val['after_first_leader_mobile']  = $afterUser['mobile'];
                    $list[] = $val;
                }
            }
            
            $this->assign('list', $list);
        }
        return $this->fetch(__FUNCTION__);
    }






}