<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * 2015-11-21
 */

namespace app\mobile\controller;

use app\common\model\TeamFound;
use app\common\logic\UsersLogic;
use app\common\logic\OrderLogic;
use app\common\logic\CommentLogic;
use app\common\logic\CompressedImageLogic;
use app\common\model\Order as OrderModel;
use common\util\File;
use think\Page;
use think\Request;
use think\db;

class Order extends MobileBase
{

    public $user_id = 0;
    public $user = array();

    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $user = session('user');
            $user = M('users')->where("user_id", $user['user_id'])->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
            $this->assign('user', $user); //存储用户信息
            $this->assign('user_id', $this->user_id);
        } else {
            header("location:" . U('User/login'));
            exit;
        }
        $order_status_coment = array(
            'WAITPAY' => '待付款 ', //订单查询状态 待支付
            'WAITSEND' => '待发货', //订单查询状态 待发货
            'WAITRECEIVE' => '待收货', //订单查询状态 待收货
            'WAITCCOMMENT' => '待评价', //订单查询状态 待评价
        );
        $this->assign('order_status_coment', $order_status_coment);
    }

    /**
     * User: Simony
     * Function:订单列表
     * @return mixed
     */
    public function order_list()
    {
        //发货后xx天未评价的自动好评
        $res = model('Common/credit/CreditUserLog')->autoComment();

        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);


        $giveGiftMdl = model('Common/giveGift');
        $usersMdl = model('Common/Users');
        $userLevelMdl = model('Common/UserLevel');
        //条件搜索
        $type = I('get.type', 'give'); // give:送礼 collect:收礼
        $status = I('get.order_type','');
        $mobile = I('get.mobile','');
        $page = I('get.p','1','intval');
        
            $dataName = array(
                'giveTitle' => lang('Order_order_list_chuangguanjilu'),
                'collectTitle' => lang('Order_order_list_chuangguanshenhejilu'),
                'giveTypeName' => lang('Order_order_list_chuangguan'),
                'collectTypeName' => lang('Order_order_list_chuangguan'),
            );
            $statusStr = [
                '0'  => lang('Order_order_list_daiqueren'),
                '1'  => lang('Order_order_list_yiqueren'),
                '-1' => lang('Order_order_list_yijujue'),
            ]; 

        $title = $dataName[$type . 'Title'];
        $typeName = $dataName[$type . 'TypeName'];
        $field = $type . '_user_id';
        $where[$field] = $this->user_id;
        if ($mobile) {
            $ia = check_mobile($mobile);
            if($ia == false){
               $this->error(lang('Index_sousuotishi'));
            }
            
            $user_ids = $usersMdl->getUserIdByMobileOrNickname($mobile);
            if($type == 'collect'){
                $where['give_user_id'] = ['in',$user_ids];
            }else{
                $where['collect_user_id'] = ['in',$user_ids];
            }
        }
        $status == 1 && $where['status'] = 0;
        $status == 2 && $where['status'] = 1;
        $status == 3 && $where['status'] = -1;
        
        $count = $giveGiftMdl->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $order_str = "addtime DESC";
        $order_list = $giveGiftMdl->order($order_str)->where($where)->page($page . ',' . $Page->listRows)->select();
        $whereUserField = $type == 'give' ? 'collect_user_id' : 'give_user_id';
        
        $user_ids2 = [];
        foreach ($order_list as &$list) {
            $_user = $usersMdl->getByPk($list[$whereUserField]);
            // $list['name'] = $_user['name'] ? $_user['name'] : $_user['mobile'];
            $list['name'] = $_user['nickname'] ? $_user['nickname'] : $_user['mobile'];
            $list['mobile'] = $_user['mobile'];
            $list['head_pic'] = $_user['head_pic'];
            $list['paytypeWx'] = model('Common/Paytype')->where(['user_id'=>$list[$whereUserField],'type'=>2])->value('account');  
            $list['paytypeZfb'] = model('Common/Paytype')->where(['user_id'=>$list[$whereUserField],'type'=>1])->value('account');  
            //要闯的关卡等级（用户要升的等级）
            $give_user_level_next = $list['give_user_level'] + 1;
            $list['checkpoint_level_name'] = $userLevelMdl->where(['level_id' => $give_user_level_next])->value('show_name');
           
            $user_ids2[] = $list['collect_user_id'];
        }
         
        /**
         * [$user_shop_manage 用户个人商城管理]
         */
        $childShopLink = model('Common/UserShopManage')->getChildShopLink($user_ids2);//子商城 or e店 or 第三方链接
        $data = [
            'lists' => $order_list,
            'active' => 'order_list',
            'active_status' => I('get.type'),
            'title' => $title,
            'typeName' => $typeName,
            'statusStr' => $statusStr,
            'user_id'   => $this->user_id,
            'childShopLink' => $childShopLink,
        ];
        $this->assign($data);
        if (IS_AJAX) return $this->fetch('order/ajax_order_list');
        return $this->fetch('order/order_list');
    }

    /**
     * 订单详情
     * @return mixed
     */
    public function order_detail()
    {
        $id = input('id/d', 0);
        $type = I('type/d');
        $wait_receive = input('waitreceive/s', '');
        $give_gift_model = model('GiveGift');
        $order = $give_gift_model->with('user_address')->where(['id' => $id])->find();
        $usersModel = M('users');
        
        if($type == 2){  //送礼人
            $collect_user = $usersModel->where(['user_id' => $order['give_user_id']])->find();
            $collect_user_id = $order['give_user_id'];
            $collectUserLevel = model('Common/users/UserLevel')->getLevelInfo($order['give_user_level'] + 1);
        }else{  //收礼人
            $collect_user = $usersModel->where(['user_id' => $order['collect_user_id']])->find();
            // p($collect_user);
            $collect_user_id = $order['collect_user_id'];
            $collectUserLevel = model('Common/users/UserLevel')->getLevelInfo($order['give_user_level']+ 1);
        }   
     

        $collect_user['paytype'] = model('common/Paytype')->getCol('id,account,type',['user_id'=>$collect_user_id]); 

        $give_user_op = ($this->user_id == $order['collect_user_id'] && $order['status'] == 0) ? true : false; //是否有收礼操作权限
        
    
        $data = [
            'give_user_op' => $give_user_op,
            'collect_user' => $collect_user,
            'order_info' => $order,
            'collectUserLevel' => $collectUserLevel,
        ];
        // p($data);
        $this->assign($data);

        if ($wait_receive) {  //待收货详情
            return $this->fetch('wait_receive_detail');
        }

        return $this->fetch();
    }

    /**
     * User: Simony 审核
     * Function:更新
     */
    public function UpdateOrderStatus()
    {
        $id = I('post.order_id', 0);
        $type = I('type');
        $data['remark'] = I('post.remark');
        $handle_user_id = I('post.handle_user_id',0);

        if ($type == 'refuse') {
            $data['status'] = -1;
            $url = '';
        } else {
            $data['status'] = 1;
            $url = U("Mobile/Order/send",['id'=>$id]);
        }

        $give_gift_model = model('Common/GiveGift');
        $res = $give_gift_model->changeStatusByMobile($id,$data['status'],$data['remark'],$this->user_id,$handle_user_id);

        if ($res) {
            $collect_type = $give_gift_model->where(['id' => $id])->value('collect_type');
            if($collect_type == 1){
                $this->ajaxReturn(['status' => 1, 'msg' => '成功','data'=>$collect_type]);
            }else{
                $this->ajaxReturn(['status' => 1, 'msg' => '成功','url'=>$url]);
            }
            // $collect_type == 1 ? $this->ajaxReturn(['status' => 1, 'msg' => '成功']) : $this->ajaxReturn(['status' => 1, 'msg' => '成功','url'=>$url]);
        } else {
            $this->ajaxReturn(['status' => -1, 'msg' => $give_gift_model->getError()]);
        }
        // $this->ajaxReturn(['status' => 1, 'msg' => '操作成功','url'=>"Mobile/Order/send"]);
       
        // $where = [
        //     'id' => $id,
        //     // 'collect_user_id' => $this->user_id,
        //     'status' => 0
        // ];
        // $res = model('Common/UserMobileAdmin')->isMobileAdminUser($this->user_id);
        // if(!$res) $where['collect_user_id'] = $this->user_id;

        // $order = $give_gift_model->where($where)->find();

        // if (!$order) $this->ajaxReturn(['status' => -1, 'msg' => '没有权限']);
        
        

        
        // $data['confirmtime'] = time();

        // trans();
        // $res = $give_gift_model->where($where)->save($data);    //更新状态
      
        // /**
        //  * [$nextUserInfo 获取下个等级信息]
        //  */
        // $nextUserInfo = model('Common/users/UserLevel')->getLevelInfo($order['give_user_level'] + 1);

        // if(!empty($nextUserInfo['need_cross_level']))
        // {
        //     /**
        //      * [判断订单数量]
        //      */
        //     $orderCount = [
        //         'give_user_level' => $order['give_user_level'],
        //         'give_user_id' => $order['give_user_id'],
        //         'status' => 1,
        //         'give_type' => 1,
        //     ];

        //     $croosOrderCount = [
        //         'give_user_level' => $order['give_user_level'],
        //         'give_user_id' => $order['give_user_id'],
        //         'status' => 1,
        //         'give_type' => 2,
        //     ];


        //     $checkOrderCount  = (int)$give_gift_model->where($orderCount)->count();
        //     $checkCroosOrderCount  = (int)$give_gift_model->where($croosOrderCount)->count();

           
        //     if( ($checkOrderCount + $checkCroosOrderCount) != 2 )
        //     {
        //         trans('commit');

        //         if($type == 'refuse') {
        //             $this->ajaxReturn(['status' => 1, 'msg' => '成功']);

        //         }else{
        //             $this->ajaxReturn(['status' => 1, 'msg' => '成功','url'=>U("Mobile/Order/send",['id'=>$id])]);
                    
        //         }        
        //         // if($data['status']==1){
        //         //     $this->ajaxReturn(['status' => 1, 'msg' => '成功','url'=>U("Mobile/Order/send",['id'=>$id])]);
        //         // }else{
        //         //     $this->ajaxReturn(['status' => 1, 'msg' => '成功']);
        //         // }
        //     }elseif (empty($res)) {
        //         trans('rollback');
        //         $this->ajaxReturn(['status' => -1, 'msg' => '操作失败']);
        //     }
        // }
       

        // //更新送礼人等级
        // if ($data['status'] == 1) {
        //     $give_user = M('users')->where(['user_id' => $order['give_user_id']])->find();
        //     $level_info = M('user_level')->where('level_id', 'GT', $give_user['level'])->limit(1)->select(); //用户等级信息
        //     $next_user_level = $level_info[0]; //下一个等级信息
        //     $userLevelLogMdl = model('Common/users/UserLevelLog');
        //     $res1 = $userLevelLogMdl->addData($order['give_user_id'], $next_user_level['level_id'], '', '');
        // }
        // if ( (isset($res1) && $res1 && $res) || (!isset($res1) && $res) ) {
        //     trans('commit');
        //     if($data['status']==1){
        //         $this->ajaxReturn(['status' => 1, 'msg' => '成功','url'=>U("Mobile/Order/send",['id'=>$id])]);
        //     }else{
        //         $this->ajaxReturn(['status' => 1, 'msg' => '成功']);
        //     }
        // } else {
        //     trans('rollback');
        //     $this->ajaxReturn(['status' => -1, 'msg' => '操作失败']);
        // }
        // // $this->ajaxReturn(['status' => 1, 'msg' => '操作成功','url'=>"Mobile/Order/send"]);
        // exit;

    }

    public function order_detail_old()
    {
        $id = input('id/d', 0);
        $Order = new OrderModel();
        $wait_receive = input('waitreceive/s', '');


        $order = $Order->with('OrderGoods')->where(['order_id' => $id, 'user_id' => $this->user_id])->find();
//         p($order);exit;
        if (!$order) {
            $this->error('没有获取到订单信息');
        }
        $order = set_btn_order_status($order->toArray());  // 添加属性  包括按钮显示属性 和 订单状态显示属性
        //获取订单
        if ($order['prom_type'] == 5) {   //虚拟订单
            $this->redirect(U('virtual/virtual_order', ['order_id' => $id]));
        }


        if ($wait_receive) {  //待收货详情
            return $this->fetch('wait_receive_detail');
        }

        return $this->fetch();
    }

    /**
     * 物流信息
     * @return mixed
     */
    public function express()
    {
        $order_id = I('get.order_id/d', 0);
        $order_goods = M('order_goods')->where("order_id", $order_id)->select();
        if (empty($order_goods) || empty($order_id)) {
            $this->error('没有获取到订单信息');
        }
        $delivery = M('delivery_doc')->where("order_id", $order_id)->find();
        $this->assign('order_goods', $order_goods);
        $this->assign('delivery', $delivery);
        return $this->fetch();
    }

    /**
     * 取消订单
     */
    public function cancel_order()
    {
        $id = I('get.id/d');
        //检查是否有积分，余额支付
        $logic = new OrderLogic();
        $data = $logic->cancel_order($this->user_id, $id);
        $this->ajaxReturn($data);
    }

    /**
     * 取消详情
     * @return \think\mixed
     */
    public function cancel_order_info()
    {
        $order_id = I('order_id/d', 0);
        $Orders = new OrderModel();
        $order = $Orders->where(array('order_id' => $order_id, 'order_status' => 3, 'pay_status' => ['gt', 0]))->find();

        if (!$order) {
            $this->error('非法操作！');
        }

        $this->assign('order', $order);
        return $this->fetch();
    }


    /**
     * 确定收货成功
     */
    public function order_confirm()
    {
        $id = I('id/d', 0);
        $data = confirm_order($id, $this->user_id);
        if (request()->isAjax()) {
            $this->ajaxReturn($data);
        }
        if ($data['status'] != 1) {
            $this->error($data['msg'], U('Mobile/Order/order_list'));
        } else {
            $model = new UsersLogic();
            $order_goods = $model->get_order_goods($id);
            $this->assign('order_goods', $order_goods);
            return $this->fetch();
            exit;
        }
    }

    //订单支付后取消订单
    public function refund_order()
    {
        $order_id = I('get.order_id/d');

        $order = M('order')
            ->field('order_id,pay_code,pay_name,user_money,integral_money,coupon_price,order_amount,consignee,mobile,prom_type')
            ->where(['order_id' => $order_id, 'user_id' => $this->user_id])
            ->find();

        $this->assign('user', $this->user);
        $this->assign('order', $order);
        return $this->fetch();
    }

    //申请取消订单
    public function record_refund_order()
    {
        $order_id = input('post.order_id', 0);
        $user_note = input('post.user_note', '');
        $consignee = input('post.consignee', '');
        $mobile = input('post.mobile', '');

        $logic = new \app\common\logic\OrderLogic;
        $return = $logic->recordRefundOrder($this->user_id, $order_id, $user_note, $consignee, $mobile);

        $this->ajaxReturn($return);
    }

    /**
     * 申请退货
     */
    public function return_goods()
    {
        $rec_id = I('rec_id', 0);
        $return_goods = Db::name('return_goods')->where(array('rec_id' => $rec_id))->find();
        if (!empty($return_goods)) {
            $this->error('已经提交过退货申请!', U('Order/return_goods_info', array('id' => $return_goods['id'])));
        }
        $order_goods = Db::name('order_goods')->where(array('rec_id' => $rec_id))->find();
        $order = Db::name('order')->where(array('order_id' => $order_goods['order_id'], 'user_id' => $this->user_id))->find();
        $auto_service_date = tpCache('shopping.auto_service_date'); //申请售后时间段
        $confirm_time = strtotime("-$auto_service_date day");
        if ($order['add_time'] < $confirm_time) {
            return $this->fetch('return_error');
        }
        if (empty($order)) $this->error('非法操作');
        if (IS_POST) {
            $model = new OrderLogic();
            $res = $model->addReturnGoods($rec_id, $order);  //申请售后
            if ($res['status'] == 1) $this->success($res['msg'], U('Order/return_goods_list'));
            $this->error($res['msg']);
        }
        $region_id[] = tpCache('shop_info.province');
        $region_id[] = tpCache('shop_info.city');
        $region_id[] = tpCache('shop_info.district');
        $region_id[] = 0;
        $return_address = Db::name('region')->where("id in (" . implode(',', $region_id) . ")")->getField('id,name');
        $this->assign('return_address', implode(',', $return_address));
        $this->assign('return_type', C('RETURN_TYPE'));
        $this->assign('goods', $order_goods);
        $this->assign('order', $order);
        return $this->fetch();
    }

    /**
     * 退换货列表
     */
    public function return_goods_list()
    {
        //退换货商品信息
        $count = Db::name('return_goods')->where("user_id", $this->user_id)->count();
        $pagesize = C('PAGESIZE');
        $page = new Page($count, $pagesize);
        $list = Db::name('return_goods')->alias('rg')
            ->field('rg.*,og.goods_name,og.spec_key_name')
            ->join('order_goods og', 'rg.rec_id=og.rec_id', 'LEFT')
            ->where(['rg.user_id' => $this->user_id])
            ->order("rg.id desc")
            ->limit("{$page->firstRow},{$page->listRows}")
            ->select();
        $state = C('REFUND_STATUS');
        $this->assign('list', $list);
        $this->assign('state', $state);
        $this->assign('page', $page->show());// 赋值分页输出
        if (I('is_ajax')) {
            return $this->fetch('ajax_return_goods_list');
            exit;
        }
        return $this->fetch();
    }

    /**
     *  退货详情
     */
    public function return_goods_info()
    {
        $id = I('id/d', 0);
        $return_goods = M('return_goods')->where("id = $id")->find();
        if (empty($return_goods)) {
            $this->error('参数错误');
        }
        $return_goods['seller_delivery'] = unserialize($return_goods['seller_delivery']);  //订单的物流信息，服务类型为换货会显示
        $return_goods['delivery'] = unserialize($return_goods['delivery']);  //订单的物流信息，服务类型为换货会显示
        if ($return_goods['imgs'])
            $return_goods['imgs'] = explode(',', $return_goods['imgs']);
        $goods = M('order_goods')->where("rec_id = {$return_goods['rec_id']} ")->find();
        $this->assign('state', C('REFUND_STATUS'));
        $this->assign('return_type', C('RETURN_TYPE'));
        $this->assign('goods', $goods);
        $this->assign('return_goods', $return_goods);
        return $this->fetch();
    }
    /**
     * 去发货
     */
    public function send()
    {
        $id    = input('id/d', 0); 
        $voucher_type = I('voucher_type/d',1);
        $voucher_order_num = I('voucher_order_num','','trim');
        $shipping_name = I('shipping_name','','trim');
        if (IS_POST) {

            $giftMdl = model('Common/GiveGift');
            $order   = $giftMdl->where(['id' => $id])->find();
            if (!$order) {
                $this->error('不存在该记录');
            }
           
            if ($_FILES['voucher']['tmp_name'] && $voucher_type == 1) {
                // p($_FILES);
                $file = $this->request->file('voucher');
                $image_upload_limit_size = config('image_upload_limit_size');
                $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
                $dir = UPLOAD_PATH . 'voucher/';
                // p($dir);
                if (!($_exists = file_exists(UPLOAD_PATH))) {
                    $isMk = mkdir(UPLOAD_PATH);
                }
                if (!($_exists = file_exists($dir))) {
                    $isMk = mkdir($dir);
                }
                $parentDir = date('Ymd');
                $info = $file->validate($validate)->move($dir, true);
                if ($info) {
                    $voucher_pic = '/'.$dir . $parentDir . '/' . $info->getFilename();

                    //图片压缩 并上传阿里云
                    $ossClient = new \app\common\logic\OssLogic;
                    $uploadImg = $_SERVER['DOCUMENT_ROOT'];
                    $CompressedImageMdl = new CompressedImageLogic();
                    $CompressedImageMdl->compressedImage($uploadImg.DIRECTORY_SEPARATOR.$voucher_pic,$uploadImg.$voucher_pic,800);
                    $file = $_FILES['voucher'];
                    list($image,$type) = explode('/', $file['type']);
                    $object = uniqid('sale_') . time(). '.' . $type;
                    $object = 'order/' . $object;
                    // $res= $ossClient->uploadFile($file['tmp_name'],$object);//上传
                    $res= $ossClient->uploadFile($uploadImg.$voucher_pic,$object);//上传
                    $post['voucher_pic'] = $res['info']['url'];//获取图片路径
                    unset($info);  //开始释放变量
                    unlink($uploadImg.$voucher_pic);//删除文件
                } else {
                    $this->error($file->getError());//上传错误提示错误信息
                }
            }
            $post['voucher_type']  = $voucher_type;
            if($voucher_type == 2){
                $post['voucher_order_num']  = $voucher_order_num;
                $post['shipping_name']      = $shipping_name;
            }

            $post['delivery_type'] = 1;
            $post['collect_type']  = 2;
            $post['voucher_time']  = time();
            $res = $giftMdl->where(['id'=>$id])->update($post);
            if ($res) {
                // $model = model('Common/credit/CreditUserLog');
                // $res = $model->checkVoucherPic();//根据发货凭证上传时间 操作信用评分
                // $this->success('上传成功',U('Mobile/Order/order_list',['type'=>'collect']));
                // exit($this->success('上传成功',U('Mobile/Index/index')));
                $this->redirect(U('Mobile/Order/order_list',['type'=>'collect']));

            }else{
                $this->error('上传失败');
            }

        }
        $this->assign('id',$id);
        return $this->fetch();
    }
     /**
     * 保存上传的图片
     * @param $file
     * @param $save_path
     * @return array
     */
    public function saveUploadImage($file, $save_path)
    {
        $return_url = '';
        $state = "SUCCESS";
        $new_path = $save_path.date('Y').'/'.date('m-d').'/';

        $waterPaths = ['goods/', 'water/']; //哪种路径的图片需要放oss

        // if (in_array($save_path, $waterPaths) && tpCache('oss.oss_switch')) {
        if (!empty($save_path)) {
            //商品图片可选择存放在oss
            $object = UPLOAD_PATH.$new_path.md5(time()).'.'.pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);
            $ossClient = new \app\common\logic\OssLogic;
            $return_url = $ossClient->uploadFile($file->getRealPath(), $object);
            $file = null;//关闭文件句柄，不然无法删除
            @unlink($real_path); //上传后删除
            if (!$return_url) {
                $state = "ERROR" . $ossClient->getError();
                $return_url = '';
            }
        } else {
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->rule(function ($file) {
                return  md5(mt_rand()); // 使用自定义的文件保存规则
            })->move(UPLOAD_PATH.$new_path);
            if (!$info) {
                $state = "ERROR" . $file->getError();
            } else {
                $return_url = '/'.UPLOAD_PATH.$new_path.$info->getSaveName();
                $pos = strripos($return_url,'.');
                $filetype = substr($return_url, $pos);
                if ($save_path =='goods/' && $filetype != '.gif') {  //只有商品图才打水印，GIF格式不打水印
                    $this->waterImage(".".$return_url);  //水印
                }

                $state = $this->uploadWechatImage($save_path, $return_url);
                if ($state != 'SUCCESS') {
                    $info = null;//关闭文件句柄，不然无法删除
                    @unlink('.' . $return_url);
                    $return_url = '';
                }
            }
        }

        return [
            'state' => $state,
            'url'   => $return_url
        ];
    }

    /**
     * 修改退货状态，发货
     */
    public function checkReturnInfo()
    {
        $data = I('post.');
        $data['delivery'] = serialize($data['delivery']);
        $data['status'] = 2;
        $res = M('return_goods')->where(['id' => $data['id'], 'user_id' => $this->user_id])->save($data);
        if ($res !== false) {
            $this->ajaxReturn(['status' => 1, 'msg' => '发货提交成功', 'url' => U('Mobile/Order/return_goods_info', ['id' => $data['id']])]);
        } else {
            $this->ajaxReturn(['status' => -1, 'msg' => '提交失败', 'url' => '']);
        }
    }

    public function return_goods_refund()
    {
        $order_sn = I('order_sn');
        $where = array('user_id' => $this->user_id);
        if ($order_sn) {
            $where['order_sn'] = $order_sn;
        }
        $where['status'] = 5;
        $count = M('return_goods')->where($where)->count();
        $page = new Page($count, 10);
        $list = M('return_goods')->where($where)->order("id desc")->limit($page->firstRow, $page->listRows)->select();
        $goods_id_arr = get_arr_column($list, 'goods_id');
        if (!empty($goods_id_arr))
            $goodsList = M('goods')->where("goods_id in (" . implode(',', $goods_id_arr) . ")")->getField('goods_id,goods_name');
        $this->assign('goodsList', $goodsList);
        $state = C('REFUND_STATUS');
        $this->assign('list', $list);
        $this->assign('state', $state);
        $this->assign('page', $page->show());// 赋值分页输出
        return $this->fetch();
    }

    /**
     * 取消售后服务
     * @author lxl
     * @time 2017-4-19
     */
    public function return_goods_cancel()
    {
        $id = I('id', 0);
        if (empty($id)) $this->ajaxReturn(['status' => -1, 'msg' => '参数错误']);
        $return_goods = M('return_goods')->where(array('id' => $id, 'user_id' => $this->user_id))->find();
        if (empty($return_goods)) $this->ajaxReturn(['status' => -1, 'msg' => '参数错误']);
        $res = M('return_goods')->where(array('id' => $id))->save(array('status' => -2, 'canceltime' => time()));
        if ($res !== false) {
            $this->ajaxReturn(['status' => 1, 'msg' => '取消成功']);
        } else {
            $this->ajaxReturn(['status' => -1, 'msg' => '取消失败']);
        }
    }

    /**
     * 换货商品确认收货
     * @author lxl
     * @time  17-4-25
     * */
    public function receiveConfirm()
    {
        $return_id = I('return_id/d');
        $return_info = M('return_goods')->field('order_id,order_sn,goods_id,spec_key')->where('id', $return_id)->find(); //查找退换货商品信息
        $update = M('return_goods')->where('id', $return_id)->save(['status' => 3]);  //要更新状态为已完成
        if ($update) {
            M('order_goods')->where(array(
                'order_id' => $return_info['order_id'],
                'goods_id' => $return_info['goods_id'],
                'spec_key' => $return_info['spec_key']))->save(['is_send' => 2]);  //订单商品改为已换货
            $this->success("操作成功", U("Order/return_goods_info", array('id' => $return_id)));
        }
        $this->error("操作失败");
    }

    /**
     *  评论晒单
     * @return mixed
     */
    public function comment()
    {
        $user_id = $this->user_id;
        $status = I('get.status');
        $logic = new CommentLogic;
        $data = $logic->getComment($user_id, $status); //获取评论列表
        $this->assign('page', $data['page']);// 赋值分页输出
        $this->assign('comment_page', $data['page']);
        $this->assign('comment_list', $data['result']);
        $this->assign('active', 'comment');
        if (I('is_ajax')) {
            return $this->fetch('ajax_comment_list');
        }
        return $this->fetch();
    }

    /**
     *添加评论
     */
    public function add_comment()
    {
        if (IS_POST) {
            // 晒图片
            $files = request()->file('comment_img_file');
            $save_url = UPLOAD_PATH . 'comment/' . date('Y', time()) . '/' . date('m-d', time());
            if ($files) {
                foreach ($files as $file) {
                    // 移动到框架应用根目录/public/uploads/ 目录下
                    $image_upload_limit_size = config('image_upload_limit_size');
                    $info = $file->rule('uniqid')->validate(['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'])->move($save_url);
                    if ($info) {
                        // 成功上传后 获取上传信息
                        // 输出 jpg
                        $comment_img[] = '/' . $save_url . '/' . $info->getFilename();
                    } else {
                        // 上传失败获取错误信息
                        $this->ajaxReturn(['status' => -1, 'msg' => $file->getError()]);
                    }
                }
            }
            if (!empty($comment_img)) {
                $add['img'] = serialize($comment_img);
            }

            $user_info = session('user');
            $logic = new UsersLogic();
            $add['rec_id'] = I('rec_id/d');
            $add['goods_id'] = I('goods_id/d');
            $add['email'] = $user_info['email'];
            $hide_username = I('hide_username');
            if (empty($hide_username)) {
                $add['username'] = $user_info['nickname'];
            }
            $add['is_anonymous'] = $hide_username;  //是否匿名评价:0不是\1是
            $add['order_id'] = I('order_id/d');
            $add['service_rank'] = I('service_rank');
            $add['deliver_rank'] = I('deliver_rank');
            $add['goods_rank'] = I('goods_rank');
            $add['is_show'] = 1; //默认显示
            $add['content'] = I('content');
            $add['add_time'] = time();
            $add['ip_address'] = request()->ip();
            $add['user_id'] = $this->user_id;

            //添加评论
            $row = $logic->add_comment($add);
            if ($row['status'] == 1) {
                $this->ajaxReturn(['status' => 1, 'msg' => '评论成功', 'url' => U('/Mobile/Order/comment', ['status' => 1])]);
            } else {
                $this->ajaxReturn(['status' => -1, 'msg' => $row['msg']]);
            }
        }
        $rec_id = I('rec_id/d');
        $order_goods = M('order_goods')->where("rec_id", $rec_id)->find();
        $order_info = Db::name('order')->where("order_id", $order_goods['order_id'])->find();
        $this->assign('order_goods', $order_goods);
        $this->assign('rec_id', $rec_id);
        $this->assign('order_info', $order_info);
        return $this->fetch();
    }

    /**
     * 待收货列表
     */
    public function wait_receive()
    {
        $where = ' user_id=' . $this->user_id;
        //条件搜索
        if (I('type') == 'WAITRECEIVE') {
            $where .= C(strtoupper(I('type')));
        }
        $count = M('order')->where($where)->count();
        $pagesize = C('PAGESIZE');
        $Page = new Page($count, $pagesize);
        $show = $Page->show();
        $order_str = "order_id DESC";
        $order_list = M('order')->order($order_str)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        //获取订单商品
        $model = new UsersLogic();
        foreach ($order_list as $k => $v) {
            $order_list[$k] = set_btn_order_status($v);  // 添加属性  包括按钮显示属性 和 订单状态显示属性
            $data = $model->get_order_goods($v['order_id']);
            $order_list[$k]['goods_list'] = $data['result'];
        }

        //统计订单商品数量
        foreach ($order_list as $key => $value) {
            $count_goods_num = 0;
            foreach ($value['goods_list'] as $kk => $vv) {
                $count_goods_num += $vv['goods_num'];
            }
            $order_list[$key]['count_goods_num'] = $count_goods_num;
            //订单物流单号
            $invoice_no = M('DeliveryDoc')->where("order_id", $value['order_id'])->getField('invoice_no', true);
            $order_list[$key][invoice_no] = implode(' , ', $invoice_no);
        }
        $this->assign('page', $show);
        $this->assign('order_list', $order_list);
        if ($_GET['is_ajax']) {
            return $this->fetch('ajax_wait_receive');
            exit;
        }
        return $this->fetch();
    }

    /**
     * 评论详情
     * @return mixed
     */
    public function comment_info()
    {
        $commentLogic = new \app\common\logic\CommentLogic;
        $comment_id = I('comment_id/d');
        $res = $commentLogic->getCommentInfo($comment_id);
        if (empty($res)) {
            $this->error('参数错误！！');
        }
        if (!empty($res['comment_info']['img'])) $res['comment_info']['img'] = unserialize($res['comment_info']['img']);
        $user = get_user_info($res['comment_info']['user_id']);
        $res['comment_info']['nickname'] = $user['nickname'];
        $res['comment_info']['head_pic'] = $user['head_pic'];
        $this->assign('comment_info', $res['comment_info']);
        $this->assign('comment_id', $res['comment_info']['comment_id']);
        $this->assign('reply', $res['reply']);
        $this->assign('user', $this->user);
        return $this->fetch();
    }

    /**
     * 评论别的用户评论
     */
    public function replyComment()
    {
        $data = I('post.');
        $data['reply_time'] = time();
        $data['deleted'] = 0;
        $return = Db::name('reply')->add($data);
        if ($return) {
            Db::name('comment')->where(['comment_id' => $data['comment_id']])->setInc('reply_num');
            $data['reply_time'] = date('Y-m-d H:m', $data['reply_time']);
            $this->ajaxReturn(['status' => 1, 'msg' => '评论成功！', 'result' => $data]);
            exit;
        } else {
            $this->ajaxReturn(['status' => 0, 'msg' => "评论失败"]);
        }
    }

    /**
     *  点赞
     */
    public function ajaxZan()
    {
        $comment_id = I('post.comment_id/d');
        $user_id = $this->user_id;
        $comment_info = M('comment')->where(array('comment_id' => $comment_id))->find();  //获取点赞用户ID
        $comment_user_id_array = explode(',', $comment_info['zan_userid']);
        if (in_array($user_id, $comment_user_id_array)) {  //判断用户有没点赞过
            $result = ['status' => 0, 'msg' => '您已经点过赞了~', 'result' => ''];
        } else {
            array_push($comment_user_id_array, $user_id);  //加入用户ID
            $comment_user_id_string = implode(',', $comment_user_id_array);
            $comment_data['zan_num'] = $comment_info['zan_num'] + 1;  //点赞数量加1
            $comment_data['zan_userid'] = $comment_user_id_string;
            M('comment')->where(array('comment_id' => $comment_id))->save($comment_data);
            $result = ['status' => 1, 'msg' => '点赞成功~', 'result' => ''];
        }
        exit(json_encode($result));
    }

    //闯关评价
    public function toComment()
    {
        $id = I('post.id/d');
        $comment = I('post.comment/d');// 1.好评 2中评 3差评 
        $log_id = I('post.log_id/d');
        if(!$id || !$comment) ajaxReturn(['status'=>0,'msg'=>'参数错误！！']);
        if(in_array($comment, [1,2,3])){
            $model = model('Common/credit/CreditUserLog');
            $ggMdl = model("GiveGift");
            $user_id = $ggMdl->where(['id'=>$id])->value('collect_user_id');
            if($row=$model->where(['action_type'=>['in',[1,2,3]],'source_id'=>$id,'source_table'=>'give_gift'])->find()){
                $res = $ggMdl->where(['id'=>$id])->update(['comment_state'=>2]);
                $res = $model->commentAgain($row['id'],$user_id,$comment);//重新评论
                ajaxReturn($res);
            }else{
                $res = $ggMdl->where(['id'=>$id])->update(['comment_state'=>1]);
                $res = $model->addData($user_id,$comment,0,$id,'评论');
                if(!$res) ajaxReturn(['status'=>0,'msg'=>'评价失败','data'=>$model->getError()]);
                ajaxReturn(['status'=>1,'msg'=>'评价成功']);
            }
        }    
        ajaxReturn(['status'=>0,'msg'=>'comment:在1,2,3中']);
    }
         
}