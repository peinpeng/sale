<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * 2015-11-21
 */

namespace app\mobile\controller;


class Credit extends MobileBase
{
    /*
        * 初始化操作
        */
    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $session_user = session('user');
            $user = M('users')->where("user_id", $session_user['user_id'])->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
        } else {
            header("Location:" . U('Mobile/User/login'));
            exit;
        }
    }


    public function index()
    {

        $model = model('Common/credit/CreditUser');
        
        // $data = $model->getByPk($this->user_id);
        $data = $model->getInfo($this->user_id);

        $this->assign('credit_config_desc', tpCache('credit_config')['credit_config_desc']);

        $data && $this->assign($data->toArray());

        return $this->fetch();

    }

}
