<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * $Author: IT宇宙人 2015-08-10 $
 */
namespace app\mobile\controller;

use think\Db;
use app\common\model\WxNews;
 
class Article extends MobileBase
{
    /**
     * 文章内容页
     */
    public function detail()
    {
        $article_id = input('article_id/d', 1);
        $is_userRegAgreement = input('is_userRegAgreement/d',0);
        if($is_userRegAgreement && $is_userRegAgreement ==1){
            $article = Db::name('article')->where(['title'=>'拓客新零售用户注册协议'])->find();
            $title = '用户注册协议';
        }else{
            $article = Db::name('article')->where("article_id", $article_id)->find();
            $title = input('title',lang('User_problem_bangzhuzhongxin'));
        }
        $this->assign('title', $title);
        $this->assign('article', $article);
        return $this->fetch();
    }
    public function news()
    {
        $id = input('id');
        if (!$news = WxNews::get($id)) {
            $this->error('文章不存在了~', null, '', 100);
        }

        $news->content = htmlspecialchars_decode($news->content);
        $this->assign('news', $news);
        return $this->fetch();
    }
    
    public function agreement(){
    	$doc_code = I('doc_code','agreement');
    	$article = db::name('system_article')->where('doc_code',$doc_code)->find();
    	if(empty($article)) $this->error('抱歉，您访问的页面不存在！');
    	$this->assign('article',$article);
    	return $this->fetch();
    }

     /**
     * 拓客商学院列表页面
     */
    public function news_list()
    {
        return $this->fetch();
    }
     /**
     * 资讯/学习中心详情页
     */
    public function news_detail()
    {
        $first_leader = intval(input('first_leader', 0));//推荐人ID
        setcookie('share_user_id', $first_leader, null, '/');
        // 微信分享data
        $article_id = I('newsId', 0);
        $aticle_info = M('Article')->where(['article_id' => $article_id])->field('article_id, title, description, thumb')->find();
        $thumb_arr = explode(',', $aticle_info['thumb']);
        $aticle_info['thumb'] = $thumb_arr[0];
        $this->assign('aticle_info',$aticle_info);
        return $this->fetch();
    }

    /**
     * 拓客商学院分类api
     */
    public function newsSort() {
        //拓客商学院 
        $cat_name = '拓客商学院';
        $cat_id = M('ArticleCat')->where(['cat_name' => $cat_name, 'show_in_nav' => 1])->value('cat_id');

        //获取常见问题 成功案例 id
        $cat_name_learn = '学习中心';
        $cat_id_learn  = M('ArticleCat')->where(['cat_name' => $cat_name_learn, 'show_in_nav' => 1])->value('cat_id');

        $_article_cat  = M('ArticleCat')->where(['parent_id' => ['in',[$cat_id,$cat_id_learn]], 'show_in_nav' => 1])->order('sort_order ASC')->select();
        // $_article_cat = M('ArticleCat')->where(['parent_id' => $cat_id, 'show_in_nav' => 1])->order('sort_order ASC')->select();
        
        $article_cat = [];
        $result = [];
        foreach ($_article_cat as $k => $v) {
            $article_cat['newsSort'][$k]['sortId'] = $v['cat_id'];
            $article_cat['newsSort'][$k]['sortTitle'] = $v['cat_name'];
            $article_cat['newsSort'][$k]['sortEnTitle'] = $v['english_name'];
            $article_cat['newsSort'][$k]['link'] = $v['link']?:'';
        }
        $video_cat = ['sortId' => 10002, 'sortTitle' => '视频','sortEnTitle'=>lang('Business_college_index_shiping')];
        $music_cat = ['sortId' => 10001, 'sortTitle' => '音频','sortEnTitle'=>lang('Business_college_index_yinping')];
        $recommend_cat = ['sortId' => 10000, 'sortTitle' => '精选','sortEnTitle'=>lang('Business_college_index_jingxuan')];

        array_unshift($article_cat['newsSort'], $video_cat);
        array_unshift($article_cat['newsSort'], $music_cat);
        array_unshift($article_cat['newsSort'], $recommend_cat);
        $this->jsonReturn(1, '获取成功', $article_cat);
    }
    /**
     * 拓客商学院列表api
     */
    public function newsList() {
        $cat_id = intval(input('sortId', 0));//分类ID
        $cat_id = $cat_id ? $cat_id : 10000;
        $media_id  = intval(input('mediaId', 0));//音频或视频分类
        $curr_page = intval(input('currPage', 1));//当前页码
        $page_size = intval(input('pageSize', 10));//每页几条
        $data['currPage'] = $curr_page;
        $data['list'] = [];
        // 获取推荐资讯
        switch ($cat_id) {
            case 10000:
                $cat_name = '拓客商学院';
                $cat_id = M('ArticleCat')->where(['cat_name' => $cat_name, 'show_in_nav' => 1])->value('cat_id');
                // $cat_id = 1;//拓客商学院
                // 获取拓客商学院所有子类
                $sub_cats = M('ArticleCat')->where("parent_id = $cat_id")->column('cat_id');
                $map['is_recommend'] = 1;
                $map['is_open'] = 1;
                $map['cat_id'] = array('in', $sub_cats);
                $count = M('Article')->where($map)->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where($map)->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            case 10001:
                $count = M('Article')->where(array('article_type' => 3,'is_open' => 1))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['article_type' => 3, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            case 10002:
                $count = M('Article')->where(array('article_type' => 1,'is_open' => 1))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['article_type' => 1, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
            default:
                $count = M('Article')->where(array('cat_id' => $cat_id))->count();
                $data['totalCount'] = $count;
                $data['totalPage'] = ceil($count/$page_size);
                $article_list = M('Article')->where(['cat_id' => $cat_id, 'is_open' => 1])->order('is_top DESC, add_time DESC')->page($curr_page,$page_size)->select();
                $data['list'] = $this->formatForNewsList($article_list);
                break;
        }
        if (empty($data['list'])) {
            $code = 1000;
            $msg = '没有更多了';
        } else {
            $code = 1;
            $msg = '成功';
        }
        $this->jsonReturn($code, $msg, $data);
    }

     /**
     * 资讯内容页格式化
     */
    public function formatForNewsList($article_list){
        // p($article_list);description
        $list = [];
        foreach ($article_list as $k => $v){
            $list[$k]['type'] = $v['article_type'];
            $list[$k]['showStyle'] = $v['show_type'];
            $list[$k]['is_top'] = $v['is_top'];
            $list[$k]['desc'] = $v['description'];
            $list[$k]['title'] = $v['title'];
            $list[$k]['author'] = $v['author'];
            $list[$k]['newsId'] = $v['article_id'];
            $click = $v['click'] + $v['click_base'];
            $click = $click >= 10000 ? round($click/10000, 1) .'万' : $click;
            $list[$k]['readNum'] = $click;
            // $list[$k]['detailUrl'] = SITE_URL.U('/Mobile/Article/news_detail', ['newsId' => $v['article_id']]);
            $list[$k]['detailUrl'] = SITE_URL.U('/Mobile/Article/news_detail') . '?newsId=' . $v['article_id'];
            $keywords_arr = [];
            if ($v['keywords']) {
                $keywords = explode(',', $v['keywords']);
                foreach ($keywords as $key => $value) {
                    // $keywords_arr[$key]['name'] = $value;
                    $keywords_arr[$key] = $value;
                }
            }
            $list[$k]['tagList'] = $keywords_arr;
            $thumb_arr = [];
            if ($v['thumb']) {
                $thumb = explode(',', $v['thumb']);
                foreach ($thumb as $key => $value) {
                    // $thumb_arr[$key]['imgUrl'] = SITE_URL.$value;
                    $thumb_arr[$key] = SITE_URL.$value;
                }
            }
            $list[$k]['coverList'] = $thumb_arr;
            $list[$k]['fileUrl'] = $v['file_url'] ? SITE_URL.$v['file_url'] : '';
        }
        return $list;


    }
    
     /**
     * 资讯内容页api
     */
    public function ajax_detail()
    {
        // $share_user_id = cookie('share_user_id');//分享人
        // $loginToken = input('loginToken', '');
        // if (empty($loginToken)) {
        //     $session_user = session('user');
        //     $user_id = $session_user['user_id'] ? $session_user['user_id'] : 0;
        // } else {
        //     $user_id = M('user_token')->where(['token' => $loginToken])->getField('user_id');
        // }
        // $article_id = intval(input('article_id', 73));//文章ID
        $article_id = intval(input('article_id', ''));//文章ID
        $article = M('Article')->where(['article_id' => $article_id])->field('article_id, cat_id, title, content, author, keywords, article_type, add_time, file_url, description, click, click_base, thumb,video_link')->find();
        $article['keywords'] = $article['keywords'] ? explode(',', $article['keywords']) : [];
        $article['thumb'] = $article['thumb'] ? explode(',', $article['thumb']) : [];
        $click = $article['click'] + $article['click_base'];
        $article['click'] = $click >= 10000 ? round($click/10000, 1) . '万' : $click ;
        // $article['zan'] = $article['zan'] + $article['zan_base'];
        // $article['share'] = $article['share'] + $article['share_base'];
        $article['add_time'] = date('Y-m-d H:i:s', $article['add_time']);
        $article['content'] = htmlspecialchars_decode($article['content']);
        // 是否点赞
        // if ($user_id) {
        //     $zan_status = M('zan')->where(['user_id' => $user_id, 'target_id' => $article_id])->count();
        //     $article['hasZan'] = $zan_status ? 1 : 0;
        // } else {
        //     $article['hasZan'] = 0;
        // }
        
        unset($article['click_base']);
        // unset($article['zan_base']);
        // unset($article['share_base']);
        $data['article'] = $article;

        // 相关文章列表
        // $map['article_id']  = array('neq', $article_id);
        // $map['cat_id']  = $article['cat_id'];
        // $map['is_open']  = 1;
        // $related_list = M('Article')->where($map)->order('rand()')->limit(5)->select();
        // foreach ($related_list as $k => $v){
        //     $data['related_list'][$k]['type'] = $v['article_type'];
        //     $data['related_list'][$k]['title'] = $v['title'];
        //     if ($v['is_top'] == 1) {
        //         $data['related_list'][$k]['titleStr'] = '置顶';
        //     } else {
        //         $data['related_list'][$k]['titleStr'] = '';
        //     }
        //     $data['related_list'][$k]['showStyle'] = $v['show_style'];
        //     $data['related_list'][$k]['desc'] = $v['description'];
        //     $data['related_list'][$k]['tutorialId'] = $v['article_id'];
        //     $click = $v['click'] + $v['click_base'];
        //     $click = $click >= 10000 ? round($click/10000, 1) .'万' : $click;
        //     $data['related_list'][$k]['readNum'] = $click;
        //     // $data['related_list'][$k]['likeNum'] = $v['zan'] + $v['zan_base'];
        //     // $data['related_list'][$k]['detailUrl'] = SITE_URL.U('/Mobile/Article/news_detail', ['newsId' => $v['article_id']]);
        //     $data['list'][$k]['detailUrl'] = SITE_URL.U('/Mobile/Article/news_detail') . '?newsId=' . $v['article_id'];
        //     $data['related_list'][$k]['author'] = $v['author'];
        //     $thumb_arr = [];
        //     if ($v['thumb']) {
        //         $thumb = explode(',', $v['thumb']);
        //         foreach ($thumb as $key => $value) {
        //             // $thumb_arr[$key]['imageUrl'] = SITE_URL.$value;
        //             $thumb_arr[$key] = SITE_URL.$value;
        //         }
        //     }
        //     $data['related_list'][$k]['coverList'] = $thumb_arr;
        // }

        // // 分享信息
        // $ShareLogic = new ShareLogic();
        // $data['share_info'] = $ShareLogic->articleShareInfo($share_user_id);
        // setcookie('share_user_id','',time()-3600,'/');
        
        // 浏览次数+1
        M('Article')->where(['article_id' => $article_id])->setInc('click');
        $this->jsonReturn(1, '成功', $data);
    }

}