<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: 当燃 2016-01-09
 */
namespace app\mobile\controller;
use app\common\logic\UsersLogic;
use app\common\logic\place\PlaceMatchCompensateLogic;
use think\Loader;
use app\common\logic\UserLogic;
use app\mobile\controller\User;

use think\Db;
use think\Image;
use think\cache\Driver\Redis;

class Test extends MobileBase {

     public function temporaryBindForMobile()
    {
        $phone = input('phone');
        $firstLeaderMobile = input('firstleaderMobile');
P(111);
        $result = model('common/place/PlaceTemporaryLocking')->temporaryBindForMobile($firstLeaderMobile,$phone);
        api_response($result['code'], $result['msg'], $result['data']);
    }
    //获取用户团队链下级，下下级。。。。链
    public function getTeamAll()
    {

        $result = (new UserLogic())->userLogin('13824826885', '123456', null, '1', '155014');
        api_response(10000, '登录成功', $result);
        $userId = I('user_id','153459159','trim');
        //获取团队链
        $userIds = model('common/distribut/DistributTeam')->getUserTeamsAllForMobile($userId);
        // p($userIds);
        $res = $this->getUserLevelUpd($userId);
        p($res);

    }
     /**
     * 获取用户层级用户
     * @Author    LP
     * @DateTime  2019-02-26T17:37:20+0800
     * @version   [version]
     * @parameter String
     * @param     [type]                   $user_id [description]
     * @return    [type]                            [description]
     */
    public function getUserLevelUpd($user_id,$data=[],$pid=0){
        $team = model('common/distribut/DistributTeam')->alias('a')
        ->join('Users u','a.user_id=u.user_id','LEFT')
        ->where(['parent_id'=>['in',$user_id]])
        ->column('a.parent_id,a.user_id,u.mobile','a.user_id');
        if($team && count($team)){

            //判断用户是否报单
            foreach ($team as $k => $v) {
                $data[$k][0]['name'] = $v['mobile'];
                $data[$k][0]['parent_id'] = $v['parent_id'];
                $data[$k][0]['id'] = $k;
                $data[$k][0]['pid'] = $pid==0?0:$v['parent_id'];
            }
            $pid += 1;
            return $this->getUserLevelUpd(array_keys($team),$data,$pid);
        }
        return $data;
    }
    /**
     * 获取当前当前分类以及子分类
     * @param array $cate 要放入的数组
     * @param array $pid  parent_id 为0
     * @return array  $tree 返回所有分类数组
     */
     public function sortOut($cate,$pid=0,$level=0)
    {
        $tree = array();
        foreach($cate as $v)
        {
            if($v['parent_id'] == $pid)
            {
                $v['level'] = $level + 1;
                $tree[$v['user_id']] = $v;
                //根据父级id查找所有的子分类
                $data = model('common/distribut/DistributTeam')->getUserTeamsAllForMobile($v['user_id']);
                // p($data);
                $tree = array_merge($tree, $this->sortOut($data,$v['user_id'],$level+1));
            }
        }
        return $tree;
    }

    public function getUserRegAgreementUrl()
    {
        $result = model('common/Article')->getAgreement(4);
        p($result);
        $result = model('common/Article')->getUserRegAgreementUrl();
        api_response(10000, '请求成功',['url'=>$result]);
    }
     public function testGuide()
    {


        $result = new User();
        $res = $result->getGuideImgForOther();
        p($res);
    }
     public function getGuideImgTest()
    {

        $popupAdList = model('common/Ad')->getIndexAd(1);
        // p($popupAdList);
        api_response(10000, '请求成功',['popupAdList'=>$popupAdList]);
    //     $guideImg     = model('common/nav/Navigation')->getNavGuideImg(5);
    //     api_response(10000, '请求成功',['url'=>$guideImg]);
    }

     public function appUpdate()
    {
        // $loginToken = trim(I('loginToken')); //用户id

        // if (!empty($loginToken)) {
        //     $user_id = $this->getUserId();
        // }
        $version =' 0.01';

        $client = 'android'; //1.iOS 2.android 3.H5

        if (empty($version) || empty($client)) {
            exit(api_ajaxReturn(1002, '参数不全'));
        }

        $data['appUpdate'] = M('AppConfig')->where(['name' => ucfirst(strtolower($client))])->find();
        if (isset($data['appUpdate'])) {
            $data['appUpdate']['backgroundImg']   = $data['appUpdate']['uiUpdateImg'] ? SITE_URL.$data['appUpdate']['uiUpdateImg'] : '';
            $data['appUpdate']['backgroundImgEn'] = $data['appUpdate']['uiUpdateImgEn'] ? SITE_URL.$data['appUpdate']['uiUpdateImgEn'] : '';
            $data['appUpdate']['updateType']      = $data['appUpdate']['updateDateType'] == 0 ? 1 : $data['appUpdate']['updateDateType'];
            $data['appUpdate']['cityVersion']     = $data['appUpdate']['cityAppUpdateVersion'];
            
        }

        $data['appUpdate']['isCanUpdate'] = 0;

        if (version_compare($version, $data['appUpdate']['newVersion']) == -1) {
            $data['appUpdate']['isCanUpdate'] = 1;
        } else {
            $data['appUpdate']['forceUpdate'] = 0;
        }

        if ($client == 'iOS') {
            $data['appUpdate']['showAppUpdate'] = $data['appUpdate']['showAppUpdateVersion'] != $version ? 1 : $data['appUpdate']['showAppUpdate'];
        }
        unset($data['appUpdate']['uiUpdateImg'],
                $data['appUpdate']['uiUpdateImgEn'],
                $data['appUpdate']['id'],
                $data['appUpdate']['isAuditing'],
                $data['appUpdate']['versionData'],
                $data['appUpdate']['cityAppUpdateVersion']
                // $data['appUpdate']['showAppUpdateVersion']
            );
        p($data);
        api_response(10000, '请求成功',$data['appUpdate']);

        // exit(api_ajaxReturn(1, '成功', $data['appUpdate']));

    }

    
    /**
     * [grantDirectPrize 发放直推奖]
     * @return [type] [description]
     */
    public function grantDirectPrize()
    {
       $logic = model('Common/OperationPatternLogic','logic');
       p($logic->grantDirectPrize(7,0,100,'vip'));
    }

    /**
     * [grantWeightPrize 发放加权分红]
     * @return [type] [description]
     */
    public function grantWeightPrize()
    {
       $logic = model('Common/OperationPatternLogic','logic');
       p($logic->grantWeightPrize(7,0,100,'vip'));
    }

    /**
     * [grantOperationPrize 发放运营分红]
     * @return [type] [description]
     */
    public function grantOperationPrize()
    {
       $logic = model('Common/OperationPatternLogic','logic');
       p($logic->grantOperationPrize(57,0,100,'vip'));
    }

    /**
     * [reg 注册会员]
     * @return [type] [description]
     */
    public function reg()
    {
        // trans();
        $first_leader_users = get_user_info('18520591016',2);
        cookie('first_leader',$first_leader_users['user_id']);
        $logic = model('Common/UsersLogic','logic');
        $username = I('username','18520591015','trim');
        $password = I('password',encrypt('123456'),'trim');
        p($logic->reg($username,$password,$password,0,[],'18520591015'));
    }

  
    /**
     * [getParentInfos 获取用户直推]
     * @return [type] [description]
     */
    public function getParentInfos($userId = 57)
    {
        $parentIds = model('Common/distribut/DistributTeam')->getUserParentIds($userId);
        p(model('Common/Users')->getParentInfos($parentIds,'user_id,level',6));
    }

    /**
     * [grantRolePrize 发放角色补贴]
     * @param  integer $userId [description]
     * @return [type]          [description]
     */
    public function grantRolePrize($userId = 57)
    {
       $logic = model('Common/OperationPatternLogic','logic');
       p($logic->grantRolePrize($userId,0,100,'vip'));
    }

    /**
     * [getMaxUserLevel 获取当前用户最大等级]
     * @return [type] [description]
     */
    public function getMaxUserLevel()
    {
        p(model('common/users/UserLevel')->max('level_id'));
    }

    /**
     * [update_pay_status 支付成功，与预计收益测试]
     * @param  [type] $order_sn [description]
     * @return [type]           [description]
     */
    public function update_pay_status($orderSn)
    {
        p(update_pay_status($orderSn));
    }

    public function updateUserLevel($userId)
    {
        $userLogic = model('Common/UsersLogic','logic');
        // $userLogic->setUserById($userId);
        p($userLogic->updateUserLevel());
    }

    /**
     * [getUserCount 获取当前用户等级总数]
     * @param  [type] $level [description]
     * @return [type]        [description]
     */
    public function getUserCount($level)
    {
        return model('Common/Users')->where(['level'=>$level])->count();
    }

    //升级记录 
    public function userLevelLog($user_id,$level,$source='default',$description='')
    {       
        $userLevelLogMdl = model('Common/users/UserLevelLog');
        $res = $userLevelLogMdl->addData($user_id,$level,$source,$description);
        p($res);
    }

    public function getLevelList()
    {
        p(model('common/users/UserLevel')->getLevelInfo(6));
    }

    /**
     * [getGiveGifeUserInfo description]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function getGiveGifeUserInfo($userId)
    {
        $logic = model('Common/UsersLogic','logic');
        p($logic->getGiveGifeUserInfo(57));
    }


    /**
     * [getUserParentIds description]
     * @return [type] [description]
     */
    public function getUserParentIds($userId)
    {
        $parentIds = model('common/distribut/DistributTeam')->getUserParentIds($userId);
        p($parentIds);
    }

    /**
     * [updateTeam description]
     * @param  integer $user_id [description]
     * @return [type]           [description]
     */
    public function updateTeam($user_id=0)
    {
        if(!$user_id){
            $userIds = model('Common/Users')->column('user_id');
        }else{
            $userIds = [$user_id];
        }
        $userTeamMdl = model('Common/distribut/DistributTeam');
        foreach ($userIds as $k => $v) {
            $res = $userTeamMdl->getParentIdStr($v);
            if(!$res) p($res);
            dump($res);
        }
    }

    /**
     * [getCollectUserId description]
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function getCollectUserId($user_id,$type = 0)
    {
        $logic = model('Common/UsersLogic','logic');
        p($logic->getCollectUserId($user_id,$type));
    }

    /**
     * [checkIsGift description]
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function checkIsGift($user_id)
    {
        p(model('Common/GiveGift')->checkIsGift($user_id));
    }

    /**
     * [teamNum description]
     * @return [type] [description]
     */
    public function teamNum($user_id,$level){
         $userTeamMdl = model('Common/distribut/DistributTeam');
         p($userTeamMdl->teamNum($user_id,$level),1);
         ps($userTeamMdl);
    }



    /**
     * [test1 description]
     * @return [type] [description]
     */
    public function test1($user_id)
    {
        $logic = model('Common/UsersLogic','logic');
        p($logic->getCollectUserId($user_id,1));
    }

     

    public function getDetailAddress(){
        p($this->request->baseUrl());      
    }

    //用户信用评分记录表
    public function addDataCreditUser($user_id = 1000000,$action_type = 4,$point = 0,$source_id = 5)
    {
        $model = model('Common/GiveGift');
        $model = model('Common/credit/CreditUserLog');
        // $res = $model->addData($user_id,\app\common\model\credit\CreditUserLog::COMPLAIN,$point,$source_id,'投诉成功');
        // $res = $model->checkLock($user_id,16);
        // $res = $model->thawAdmin($user_id);
        // $res = $model->checkVoucherPic();
        // $res = $model->updateCreditCount($user_id);
        $res = $model->autoComment();
        // ps($model);
        // dump($model->getError());
        p($res);
    }

    public function checkVoucherPic()
    {
        $model = model('Common/credit/CreditUserLog');
        $res = $model->checkVoucherPic();
        p($res);
    }

    public function autoComment()
    {
        $model = model('Common/credit/CreditUserLog');
        $res = $model->autoComment();
        p($res);
    }

    public function getLockUser()
    {
        p(model('Common/Users')->getLockUser());
    }


     /**
     * 变更上级，更换上级链.
     * /mobile/Test/updateUserParent/mobile/18520591050/pmobile/18520591015
     * @param number $mobile
     * @param number $pmobile
     * @return type
     */
    public function updateUserParent($mobile, $pmobile)
    {

        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        $pUserInfo = model('Common/Users')->getBy('mobile',$pmobile);
        if(empty($userInfo) || empty($pUserInfo)) exit(print '权限不足,或者账号有问题');
        $userId = $userInfo['user_id'];
        $parentId = $pUserInfo['user_id'];

        $teamAll = model('Common/distribut/DistributTeam')->getUserTeamsAll($userId);
        if(in_array($parentId, $teamAll)){
            exit(print '不能把你下级团队的成员修改成你的上级');
        }

        $userTeam = model('Common/distribut/DistributTeam')->where(['user_id' => $userId])->find();
        // p($userTeam);
        $parentTeam = model('Common/distribut/DistributTeam')->where(['user_id' => $parentId])->find();

        model('Common/distribut/DistributTeam')->where(['user_id' => $userId])->update(['parent_id' => $parentId]);
        //新上级链
        $parentIdsNew = $parentTeam ? $parentTeam[parent_id_str].'_('.$parentId.')' : 'team_('.$parentId.')';
        //更新所有$userId的子孙及他自己的上级链
        $sql = "update saas_distribut_team set `parent_id_str` = REPLACE (parent_id_str,'$userTeam[parent_id_str]','$parentIdsNew')
        where  (`parent_id_str` like '$userTeam[parent_id_str]_($userId)%' ) or `id`='$userTeam[id]'";
         
        $res = Db::execute($sql);
        if($res) {
            model('Common/Users')->updateBy(['user_id'=>$userId],['first_leader'=>$parentId]);
            return '更新成功';
        }
        return '已更新请查看数据';
    }

    public function pstr($mobile)
    {
        function getMobile($userId)
        {
            $userInfo = model('Common/Users')->getBy('user_id',$userId);
            return $userInfo['mobile'];
        }
        function getUserId($mobile)
        {
            $userInfo = model('Common/Users')->getBy('mobile',$mobile);
            return $userInfo['user_id'];
        }
        function strR($userId)
        {
            $str = '';
            $arr = model('Common/distribut/DistributTeam')->getUserParentIds($userId);
            foreach ($arr as $k => $v) {
                $str .= getMobile($v).'->';
            }
            return $str.'('.$userId.')'.'<br/>';
        }
        $userId = getUserId($mobile);
        echo $mobile.' : '.strR($userId);
        $userTeam = model('Common/distribut/DistributTeam')->where(['parent_id_str' => ['like','%('.$userId.')%']])->column('user_id');
        foreach ($userTeam as $tk => $uid) {
            echo getMobile($uid).' : '.strR($uid);
        }
    }



    // public function updateUserParent($mobile, $pmobile)
    // {

    //     $userMdl = model('Common/Users');
    //     $distributTeamMdl = model('Common/distribut/DistributTeam');

    //     $userInfo = $userMdl->getBy('mobile',$mobile);
    //     $pUserInfo = $userMdl->getBy('mobile',$pmobile);

    //     if(empty($userInfo) || empty($pUserInfo)) exit(print '权限不足,或者账号有问题');
        
    //     $userTeam = $distributTeamMdl->where(['user_id' => $userInfo['user_id']])->find();
    //     // p($userTeam);
    //     $parentTeam = $distributTeamMdl->where(['user_id' => $pUserInfo['user_id']])->find();

    //     trans();

    //     $distributTeamMdl->where(['user_id' => $userId])->update(['parent_id' => $parentId]);

    //     $sonParentStr = $userTeam['parent_id_str'];

    //     $fatherParentStr = $parentTeam['parent_id_str'];


    //     /**
    //      * 更新直接子级
    //      */



    //     p($userId,1);
    //     p($parentId,1);
    //     p($sonParentStr,1);
    //     p($fatherParentStr,1);




    //     // if($res) {
    //     //     model('Common/Users')->updateBy(['user_id'=>$userId],['first_leader'=>$parentId]);
    //     //     return '更新成功';
    //     // }
        
    //     return '已更新请查看数据';
    // }


    public function getUrl($mobile = '')
    {
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        empty($userInfo) && exit(print '权限不够或参数有误');
        $list = model('Common/GiveGift')->getList(['give_user_id'=>$userInfo['user_id'],'status'=>0],[],false,['collectUser']);
        empty($userInfo) && exit(print '暂无该闯关手机号待审核信息');
        foreach ($list as $k => $v) {
            echo 'http://sale.10mf.org/index.php'.U('Mobile/order/order_detail',['id'=>$v['id'],'type'=>$v['give_type']]).'------- 上级手机号'.$v['collect_user']['mobile'].'<br/>';
        }
        return ;
    }

    public function time($date)
    {
        p(date('Y-m-d H:i:s',$date));
        p(strtotime($date));
    }

    public function select1()
    {
        $model = model('Common/distribut/DistributTeam');
        $userMdl = model('Common/Users');

        $res = $userMdl->where(['level'=>['>=',6]])->column('mobile,level','user_id');
        $arr = [];
        $mobiles = [];
        foreach ($res as $k => $v) {
            # code...
            $teamNum = $model->teamNum($k,2);
            $arr2 = [];
            if($teamNum < 81){
                echo $arr2['user_id'] = $k;
                echo ' , ';
                echo $arr2['mobile'] = $v['mobile'];
                echo ' , ';
                echo $arr2['level'] = $v['level'];
                echo ' , ';
                echo $arr2['teamNum'] = $teamNum;
                echo '<br/>';
                $arr[] = $arr2;
            }
        }
        

        // p($mobiles);
        // print_r($arr);
    }
      public function select2()
    {
        $model = model('Common/distribut/DistributTeam');
        $userMdl = model('Common/Users');
        $mobile = 13793506438;
        // $mobile = 13824826885;

        $user_id = $userMdl->where(['mobile'=>$mobile])->value('user_id');
        // p($user_id);
        $num = $model->teamNum($user_id);
        p($num);
        
    }
   
        // p($sender);
    public function excel()
    {

        $res = '13909599405 13505197976 9
13099583031 13909599405 1
13337690996 13909599405 1
18195154395 13909599405 3
13352275118 13909599405 1
18950316627 13909599405 1
';
                   
        $arr = explode("\n",  $res);
        $array = [];
        $reg_array = [];
        //格式化数据
        foreach ($arr as $key => $value) {

            $arr[$key] = trim($value);
            $arr[$key] = explode(' ', $value); 
            foreach ($arr[$key] as $k => $v) {
                if($k == 0) $k = 'mobile';
                if($k == 2) $k = 'level_id';
                if($k == 1) $k = 'first_mobile';
                $array[$key][$k] = trim($v);
                if($k == 'level_id'){
                    $array[$key][$k] = $v+1;
                }
            }
        }

        $logic = new UsersLogic();
        $userMdl = model('Common/Users');
        //上级已存在则注册，并添加数据和更改上级链
        foreach ($array as $k => $v) {
            $user_id = $userMdl->where(['mobile'=>$v['mobile']])->value('user_id');
            $first_leader = $userMdl->where(['mobile'=>$v['first_mobile']])->find();
            if(!$user_id && $first_leader){
                $res = $logic->reg_Excel($v['first_mobile'],$v['mobile'],$v['level_id']);
                if($res) array_push($reg_array, $res) ;
            }
            
        }
        p($reg_array);

    }
    public function reg_Excel($invite,$mobile,$level_id)
    {
        $logic = new UsersLogic();
        $invite = get_user_info($invite, 2);//根据手机号查找邀请人
        // $p = md5('123456');
        // p($p);
        $password = '57deea2a6a3ef96106e4c79dcf1fb9ca';
        $password2 = '57deea2a6a3ef96106e4c79dcf1fb9ca';
        $nickname = $mobile;
        $username = $mobile;
        $data = $logic->regForExcel($username, $password, $password2, 0, $invite,$nickname,'',$level_id);
         return $data['status'];
        
        
    }
        /**
     * 注册
     * @param $username  邮箱或手机
     * @param $password  密码
     * @param $password2 确认密码
     * @param int $push_id
     * @param array $invite
     * @param string $nickname
     * @param string $head_pic
     * @return array
     */
    public function regForExcel($username,$password,$password2,$push_id = 0,$invite=array(),$nickname="",$head_pic="",$level_id){
        $is_validated = 0 ;
        

        if(check_mobile($username)){
            $is_validated = 1;
            $map['mobile_validated'] = 1;
            $map['mobile'] = $username; //手机注册
        }
        if($is_validated != 1)
            return array('status'=>-1,'msg'=>'请用手机号或邮箱注册','result'=>'');
        $map['nickname'] = $nickname ? $nickname : $username;
        if(!empty($head_pic)){
            $map['head_pic'] = $head_pic;
        }else{
            $map['head_pic']='/template/mobile/rainbow/static/images/user68.jpg';
        }

        $data=[
            'nickname' =>$map['nickname'],
            'password' =>$password,
            'password2'=>$password2,
        ];
        $UserRegValidate = Loader::validate('User');
        if(!$UserRegValidate->scene('reg')->check($data)){
            return array('status'=>-1,'msg'=>$UserRegValidate->getError(),'result'=>'');
        }
        $map['password'] = $password;
        $map['reg_time'] = time();
        $map['first_leader'] = $invite['user_id'];  //推荐人id
        $map['paypwd'] = encrypt(substr($username, -6,6));

        // 如果找到他老爸还要找他爷爷他祖父等
        if($map['first_leader'])
        {
            $first_leader = Db::name('users')->where("user_id = {$map['first_leader']}")->find();
            $map['second_leader'] = $first_leader['first_leader'];
            $map['third_leader'] = $first_leader['second_leader'];
            //他上线分销的下线人数要加1
            Db::name('users')->where(array('user_id' => $map['first_leader']))->setInc('underling_number');
            Db::name('users')->where(array('user_id' => $map['second_leader']))->setInc('underling_number');
            Db::name('users')->where(array('user_id' => $map['third_leader']))->setInc('underling_number');
        }else
        {
            return array('status'=>0,'msg'=>'注册失败,请填写推荐人手机号！');
        }

        if(is_array($invite) && !empty($invite)){
            $map['first_leader'] = $invite['user_id'];
            $map['second_leader'] = $invite['first_leader'];
            $map['third_leader'] = $invite['second_leader'];
        }
        $map['push_id'] = $push_id; //推送id
        $map['token'] = md5(time().mt_rand(1,999999999));
        $map['last_login'] = time();
        $map['level'] = $level_id;
       
        $user_id = Db::name('users')->insertGetId($map);
       
        if($user_id === false) return array('status'=>-1,'msg'=>'注册失败');
            
        //更新会员的团队
        $res = model('Common/distribut/DistributTeam')->getParentIdStr($user_id);
        $user = Db::name('users')->where("user_id", $user_id)->find();

        return array('status'=>1,'msg'=>'注册成功','result'=>$user);
    }
    
    public function img(){
        $img_path = 'D:\phpStudy\PHPTutorial\WWW\sale\public\upload\identityCard\20190215\90a99e319a5ca849708080f8c9548d1f.jpg';
        $save_path = 'D:\phpStudy\PHPTutorial\WWW\sale\public\upload\identityCard\20190215\90a99e319a5ca849708080f8c9548d1f.jpg';
        $thumb_w = 800;

        $res = $this->compressedImage($img_path,$save_path,$thumb_w);
        $ossClient = new \app\common\logic\OssLogic;
        $return_url = $ossClient->uploadFile($res);
       return $res;

    }


    /**
   * desription 压缩图片
   * @param sting $imgsrc 图片路径
   * @param string $imgdst 压缩后保存路径
   */
  public function compressedImage($imgsrc, $imgdst ,$thumb_w) {
    list($width, $height, $type) = getimagesize($imgsrc);
     
    $new_width = $width;//压缩后的图片宽
    $new_height = $height;//压缩后的图片高
         
    if($width >= $thumb_w){
      $per = $thumb_w / $width;//计算比例
      $new_width = $width * $per;
      $new_height = $height * $per;
    }
     
    switch ($type) {
      case 1:
        $giftype = check_gifcartoon($imgsrc);
        if ($giftype) {
          header('Content-Type:image/gif');
          $image_wp = imagecreatetruecolor($new_width, $new_height);
          $image = imagecreatefromgif($imgsrc);
          imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
          //90代表的是质量、压缩图片容量大小
          imagejpeg($image_wp, $imgdst, 90);
          imagedestroy($image_wp);
          imagedestroy($image);
        }
        break;
      case 2:
        header('Content-Type:image/jpeg');
        $image_wp = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($imgsrc);
        // p($image);
        imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        //90代表的是质量、压缩图片容量大小
        imagejpeg($image_wp, $imgdst, 90);
        imagedestroy($image_wp);
        imagedestroy($image);
        break;
      case 3:
        header('Content-Type:image/png');
        $image_wp = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefrompng($imgsrc);
        imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        //90代表的是质量、压缩图片容量大小
        imagejpeg($image_wp, $imgdst, 90);
        imagedestroy($image_wp);
        imagedestroy($image);
        break;
    }
  }
  public function testUpdata(){
    $model = model('Common/credit/CreditUserLog');
    $res = $model->checkVoucherPic();//根据发货凭证上传时间 操作信用评分
  }


  public function testSlipping(){
    // $res = [1,2];
    // dump($res);
    // unset($res);
    // $res = 11;
    // p($res);
    $model = model('Common/SlippingUsers');
    $res = $model->changeUserToConnecter(1);
    p($res);
    // $res = $model->getConnecterId($res);
  }

    // //获取等级大于等于2且有下级的用户
    // public function getIds(){

    //     $model = model('Common/Users');
    //     $Ids = $model->alias('s')
    //                 ->join('Users a','a.user_id = s.first_leader','LEFT')
    //                 ->where(['a.level'=>['egt',2]])
    //                 ->group('s.first_leader')
    //                 ->column('s.first_leader');
    //                 ps($model);
    //     if($Ids){
    //         $result = $this->insertslipping($Ids);
    //     }else{
    //         return false;
    //     }

    // }
    //插入接点人记录表
    public function insertslipping($data,$array){
       
        $model = model('Common/SlippingCount');
        foreach ($data as $k => $v) {
            $res = [
                'slipping_num'=> '0',
                'referrer_num' => '0',
                'surplus_num' => '3',
                'add_time'=> time(),
                'reg_time'=> $v['reg_time'],
                'direct_num'=>$v['count'],
                'connecter_id'=> $v['first_leader'],
            ];
           $res = $model->add($res);
           if($res){
            dump($k);
           }else{
            return false;
           } 
        }

        foreach ($array as $k => $v) {
            $res = [
                'slipping_num'=> '0',
                'referrer_num' => '0',
                'surplus_num' => '2',
                'reg_time'=> $v['reg_time'],
                'add_time'=> time(),
                'direct_num'=>$v['count'],
                'connecter_id'=> $v['first_leader'],
            ];
           $res = $model->add($res);
           if($res){
            dump($k);
           }else{
            return false;
           } 
        }
        return true;

    }
    //获取等级大于等于2的用户ID及下级数量    并存入接点人数据统计表
    public function getFirstLeaderNum(){
        //查询等级大于等于2且下级人数大于等于3的接点人UID,注册时间,直推下级数量
        $first_leaderIds = DB::query("SELECT `s`.`first_leader`,`a`.`reg_time`,count(`s`.`first_leader`) as `count` FROM `saas_users` `s`LEFT JOIN `saas_users` `a` ON `s`.`first_leader`=`a`.`user_id` WHERE  `a`.`level` >= 2 AND `s`.`level` >= 2 GROUP BY `s`.`first_leader` HAVING count(`s`.`first_leader`) >2 ORDER BY `count` DESC;");
        //查询等级大于等于2且下级人数少于3的接点人UID,注册时间,直推下级数量
        $first_leaderIds1 = DB::query("SELECT `s`.`first_leader`,`a`.`reg_time`,count(`s`.`first_leader`) as `count` FROM `saas_users` `s`LEFT JOIN `saas_users` `a` ON `s`.`first_leader`=`a`.`user_id` WHERE  `a`.`level` >= 2 AND `s`.`level` >= 2 GROUP BY `s`.`first_leader` HAVING count(`s`.`first_leader`) >0 AND count(`s`.`first_leader`) <3 ORDER BY `count` DESC;");
        // p($first_leaderIds);
        // 
        // 
        // 
        // $first_leaderIds = DB::query("SELECT `a`.`first_leader`,`s`.`reg_time`,count(`a`.`first_leader`) as `count` FROM `saas_users` `s`LEFT JOIN `saas_users` `a` ON `s`.`user_id`=`a`.`first_leader` WHERE  `a`.`level` >= 2 AND  `s`.`level` >= 2 GROUP BY `s`.`first_leader` HAVING count(`a`.`first_leader`) >2 ORDER BY `count` DESC;");

        // p($first_leaderIds1);
        $result = $this->insertslipping1($first_leaderIds,$first_leaderIds1);
        if($result === true) echo "ok";

    }



    //批量插入接点人记录表
    public function insertslipping1($data,$array){
       
        $model = model('Common/SlippingCount');
        $res = [];
        foreach ($data as $k => $v) {
            $res[] = [
                'slipping_num'=> '0',
                // 'referrer_num' => '0',
                'surplus_num' => '3',
                'add_time'=> time(),
                'reg_time'=> $v['reg_time'],
                'direct_num'=>$v['count'],
                'connecter_id'=> $v['first_leader'],
            ];
          
        }

        $res = $model->insertAll($res);
        if(!$res) return false;
        $res1 = [];
        foreach ($array as $k => $v) {
            $res1[] = [
                'slipping_num'=> '0',
                // 'referrer_num' => '0',
                'surplus_num' => '2',
                'reg_time'=> $v['reg_time'],
                'add_time'=> time(),
                'direct_num'=>$v['count'],
                'connecter_id'=> $v['first_leader'],
            ];
        }
        $result = $model->insertAll($res1);
        if(!$result) return false;
        return true;

    }

    // //批量插入滑落人记录
    // public function insertslipping1($data,$array){
       
    //     $model = model('Common/SlippingCount');
    //     $res = [];
    //     foreach ($data as $k => $v) {
    //         $res[] = [
    //             'slipping_num'=> '0',
    //             // 'referrer_num' => '0',
    //             'surplus_num' => '3',
    //             'add_time'=> time(),
    //             'reg_time'=> $v['reg_time'],
    //             'direct_num'=>$v['count'],
    //             'connecter_id'=> $v['first_leader'],
    //         ];
          
    //     }

    //     $res = $model->insertAll($res);
    //     if(!$res) return false;
    //     $res1 = [];
    //     foreach ($array as $k => $v) {
    //         $res1[] = [
    //             'slipping_num'=> '0',
    //             // 'referrer_num' => '0',
    //             'surplus_num' => '2',
    //             'reg_time'=> $v['reg_time'],
    //             'add_time'=> time(),
    //             'direct_num'=>$v['count'],
    //             'connecter_id'=> $v['first_leader'],
    //         ];
    //     }
    //     $result = $model->insertAll($res1);
    //     if(!$result) return false;
    //     return true;

    // }
    //查询账号下面的直推链
    public function getDistributTeamForMobile(){

        $mobile = '18656439131';
        $dtMdl = model('Common/distribut/DistributTeam');

        $user_id = Db::name('users')->where("mobile = {$mobile}")->value('user_id');
        // p($user_id);

        $ids = $dtMdl->getUserTeamsAll($user_id);
        // p($ids);
        $id = array_unshift($ids,153456493);
        // p($ids);
        // p($ids);
        $list = $dtMdl->getUserTeamsAllInfo($user_id);
        // p(collection($list)->toArray());
        // foreach ($list as $k => $v) {
        //     $list[$k]['parent_id_str'] = str_replace('team_(4294967295)_(1)_(153456479)_(153456480)','team_(4294967295)',$v['parent_id_str']);
        //     // $res = $dtMdl->where(['user_id'=>$v['user_id']])->update(['parent_id_str'=> $list[$k]['parent_id_str']]);
        //     $res = $dtMdl->where(['user_id'=>$v['user_id']])->save(array('parent_id_str' => $list[$k]['parent_id_str']));
        //     // dump($res);
        // }
        
        
        // $res =$dtMdl->where(['user_id'=>['notin',$ids]])->delete();
        // dump($ids);
        // $res = Db::name('users')->where(['user_id'=>['notin',$ids]])->order('level desc')->select();
        $res = Db::name('users')->where(['user_id'=>['notin',$ids]])->delete();
        p($res);
        // if(empty$res){
        //     return false;
        // }

        // $res = $dtMdl->where(['user_id'=>['notin',$ids]])->delete();
        // if(!$res){
        //     return false;
        // }
        return true;
    }


    public function redisTest(){
        // echo phpinfo();die;
        $redis = new \Redis();
        $redis->connect(config('cache.host'),config('cache.port'));
        $redis->auth(config('cache.password')); //密码验证
        $k = 1122;
        $name = $redis->set($k,'xiaoi111',1);
        $name = $redis->get($k);
    
        dump($name);
        // sleep(3);
        $name = $redis->get($k);
        cache('mobileIndexCache',12,2);
        $res = cache('mobileIndexCache');
        dump($res);
        sleep(3);
        $res = cache('mobileIndexCache');
        p($res);




    }
    /**
     * 2019-04-10数据平移注册
     * [getUserInfoList 根据用户ID格式化用户数据]
     * @return [type] [description]
     */
    public function getUserInfoList(){

        $ids = "";

    $arr = explode("\n",  $ids);

    foreach ($arr as $k => $v) {
       $arr[$k] = trim($v);
    }
    $users  = model('Common/Users')->where(['user_id'=>['IN',$arr]])->select();  //users用户信息
    $list = $this->getUserInfoListFormat($users);  //格式化数据
    $list = json_encode($list);                    //转json
    // p($arr);
    // ps(model('Common/Users'));

    p($list); //输出数据复制到 userInfoList() 方法里面
    // $distributTeam = model('Common/distribut/DistributTeam')->where(['user_id'=>['IN',$arr]])->select(); //直推链
    // $upgradeLog    = model('Common/users/UserLevelLog')->where(['user_id'=>['IN',$arr]])->select();      //升级记录
    // $giveGiftLog   = model('Common/GiveGift')->where(['give_user_id'=>['IN',$arr]])->select();           //闯关记录
    // $slippingCount = model('Common/SlippingCount')->where(['connecter_id'=>['IN',$arr]])->select();      //接点人
    // $creditUserLog = model('Common/credit/CreditUserLog')->where(['user_id'=>['IN',$arr]])->select();    //信用分记录


    // ps (model('Common/distribut/DistributTeam'));

    }

    /**
     * 2019-04-10数据平移注册
     * [formit 格式化数据]
     * @param  [type] $users [description]
     * @return [type]        [description]
     */
    public function getUserInfoListFormat($users){
        $list = $data =[];
        foreach ($users as $k => $v) {
            $data = $list[$k]['mobile'] = $v['mobile'];
            $data = $list[$k]['password'] = encrypt(substr($v['mobile'], 5));
            $data = $list[$k]['paypwd'] = encrypt(substr($v['mobile'], 5));
            $data = $list[$k]['underling_number'] = $v['underling_number']?$v['underling_number']:'';
            $data = $list[$k]['reg_time'] = $v['reg_time'];
            $data = $list[$k]['last_login'] = $v['last_login'];
            $data = $list[$k]['head_pic'] = $v['head_pic'];
            $data = $list[$k]['nickname'] = $v['nickname'];

            $referrer_id = model('Common/SlippingUsers')->where(['slipping_id'=>$v['user_id']])->value('referrer_id'); //滑落用户的引荐人id
            $referrer_mobile = model('Common/Users')->where(['user_id'=>$referrer_id])->value('mobile');
            $data = $list[$k]['first_leader'] = $referrer_mobile ? $referrer_mobile : model('Common/Users')->where(['user_id'=>$v['first_leader']])->value('mobile');
        }
        
            // $res = $this->regForGetUserInfoLists($list);

        return $list;

    }


    /************************************************************************ 新数据库注册操作 start **************************************************************/
    /**
     * 2019-04-10数据平移注册
     * [regForGetUserInfoLists 循环注册]
     * @return [type] [description]
     */
     public function regForGetUserInfoLists()
    {
        $list = $this->userInfoList();
        $i = 0;
        dump(time());
        foreach ($list as $k => $v) {
            $res = model('Common/Users')->where(['mobile'=>$v['mobile']])->count();
            $invite = model('Common/Users')->where(['mobile'=>$v['first_leader']])->find();
            if($res<1){
                $data = $this->regForGetUserInfoList($v['mobile'], $v['password'], $v['password'], 0, $invite,$v['nickname'],$v['head_pic'],1);
                if($data['status']<1 ) {
                    dump($data['msg']);
                }else{
                     dump($k);
                } 
            }else{
                $i+=1;
                dump($v['mobile'].'已注册'.$i);
            }
        }
         // return $data['status'];
        
        
    }
    /**
     * 2019-04-10数据平移注册
     * @param $username  手机
     * @param $password  密码
     * @param $password2 确认密码
     * @param int $push_id
     * @param array $invite  推荐人用户信息
     * @param string $nickname 昵称
     * @param string $head_pic 头像
     * @return array
     */
    public function regForGetUserInfoList($username,$password,$password2,$push_id = 0,$invite=array(),$nickname="",$head_pic="",$level_id){
        $is_validated = 0 ;
        
        if(check_mobile($username)){
            $is_validated = 1;
            $map['mobile_validated'] = 1;
            $map['mobile'] = $username; //手机注册
        }
        if($is_validated != 1)
            return array('status'=>-1,'msg'=>'请用手机号或邮箱注册','result'=>'');
        $map['nickname'] = $nickname ? $nickname : $username;
        if(!empty($head_pic)){
            $map['head_pic'] = $head_pic;
        }else{
            $map['head_pic']='/template/mobile/rainbow/static/images/user68.jpg';
        }

        $data=[
            'nickname' =>$map['nickname'],
            'password' =>$password,
            'password2'=>$password2,
        ];
        $UserRegValidate = Loader::validate('User');
        if(!$UserRegValidate->scene('reg')->check($data)){
            return array('status'=>-1,'msg'=>$UserRegValidate->getError(),'result'=>'');
        }
        $map['password'] = $password;
        $map['reg_time'] = time();

        $slippingUsersMdl = model('Common/SlippingUsers');
        $inviter = $invite['user_id']; //推荐人ID
        
        //判断推荐人是否为引荐人 即被推荐人是否为滑落用户
        $res = $slippingUsersMdl->isDirecter($invite['user_id']);
        if($res){
            unset($invite);
            $invite = get_user_info($res, 0);//根据接点人ID查找接点人
        }    


        $map['first_leader'] = $invite['user_id'];  //推荐人id
        $map['paypwd'] = encrypt(substr($username, -6,6));
        // 如果找到他老爸还要找他爷爷他祖父等
        if($map['first_leader'])
        {  
            $first_leader = Db::name('users')->where("user_id = {$map['first_leader']}")->find();
            $map['second_leader'] = $first_leader['first_leader'];
            $map['third_leader'] = $first_leader['second_leader'];
            //他上线分销的下线人数要加1
            Db::name('users')->where(array('user_id' => $map['first_leader']))->setInc('underling_number');
            Db::name('users')->where(array('user_id' => $map['second_leader']))->setInc('underling_number');
            Db::name('users')->where(array('user_id' => $map['third_leader']))->setInc('underling_number');
        }else
        {
            return array('status'=>0,'msg'=>$username.'注册失败,请填写推荐人手机号！');
        }

        if(is_array($invite) && !empty($invite)){
            $map['first_leader'] = $invite['user_id'];
            $map['second_leader'] = $invite['first_leader'];
            $map['third_leader'] = $invite['second_leader'];
        }
        $map['push_id'] = $push_id; //推送id
        $map['token'] = md5(time().mt_rand(1,999999999));
        $map['last_login'] = time();
        $map['level'] = $level_id;
       
        $user_id = Db::name('users')->insertGetId($map);
        if($user_id === false) return array('status'=>-1,'msg'=>$username.'注册失败');

        //注册成功后 如果注册是滑落人添加滑落人记录
        $inviter = get_user_info($inviter, 0); //根据手机号查找邀请人
        if ( $invite['user_id'] != $inviter['user_id']){
          $res = $slippingUsersMdl->toConnect($invite['user_id'],$user_id,$inviter['user_id']);
          if(!$res) $this->ajaxReturn(array('status' => -1, 'msg' => $username.'注册成功,添加滑落人记录失败'));
        }
            
        //更新会员的团队
        $res = model('Common/distribut/DistributTeam')->getParentIdStr($user_id);
        $user = Db::name('users')->where("user_id", $user_id)->find();

        return array('status'=>1,'msg'=>'注册成功','result'=>$user);
    }
    /**
     * 2019-04-10数据平移注册
     * [userInfoList 旧数据库要转移的用户数据]
     * @return [type] [description]
     */
    public function userInfoList(){
        $list = '';
    $list = json_decode($list,true);
    return $list;
    }

    /************************************************************************ 新数据库注册操作 end **************************************************************/




    public  function connecterMaxLevel($uid='',$afterLevel='4')
    {   
        $usersModel         = model('common/Users');
        $slippingConfigMdl  = model('common/SlippingConfig');
        
        $configMaxLevel = $slippingConfigMdl->where(['enable'=>1])->order('level DESC')->find();
        // p(collection($configMaxLevel)->toArray());
         if($afterLevel > $configMaxLevel['level']){
          return $configMaxLevel['level'];
        }else{
            return $afterLevel;
        }
    }


    /********************************************************** 2019-4-17 销售任务v1.2滑落规则接点人修改及商业模式修改 start *************************************************************/

    //修改接点人数据库表数据
    public  function changeSlippingCount()
    {   
        $usersModel        = model('common/Users');
        $slippingCountMdl  = model('common/SlippingCount');
        // $res = $usersModel->alias('u')->join('saas_slipping_count c','c.connecter_id=u.user_id')->where(['u.level'=>['=',3],'c.enable'=>0])->select();
        // $res = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['>',2],'c.enable'=>0,'c.slipping_num'=>0])->field('u.mobile,c.*')->select();
        // p(collection($res)->toArray());

        // $sql = "select connecter_id FROM saas_slipping_count LEFT JOIN
        //         (select user_id as i from saas_users where level > 1) as t1
        //         ON saas_slipping_count.connecter_id=t1.i where t1.i IS NULL";
        // $res = M('saas_slipping_count')->query($sql);
        // p($res);

        $count1 = $slippingCountMdl->count('id');
        dump('已有接点人数为：'.$count1);
        $countuser = $usersModel->where(['level'=>['>',1]])->count('user_id');
        dump('用户表level>1的接点人数为：'.$countuser);
        $countuser1 = $usersModel->count('user_id');
        dump('用户表总人数为：'.$countuser1);

       
        $insertNum = $countuser - $count1;
        dump('要插入的接点人数量为：'.$insertNum);
        $table = 'saas_slipping_count';

        /**
         * 批量更新数据  "UPDATE slipping_count SET add_time = ".$time." where add_time = 0";
         * update student s set city_name = (select name from city where code = s.city_code);
         */
        // $c_time = time();

       
        /**
        * 批量插入接点人数据
        * INSERT INTO 目标表  
        * (字段1, 字段2, ...)  
        * SELECT 字段1, 字段2, ...  
        * FROM 来源表  
        * WHERE not exists (select * from 目标表  
        * where 目标表.比较字段 = 来源表.比较字段); 
        */
       
        $sql = "insert into 
                    saas_slipping_count (
                        connecter_id,reg_time
                    ) 
                select 
                    user_id as connecter_id,reg_time 
                from 
                    saas_users 
                WHERE 
                    level > 1 
                AND 
                    not exists (
                        select * 
                        from 
                            saas_slipping_count  
                        where 
                            saas_slipping_count.connecter_id = saas_users.user_id
                        ); ";
        $res =  M($table)->query($sql);

        /**
        * 更新插入接点人表的添加时间
        * @var string
        */
        $time = time();
        $sql = "UPDATE saas_slipping_count SET add_time = ".$time." where add_time = 0";
        $res = M($table)->query($sql);

        /**
        * 更新接点人表剩余接点人数为用户等级(level - 1)
        * @var string
        */
        $c_time = time();
        $sql = "UPDATE  saas_slipping_count c set c.surplus_num = (select level from saas_users where user_id = c.connecter_id)-1,update_time =".$c_time;
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量大于上限数量4 则设剩余接点数量为4
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = 4 where surplus_num >4 ";
        $res =  M($table)->query($sql);


        /**
         * 更新接点人表剩余接点人数减去已接点人数
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = surplus_num-slipping_num ";
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量少于一 则更改剩余人数量为0及设为不能接点enable = 0
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = 0,enable = 0 where surplus_num < 1 ";
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量大于0 则设为可接点 enable = 1
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set enable = 1  where surplus_num >0 ";
        $res =  M($table)->query($sql);

        dump('11111111111111111111111111111111111111111111111111111111111111111');
        $count2 = $slippingCountMdl->count('id');
        dump('插入接点人数据后接点表人数为：'.$count2);
        $countuser1 = $usersModel->where(['level'=>['<',2]])->count('user_id');
        dump('用户表level=<1的数量为'.$countuser1);
        // $countuser = $usersModel->count('user_id');
        $countuser2 = $usersModel->count('user_id');
        dump('用户表人数为：'.$countuser2);
        
        p('成功');

   }



    // public function users()
    // {
    //     $table = __FUNCTION__;
    //     p($table);

    //     $res = M($table)->query($this->TRUNCATE.'cloud_'.$table);//TRUNCATE数据
    //     dump($this->TRUNCATE.'cloud_'.$table);

    //     //主题数据
    //     $sql = "insert into cloud_users (user_id,mobile,tk_id,password,paypwd,sex,province,city,district,nickname,head_pic,reg_time) select user_id,mobile,tk_id,password,paypwd,sex,province,city,district,name as nickname,head_pic,reg_time from mall.kt_users";
    //     $res = M($table)->query($sql);

    //     //会员
    //     $sql = "UPDATE cloud_users SET level = 2 where user_id in (select user_id from mall.kt_cloud_card_user where package_id = 1)";
    //     $res = M($table)->query($sql);

    //     //代理一 以上
    //     $sql = "UPDATE cloud_users c,mall.kt_cloud_card_user k SET c.level = k.package_id WHERE c.user_id=k.user_id AND k.package_id>2";
    //     $res = M($table)->query($sql);

    //     //更新加入时间
    //     $sql = "UPDATE cloud_users c,mall.kt_cloud_card_user k SET c.reg_time = k.create_time WHERE c.user_id=k.user_id";
    //     $res = M($table)->query($sql);

    //     //更新过期时间
    //     $sql = "UPDATE cloud_users c,mall.kt_cloud_card_user k SET c.validity_time = k.validity_time WHERE k.package_id > 0 AND c.user_id=k.user_id";
    //     $res = M($table)->query($sql);

    //     echo 'users_finished.',PHP_EOL;
    // }
    /********************************************************** 2019-4-17 销售任务v1.2滑落规则接点人修改及商业模式修改 end *************************************************************/


    /********************************************************** 2019-4-22 销售任务v1.2滑落规则接点人修改及商业模式修改 start *************************************************************/
    public function checkDate()
    {
        $usersModel        = model('common/Users');
        $slippingCountMdl  = model('common/SlippingCount'); 

        $res = $slippingCountMdl->where(['enable'=>0,'surplus_num'=>['>',0]])->select();
        // $res = $slippingCountMdl->where(['enable'=>0,'surplus_num'=>['>',0],'slipping_num'=>['>',0]])->select();
        // $res1 = $slippingCountMdl->where(['enable'=>1,'surplus_num'=>['>',0],'slipping_num'=>['>',0]])->select();
        $count1 = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['<',3],'enable'=>1])->select();

        // $count2 = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where()->select();
        // dump($count2);
        dump($count1);
        p($res);
        return $res;

    }



    /**
     * 滑落规则变更
     * 1星 可接点人数为 0
     * 2星 可接点人数为 1
     * 3星 可接点人数为 2
     * 4星 可接点人数为 3
     */
    //修改接点人数据库表数据
    public  function changeSlippingCount1()
    {   
        $usersModel        = model('common/Users');
        $slippingCountMdl  = model('common/SlippingCount'); 

        $count = $slippingCountMdl->count('id');
        dump('已有接点人数为：'.$count);
        $count1 = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['>',2]])->count('c.id');
        dump('已有等级大于1星接点人数为：'.$count1);
        // $count2 ="select 
        //             connecter_id
        //         from 
        //             saas_slipping_count 
        //         WHERE 
        //             (surplus_num + slipping_num )>3";

        // dump('等级小于2星的接点人数为：'.$count2);
        $countuser = $usersModel->where(['level'=>['>',2]])->count('user_id');
        dump('用户表level>2的接点人数为：'.$countuser);
        // $countuser1 = $usersModel->count('user_id');
        // dump('用户表总人数为：'.$countuser1);
       
        $insertNum = $countuser - $count1;
        dump('要插入的接点人数量为：'.$insertNum);
        // p('111111111111');
        $table = 'saas_slipping_count';

        /**
         * 批量更新数据  "UPDATE slipping_count SET add_time = ".$time." where add_time = 0";
         * update student s set city_name = (select name from city where code = s.city_code);
         */
        // $c_time = time();

       
        /**
        * 批量插入接点人数据
        * INSERT INTO 目标表  
        * (字段1, 字段2, ...)  
        * SELECT 字段1, 字段2, ...  
        * FROM 来源表  
        * WHERE not exists (select * from 目标表  
        * where 目标表.比较字段 = 来源表.比较字段);
        *  
        * 插入等级大于1星的用户到接点表
        */
       
        $sql = "insert into 
                    saas_slipping_count (
                        connecter_id,reg_time
                    ) 
                select 
                    user_id as connecter_id,reg_time 
                from 
                    saas_users 
                WHERE 
                    level > 2 
                AND 
                    not exists (
                        select * 
                        from 
                            saas_slipping_count  
                        where 
                            saas_slipping_count.connecter_id = saas_users.user_id
                        ); ";
        $res =  M($table)->query($sql);

        /**
        * 更新插入接点人表的添加时间
        * @var string
        */
        $time = time();
        $sql = "UPDATE saas_slipping_count SET add_time = ".$time." where add_time = 0";
        $res = M($table)->query($sql);

        /**
        * 更新接点人表剩余接点人数为用户等级(level - 2)
        * level = 3 可接点 1
        * level = 4 可接点 2
        * level = 5 可接点 3
        * @var string
        */
        $c_time = time();
        $sql = "UPDATE  saas_slipping_count c set c.surplus_num = (select level from saas_users where user_id = c.connecter_id)-2,update_time =".$c_time;
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量大于上限数量3 则设剩余接点数量为3
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = 3 where surplus_num >3 ";
        $res =  M($table)->query($sql);


        /**
         * 更新接点人表剩余接点人数减去已接点人数
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = surplus_num-slipping_num ";
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量少于一 则更改剩余人数量为0及设为不能接点enable = 0
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set surplus_num = 0,enable = 0 where surplus_num < 1 ";
        $res =  M($table)->query($sql);

        /**
         * 更新接点人表 如果剩余接点数量大于0 则设为可接点 enable = 1
         * @var string
         */
        $sql = "UPDATE  saas_slipping_count  set enable = 1  where surplus_num >0 ";
        $res =  M($table)->query($sql);


        /**
         * 更新接点人表 如果用户等级小于3 则设为不可接点 enable = 0
         * @var string
         */
        $res = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['<',3]])->update(['c.enable'=>0]);
        dump($res);

        dump('11111111111111111111111111111111111111111111111111111111111111111');
        // $count2 = $slippingCountMdl->count('id');
        $count1 = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['>',2]])->count('c.id');
        dump('插入接点人数据后接点表用户level>2人数为：'.$count1);
        $countuser1 = $usersModel->where(['level'=>['>',2]])->count('user_id');
        dump('用户表level>2的数量为'.$countuser1);
        // $countuser = $usersModel->count('user_id');
        $countuser2 = $usersModel->count('user_id');
        dump('用户表人数为：'.$countuser2);
        
        p('成功');

   }


    /********************************************************** 2019-4-22 销售任务v1.2滑落规则接点人修改及商业模式修改 end *************************************************************/


    /********************************************************** 2019-5-8  销售任务v1.3滑落规则接点人修改及商业模式修改 start ***********************************************************/
    /**2019-5-8 销售任务1.3版滑落规则修改 数据库接点人表用户等级大于等于5星则不能再接点(包括闯关升级,pc端手机端后台修改等级)
    * @author  Jaywoo
    * [changeRuleForLeve5 更改接点人表用户等级>=5为不能接点]
    * @return [type] [description]
    */
    public function changeRuleForLeve5(){

        $slippingCountMdl  = model('common/SlippingCount'); 
        // $data = [
        //     'enable'=>0,
        //     'surplus_num'=>0
        // ];
        // trans();
        // $res = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.level'=>['egt',5]])->update($data);
        // $res = $slippingCountMdl->alias('c')->join('saas_users u','c.connecter_id=u.user_id')->where(['u.user_id'=>153459159])->update(['enable'=>1]);
        // if($res){
        //     trans('commit');
        //     p('成功');
        // }else{
        //     trans('rollback');
        //     p('失败');
        // }


    }
    /********************************************************** 2019-5-8  销售任务v1.3滑落规则接点人修改及商业模式修改 end *************************************************************/

    public function changeLevelForPlace(){
        $model = model('Common/UserLevelLog');

        $res = $model->addData('153456547',2,3,'22222');
        dump($model->getError());
        p($res);
    }

    public function ttnse(){
        $mdl = new PlaceMatchCompensateLogic();
        $res = $mdl->checkLeaderIsChange('153456547');
        p($res);
    }

    public function cestt(){
        $mdl = new PlaceMatchCompensateLogic();
        $res = $mdl->addLossLog('153684045','153683992','153683961');
        p($res);
    }
    
    

     //获取上级链
    public function getFirstLeaderTeam()
    {
        $user_id = 153459159;
        // $res = model('Common/distribut/DistributTeam')->getUserParentIds($user_id);
        // p($res);
        $mdl = new PlaceMatchCompensateLogic();
        $res = $mdl->getSourceAndTypeForMatchingLog('153477660','2');
        p($res);
        $userTeam = model('Common/distribut/DistributTeam')->where(['user_id'=>$user_id])->find();
        $ids = substr($userTeam['parent_id_str'],5);
        $ids = str_ireplace(['(',')'],'',$ids);
        $ids = explode('_', $ids);
        $ids = array_reverse($ids);
        p($ids);
        if(empty($userTeam)) return false;
        return $userTeam['parent_id_str'];
    }

    public function redis()
    {
        $redis = new Redis();

        $redis->connect('127.0.0.1', 6379);

        $redis->set('name','zhou', 10);

        $key_1 = $redis->get('name');

        echo $key_1;

    }

    public function tozk()
    {
        $param = [
            0 => [],
            1 => [153683896],
            2 => [153683860],
        ];
        $url = tpCache('shop_info.retail').'/child/Thirdapi/getChildShopLink';
        p(httpRequest($url,'POST',['param'=>json_encode($param)]));
    }


    public function toPlace()
    {
        $this->waitingMatchingMdl = model('Common/place/PlaceWaitingMatching');
        $count = $this->waitingMatchingMdl->where(['state'=>1])->count();
        if($count > 1){
            $this->waitingMatchingMdl->where(['state'=>1])->update(['state'=>0]);
        }elseif($count == 1){
            return $this->waitingMatchingMdl->where(['state'=>1])->value('user_id');
        }

        //无人在位，开始上位了（登基流程）
        $row2 = $this->waitingMatchingMdl->where(['state'=>0])->order($this->waitingMatchingMdl->getOrderByStr())->find();

        //登基
        $this->waitingMatchingMdl->where(['user_id'=>$row2['user_id']])->update(['state'=>1,'update_time'=>time()]);

        return $row['user_id'];
    }

    public function toPlace2()
    {
        $this->waitingMatchingMdl = model('Common/place/PlaceWaitingMatching');
        $row = $this->waitingMatchingMdl->where(['state'=>1])->order($this->waitingMatchingMdl->getOrderByStr())->find();
        $this->waitingMatchingMdl->where(['state'=>1,'id'=>['<>',$row['id']]])->update(['state'=>0]);
        return $row['user_id'];
    }

    public function savePlace($user_id=153683991)
    {
        
        $logic = model('Common/place/PlaceMatchCompensateLogic','logic');
        $res = $logic->savePlace($user_id);
        p($res);
    }

    public function give()
    {
        $where = [
            'give_user_level' => 1,
            'give_user_id' => 153684062,
        ];
        $order2 = model('GiveGift')->where($where)->order('id desc')->limit(2)->select();
        if(count($order2) == 2){
            if(abs($order2[0]['addtime'] - $order2[1]['addtime']) > 360){
                unset($order2[1]);
            }
        }
        $arr = array_column($order2, 'id');
        $status = 1;
        if($status == 1){
            $urlSuffix = 'bonusFormal';
        }else{
            $urlSuffix = 'bonusRollback';
        }
        $url = tpCache('retail.api_link').'/api/Barrier/'.$urlSuffix;
        //$url = 'http://shopone.kitnote.com/api/Barrier/'.$urlSuffix;
        $result = httpRequest($url,'POST',['giveGiftId'=>implode(',', $arr)]);
        var_dump($result);die;

    }

    public function dian()
    {
        cache('give_gift_6666',1);
        $res = cache('?give_gift_*');
        // $res = cache('give_gift_6666');
        p($res);
    }
}