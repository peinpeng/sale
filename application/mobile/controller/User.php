<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * 2015-11-21
 */

namespace app\mobile\controller;

use app\common\logic\CartLogic;
use app\common\logic\MessageLogic;
use app\common\logic\UsersLogic;
use app\common\logic\UserLogic;
use app\common\logic\SlippingUsersLogic;
use app\common\logic\OrderLogic;
use app\common\model\UserAddress;
use app\common\logic\CompressedImageLogic;
use app\common\util\TpshopException;
use think\Page;
use think\Verify;
use think\Loader;
use think\db;

class User extends MobileBase
{

    public $user_id = 0;
    public $user = array();

    /*
    * 初始化操作
    */
    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $session_user = session('user');
            $select_user = M('users')->where("user_id", $session_user['user_id'])->find();
            $oauth_users = M('OauthUsers')->where(['user_id' => $session_user['user_id']])->find();
            empty($oauth_users) && $oauth_users = [];
            $user = array_merge($select_user, $oauth_users);
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
            $this->assign('user', $user); //存储用户信息
        }
        $nologin = array(
            'login', 'pop_login', 'do_login', 'logout', 'verify', 'set_pwd', 'finished',
            'verifyHandle', 'reg', 'send_sms_reg_code', 'find_pwd', 'check_validate_code',
            'forget_pwd', 'check_captcha', 'check_username', 'send_validate_code', 'express', 'bind_guide', 'bind_account', 'bind_reg','abnormal_unusual','share','codeLogin','do_login_mobile','activityPage','memberPlace'
        );
        $is_bind_account = tpCache('basic.is_bind_account');
        if (!$this->user_id && !in_array(ACTION_NAME, $nologin)) {
            if (strstr($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') && $is_bind_account) {
                header("location:" . U('Mobile/User/bind_guide'));//微信浏览器, 调到绑定账号引导页面
            } else {
                header("location:" . U('Mobile/User/login'));
            }
            exit;
        }

        $order_status_coment = array(
            'WAITPAY' => '待付款 ', //订单查询状态 待支付
            'WAITSEND' => '待发货', //订单查询状态 待发货
            'WAITRECEIVE' => '待收货', //订单查询状态 待收货
            'WAITCCOMMENT' => '待评价', //订单查询状态 待评价
        );
        $this->assign('order_status_coment', $order_status_coment);

        $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : U("Mobile/User/index");
        $this->assign('referurl', $referurl);
    }

    /*
     * 用户中心首页
     */
    public function index()
    {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);
        
        $userLogic = model('Common/UsersLogic', 'logic');
        $first_leader = $userLogic->get_info($this->user['first_leader']);

        $level_name = M('user_level')->where("level_id", $this->user['level'])->cache(true)->getField('level_name'); // 等级名称
        // $userNav = model('common/nav/Navigation')->getNavigationByPosition(1);
        $userNav = model('common/nav/Navigation')->getNavigationByPosition(12); //导航
        // p($userNav);
        $usersModel = model('common/Users');
        $ztjnum = $usersModel->getFisrtLeaderNumAttr($this->user['user_id']);//直推人数
        $teamnum = model('common/distribut/DistributTeam')->getUserTeamsAllCount($this->user['user_id']);//团队人数
        $level_list = model('Common/users/UserLevel')->column('*', 'level_id'); //用户等级信息

        /**
         * [$canRegForOthers 是否能帮他人注册]
         */
        $canRegForOthers = $this->user['level'] >=2 ? true : false;

        $this->assign('userNav', $userNav);
        //获取用户信息的数量
        $this->assign('level_name', $level_name);
        $this->assign('user', $this->user);
        $this->assign('first_leader', $first_leader['result']);
        $this->assign('ztjnum', $ztjnum);
        $this->assign('teamnum', $teamnum);
        $this->assign('level_list', $level_list);
        $this->assign('canRegForOthers', $canRegForOthers);

        $this->assign('main_link', jump_stitch(tpCache('retail.main_link')));

        //接点人
        // $slippingUser = model('Common/slippingUsers')->where(['slipping_id'=>$this->user['user_id']])->find();
        // p($slippingUser);
        // if($slippingUser){
        //     $referrer     = model('Common/Users')->getBy('user_id',$slippingUser['referrer_id']);
        //     $connecter    = model('Common/Users')->getBy('user_id',$slippingUser['connecter_id']);

        //     $slippingUser['referrer_mobile']  = $referrer  && !empty($referrer['nickname'])?$referrer['nickname']:$referrer['mobile'];
        //     $slippingUser['connecter_mobile'] = $connecter && !empty($connecter['nickname'])?$connecter['nickname']:$connecter['mobile'];
        // }
        // $this->assign('slippingUser', $slippingUser);

        //用户资料
        $direcUser = model('Common/Users')->getBy('user_id',$this->user['first_leader']);
        $userData['directUserMobile'] = !empty($direcUser['nickname'])?$direcUser['nickname']:(isset($direcUser['mobile'])?$direcUser['mobile']:'');
        $userData['userId'] = $this->user_id;
        $userData['nickName'] = $this->user['nickname']?$this->user['nickname']:$this->user['mobile'];
        $userData['creditPoint'] = (int)model('Common/credit/CreditUser')->getBy('user_id',$this->user_id,'total_point');
        $userData['userContribution'] = (int)model('Common/Users')->getFisrtLeaderNumAttr($this->user_id,0);
        $userData['teamContribution'] = (int)model('Common/distribut/DistributTeam')->teamNum($this->user_id);
        $userData['compensateNum'] = (int)model('Common/place/PlaceLossCount')->getBy('user_id',$this->user_id,'loss_num');
        $userData['head_pic'] = model('Common/Users')->where(['user_id'=>$this->user_id])->value('head_pic');
        $userData['temporary_locking_num'] = db('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$this->user_id,'status'=>['in',[1,3]]])->count();
        $this->assign('userData', $userData);

        return $this->fetch();
    }
    /**
     * [getBackground 获取背景图片]
     */
    public function setHeadPic()
    {
        return $this->fetch();
    }



    public function logout()
    {
        $function = function(){
            session_unset();
            session_destroy();
            setcookie('uname', '', time() - 3600, '/');
            setcookie('cn', '', time() - 3600, '/');
            setcookie('user_id', '', time() - 3600, '/');
            setcookie('PHPSESSID', '', time() - 3600, '/');
            setcookie('shareId', '', time() - 3600, '/');
        };

        //第三方s
        if(input('apiKey')){
            $supplyLogic = model('SupplyLogic','logic\supply');
            $supplyLogic->beginLogin();
            $supplyLogic->loginRedirect($this->user_id, $function);
        }
        //第三方e

        $function();
        header("Location:" . U('Mobile/User/login'));
        exit();
    }

    /*
     * 我的钱包
     */
//    public function account()
//    {
//        $user = model('users')->get($this->user_id);
//        //获取账户资金
//        $logic = new UsersLogic();
//        $user['withdraw_account'] = model('common/withdrawals/WithdrawalAccount')->getBy('user_id', $this->user_id);
//
//        $this->assign('user', $user);
//
//        return $this->fetch();
//    }

//    public function account_list()
//    {
//        $type = I('type', '', 'trim');
//        $startTime = I('start_time', 0, 'trim');
//        $endTime = I('end_time', 0, 'trim');
//        $p = I('p', 1, 'intval');
//        $moneyLogMl = model('common/users/UserMoneyLog');
//        $memberLogic = model('common/user/MemberCenterLogic', 'logic');
//
//        $bill = $memberLogic->myBill($this->user_id, $type, $startTime, $endTime, $p);
//        $this->assign('type', $type);
//        $this->assign('bill', $bill['list']);
//        if ($_GET['is_ajax']) {
//            return $this->fetch('ajax_account_list');
//        }
//        $seachType = $moneyLogMl->_seachType;
//        $this->assign('seachType', json_encode($seachType));
//        return $this->fetch();
//    }
//
//    public function account_detail()
//    {
//        $log_id = I('id/d', 0);
//        $detail = model('common/users/UserMoneyLog')->getBy('id', $log_id);
//        // dump($log_id);exit;
//        $this->assign('detail', $detail);
//        return $this->fetch();
//    }
//
//    /**
//     * 优惠券
//     */
//    public function coupon()
//    {
//        $logic = new UsersLogic();
//        $data = $logic->get_coupon($this->user_id, input('type'));
//        foreach ($data['result'] as $k => $v) {
//            $user_type = $v['use_type'];
//            $data['result'][$k]['use_scope'] = C('COUPON_USER_TYPE')["$user_type"];
//            if ($user_type == 1) { //指定商品
//                $data['result'][$k]['goods_id'] = M('goods_coupon')->field('goods_id')->where(['coupon_id' => $v['cid']])->getField('goods_id');
//            }
//            if ($user_type == 2) { //指定分类
//                $data['result'][$k]['category_id'] = Db::name('goods_coupon')->where(['coupon_id' => $v['cid']])->getField('goods_category_id');
//            }
//        }
//        $coupon_list = $data['result'];
//        $this->assign('coupon_list', $coupon_list);
//        $this->assign('page', $data['show']);
//        if (input('is_ajax')) {
//            return $this->fetch('ajax_coupon_list');
//            exit;
//        }
//        return $this->fetch();
//    }
//    
    /**
     * [getBackground 获取背景图片]
     */
    public function getBackground()
    {
        $scene = I('scene');
        if(empty($scene)) exit(json_encode(['status'=>0, 'msg'=>'缺少场景参数', 'data'=>'']));
        $result = (new UserLogic())->getBackground($scene);
        exit(json_encode(['status'=>1, 'msg'=>'请求成功', 'data'=>$result]));
    }

    /**
     *  登录
     */
    public function login()
    {
        // p(cookie('shareId'));
        //第三方接入s
        model('SupplyLogic','logic\supply')->beginLogin();
        //第三方接入e

        //如果是安卓
        if(judge_app('', 'Android')){
            echo '<script> window.android.appLogin(); </script>';exit;
        }
        //如果是ios
        if(judge_app('', 'Ios')){
            echo "<script> window.webkit.messageHandlers.appLogin.postMessage({'signId': 1033,'markId': 1});</script>";exit;
        }
        if ($this->user_id > 0) {
            //第三方s
            model('SupplyLogic','logic\supply')->loginRedirect($this->user_id);
            //第三方e
            
//            header("Location: " . U('Mobile/User/index'));
            $this->redirect('Mobile/User/index');
        }
        
        $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : U("Mobile/User/index");
        $this->assign('referurl', $referurl);
        return $this->fetch();
    }
    /**
     *  短信登录页面
     */
    public function codeLogin()
    {
        //第三方接入s
        model('SupplyLogic','logic\supply')->beginLogin();
        //第三方接入e

        //如果是安卓
        if(judge_app('', 'Android')){
            echo '<script> window.android.appLogin(); </script>';exit;
        }
        //如果是ios
        if(judge_app('', 'Ios')){
            echo "<script> window.webkit.messageHandlers.appLogin.postMessage({'signId': 1033,'markId': 1});</script>";exit;
        }
        if ($this->user_id > 0) {
//            header("Location: " . U('Mobile/User/index'));
            $this->redirect('Mobile/User/index');
        }
            
        $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : U("Mobile/User/index");
        $this->assign('referurl', $referurl);
        return $this->fetch();
    }


    /**
     * 登录
     */
    public function do_login()
    {
        $username = trim(I('post.username'));
        $password = trim(I('post.password'));
        // $code = I('post.mobile_code','');
        // $scene = I('post.scene', 7);  //场景 7用户短信登录身份验证

        //验证码验证
        if (isset($_POST['verify_code'])) {
            $verify_code = I('post.verify_code');
            $verify = new Verify();
            if (!$verify->check($verify_code, 'user_login')) {
                $res = array('status' => 0, 'msg' => '验证码错误');
                exit(json_encode($res));
            }
        }
        $logic = new UsersLogic();
        // if($code){
        //   $session_id = session_id();
        //   $check_code = $logic->check_validate_code($code, $username, 'phone', $session_id, $scene);
        //   if ($check_code['status'] != 1) {
        //       exit(json_encode($check_code));
        //   }
        // }
       
        // $res = $code? $logic->login_mobile($username,$code):$logic->login($username, $password);
        $res = $logic->login($username, $password);
        if ($res['status'] == 1) {
            $res['url'] = htmlspecialchars_decode(I('post.referurl'));
            session('user', $res['result']);
            setcookie('user_id', $res['result']['user_id'], null, '/');
            setcookie('shareId', $res['result']['user_id'], null, '/');
            setcookie('is_distribut', $res['result']['is_distribut'], null, '/');
            $nickname = empty($res['result']['nickname']) ? $username : $res['result']['nickname'];
            setcookie('uname', urlencode($nickname), null, '/');
            setcookie('cn', 0, time() - 3600, '/');
            $cartLogic = new CartLogic();
            $cartLogic->setUserId($res['result']['user_id']);
            $cartLogic->doUserLoginHandle();// 用户登录后 需要对购物车 一些操作
            $orderLogic = new OrderLogic();
            $orderLogic->setUserId($res['result']['user_id']);//登录后将超时未支付订单给取消掉
            $orderLogic->abolishOrder();
        }
        //第三方s
        model('SupplyLogic','logic\supply')->loginRedirect($res['result']['user_id']);
        //第三方e
        exit(json_encode($res));
    }
     /**
     * 手机验证码登录
     */
    public function do_login_mobile()
    {
        $username = trim(I('post.username'));
        $code = I('post.mobile_code', '');
        // p($code);
        $scene = I('post.scene', 7);  //场景 7用户短信登录身份验证
        //验证码验证
        // $session_id = session_id();
        // $check_code = $logic->check_validate_code($code, $username, 'phone', $session_id, $scene);
        // if ($check_code['status'] != 1) {
        //     exit(json_encode($check_code));
        // }

        $logic = new UsersLogic();
        $res = $logic->login_mobile($username,$code);
        if($res['status'] ==-1)$res['status'] = 999;
        if ($res['status'] == 1) {
            $res['url'] = htmlspecialchars_decode(I('post.referurl'));
            session('user', $res['result']);
            setcookie('user_id', $res['result']['user_id'], null, '/');
            setcookie('is_distribut', $res['result']['is_distribut'], null, '/');
            $nickname = empty($res['result']['nickname']) ? $username : $res['result']['nickname'];
            setcookie('uname', urlencode($nickname), null, '/');
            setcookie('cn', 0, time() - 3600, '/');
            $cartLogic = new CartLogic();
            $cartLogic->setUserId($res['result']['user_id']);
            $cartLogic->doUserLoginHandle();// 用户登录后 需要对购物车 一些操作
            $orderLogic = new OrderLogic();
            $orderLogic->setUserId($res['result']['user_id']);//登录后将超时未支付订单给取消掉
            $orderLogic->abolishOrder();
        }
        exit(json_encode($res));
    }


    /**
     *  注册 (不用)
     */
   public function reg()
   {
       if ($this->user_id > 0) {
           $this->redirect(U('Mobile/User/index'));
       }
       $reg_sms_enable = tpCache('sms.regis_sms_enable');
       $reg_smtp_enable = tpCache('sms.regis_smtp_enable');

       if (IS_POST) {
           $logic = new UsersLogic();
           //验证码检验
           $this->verifyHandle('user_reg');
           $nickname = I('post.nickname', '');
           $username = I('post.username', '');
           $password = I('post.password', '');
           $password2 = I('post.password2', '');
           $paytypeWx = I('post.paytypeWx', '');
           $paytypeZfb = I('post.paytypeZfb', '');
           //检测用户是否已存在
            if(model('Common/Users')->where(['mobile'=>$username])->find()){
              $this->ajaxReturn(array('status' => -1, 'msg' => '用户已存在'));
              exit;
            }

           $is_bind_account = tpCache('basic.is_bind_account');
           //是否开启注册验证码机制
           $code = I('post.mobile_code', '');
           $scene = I('post.scene', 1);

           $session_id = session_id();

           //是否开启注册验证码机制
           if (check_mobile($username)) {
               if ($reg_sms_enable) {
                   //手机功能没关闭
                   $check_code = $logic->check_validate_code($code, $username, 'phone', $session_id, $scene);
                   if ($check_code['status'] != 1) {
                       $this->ajaxReturn($check_code);
                   }
               }
           }
           //是否开启注册邮箱验证码机制
           if (check_email($username)) {
               if ($reg_smtp_enable) {
                   //邮件功能未关闭
                   $check_code = $logic->check_validate_code($code, $username);
                   if ($check_code['status'] != 1) {
                       $this->ajaxReturn($check_code);
                   }
               } else {
                   if (!$this->verifyHandle('user_reg')) {
                       $this->ajaxReturn(array('status' => -1, 'msg' => '图像验证码错误'));
                       exit;
                   };
               }
           }
           // $slippingUsersMdl = model('Common/SlippingUsers');
           $slippingUsersLogic = new SlippingUsersLogic;
           $invite = $inviter = I('invite');
           if (!empty($invite)) {
               $invite = get_user_info($invite, 2);//根据手机号查找邀请人

               if (empty($invite)) {
                   $this->ajaxReturn(['status' => -1, 'msg' => '推荐人不存在', 'result' => '']);
               }
               if ($invite['level'] <= 1) {
                   $this->ajaxReturn(['status' => -1, 'msg' => '该账号暂无推荐权限', 'result' => '']);
               }
               //判断推荐人账号是否被冻结
               if ($invite['is_lock'] == 1) {
                   $this->ajaxReturn(['status' => -1, 'msg' => '该账号暂时被冻结无推荐权限', 'result' => '']);
               }
               //判断推荐人是否为引荐人 即被推荐人是否为滑落用户
               // $res = $slippingUsersMdl->isDirecter($invite['user_id']);
               // $res = $slippingUsersLogic->slippingRule($invite['user_id'],'referrer');
               // if($res['status'] ==1){
               //  unset($invite);
               //  $invite = get_user_info($res['data'], 0);//根据接点人ID查找接点人
               // }



           } else {
               $invite = array();
           }
           cookie('first_leader', $invite['user_id']);
           if ($is_bind_account && session("third_oauth")) { //绑定第三方账号
               $thirdUser = session("third_oauth");
               $head_pic = $thirdUser['head_pic'];
               $data = $logic->reg($username, $password, $password2, 0, $invite, $nickname, $head_pic);
               //用户注册成功后, 绑定第三方账号
               $userLogic = new UsersLogic();
               $data = $userLogic->oauth_bind_new($data['result']);
           } else {
               $data = $logic->reg($username, $password, $password2, 0, $invite,$nickname);
               //添加用户支付宝跟微信
              if ($data && ($paytypeZfb || $paytypeWx)){
                $paytypeModel = model('common/Paytype');

                $resZfb = !empty($paytypeZfb) ? $paytypeModel->addOrSave($data['result']['user_id'], 1, ['account' => $paytypeZfb, 'type' => 1]) : '';
                $resWx  = !empty($paytypeWx) ? $paytypeModel->addOrSave($data['result']['user_id'], 2, ['account' => $paytypeWx, 'type' => 2]) : '';

              }
           }


           if ($data['status'] != 1) $this->ajaxReturn($data);

           //注册成功后 如果注册是滑落人添加滑落人记录
           // $inviter = get_user_info($inviter, 2);//根据手机号查找邀请人
           // if ( $data['result']['first_leader'] != $inviter['user_id']){
           //    // $res = $slippingUsersMdl->toConnect($data['result']['first_leader'],$data['result']['user_id'],$inviter['user_id']);
           //    $res = $slippingUsersLogic->slippingRule($data['result']['user_id'],'connecter','',$inviter['user_id'],$data['result']['first_leader']);
           //    if($res['status'] ==0) $this->ajaxReturn(array('status' => -1, 'msg' => '注册成功,添加滑落人记录失败'));
           //  }

           //获取公众号openid,并保持到session的user中
           $oauth_users = M('OauthUsers')->where(['user_id' => $data['result']['user_id'], 'oauth' => 'weixin', 'oauth_child' => 'mp'])->find();
           $oauth_users && $data['result']['open_id'] = $oauth_users['open_id'];

           session('user', $data['result']);
           setcookie('user_id', $data['result']['user_id'], null, '/');
           setcookie('is_distribut', $data['result']['is_distribut'], null, '/');
           $cartLogic = new CartLogic();
           $cartLogic->setUserId($data['result']['user_id']);
           $cartLogic->doUserLoginHandle();// 用户登录后 需要对购物车 一些操作
           $this->ajaxReturn($data);
           exit;
       }
       $shareId = I('uid', 0, 'intval');
       $shareId || $shareId = cookie('shareId');
       if ($shareId) {
           $tk = get_user_info($shareId);
           $this->assign('tk_mobile', $tk['mobile']);
           $this->assign('shareId', $shareId);
       }
       $this->assign('regis_sms_enable', $reg_sms_enable); // 注册启用短信：
       $this->assign('regis_smtp_enable', $reg_smtp_enable); // 注册启用邮箱：
       $sms_time_out = tpCache('sms.sms_time_out') > 0 ? tpCache('sms.sms_time_out') : 120;
       $this->assign('sms_time_out', $sms_time_out); // 手机短信超时时间
       return $this->fetch();
   }

    /**
     * User: Simony
     * Function:为他人注册
     * @return mixed
     */
    public function regForOther()
    {
        if($this->user_id == 1)  $this->error('不要拿顶级账号为他人注册');

        // if($this->user['level'] <= 1)   $this->error('暂无帮助他人注册权限！');

        if (IS_POST) {
            $logic = new UsersLogic();
            //验证码检验
            $nickname = I('post.nickname', '');
            $username = I('post.username', '');
            $password = I('post.password', '');
            $password2 = I('post.password2', '');
            $paytypeWx = I('post.paytypeWx', '');
            $paytypeZfb = I('post.paytypeZfb', '');

            //检测用户是否已存在
            if(model('Common/Users')->where(['mobile'=>$username])->find()){
              $this->ajaxReturn(array('status' => -1, 'msg' => '用户已存在'));
              exit;
            }

            $is_bind_account = tpCache('basic.is_bind_account');


            $session_id = session_id();

            if (!$this->verifyHandle('user_reg')) {
                $this->ajaxReturn(array('status' => -1, 'msg' => '图像验证码错误'));
                exit;
            };
            $invite = $this->user;

            //判断用户是否可以成为引荐人（即被注册人是否为滑落人）
            // $slippingUsersMdl = model('Common/SlippingUsers');
            // $slippingUsersLogic = new SlippingUsersLogic;
            // $res = $slippingUsersLogic->slippingRule($invite['user_id'],'referrer');
            // if($res['status'] ==1){
            //   unset($invite);
            //   $invite = get_user_info($res['data'], 0); //根据接点人ID查找接点人
            // }

            cookie('first_leader', $invite['user_id']);
            $data = $logic->reg($username, encrypt($password), encrypt($password2), 0, $invite,$nickname);
            if ($data['status'] != 1) $this->ajaxReturn($data);

            // //注册成功后 如果注册是滑落人添加滑落人记录
            // if ( $data['result']['first_leader'] != $this->user_id){
            //   $res = $slippingUsersLogic->slippingRule($data['result']['user_id'],'connecter','',$this->user_id,$data['result']['first_leader']);

            //    // $res = $slippingUsersMdl->toConnect($data['result']['first_leader'],$data['result']['user_id'],$this->user_id);
            //    if($res['status'] ==0) $this->ajaxReturn(array('status' => -1, 'msg' => '添加滑落人记录失败'));
            // }

            //用户直推人数增加 判断是否可以成为接点人 如果是则添加接点人记录
            // $result = $slippingUsersMdl->changeUserToConnecter($this->user_id,'regForOther');

            //添加用户支付宝跟微信
            if ($paytypeZfb || $paytypeWx){
              $paytypeModel = model('common/Paytype');

              $resZfb = !empty($paytypeZfb) ? $paytypeModel->addOrSave($data['result']['user_id'], 1, ['account' => $paytypeZfb, 'type' => 1]) : '';
              $resWx  = !empty($paytypeWx) ? $paytypeModel->addOrSave($data['result']['user_id'], 2, ['account' => $paytypeWx, 'type' => 2]) : '';

            }
            $this->ajaxReturn($data);
            exit;
        }
        $uid = I('uid', 0, 'intval');
        if ($uid) {
            $tk = get_user_info($uid);
            $this->assign('tk_mobile', $tk['mobile']);
        }
        $sms_time_out = tpCache('sms.sms_time_out') > 0 ? tpCache('sms.sms_time_out') : 120;
        $this->assign('sms_time_out', $sms_time_out); // 手机短信超时时间
        return $this->fetch();
    }

    public function bind_guide()
    {
        $data = session('third_oauth');
        //没有第三方登录的话就跳到登录页
        if (empty($data)) {
            $this->redirect('User/login');
        }
        $this->assign("nickname", $data['nickname']);
        $this->assign("oauth", $data['oauth']);
        $this->assign("head_pic", $data['head_pic']);
        return $this->fetch();
    }

    /**
     * 绑定已有账号
     * @return \think\mixed
     */
    public function bind_account()
    {
        $mobile = input('mobile/s');
        $verify_code = input('verify_code/s');
        //发送短信验证码
        $logic = new UsersLogic();
        $check_code = $logic->check_validate_code($verify_code, $mobile, 'phone', session_id(), 1);
        if ($check_code['status'] != 1) {
            $this->ajaxReturn(['status' => 0, 'msg' => $check_code['msg'], 'result' => '']);
        }
        if (empty($mobile) || !check_mobile($mobile)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '手机格式错误']);
        }
        $users = Db::name('users')->where('mobile', $mobile)->find();
        if (empty($users)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '账号不存在']);
        }
        $user = new \app\common\logic\User();
        $user->setUserById($users['user_id']);
        $cartLogic = new CartLogic();
        try {
            $user->checkOauthBind();
            $user->oauthBind();
            $user->doLeader();
            $user->refreshCookie();
            $cartLogic->setUserId($users['user_id']);
            $cartLogic->doUserLoginHandle();
            $orderLogic = new OrderLogic();//登录后将超时未支付订单给取消掉
            $orderLogic->setUserId($users['user_id']);
            $orderLogic->abolishOrder();
            $this->ajaxReturn(['status' => 1, 'msg' => '绑定成功']);
        } catch (TpshopException $t) {
            $error = $t->getErrorArr();
            $this->ajaxReturn($error);
        }
    }

    /**
     * 先注册再绑定账号
     * @return \think\mixed
     */
    public function bind_reg()
    {
        $mobile = input('mobile/s');
        $verify_code = input('verify_code/s');
        $password = input('password/s');
        $nickname = input('nickname/s', '');
        if (empty($mobile) || !check_mobile($mobile)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '手机格式错误']);
        }
        if (empty($password)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '请输入密码']);
        }
        $logic = new UsersLogic();
        $check_code = $logic->check_validate_code($verify_code, $mobile, 'phone', session_id(), 1);
        if ($check_code['status'] != 1) {
            $this->ajaxReturn(['status' => 0, 'msg' => $check_code['msg'], 'result' => '']);
        }
        $thirdUser = session('third_oauth');
        $data = $logic->reg($mobile, $password, $password, 0, [], $nickname, $thirdUser['head_pic']);
        if ($data['status'] != 1) {
            $this->ajaxReturn(['status' => 0, 'msg' => $data['msg'], 'result' => '']);
        }
        $user = new \app\common\logic\User();
        $user->setUserById($data['result']['user_id']);
        try {
            $user->checkOauthBind();
            $user->oauthBind();
            $user->refreshCookie();
            $this->ajaxReturn(['status' => 1, 'msg' => '绑定成功']);
        } catch (TpshopException $t) {
            $error = $t->getErrorArr();
            $this->ajaxReturn($error);
        }
    }

    public function ajaxAddressList()
    {
        $UserAddress = new UserAddress();
        $address_list = $UserAddress->where('user_id', $this->user_id)->order('is_default desc')->select();
        if ($address_list) {
            $address_list = collection($address_list)->append(['address_area'])->toArray();
        } else {
            $address_list = [];
        }
        $this->ajaxReturn($address_list);
    }

    /*
     * 用户地址列表
     */
    public function address_list()
    {
        $referurl = $_SERVER['HTTP_REFERER']?:$_SERVER['REQUEST_URI'];
        $address_lists = get_user_address_list($this->user_id);
        $region_list = get_region_list();
        $this->assign('region_list', $region_list);
        $this->assign('lists', $address_lists);
        $this->assign('referurl', $referurl);
        return $this->fetch();
    }

    /**
     * 保存地址
     */
    public function addressSave()
    {
        $address_id = input('address_id/d', 0);
        $data = input('post.');
        $userAddressValidate = Loader::validate('UserAddress');
        if (!$userAddressValidate->batch()->check($data)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => $userAddressValidate->getError()]);
        }
        if (!empty($address_id)) {
            //编辑
            $userAddress = UserAddress::get(['address_id' => $address_id, 'user_id' => $this->user_id]);
            if (empty($userAddress)) {
                $this->ajaxReturn(['status' => 0, 'msg' => '参数错误']);
            }
        } else {
            //新增
            $userAddress = new UserAddress();
            $user_address_count = Db::name('user_address')->where("user_id", $this->user_id)->count();
            if ($user_address_count >= 20) {
                $this->ajaxReturn(['status' => 0, 'msg' => '最多只能添加20个收货地址']);
            }
            $data['user_id'] = $this->user_id;
        }
        $userAddress->data($data);
        $userAddress['longitude'] = true;
        $userAddress['latitude'] = true;
        $row = $userAddress->save();
        if ($row !== false) {
            $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => ['address_id' => $userAddress->address_id]]);
        } else {
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败']);
        }
    }

    /*
     * 添加地址
     */
    public function add_address()
    {
        $source = input('source');
        if (IS_POST) {
            $post_data = input('post.');
            $logic = new UsersLogic();
            $data = $logic->add_address($this->user_id, 0, $post_data);
            
            //用户新增第一个地址添加到商城为默认地址
            // if(empty($post_data['id'])){
            //     $url = tpCache('retail.api_link').'/api/Area/addUserAddress';
            // // p($url);
            //     $data = [
            //         'member_id'=>$this->user_id,
            //         'address_realname'=>$post_data['consignee'],
            //         'province_id'=>$post_data['province'],
            //         'city_id'=>$post_data['city'],
            //         'district_id'=>$post_data['district'],
            //         'area_id'=>$post_data['city'],
            //         'address_detail'=>$post_data['address'],
            //         'address_mob_phone'=>$post_data['mobile'],
            //     ];

            //     $res = httpRequest($url,'POST',$data);
            //     // p($httpRequest);
            // }

            $goods_id = input('goods_id/d');
            $item_id = input('item_id/d');
            $goods_num = input('goods_num/d');
            $order_id = input('order_id/d');
            $action = input('action');
            if ($data['status'] != 1) {
                $this->ajaxReturn($data);
            } else {
                $preurl = trim(I('preurl'));
                $type = I('type');
                switch ($type) {
                  case 'address':
                    $typeStr = '?address_id='.$data['result'];
                    break;
                  case 'croos':
                    $typeStr = '?croos_address_id='.$data['result'];
                    break;
                }
                $data['url'] = !empty($preurl)?base64($preurl,'decode').$typeStr:U('/Mobile/User/address_list');
                $this->ajaxReturn($data);
            }

        }
        $p = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        $this->assign('province', $p);
        $this->assign('source', $source);
        return $this->fetch();

    }

    /*
     * 地址编辑
     */
    public function edit_address()
    {
        $id = I('id/d');
        $address = M('user_address')->where(array('address_id' => $id, 'user_id' => $this->user_id))->find();
        if (IS_POST) {
            $post_data = input('post.');
            $source = $post_data['source'];
            $logic = new UsersLogic();
            $data = $logic->add_address($this->user_id, $id, $post_data);
            if ($data['status'] != 1) {
                $this->error($data['msg']);
            } else {
                $preurl = trim(I('preurl'));
                $type = I('type');
                switch ($type) {
                  case 'address':
                    $typeStr = '?address_id='.$data['result'];
                    break;
                  case 'croos':
                    $typeStr = '?croos_address_id='.$data['result'];
                    break;
                }
                $data['url'] = !empty($preurl)?base64($preurl,'decode').$typeStr:U('/Mobile/User/address_list');
                $this->ajaxReturn($data);
            }
        }
        //获取省份
        $p = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        $c = M('region')->where(array('parent_id' => $address['province'], 'level' => 2))->select();
        $d = M('region')->where(array('parent_id' => $address['city'], 'level' => 3))->select();
        if ($address['twon']) {
            $e = M('region')->where(array('parent_id' => $address['district'], 'level' => 4))->select();
            $this->assign('twon', $e);
        }
        $this->assign('province', $p);
        $this->assign('city', $c);
        $this->assign('district', $d);
        $this->assign('address', $address);
        return $this->fetch();
    }

    /*
     * 设置默认收货地址
     */
    public function set_default()
    {
        $id = I('get.id/d');
        $source = I('get.source');

        M('user_address')->where(array('user_id' => $this->user_id))->save(array('is_default' => 0));
        $row = M('user_address')->where(array('user_id' => $this->user_id, 'address_id' => $id))->save(array('is_default' => 1));
        if ($row) {
          $data = ['status'=>1,'msg'=>'设置成功'];
          $preurl = trim(I('preurl'));
          $type = I('type');
          switch ($type) {
            case 'address':
              $typeStr = '?address_id='.$id;
              break;
            case 'croos':
              $typeStr = '?croos_address_id='.$id;
              break;
          }
          $data['url'] = !empty($preurl)?base64($preurl,'decode').$typeStr:U('/Mobile/User/address_list');
          return $this->ajaxReturn($data);
        }
        return $this->ajaxReturn(['status'=>0,'msg'=>'设置失败']);
        header("Location:" . U('Mobile/User/address_list'));

    }

    /*
     * 地址删除
     */
    public function del_address()
    {
        $id = I('request.id/d');
        $type = I('request.type');
        $preurl = I('request.preurl');

        $address = M('user_address')->where("address_id", $id)->find();
        $row = M('user_address')->where(array('user_id' => $this->user_id, 'address_id' => $id))->delete();
        // 如果删除的是默认收货地址 则要把第一个地址设置为默认收货地址
        if ($address['is_default'] == 1) {
            $address2 = M('user_address')->where("user_id", $this->user_id)->find();
            $address2 && M('user_address')->where("address_id", $address2['address_id'])->save(array('is_default' => 1));
        }
        if (!$row)
          return  $this->ajaxReturn(['status'=>0,'msg'=>'操作失败', 'url'=>U('User/address_list',['preurl'=>$preurl,'type'=>$type])]);
        else
          return  $this->ajaxReturn(['status'=>1,'msg'=>"操作成功",  'url'=>U('User/address_list',['preurl'=>$preurl,'type'=>$type])]);

    }


    /*
     * 个人信息
     */
    public function userInfo()
    {
        $userLogic = new UsersLogic();
        $user_info = $userLogic->get_info($this->user_id); // 获取用户信息
        $user_info = $user_info['result'];
        $first_leader = $userLogic->get_info($this->user['first_leader']);
        $first_leader = $first_leader['result'];
        if (IS_POST) {
            if ($_FILES['head_pic']['tmp_name']) {
                $file = $this->request->file('head_pic');
                $image_upload_limit_size = config('image_upload_limit_size');
                $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
                $dir = UPLOAD_PATH . 'head_pic/';
                if (!($_exists = file_exists($dir))) {
                    $isMk = mkdir($dir);
                }
                $parentDir = date('Ymd');
                $info = $file->validate($validate)->move($dir, true);
                if ($info) {
                    $post['head_pic'] = '/' . $dir . $parentDir . '/' . $info->getFilename();
                } else {
                    $this->error($file->getError());//上传错误提示错误信息
                }
            }
            I('post.nickname') ? $post['nickname'] = I('post.nickname') : false; //昵称
            I('post.qq') ? $post['qq'] = I('post.qq') : false;  //QQ号码
            I('post.head_pic') ? $post['head_pic'] = I('post.head_pic') : false; //头像地址
            I('post.sex') ? $post['sex'] = I('post.sex') : $post['sex'] = 0;  // 性别
            I('post.birthday') ? $post['birthday'] = strtotime(I('post.birthday')) : false;  // 生日
            I('post.province') ? $post['province'] = I('post.province') : false;  //省份
            I('post.city') ? $post['city'] = I('post.city') : false;  // 城市
            I('post.district') ? $post['district'] = I('post.district') : false;  //地区
            I('post.email') ? $post['email'] = I('post.email') : false; //邮箱
            I('post.mobile') ? $post['mobile'] = I('post.mobile') : false; //手机

            $email = I('post.email');
            $mobile = I('post.mobile');
            $code = I('post.mobile_code', '');
            $scene = I('post.scene', 6);

            if (!empty($email)) {
                $c = M('users')->where(['email' => input('post.email'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("邮箱已被使用");
            }
            if (!empty($mobile)) {
                $c = M('users')->where(['mobile' => input('post.mobile'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("手机已被使用");
                if (!$code)
                    $this->error('请输入验证码');
                $check_code = $userLogic->check_validate_code($code, $mobile, 'phone', $this->session_id, $scene);
                if ($check_code['status'] != 1)
                    $this->error($check_code['msg']);
            }

            if (!$userLogic->update_info($this->user_id, $post))
                $this->error("保存失败");
            setcookie('uname', urlencode($post['nickname']), null, '/');
            $this->success("操作成功", U('User/userInfo'));
            exit;
        }
        //  获取省份
        $province = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        //  获取订单城市
        $city = M('region')->where(array('parent_id' => $user_info['province'], 'level' => 2))->select();
        //  获取订单地区
        $area = M('region')->where(array('parent_id' => $user_info['city'], 'level' => 3))->select();
        $this->assign('province', $province);
        $this->assign('city', $city);
        $this->assign('area', $area);
        $this->assign('user', $user_info);
        $this->assign('first_leader', $first_leader);
        $this->assign('sex', C('SEX'));
        //从哪个修改用户信息页面进来，
        $dispaly = I('action');
        if ($dispaly != '') {
            return $this->fetch("$dispaly");
        }
        return $this->fetch();
    }

    /**
     * 修改绑定手机
     * @return mixed
     */
    public function setMobile()
    {
        $userLogic = new UsersLogic();
        $type = I('type',1,'intval');
        
        $title = $type == 1 ? '绑定手机号' : '备用手机号';
        if (IS_POST) {
            $mobile =input('mobile');
            $verify_code = I('verify_code');
            $password = I('password', '');
            $status = I('status', 0);
            $type = I('type',0,'intval');

            if($type != 1 && $type != 2){
                if(IS_AJAX) exit(api_ajaxReturn(0,'type值只能为1，或者2'));
                $this->error('type值只能为1，或者2');
            }
            //p($type);

            if (!$this->verifyHandle('forget')){
              if(IS_AJAX) exit(api_ajaxReturn(0,'图像验证码错误'));
              $this->error('图像验证码错误');
            } 
            if (encrypt($password) != $this->user['password']) {
              if(IS_AJAX) exit(api_ajaxReturn(0,'登录密码不正确'));
              $this->error('登录密码不正确');
            }


            if($type == 1)
            {
              $c = Db::name('users')->where(['mobile' => $mobile, 'user_id' => ['<>', $this->user_id]])->count();
              if($c){
                if(IS_AJAX) exit(api_ajaxReturn(0,'手机已被使用'));
                $this->error('手机已被使用');
              }
              $beforeMobile = Db::name('users')->where(['user_id' => $this->user_id])->value('mobile');
              $res = Db::name('users')->where(['user_id' => $this->user_id])->update(['mobile' => $mobile]);
              //添加修改手机号记录
               model('commen/MobileChangeLog')->addLog($this->user_id,$type,$beforeMobile,$mobile,'用户修改手机号');
            }else{
              // $c = Db::name('users')->where(['mobile_bak' => $mobile, 'user_id' => ['<>', $this->user_id]])->count();
              // $c && $this->error('备用手机已被使用');
              $beforeMobileBak = Db::name('users')->where(['user_id' => $this->user_id])->value('mobile_bak');
              $res = Db::name('users')->where(['user_id' => $this->user_id])->update(['mobile_bak' => $mobile]);
              //添加修改手机号记录
              model('commen/MobileChangeLog')->addLog($this->user_id,$type,$beforeMobileBak,$mobile,'用户修改备用手机号');
            }

            if ($res) {
                $source = I('source');
                !empty($source) && $this->success('绑定成功', U("User/$source"));
                if(IS_AJAX) exit(api_ajaxReturn(1,'修改成功',U('User/userDetail')));
                $this->success('修改成功', U('User/userDetail'));
            }
            if(IS_AJAX) exit(api_ajaxReturn(0,'修改失败'));
            $this->error('修改失败');
        }
        $this->assign('status', $status);
        $this->assign('title', $title);
        $this->assign('type', $type);
        return $this->fetch();
    }

    /*
     * 邮箱验证
     */
    public function email_validate()
    {
        $userLogic = new UsersLogic();
        $user_info = $userLogic->get_info($this->user_id); // 获取用户信息
        $user_info = $user_info['result'];
        $step = I('get.step', 1);
        //验证是否未绑定过
        if ($user_info['email_validated'] == 0)
            $step = 2;
        //原邮箱验证是否通过
        if ($user_info['email_validated'] == 1 && session('email_step1') == 1)
            $step = 2;
        if ($user_info['email_validated'] == 1 && session('email_step1') != 1)
            $step = 1;
        if (IS_POST) {
            $email = I('post.email');
            $code = I('post.code');
            $info = session('email_code');
            if (!$info)
                $this->error('非法操作');
            if ($info['email'] == $email || $info['code'] == $code) {
                if ($user_info['email_validated'] == 0 || session('email_step1') == 1) {
                    session('email_code', null);
                    session('email_step1', null);
                    if (!$userLogic->update_email_mobile($email, $this->user_id))
                        $this->error('邮箱已存在');
                    $this->success('绑定成功', U('Home/User/index'));
                } else {
                    session('email_code', null);
                    session('email_step1', 1);
                    redirect(U('Home/User/email_validate', array('step' => 2)));
                }
                exit;
            }
            $this->error('验证码邮箱不匹配');
        }
        $this->assign('step', $step);
        return $this->fetch();
    }

    /*
    * 手机验证
    */
    public function mobile_validate()
    {
        $userLogic = new UsersLogic();
        $user_info = $userLogic->get_info($this->user_id); // 获取用户信息
        $user_info = $user_info['result'];
        $step = I('get.step', 1);
        //验证是否未绑定过
        if ($user_info['mobile_validated'] == 0)
            $step = 2;
        //原手机验证是否通过
        if ($user_info['mobile_validated'] == 1 && session('mobile_step1') == 1)
            $step = 2;
        if ($user_info['mobile_validated'] == 1 && session('mobile_step1') != 1)
            $step = 1;
        if (IS_POST) {
            $mobile = I('post.mobile');
            $code = I('post.code');
            $info = session('mobile_code');
            if (!$info)
                $this->error('非法操作');
            if ($info['email'] == $mobile || $info['code'] == $code) {
                if ($user_info['email_validated'] == 0 || session('email_step1') == 1) {
                    session('mobile_code', null);
                    session('mobile_step1', null);
                    if (!$userLogic->update_email_mobile($mobile, $this->user_id, 2))
                        $this->error('手机已存在');
                    $this->success('绑定成功', U('Home/User/index'));
                } else {
                    session('mobile_code', null);
                    session('email_step1', 1);
                    redirect(U('Home/User/mobile_validate', array('step' => 2)));
                }
                exit;
            }
            $this->error('验证码手机不匹配');
        }
        $this->assign('step', $step);
        return $this->fetch();
    }

    /**
     * 用户收藏列表
     */
    public function collect_list()
    {
        $userLogic = new UsersLogic();
        $data = $userLogic->get_goods_collect($this->user_id);
        $this->assign('page', $data['show']);// 赋值分页输出
        $this->assign('goods_list', $data['result']);
        if (IS_AJAX) {      //ajax加载更多
            return $this->fetch('ajax_collect_list');
            exit;
        }
        return $this->fetch();
    }

    /*
     *取消收藏
     */
    public function cancel_collect()
    {
        $collect_id = I('collect_id/d');
        $user_id = $this->user_id;
        if (M('goods_collect')->where(['collect_id' => $collect_id, 'user_id' => $user_id])->delete()) {
            $this->success("取消收藏成功", U('User/collect_list'));
        } else {
            $this->error("取消收藏失败", U('User/collect_list'));
        }
    }

    /**
     * 我的留言
     */
    public function message_list()
    {
        C('TOKEN_ON', true);
        if (IS_POST) {
            if (!$this->verifyHandle('message')) {
                $this->error('验证码错误', U('User/message_list'));
            };

            $data = I('post.');
            $data['user_id'] = $this->user_id;
            $user = session('user');
            $data['user_name'] = $user['nickname'];
            $data['msg_time'] = time();
            if (M('feedback')->add($data)) {
                $this->success("留言成功", U('User/message_list'));
                exit;
            } else {
                $this->error('留言失败', U('User/message_list'));
                exit;
            }
        }
        $msg_type = array(0 => '留言', 1 => '投诉', 2 => '询问', 3 => '售后', 4 => '求购');
        $count = M('feedback')->where("user_id", $this->user_id)->count();
        $Page = new Page($count, 100);
        $Page->rollPage = 2;
        $message = M('feedback')->where("user_id", $this->user_id)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $showpage = $Page->show();
        header("Content-type:text/html;charset=utf-8");
        $this->assign('page', $showpage);
        $this->assign('message', $message);
        $this->assign('msg_type', $msg_type);
        return $this->fetch();
    }

    /**账户明细*/
    public function points()
    {
        $type = I('type', 'all');    //获取类型
        $this->assign('type', $type);
        if ($type == 'recharge') {
            //充值明细
            $count = M('recharge')->where("user_id", $this->user_id)->count();
            $Page = new Page($count, 16);
            $account_log = M('recharge')->where("user_id", $this->user_id)->order('order_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        } else if ($type == 'points') {
            //积分记录明细
            $count = M('account_log')->where(['user_id' => $this->user_id, 'pay_points' => ['<>', 0]])->count();
            $Page = new Page($count, 16);
            $account_log = M('account_log')->where(['user_id' => $this->user_id, 'pay_points' => ['<>', 0]])->order('log_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        } else {
            //全部
            $count = M('account_log')->where(['user_id' => $this->user_id])->count();
            $Page = new Page($count, 16);
            $account_log = M('account_log')->where(['user_id' => $this->user_id])->order('log_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        }
        $show = $Page->show();
        $this->assign('account_log', $account_log);
        $this->assign('page', $show);
        $this->assign('listRows', $Page->listRows);
        if ($_GET['is_ajax']) {
            return $this->fetch('ajax_points');
            exit;
        }
        return $this->fetch();
    }


    public function points_list()
    {
        $type = I('type', 'all');
        $usersLogic = new UsersLogic;
        $result = $usersLogic->points($this->user_id, $type);

        $this->assign('type', $type);
        $showpage = $result['page']->show();
        $this->assign('account_log', $result['account_log']);
        $this->assign('page', $showpage);
        if ($_GET['is_ajax']) {
            return $this->fetch('ajax_points');
        }
        return $this->fetch();
    }


    /*
     * 密码修改
     */
    public function password()
    {
        if (IS_POST) {
            $logic = new UsersLogic();
            $data = $logic->get_info($this->user_id);
            $user = $data['result'];
            if ($user['mobile'] == '' && $user['email'] == '')
                $this->ajaxReturn(['status' => -1, 'msg' => '请先绑定手机或邮箱', 'url' => U('/Mobile/User/userDetail')]);
            $userLogic = new UsersLogic();
            $data = $userLogic->password($this->user_id, I('post.old_password'), I('post.new_password'), I('post.confirm_password'));
            if ($data['status'] == -1)
                $this->ajaxReturn(['status' => -1, 'msg' => $data['msg']]);
            $this->ajaxReturn(['status' => 1, 'msg' => $data['msg'], 'url' => U('/Mobile/User/userDetail')]);
            exit;
        }
        return $this->fetch();
    }

   function forget_pwd()
   {
       if ($this->user_id > 0) {
           $this->redirect("User/index");
       }
       if (IS_POST) {
           $username = I('mobile');
           $password = I('post.password');
           if (!empty($username)) {
               if (!$this->verifyHandle('forget_pwd')) {
                   $this->ajaxReturn(['status' => -1, 'msg' => "验证码错误"]);
               };
               $field = 'mobile';

               if (check_email($username)) {
                   $field = 'email';
               }

               $userModel = M('users');
               $user = $userModel->where("email", $username)->whereOr('mobile', $username)->find();
               if ($user) {
//                    $sms_status = checkEnableSendSms(2);
//                    session('find_password', array('user_id' => $user['user_id'], 'username' => $username,
//                        'email' => $user['email'], 'mobile' => $user['mobile'], 'type' => $field, 'sms_status' => $sms_status['status']));
//                    $regis_smtp_enable = $this->tpshop_config['smtp_regis_smtp_enable'];
                   $res=M('users')->where("user_id", $user['user_id'])->save(array('password' => encrypt($password)));
                   $this->ajaxReturn(['status' => 1, 'msg' => "操作成功", 'url' => U('User/Index')]);
                   /*if($res){
                       $this->ajaxReturn(['status' => 1, 'msg' => "操作成功", 'url' => U('User/Index')]);
                   }else{
                       $this->ajaxReturn(['status' => -1, 'msg' => "操作失败"]);
                   }*/
//                    $this->ajaxReturn(['status' => -1, 'msg' => "未开启忘记密码场景", 'url' => U('User/find_pwd')]);
                   exit;
               } else {
                   $this->ajaxReturn(['status' => -1, 'msg' => "用户名不存在，请检查"]);
               }
           }
       }
       return $this->fetch();
   }

    function find_pwd()
    {
        if ($this->user_id > 0) {
            header("Location: " . U('User/index'));
        }
        $user = session('find_password');
        if (empty($user)) {
            $this->error("请先验证用户名", U('User/forget_pwd'));
        }
        $this->assign('user', $user);
        return $this->fetch();
    }


    public function set_pwd()
    {
        if ($this->user_id > 0) {
            $this->redirect('Mobile/User/index');
        }
        $check = session('validate_code');
        $find_password = session('find_password');
        $field = $find_password['field'];
        $sms_status = session('find_password')['sms_status'];
        $regis_smtp_enable = $this->tpshop_config['smtp_regis_smtp_enable'];
        $is_check_code = false;
        //需要验证邮箱或者手机
        if ($field == 'email' && $regis_smtp_enable == 1) $is_check_code = true;
        if ($field == 'mobile' && $sms_status['status'] == 1) $is_check_code = true;
        if ((empty($check) || $check['is_check'] == 0) && $is_check_code) {
            $this->error('验证码还未验证通过', U('User/forget_pwd'));
        }
        if (IS_POST) {
            $data['password'] = $password = I('post.password');
            $data['password2'] = $password2 = I('post.password2');
            $UserRegvalidate = Loader::validate('User');
            if (!$UserRegvalidate->scene('set_pwd')->check($data)) {
                $this->error($UserRegvalidate->getError(), U('User/forget_pwd'));
            }
            M('users')->where("user_id", $find_password['user_id'])->save(array('password' => encrypt($password)));
            session('validate_code', null);
            return $this->fetch('reset_pwd_sucess');
        }
        $is_set = I('is_set', 0);
        $this->assign('is_set', $is_set);
        return $this->fetch();
    }

    /**
     * 验证码验证
     * $id 验证码标示
     */
    private function verifyHandle($id)
    {
        $verify = new Verify();
        if (!$verify->check(I('post.verify_code'), $id ? $id : 'user_login')) {
            return false;
        }
        return true;
    }

    /**
     * 验证码获取
     */
    public function verify()
    {
        //验证码类型
        $type = I('get.type') ? I('get.type') : 'user_login';
        $config = array(
            'fontSize' => 30,
            'length' => 4,
            'imageH' => 60,
            'imageW' => 300,
            'fontttf' => '5.ttf',
            'useCurve' => false,
            'useNoise' => false,
        );
        $Verify = new Verify($config);
        $Verify->entry($type);
        exit();
    }

    /**
     * 账户管理
     */
    public function accountManage()
    {
        return $this->fetch();
    }

    public function recharge()
    {
        $order_id = I('order_id/d');
        $paymentList = M('Plugin')->where(['type' => 'payment', 'code' => ['neq', 'cod'], 'status' => 1, 'scene' => ['in', '0,1']])->select();
        $paymentList = convert_arr_key($paymentList, 'code');
        //微信浏览器
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger')) {
            unset($paymentList['weixinH5']);
        } else {
            unset($paymentList['weixin']);
        }
        foreach ($paymentList as $key => $val) {
            $val['config_value'] = unserialize($val['config_value']);
            if ($val['config_value']['is_bank'] == 2) {
                $bankCodeList[$val['code']] = unserialize($val['bank_code']);
            }
        }
        $bank_img = include APP_PATH . 'home/bank.php'; // 银行对应图片
        $this->assign('paymentList', $paymentList);
        $this->assign('bank_img', $bank_img);
        $this->assign('bankCodeList', $bankCodeList);

        // 查找最近一次充值方式
        $recharge_arr = Db::name('Recharge')->field('pay_code')->where('user_id', $this->user_id)
            ->order('order_id desc')->find();
        $alipay = 'alipayMobile'; //默认支付宝支付
        if ($recharge_arr) {
            foreach ($paymentList as $key => $item) {
                if ($key == $recharge_arr['pay_code']) {
                    $alipay = $recharge_arr['pay_code'];
                }
            }
        }
        $this->assign('alipay', $alipay);

        if ($order_id > 0) {
            $order = M('recharge')->where("order_id", $order_id)->find();
            $this->assign('order', $order);
        }
        return $this->fetch();
    }

    public function recharge_list()
    {
        $usersLogic = new UsersLogic;
        $result = $usersLogic->get_recharge_log($this->user_id);  //充值记录
        $this->assign('page', $result['show']);
        $this->assign('lists', $result['result']);
        if (I('is_ajax')) {
            return $this->fetch('ajax_recharge_list');
        }
        return $this->fetch();
    }

    //添加、编辑提现账号
    public function add_card()
    {
        $user_id = $this->user_id;
        $data = I('post.');
        if ($data['type'] == 0) {
            $info['cash_alipay'] = $data['card'];
        }
        if ($data['type'] == 1) {
            $info['cash_weixinpay'] = $data['card'];
        }
        $info['realname'] = $data['cash_name'];
        $info['user_id'] = $user_id;
        $res = DB::name('user_extend')->where('user_id=' . $user_id)->count();
        if ($res) {
            $res2 = Db::name('user_extend')->where('user_id=' . $user_id)->save($info);
        } else {
            $res2 = Db::name('user_extend')->add($info);
        }
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功']);
    }

    public function withdrawals_account()
    {
        $user = get_user_info($this->user_id);
        $userAccount = model('common/withdrawals/WithdrawalAccount')->getBy('user_id', $this->user_id);
        if ($userAccount) {
            $this->assign('userAccount', $userAccount);
            return $this->fetch('account_info');
        }
        $this->assign('user', $user);
        return $this->fetch();
    }

    /**
     * 绑定提现账号
     */
    public function ajaxAddWithdraw()
    {
        if (!IS_AJAX) {
            $this->jsonReturn(-1, '请求有误');
        }
        $data = I('post.');
        $data['user_id'] = $this->user_id;
        $data = array_map(function ($value) {
            return trim($value);
        }, $data);
        $data['type'] = $data['type'] ?: 3;
        $withdrawLogic = model('common/user/WithdrawLogic', 'logic');
        $user = get_user_info($this->user_id);
        $data['mobile'] = $user['mobile'];
        $res = $withdrawLogic->addWithdrawAccount($data);
        if ($res === false) {
            $this->jsonReturn(-1, $withdrawLogic->getError());
        }
        $this->jsonReturn(1, '添加成功');
    }

    /**
     * 申请提现记录
     */
    public function withdrawals()
    {
        C('TOKEN_ON', true);
        $userAccount = model('common/withdrawals/WithdrawalAccount')->getBy('user_id', $this->user_id);
        if (!$userAccount) {
            $this->error('请先绑定提现账号', U('User/withdrawals_account'));
        }
        if (IS_POST) {
            $data = I('post.');
            $withdraLogic = model('common/user/WithdrawLogic', 'logic');
            $withdrawRes = $withdraLogic->withdraw($this->user_id, $data);
            if ($withdrawRes === false) {
                $this->ajaxReturn(['status' => 0, $withdraLogic->getError()]);
            } else {
                $this->ajaxReturn(['status' => 1, '提现成功']);
            }
        }
        $user = model('users')->get($this->user_id);

//         dump($user);exit;
        //获取用户绑定openId
        $oauthUsers = M("OauthUsers")->where(['user_id' => $this->user_id, 'oauth' => 'wx'])->find();
        $openid = $oauthUsers['openid'];
        $this->assign('userAccount', $userAccount);
        $this->assign('cash_config', tpCache('cash'));//提现配置项
        $this->assign('user_money', $user['wallet_money']);    //用户余额
        $this->assign('openid', $openid);    //用户绑定的微信openid
        return $this->fetch();
    }

    /**
     * 申请记录列表
     */
    public function withdrawals_list()
    {
        $withdrawals_where['user_id'] = $this->user_id;
        $count = M('withdrawals')->where($withdrawals_where)->count();
        // $pagesize = C('PAGESIZE'); //10条数据，不显示滚动效果
        // $page = new Page($count, $pagesize);
        $page = new Page($count, 15);
        $list = M('withdrawals')->where($withdrawals_where)->order("id desc")->limit("{$page->firstRow},{$page->listRows}")->select();

        $this->assign('page', $page->show());// 赋值分页输出
        $this->assign('list', $list); // 下线
        if (I('is_ajax')) {
            return $this->fetch('ajax_withdrawals_list');
        }
        return $this->fetch();
    }

    /**
     * 我的关注
     * @author lxl
     * @time   2017/1
     */
    public function myfocus()
    {
        return $this->fetch();
    }

    /**
     *  用户消息通知
     * @author dyr
     * @time 2016/09/01
     */
    public function message_notice()
    {
        return $this->fetch();
    }

    /**
     * ajax用户消息通知请求
     * @author dyr
     * @time 2016/09/01
     */
    public function ajax_message_notice()
    {
        $type = I('type');
        $message_model = new MessageLogic();
        if ($type === '0') {
            //系统消息
            $user_sys_message = $message_model->getUserMessageNotice();
        } else if ($type == 1) {
            //活动消息：后续开发
            $user_sys_message = array();
        } else {
            //全部消息：后续完善
            $user_sys_message = $message_model->getUserMessageNotice();
        }
        $this->assign('messages', $user_sys_message);
        return $this->fetch('ajax_message_notice');

    }

    /**
     * ajax用户消息通知请求
     */
    public function set_message_notice()
    {
        $type = I('type');
        $msg_id = I('msg_id');
        $user_logic = new UsersLogic();
        $res = $user_logic->setMessageForRead($type, $msg_id);
        $this->ajaxReturn($res);
    }

    /**
     * 查看消息详情
     */
    public function message_detail()
    {
        $msg_id = I('id/d', 0);
        $user_message = Db::name('user_message')->alias('um')
            ->join('message m', 'um.message_id=m.message_id')
            ->where(['um.user_id' => $this->user_id, 'um.message_id' => $msg_id])->find();
        $user_message['category_name'] = C('CATEGORY')[$user_message['category']];
        $this->assign('user_message', $user_message);
        M('user_message')->where(['user_id' => $this->user_id, 'message_id' => $msg_id])->save();
        return $this->fetch();
    }


    /**
     * 设置消息通知
     */
    public function set_notice()
    {
        //暂无数据
        return $this->fetch();
    }

    /**
     * 浏览记录
     */
    public function visit_log()
    {
        $count = M('goods_visit')->where('user_id', $this->user_id)->count();
        $Page = new Page($count, 20);
        $visit = M('goods_visit')->alias('v')
            ->field('v.visit_id, v.goods_id, v.visittime, g.goods_name, g.shop_price, g.cat_id')
            ->join('__GOODS__ g', 'v.goods_id=g.goods_id')
            ->where('v.user_id', $this->user_id)
            ->order('v.visittime desc')
            ->limit($Page->firstRow, $Page->listRows)
            ->select();

        /* 浏览记录按日期分组 */
        $curyear = date('Y');
        $visit_list = [];
        foreach ($visit as $v) {
            if ($curyear == date('Y', $v['visittime'])) {
                $date = date('m月d日', $v['visittime']);
            } else {
                $date = date('Y年m月d日', $v['visittime']);
            }
            $visit_list[$date][] = $v;
        }

        $this->assign('visit_list', $visit_list);
        if (I('get.is_ajax', 0)) {
            return $this->fetch('ajax_visit_log');
        }
        return $this->fetch();
    }

    /**
     * 删除浏览记录
     */
    public function del_visit_log()
    {
        $visit_ids = I('get.visit_ids', 0);
        $row = M('goods_visit')->where('visit_id', 'IN', $visit_ids)->delete();

        if (!$row) {
            $this->error('操作失败', U('User/visit_log'));
        } else {
            $this->success("操作成功", U('User/visit_log'));
        }
    }

    /**
     * 清空浏览记录
     */
    public function clear_visit_log()
    {
        $row = M('goods_visit')->where('user_id', $this->user_id)->delete();

        if (!$row) {
            $this->error('操作失败', U('User/visit_log'));
        } else {
            $this->success("操作成功", U('User/visit_log'));
        }
    }

    /**
     * 支付密码
     * @return mixed
     */
//    public function paypwd()
//    {
//        //检查是否第三方登录用户
//        $user = M('users')->where('user_id', $this->user_id)->find();
//        if ($user['mobile'] == '')
//            $this->error('请先绑定手机号', U('User/setMobile', ['source' => 'paypwd']));
//        $step = I('step', 1);
////        if ($step > 1) {
////            $check = session('validate_code');
////            if (empty($check)) {
////                $this->error('验证码还未验证通过', U('mobile/User/paypwd'));
////            }
////        }
//        if (IS_POST) {
//            $userLogic = new UsersLogic();
//
//            $new_password = trim(I('new_password'));
//            $confirm_password = trim(I('confirm_password'));
//            $oldpaypwd = trim(I('old_password'));
//            //以前设置过就得验证原来密码
//            if (!empty($user['paypwd']) && ($user['paypwd'] != encrypt($oldpaypwd))) {
//                $this->ajaxReturn(['status' => -1, 'msg' => '原密码验证错误！', 'result' => '']);
//            }
//            $data = $userLogic->paypwd($this->user_id, $new_password, $confirm_password);
//            $this->ajaxReturn($data);
//            exit;
//        }
//        $this->assign('step', $step);
//        return $this->fetch();
//    }

    /**
     * User: Simony
     * Function:设置晋升密码
     * @return mixed
     */
    function paypwd()
    {

        if(IS_POST)
        {
            $userMdl = model('Common/Users');
            $old_password = trim(I('old_password'));
            $new_password = trim(I('new_password'));
            $confirm_password = trim(I('confirm_password'));
            $paypwd = $userMdl->getBy('user_id',$this->user_id,'paypwd');
            if(encrypt($old_password) != $paypwd) return $this->ajaxReturn(['status'=>0,'msg'=>'旧密码不匹配！']);
            if($new_password != $confirm_password) return $this->ajaxReturn(['status'=>0,'msg'=>'2次输入密码不一致，请检查后重试']);
            $res = $userMdl->updateBy(['user_id'=>$this->user_id],['paypwd'=>encrypt($new_password)]);
            if(empty($res)) return $this->ajaxReturn(['status'=>0,'msg'=>'更新失败']);
            return $this->ajaxReturn(['status'=>1,'msg'=>'更新成功','url'=>U('mobile/User/userInfo')]);
        }

        return $this->fetch();
    }

    /**
     * 会员签到积分奖励
     * 2017/9/28
     */
    public function sign()
    {
        $userLogic = new UsersLogic();
        $user_id = $this->user_id;
        $info = $userLogic->idenUserSign($user_id);//标识签到
        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * Ajax会员签到
     * 2017/11/19
     */
    public function user_sign()
    {
        $userLogic = new UsersLogic();
        $user_id = $this->user_id;
        $config = tpCache('sign');
        $date = I('date'); //2017-9-29
        //是否正确请求
        (date("Y-n-j", time()) != $date) && $this->ajaxReturn(['status' => false, 'msg' => '签到失败！', 'result' => '']);
        //签到开关
        if ($config['sign_on_off'] > 0) {
            $map['sign_last'] = $date;
            $map['user_id'] = $user_id;
            $userSingInfo = Db::name('user_sign')->where($map)->find();
            //今天是否已签
            $userSingInfo && $this->ajaxReturn(['status' => false, 'msg' => '您今天已经签过啦！', 'result' => '']);
            //是否有过签到记录
            $checkSign = Db::name('user_sign')->where(['user_id' => $user_id])->find();
            if (!$checkSign) {
                $result = $userLogic->addUserSign($user_id, $date);            //第一次签到
            } else {
                $result = $userLogic->updateUserSign($checkSign, $date);       //累计签到
            }
            $return = ['status' => $result['status'], 'msg' => $result['msg'], 'result' => ''];
        } else {
            $return = ['status' => false, 'msg' => '该功能未开启！', 'result' => ''];
        }
        $this->ajaxReturn($return);
    }


    /**
     * vip充值
     */
    public function rechargevip()
    {
        $paymentList = M('Plugin')->where("`type`='payment' and code!='cod' and status = 1 and  scene in(0,1)")->select();
        //微信浏览器
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger')) {
            $paymentList = M('Plugin')->where("`type`='payment' and status = 1 and code='weixin'")->select();
        }
        $paymentList = convert_arr_key($paymentList, 'code');

        foreach ($paymentList as $key => $val) {
            $val['config_value'] = unserialize($val['config_value']);
            if ($val['config_value']['is_bank'] == 2) {
                $bankCodeList[$val['code']] = unserialize($val['bank_code']);
            }
        }
        $bank_img = include APP_PATH . 'home/bank.php'; // 银行对应图片
        $payment = M('Plugin')->where("`type`='payment' and status = 1")->select();
        $this->assign('paymentList', $paymentList);
        $this->assign('bank_img', $bank_img);
        $this->assign('bankCodeList', $bankCodeList);
        return $this->fetch();
    }

    // 拓客二维码
    public function code()
    {
        $user = get_user_info($this->user_id);
        $this->assign('user', $user);
        return $this->fetch();
    }


    /**
     * User: Simony
     * Function:
     */
    function forget_pwd_new()
    {
        if ($this->user_id > 0) {
            $this->redirect("User/index");
        }
        $username = I('username');
        $sms_status = checkEnableSendSms(2); //找回密码场景
        $field = 'mobile';
        if ($field == 'mobile' && !is_mobile($username)) {
            $this->ajaxReturn(['status' => -1, 'msg' => "手机号有误"]);
        }
        $forget_pwd_sms_enable = tpCache('sms.forget_pwd_sms_enable');
        //是否开启手机验证
        if (($field == 'mobile' && $sms_status['status'] < 1)) {
            $is_validate = true;
        }

        if (IS_POST) {
            if (!empty($username)) {

                $logic = new UsersLogic();
                //验证码检验
                //$this->verifyHandle('user_reg');
                $nickname = I('post.nickname', '');
                $username = I('post.username', '');
                $password = I('post.password', '');
                $password2 = I('post.password2', '');

                //是否开启注册验证码机制
                $code = I('post.mobile_code', '');
                $scene = I('post.scene', 1);

                $session_id = session_id();

                if (!$this->verifyHandle('forget')) {
                    $this->ajaxReturn(['status' => -1, 'msg' => "验证码错误"]);
                };
                //是否开启注册验证码机制
                if (check_mobile($username)) {
                    if ($forget_pwd_sms_enable) {
                        //手机功能没关闭
                        $check_code = $logic->check_validate_code($code, $username, 'phone', $session_id, $scene);
                        if ($check_code['status'] != 1) {
                            $this->ajaxReturn($check_code);
                        }
                    }
                }


                $user = M('users')->where("email", $username)->whereOr('mobile', $username)->find();
                if ($user) {
                    session('find_password', array('user_id' => $user['user_id'], 'username' => $username,
                        'email' => $user['email'], 'mobile' => $user['mobile'], 'type' => $field, 'sms_status' => $sms_status['status']));


                    $this->ajaxReturn(['status' => 1, 'msg' => "用户验证成功", 'url' => U('User/find_pwd')]);
                    exit;
                } else {
                    $this->ajaxReturn(['status' => -1, 'msg' => "用户名不存在，请检查"]);
                }
            }
        }
        return $this->fetch();
    }

    /**
     * User: Simony
     * Function:
     * @return mixed
     */
    public function forget_pwd_new1()
    {
        if ($this->user_id > 0) {
            $this->redirect("User/index");
        }
        $forget_pwd_sms_enable = tpCache('sms.forget_pwd_sms_enable');
        $reg_smtp_enable = tpCache('sms.regis_smtp_enable');

        if (IS_POST) {
            $logic = new UsersLogic();
            //验证码检验
            $this->verifyHandle('forget');
            $nickname = I('post.nickname', '');
            $username = I('post.username', '');
            $password = I('post.password', '');
            $password2 = I('post.password2', '');

            if ($password != $password2) {
                $this->ajaxReturn(['status' => -1, 'msg' => "密码和确认密码不一致"]);
            }
            //是否开启注册验证码机制
            $code = I('post.mobile_code', '');
            $scene = I('post.scene', 1);

            $session_id = session_id();

            //是否开启找回密码验证码机制
            if (check_mobile($username)) {
                if ($forget_pwd_sms_enable) {
                    //手机功能没关闭
                    $check_code = $logic->check_validate_code($code, $username, 'phone', $session_id, $scene);
                    if ($check_code['status'] != 1) {
                        $this->ajaxReturn($check_code);
                    }
                }
            }
            $user = M('users')->where("email", $username)->whereOr('mobile', $username)->find();
            if (!$user) {
                $this->ajaxReturn(['status' => -1, 'msg' => "用户不存在，请检查"]);
            }

            M('users')->where("user_id", $user['user_id'])->save(array('password' => encrypt($password)));
            session('validate_code', null);
            return $this->fetch('reset_pwd_sucess');
        }
        $this->assign('regis_sms_enable', $forget_pwd_sms_enable); // 忘记密码启用短信验证：
        $this->assign('regis_smtp_enable', $reg_smtp_enable); // 忘记密码启用邮箱：
        $sms_time_out = tpCache('sms.sms_time_out') > 0 ? tpCache('sms.sms_time_out') : 120;
        $this->assign('sms_time_out', $sms_time_out); // 手机短信超时时间
        return $this->fetch();
    }

    /**
     * User: Simony
     * Function: 更新用户升级
     * @return mixed
     */
    public function updateUserLevel()
    {

        $level_info = M('user_level')->where('level_id>=' . $this->user['level'])->limit(2)->select(); //用户等级信息
        $current_user_level = $level_info[0]; //当前用户等级信息
        $next_user_level = $level_info[1]; //下一个等级信息
        $first_leader = $this->user['first_leader']; //直推人数
        $is_uplevel = $first_leader >= $next_user_level['tjnum'] ? true : false;
        if ($is_uplevel == false) $this->ajaxReturn(['status' => -1, 'msg' => '不满足升级条件', 'result' => '']);;
        $userLogic = new UsersLogic();
        $userId = $this->user['user_id'];
        $userLogic->uplevel($userId);
//        return $this->fetch();
    }


    // 用户详细信息
    public function userDetail()
    {
        $userLogic = new UsersLogic();
        $user_info = $userLogic->get_info($this->user_id); // 获取用户信息
        $user_info = $user_info['result'];
        $first_leader = $userLogic->get_info($this->user['first_leader']);
        $first_leader = $first_leader['result'];
        if (IS_POST) {
            if ($_FILES['head_pic']['tmp_name']) {
                $file = $this->request->file('head_pic');
                $image_upload_limit_size = config('image_upload_limit_size');
                $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
                $dir = UPLOAD_PATH . 'head_pic/';
                if (!($_exists = file_exists($dir))) {
                    $isMk = mkdir($dir, 0700, true);
                }
                $parentDir = date('Ymd');
                $info = $file->validate($validate)->move($dir, true);
                if ($info) {
                    $post['head_pic'] = '/' . $dir . $parentDir . '/' . $info->getFilename();
                } else {
                    $this->error($file->getError());//上传错误提示错误信息
                }
            }

            I('post.nickname') ? $post['nickname'] = I('post.nickname') : false; //昵称
            I('post.name') ? $post['name'] = I('post.name') : false; //真实姓名
            I('post.qq') ? $post['qq'] = I('post.qq') : false;  //QQ号码
            //I('post.head_pic') ? $post['head_pic'] = I('post.head_pic') : false; //头像地址
            I('post.sex') ? $post['sex'] = I('post.sex') : $post['sex'] = 0;  // 性别
            I('post.birthday') ? $post['birthday'] = I('post.birthday', 0, 'intval') : false;  // 生日
            I('post.province') ? $post['province'] = I('post.province') : false;  //省份
            I('post.city') ? $post['city'] = I('post.city') : false;  // 城市
            I('post.district') ? $post['district'] = I('post.district') : false;  //地区
            I('post.email') ? $post['email'] = I('post.email') : false; //邮箱
            I('post.mobile') ? $post['mobile'] = I('post.mobile') : false; //手机

            $email = I('post.email');
            $mobile = I('post.mobile');
            $code = I('post.mobile_code', '');
            $scene = I('post.scene', 6);

            if (!empty($email)) {
                $c = M('users')->where(['email' => input('post.email'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("邮箱已被使用");
            }
            if (!empty($mobile)) {
                $c = M('users')->where(['mobile' => input('post.mobile'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("手机已被使用");
                if (!$code)
                    $this->error('请输入验证码');
                $check_code = $userLogic->check_validate_code($code, $mobile, 'phone', $this->session_id, $scene);
                if ($check_code['status'] != 1)
                    $this->error($check_code['msg']);
            }
            if (!$userLogic->update_info($this->user_id, $post))
                $this->error("保存失败");
            setcookie('uname', urlencode($post['nickname']), null, '/');
            $this->success("操作成功", U('Index/index'));
            exit;
        }
        //判断是否为手机端后台管理员用户
        if(!model('Common/UserMobileAdmin')->isMobileAdminUser($this->user_id)){
          $is_mobile_admin = 0;
        }else{
          $is_mobile_admin = 1;

        }
        $userCard = $this->realName(true);
        $this->assign('user', $user_info);
        $this->assign('is_mobile_admin', $is_mobile_admin);
        $this->assign('userCard', $userCard);
        $this->assign('first_leader', $first_leader);
        $this->assign('sex', C('SEX'));
        //从哪个修改用户信息页面进来，
        $dispaly = I('action');
        if ($dispaly != '') {
            return $this->fetch("$dispaly");
        }
        return $this->fetch();
    }

    /**
     * User: Simony
     * Function: 绑定支付宝
     * @return mixed
     */
//    public function bindAlPay()
//    {
//        $paytypeModel = model('common/Paytype');
//        $type = 1;
//        if (IS_POST) {
//            $account = I('post.al_account');
//            $name = I('post.name');
//            if($name){
//                model('Common/Users')->where(['user_id'=>$this->user_id])->update(['name'=>$name]);
//            }
//
//            $res = $paytypeModel->addOrSave($this->user_id,$type, ['account' => $account, 'type' => $type]);
//            if (!$res) {
//                $this->ajaxReturn(array('status' => -1, 'msg' => $paytypeModel->getLastSql()));
//                exit;
//            }
//            $this->ajaxReturn(array('status' => 1, 'msg' => '绑定成功'));
//            exit;
//        }
//        $account = $paytypeModel->where(['user_id' => $this->user_id, 'type' => $type])->getField('account');
//        $this->assign('account', $account);
//        $this->assign('user', $this->user);
//        return $this->fetch();
//    }

    /**
     * User: Simony
     * Function: type 1 支付宝 type 2 微信
     * @return mixed
     */
    public function bindAccount()
    {
        $paytypeModel = model('common/Paytype');
        $type = I('type',2,'intval');
        $title = $type == 1 ? '绑定支付宝' : '绑定微信';
        if (IS_POST) {
            $account = I('post.al_account');
            $name = I('post.name');
            if ($name) {
                model('Common/Users')->where(['user_id' => $this->user_id])->update(['name' => $name]);
            }

            $beforeAccount = $paytypeModel->where(['user_id' => $this->user_id, 'type' => $type])->value('account');
            // p($beforeAccount);
            $res = $paytypeModel->addOrSave($this->user_id, $type, ['account' => $account, 'type' => $type]);
            //添加修改记录
            $desc = $type == 1 ? '用户修改支付宝账号' : '用户修改微信账号';
            model('commen/PaytypeChangeLog')->addLog($this->user_id,$type,$beforeAccount,$account,$desc);
            if (!$res) {
                $this->ajaxReturn(array('status' => -1, 'msg' => '绑定失败'));
                exit;
            }
            $this->ajaxReturn(array('status' => 1, 'msg' => '绑定成功'));
            exit;
        }
        $account = $paytypeModel->where(['user_id' => $this->user_id, 'type' => $type])->getField('account');
       
        
        $this->assign('account', $account);
        $this->assign('user', $this->user);
        $this->assign('type', $type);
        $this->assign('title', $title);
        return $this->fetch();
    }

    /**
     * 实名认证页面
      *
     */
    public function realName($flag=false)
    {
        $isAjax   = input('is_ajax',0,'intval');
        $cardMdl  = model('Common/credit/CreditIdentityCard');
        $cardInfo = [];

        $cardInfo = $cardMdl->where(['user_id' => $this->user_id])->field('process, true_name, card_number')->find();
        if (!empty($cardInfo) && $cardInfo['process']=='success') {
            $cardInfo['card_number'] = $cardMdl->showIdCard($cardInfo['card_number']);
        }

        // $cardInfo = ['process'=>'success','true_name'=>12312,'card_number'=>123123123123];
        if ($flag == true) {
          return $cardInfo;
        }

        if ($isAjax) {
          if ($cardInfo) {
            return ajaxReturn(['code'=>1,'res'=>$cardInfo]);
          }
          return ajaxReturn(['code'=>0,'msg'=>'未验证']);
        }

        // p($cardInfo);
        $this->assign('data', $cardInfo);
        return $this->fetch();
    }


    public function upIdentityCard(){
      if (IS_POST) {
        $idCardData = $_POST;
        $frontImg   = $this->newUploadImage('file_img_list1','identityCard');
        $backImg    = $this->newUploadImage('file_img_list2','identityCard');
        if ($frontImg['code']!=1 || $backImg['code']!=1) {
          $msg = $frontImg['msg']?:$backImg['msg'];
          $this->error($msg);
          // return ajaxReturn(['code'=>0,'msg'=>$msg]);
        }

        $idCardData['card_front_photo'] = $frontImg['data'];
        $idCardData['card_back_photo']  = $backImg['data'];


        //图片压缩 并上传阿里云
        $ossClient = new \app\common\logic\OssLogic;
        $uploadImg = $_SERVER['DOCUMENT_ROOT'];
        $CompressedImageMdl = new CompressedImageLogic();
        $res_1 = $CompressedImageMdl->compressedImage($uploadImg.$idCardData['card_front_photo'],$uploadImg.$idCardData['card_front_photo'],800);//压缩图片
        $res_2 = $CompressedImageMdl->compressedImage($uploadImg.$idCardData['card_back_photo'],$uploadImg.$idCardData['card_back_photo'],800);//压缩图片
         if($res_1['code'] == -1) $this->error('上传图片格式有误，请重新上传！');
         if($res_2['code'] == -1) $this->error('上传图片格式有误，请重新上传！');
        $fileFront = $_FILES['file_img_list1'];
        $fileBack = $_FILES['file_img_list2'];

        list($image,$type) = explode('/', $fileFront['type']);
        $objectFront = uniqid('sale_front_') . time(). '.' . $type;
        $objectBack = uniqid('sale_back_') . time(). '.' . $type;

        $objectFront = 'identityCard/' . $objectFront;
        $objectBack = 'identityCard/' . $objectBack;

        $card_front_photo = $ossClient->uploadFile($uploadImg.$idCardData['card_front_photo'],$objectFront);
        $card_back_photo = $ossClient->uploadFile($uploadImg.$idCardData['card_back_photo'],$objectBack);

        $idCardData['card_front_photo'] =$card_front_photo['info']['url'];//获取图片路径
        $idCardData['card_back_photo'] =$card_back_photo['info']['url'];//获取图片路径

        // unset($fileFront);  //开始释放变量
        // unset($fileBack);  //开始释放变量

        unlink($uploadImg.$frontImg['data']);//删除文件
        unlink($uploadImg.$backImg['data']);//删除文件

        $cardMdl  = model('Common/credit/CreditIdentityCard');
        $idCardData['user_id'] = $this->user_id;
        $res = $cardMdl->regionalValidation($idCardData['card_number']);
        if($res['code']==-1) $this->error('身份证号有误');
        // p($res);
        $idCardData['area_type'] = $res['area_type'];
        $is_idNum = $cardMdl->checkIdCardNum($idCardData['card_number']);
        if ($is_idNum) {
          $result = [
              // 'code' => $code,
              'msg'  => '验证失败,该身份证号已被使用或审核中,请使用本人身份证验证',
              // 'data' => $data,
              // 'url'  => $url,
              // 'wait' => $wait,
          ];
          flashMessage($result);
          $this->redirect(U('realName'));
          // $this->error('验证失败,该身份证号已被使用,请使用本人身份证验证');
        }
        $res = $cardMdl->addData($idCardData);
        // dump($cardMdl->getError());
        if ($res == false) {
          $result = [
              // 'code' => $code,
              'msg'  => $cardMdl->getError(),
              // 'data' => $data,
              // 'url'  => $url,
              // 'wait' => $wait,
          ];
          flashMessage($result);
          $this->redirect(U('realName'));
          // $this->error($cardMdl->getError());
          // return ajaxReturn(['code'=>0,'msg'=>$cardMdl->getError()]);
        }

        $this->success('上传成功',U('Mobile/User/userDetail'));
        // return ajaxReturn(['code'=>1,'msg'=>'提交成功']);
      }
    }
    // 账号异常
    public function abnormal_unusual()
    {
        $user_id = I("user_id/d",0);
        if($user_id <= 0) $this->error('user_id不能为空');
        if(I('password_lock')){
          $this->assign('info','密码错误次数上限被冻结');
        }else{
          $this->assign('info',model('Common/credit/CreditUserLog')->lock_des($user_id));
        }
        session_unset();
        session_destroy();
        setcookie('uname', '', time() - 3600, '/');
        setcookie('cn', '', time() - 3600, '/');
        setcookie('user_id', '', time() - 3600, '/');
        setcookie('PHPSESSID', '', time() - 3600, '/');
        return $this->fetch();
    }
     // 闯关等级最高级提示页
    public function maxLevelNotice()
    {
        // $user_id = I("user_id/d",0);
        // if($user_id <= 0) $this->error('user_id不能为空');
        $this->assign('info','您已升级为最高等级了');
        // session_unset();
        // session_destroy();
        // setcookie('uname', '', time() - 3600, '/');
        // setcookie('cn', '', time() - 3600, '/');
        // setcookie('user_id', '', time() - 3600, '/');
        // setcookie('PHPSESSID', '', time() - 3600, '/');
        return $this->fetch();
    }
    
    //个人商城管理
    public function userMall()
    {
        $user_id = $this->user_id;
        $info = model('Common/UserShopManage')->getInfo($user_id);
      
        $this->assign('info',$info);
        return $this->fetch();
    }

    //个人商城管理 操作
    public function userMallHandle()
    {
        $user_id = I('user_id',$this->user_id);
        $scene = I('scene',0);
        $link = '';

        if($scene != 0 and $scene != 1 and $scene != 2 and $scene != 3){
           exit(api_ajaxReturn(0,'场景参数scene错误'));
        }
        if($scene == 1 or $scene == 2){
          $res = model('Common/UserShopManage')->getFormAndEshop($user_id,$scene);//子商城 or e店 or 第三方链接
          if($res['code'] != 10000){
             exit(api_ajaxReturn(0,$res['message']));
          }
          $link = jump_stitch($res['result']['link']);
        }

        $data = [
          'scene' => $scene,
          'update_time' => time()
        ];
        if($scene == 3){
          $url = I('link','');
          if(!$url){
            exit(api_ajaxReturn(0,'场景3时，必须传url'));
          }
          $data['url'] = $url;
          $link = $url;
        }

        $res = M('UserShopManage')->where(['user_id'=>$user_id])->update($data);

        if($res === false){
            exit(api_ajaxReturn(0,'更新失败'));
        }
        exit(api_ajaxReturn(1,'更新成功',$link));
    }


     //语言设置
     public function language()
     {

         return $this->fetch();
     }
     //成功案例

     public function successfulCase()
     {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

         return $this->fetch();
     }
     //常见问题

     public function problem()
     {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

         $articleMdl = model('Common/Article');
        //  getList($cond=[],$page=[],$isTotal=false,$with=[],$orderBy='',$limit=false)
         $cond = ['cat_id' => 12 , 'is_open'=>1];
         $orderBy = 'sort asc,article_id desc';
         $list = $articleMdl->getList($cond,[],false,[],$orderBy);
        //  dump($list);exit;
         $this->assign('list',$list);
         return $this->fetch();
     }
    //关注公众号
    public function officialAccounts()
    {
        $info = model("Common/WechatOfficialAccount")->where(['enabled'=>1])->order('sort DESC')->find();

        $this->assign('info',$info);
        return $this->fetch();
    }
    //拓客社交
    public function socializing()
    { 
      //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

        return $this->fetch();
    }
    //拓客直播
    public function live_broadcasting()
    {
      //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);
        return $this->fetch();
    }
    //占位排队

    public function memberPlace()
    {
        // $cacheState = APP_ENV=='online'?true:false;
        $user = session('user');
        $tk_id = input('tk_id');
        $referurl = $_SERVER['REQUEST_URI'];
        if(!$user) $this->redirect(withNextURL(U('Mobile/user/login',array('tk_id'=>$tk_id)),$referurl,'referurl'));

        $cacheState = false;
        $from_actpage = input('from_actpage','');

        if(!$from_actpage) $this->redirect(withNextURL(U('Mobile/user/activityPage'))); 

        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

        $placeWaitingMatchingMdl = model('Common/place/PlaceWaitingMatching');
        $userMdl = model('Common/Users');
        $placeMatchingLogMdl = model('Common/place/PlaceMatchingLog');
        $placeTemporaryLockingMdl = model('Common/place/PlaceTemporaryLocking');
        //抢位人信息
        $data['takingPlaceUser'] =  $placeWaitingMatchingMdl->with(['user'])
                            ->where(['state'=>1])->order('update_time desc')->limit(1)->find();

        //个人信息
        $data['placeUser'] = $userMdl->where(['user_id'=>$this->user_id])->cache($cacheState,300)->with(['placeWaitingMatching'])
                    ->find();

        //占位排行榜 （10个）    
        $data['placeRank'] = $placeWaitingMatchingMdl->with(['user'])->where(['state'=>0])
                             ->cache($cacheState,300)->order($placeWaitingMatchingMdl->getOrderByStr())->limit(10)->select();           
                             // p($placeWaitingMatchingMdl->getOrderByStr());
        //分享排行榜 （10个）
        $data['shareRand'] = $placeTemporaryLockingMdl->with('user')->where(['source'=>'share'])->field('*,count(id) as count')->group('temporary_firstleader_id')
                            ->cache($cacheState,300)->order('count desc,temporary_firstleader_id asc')->limit(10)->select();
        //用户排行榜前面人数
        if($data['placeUser']['placeWaitingMatching']['ranking']){
          $data['placeUser']['is_queue'] = 1;
          $data['placeUser']['placeFrontNumber'] = $data['placeUser']['placeWaitingMatching']['ranking']-1;
          if($data['placeUser']['placeFrontNumber']<0) $data['placeUser']['placeFrontNumber'] = 0;
        }else{
          $data['placeUser']['is_queue'] = 0;
          $data['placeUser']['placeFrontNumber'] = 0;
        }
        $data['placeUser']['placeFrontNumber'] = $data['placeUser']['placeWaitingMatching']['ranking']?$data['placeUser']['placeWaitingMatching']['ranking']-1:0;
        if($data['placeUser']['placeFrontNumber']<0) $data['placeUser']['placeFrontNumber'] = 0;
        //注册后锁定的人数
         $temporary_locking_num = db('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$this->user_id,'status'=>['IN',[1,3]]])->count();
        //分享后点击锁定的数量(没注册)
         $temporary_locking_num_click = model('Common/TemporaryClickLocking')->getTimes($this->user_id);
         //总锁定人数
         $data['placeUser']['temporary_locking_num']  = $temporary_locking_num + $temporary_locking_num_click;
        $this->assign($data);

        return $this->fetch();
    }
     //我要占位  status 1:0星会员点击跳转闯关 0：不能跳转，弹提示信息
    public function toPlace()
    {
      $userMdl = model('Common/Users');
      $placeWaitingMatchingMdl = model('Common/place/PlaceWaitingMatching');

      $status = 0;
      $userLevel = $userMdl->where(['user_id'=>$this->user_id])->value('level');
      switch ($userLevel) {
        //0星会员 
        case '1':
          $status = 1;
          $this->ajaxReturn(['status' => $status, 'msg' => '','url'=>U("mobile/member_center/updataLevel")]);
          break;
        default:
          $placeuser = $placeWaitingMatchingMdl->where(['user_id'=>$this->user_id,'enable'=>1])->find();
          if(empty($placeuser)){
            $this->ajaxReturn(['status' => $status, 'msg' => '您暂不能占位']);
          }
          if($placeuser['state'] == 0){
            $this->ajaxReturn(['status' => $status, 'msg' => '您正在排队中...']);
          }elseif($placeuser['state'] == 1){
            $this->ajaxReturn(['status' => $status, 'msg' => '您已成功占位,正在等待匹配...']);
          }elseif($placeuser['state'] == 2){
            $this->ajaxReturn(['status' => $status, 'msg' => '您已匹配成功,待审核中...']);
          }elseif($placeuser['state'] == 3){
            $this->ajaxReturn(['status' => $status, 'msg' => '您已完成占位']);
          }
          $this->ajaxReturn(['status' => $status, 'msg' => '您暂不能占位']);
          break;
      }
     
    }



   //分享拓客
   public function shareCode()
   {
      //页面引导
      $is_guide = isGuide($this->user_id);
      $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
      $this->assign('guide_imgs',$guide_imgs);

      $bg = model("Common/Qrcode")->where(['enabled'=>1,'title'=>['notin',['活动页','分享活动页']]])->order('sort DESC')->select();
      // p($bg);

      /* 获取返回链接 */
      $user_code['Url']           = base64_encode(U("Mobile/user/share"));

      $user_code['title_name']    = '分享拓客';

      // $ad = getAdImg('拓客二维码背景图')[0];

      $userInfo = model('Common/Users')->where(['user_id'=>$this->user_id])->find();
      $this->assign('userInfo',$userInfo);


      // $this->assign('adBg',empty($ad)?'':$ad['ad_code']);
      $this->assign('user_code',$user_code);
      $this->assign('bg',$bg);

      $this->assign('tk_id',base64_encode($this->user_id));

      return $this->fetch();
   }

    //客服
    public function service()
    {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

        $list = model("Common/CustomerService")->where(['enabled'=>1])->order('sort DESC')->select();

        $this->assign('list',$list);
        return $this->fetch();
    }
      //分享页


      public function share()
      {
          $id = I('id',0);
          $where = ['enabled'=>1];
          $id && $where['qrcode_id'] = $id;
          $info = model("Common/ShareView")->with('qrcode')->where($where)->order('sort DESC')->find();

          $this->assign('info',$info);
          return $this->fetch();
      }

      //修改用户资料
      public function editUser()
      {
          $post = [];
          I('post.nickname') ? $post['nickname'] = I('post.nickname') : false; //昵称
          I('post.name') ? $post['name'] = I('post.name') : false; //真实姓名
          I('post.qq') ? $post['qq'] = I('post.qq') : false;  //QQ号码
          I('post.headPic') ? $post['head_pic'] = I('post.headPic') : false; //头像地址
          I('post.sex') ? $post['sex'] = I('post.sex') : false;  // 性别
          I('post.birthday') ? $post['birthday'] = I('post.birthday', 0, 'strtotime') : false;  // 生日
          I('post.province') ? $post['province'] = I('post.province') : false;  //省份
          I('post.city') ? $post['city'] = I('post.city') : false;  // 城市
          I('post.district') ? $post['district'] = I('post.district') : false;  //地区
          I('post.email') ? $post['email'] = I('post.email') : false; //邮箱
          I('post.mobile') ? $post['mobile'] = I('post.mobile') : false; //手机

          $result = (new UserLogic())->editUser($this->user_id, $post, $_FILES['headPic']);
          $result = json_decode($result, true);
          if($result['code'] == 10000){
              $this->success($result['message'], U('User/userDetail'));
          }else{
              $this->error($result['message']);
          }
      }

    //前往渠道
    public function goToChannel()
    {
        $user = session('user');
        $referurl = $_SERVER['REQUEST_URI'];
        if(!$user) $this->redirect(withNextURL(U('Mobile/user/login'),$referurl,'referurl'));

        // $channel_code = I('channel_code','cloud');//goto 的渠道
        // $openId     = session('crm_openid')?:"";
        // $channel = M('Channel')->where(['channel_code'=>$channel_code])->find();
        // if(!$channel) exit('渠道不存在');

        // $url = $channel['url'];
        // $url = 'http://cloud.kitnote.cn';
        // $url = 'http://www.yun-s-j.com';
        $url = 'https://www.yun-s-j.com';

        $params = [
            'uid' => $user['user_id'],
            'name' => $user['nickname'],
            'mobile' => $user['mobile'],
            'channel_code' => 'sale',//指明url来源渠道
            'timeStamp' => time(),
            'tk_mobile' => model('Common/Users')->where(['user_id'=>$user['first_leader']])->value('mobile'),
            // 'crm_openid' => $openId,
            'returnUrl'  =>urlencode($_SERVER['HTTP_REFERER']),
        ];
        $apiLogic = new \app\common\logic\ApiLogic;
        $params['privateSign'] = $apiLogic->getKeyCloud($params);
        $url = trim($url,"/").'/cloud/index/index.html?'.http_build_query($params);
// P($url);
        header('Location: '.$url);
        // $this->redirect($url);
    }  

     //获取全国优品导航图片
    public function getGuideImgForOther()
    {
      $user_id = $this->user_id;
      $mdl = model('common/UserGuideLog');
      // $guide_nav_id = model('common/nav/Navigation')->where(['name'=>'全国优品'])->value('id');
      $guide_nav_id = 34;
      $navigation = model('common/nav/Navigation')->getInfo(['id'=>$guide_nav_id,'is_guide'=>1]);
      if(empty($navigation)) $this->ajaxReturn(['status' => 0, 'msg' => '', 'data' => []]);

      $guideLog   = $mdl->where(['user_id'=>$user_id])->find();
      //插入用户引导记录
      if(empty($guideLog)){
        $addData = [
            'user_id'=>$user_id,
            'guide_ids'=>$navigation['id'],
        ];
        $data['guide_log_id'] = $mdl->addData($addData);
        $this->ajaxReturn(['status' => 1, 'msg' => '', 'data' => $navigation['guide_imgs']]);
      }
      //存在判断是否已引导
        $guide_ids = explode(',', $guideLog['guide_ids']);
        if(in_array($navigation['id'], $guide_ids)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '', 'data' => []]);
        }
        //添加记录
        $guide_ids[] = $navigation['id'];
        $guide_ids_str = implode(',', $guide_ids);
        $mdl->where(['id'=>$guideLog['id']])->update(['guide_ids'=>$guide_ids_str]);
        $this->ajaxReturn(['status' => 1, 'msg' => '', 'data' => $navigation['guide_imgs']]);
    }  

    //全网拓客活动页
    public function activityPage()
    {
        $user_id = $this->user_id;
        $tk_id = (int)base64_decode(trim(I('tk_id')));
        $tk_id || $tk_id = (int)I('tk_id');
        $loginToken = I('loginToken');
        //设没登录用户或没注册用户注册登录跳转地址
        $activity_page_url = $_SERVER['PATH_INFO'];
        if(!$user_id) cookie('activity_page_url',$activity_page_url);
        //app 调h5全网拓客活动页面 
        if(($tk_id && !$user_id)|| $loginToken){
            $user_token = M('user_token')->order('add_time desc')->where('token', $loginToken)->find();
            $user_id = $loginToken ? $user_token['user_id'] : $tk_id;
            setcookie('shareId', $user_id, null, '/');
            if($loginToken) {
                // func_is_base64(trim(I('tk_id'))) ?session('shareId',$shareId):session('shareId',base64_encode($shareId));
                setcookie('user_id', base64_encode($user_id), null, '/'); //存cookie 分享页点击全网占位判断用
            }   
            $this->assign('tk_id',$user_id);
        }
        //用户资料
        $userInfo = model('Common/Users')->where(['user_id'=>$user_id])->field('head_pic,nickname,mobile,user_id')->find();

        //活动页内容
        $cond =[
          'title'=>'活动页',
          'enabled'=>1,
        ]; 
        $qc = model('Common/Qrcode')->where($cond)->find(); 
        if(!$qc){
            return $this->ajaxReturn(['status'=>-1,'msg'=>'二维码没配置活动页或没启用！']);
        }

        $activityInfo = model('Common/ShareView')->where(['qrcode_id'=>$qc['id'],'enabled'=>1])->order('id desc')->find();
        if(!$activityInfo){
            return $this->ajaxReturn(['status'=>-1,'msg'=>'活动页没配置或没启用！']);
        }

        $activityInfo['pic'] = explode(',', $activityInfo['thumb']);
        if(count($activityInfo['pic'])>10){
            $activityInfo['pic'] = array_slice($activityInfo['pic'],0,10);
        }

        //设置分享链接
        $arr['tk_id'] = base64_encode($user_id);
        $param = http_build_query($arr);
        $url = $_SERVER['HTTP_HOST'].$_SERVER['PATH_INFO'];
        if(strpos($url, '?') === false){
            $url = $url.'?'.$param;
        }else{
            $url = $url.'&'.$param;
        }
        
        //

        $list = [
            'userInfo'=>$userInfo,
            'activityInfo'=>$activityInfo,
        ];
         //p($list);
        $this->assign('list',$list);
        $this->assign('share_url',$url);
        return $this->fetch();
      
    }

    //爆品专区
    public function fieryGoods()
    {
        return $this->fetch();
    }

    //极差页面
    public function jicha_view()
    {
        $url = db('config')->where('name', 'redirect_page')->value('value');
        $supplyLogic = model('SupplyLogic','logic\supply');
        $supplyLogic->setRedirectUrl($url);
        $supplyLogic->loginRedirect($this->user_id);
    }

    public function testaa()
    {
        print_r($_POST);
        print_r($_GET);
    }

}
 