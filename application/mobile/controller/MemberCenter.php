<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: IT宇宙人 2015-08-10 $
 */
namespace app\mobile\controller;

use think\Db;
use think\Page;
use app\common\model\UserAddress;
use app\common\logic\SlippingUsersLogic;


class MemberCenter extends MobileBase
{
    public function _initialize(){
        parent::_initialize();
        if(!$this->user_id){
            $this->error('请先登录',U('user/login'));
        }
   
    }

    /**
     * 我的收益界面
     *
     */
    public function myProfit(){
        $user=model('common/user/MemberCenterLogic','logic')->getUserProfit($this->user_id);
        $this->assign('user',$user);
        return $this->fetch();
    }

    /**
     * 我的团队 
     *   
     */
    public function myTeam(){
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

        // $p = I('p',1);      
        // $listRows = I('listRows',10);
        // $sort2 = I('sort2','DESC'); 
        // $sort1 = I('sort1','level','trim');
        // $keywords = I('keywords','','trim');
        // $orderBy = $sort1.' '.$sort2;
        
        // // $where = ["first_leader" =>$this->user_id];
        // if($keywords!='') $where['mobile|nickname'] = ['like',"$keywords%"];
        $user = model('users')->get($this->user_id);
        // $team = model('common/user/MemberCenterLogic','logic')->myTeam($where,$orderBy,$p,$this->user_id);
        // foreach ($team as $k => $v) {
        //     $team[$k]['teamNum2'] = model('Common/Users')->getFisrtLeaderNumAttr($v['user_id'],0);
        // }
        
        $first_leader = model('Common/Users')->getFisrtLeaderNumAttr($this->user_id,0);//大于等于0星
        $teamNum      = model('Common/distribut/DistributTeam')->teamNum($user['user_id']);
        $teamNum1     = model('Common/distribut/DistributTeam')->teamNum($user['user_id'],2);

        // $slippingUsersLogic = new SlippingUsersLogic;
        // $connecterNum = $slippingUsersLogic->getConnectNum($this->user_id);
        $this->assign('first_leader',$first_leader);
        $this->assign('user',$user);
        // $this->assign('connecterNum',$connecterNum);
        $this->assign('teamNum',$teamNum);
        $this->assign('teamNum1',$teamNum1);
        $this->assign('state_level',model('Common/UserLevel')->get(2));
        
        return $this->fetch(); 
    }
    /**
     * [ajax_myTeam description]
     * @return [type] [description]
     */
    public function ajaxMyTeam()
    {
        $p = I('p',1);      
        $listRows = I('listRows',10);
        // $sort2 = I('sort2','first_leader'); 
        $sort1 = I('sort1','direct','trim');
        $keywords = I('keywords','','trim');
        // p($sort1);
        // $type = I('sort1','direct','trim');
        $orderBy = 'reg_time DESC';
        
        // $where = ["first_leader" =>$this->user_id];
        if($keywords!='') $where['mobile|nickname'] = ['like',"$keywords%"];
        $user = model('users')->get($this->user_id);
        $memberCenterLogicMdl = model('common/user/MemberCenterLogic','logic');
        $team = $sort1 == 'direct' ? $memberCenterLogicMdl->myTeam($where,$orderBy,$p,$this->user_id):$memberCenterLogicMdl->myTeamForFirstLeader($where,$orderBy,$p,$this->user_id);
        // p(collection($team)->toArray());
        foreach ($team as $k => $v) {
            $team[$k]['teamNum2'] = model('Common/Users')->getFisrtLeaderNumAttr($v['user_id'],0)?:0;
        }
        // p(collection($team)->toArray());
        
        $this->assign('team',$team);

        return $this->fetch('ajax_myTeam');
        
    }

    /**
     * [promotionOperation 晋升运营中心]
     * @return [type] [description]
     */
    public function promotionOperation()
    {
        return $this->fetch(); 
    }

    /**
     * [promotionOperation 申请升级]
     * @return [type] []
     */
    public function updataLevel()
    {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);


        $userMdl = model('Common/Users');
       
        $userLevelMdl = model('Common/users/UserLevel');
        
        $distributTeamMdl = model('Common/distribut/DistributTeam');

        $giveGiftMdl = model('Common/GiveGift');


        // if($userLevelMdl->getMaxLevel() == $this->user['level']) $this->error('已升级为最高等级了',U('Admin/Ad/positionList'));
        // if($userLevelMdl->getMaxLevel() == $this->user['level']) {
        //     $this->redirect( U('mobile/user/maxLevelNotice'));
        // }
        /**
         * [$currentLevelInfo 当前用户等级信息]
         */
        $currentLevelInfo = $userLevelMdl->getLevelInfo((int)$this->user['level']);
        
        $currentLevelName = $currentLevelInfo['show_name'];

        $currentDirectCount = $userMdl->getFisrtLeaderNumAttr($this->user_id,2);

        $currentTeamCount = $distributTeamMdl->teamNum($this->user_id,2);
        
        /**
         * [$nextLevelInfo 下一等级信息]
         */
        $nextLevelInfo = $userLevelMdl->getLevelInfo((int)$this->user['level'] + 1);
        
        $nextLevelName = $nextLevelInfo['show_name'];

        $nextNeedDirectCount = $nextLevelInfo['tjnum'];

        $nextNeedTeamCount = $nextLevelInfo['team_check_num'];

        $nextLevelRight = $nextLevelInfo['gift_describe'];
        
         /**
         * [$nextLevelInfo 下一级所邀请的等级名称(VIP1)]
         */
        $nextNeedDirectLevelName = $userLevelMdl->nextNeedDirectLevelName(2);

        /**
         * [$isFulfilUpgrade 判断是否满足升级条件]
         */
        $isFulfilUpgrade = $currentTeamCount >= $nextNeedTeamCount && $currentDirectCount >= $nextNeedDirectCount ? true : false;

        /**
         * [$isNeedCroos 是不是需要跨级]
         */
        $isNeedCroos = !empty($nextLevelInfo['need_cross_level']) ? true : false;
    
        /**
         * [$directContributionRate 直推贡献比率]
         */
        $directContributionRate = $currentDirectCount > 0 && $nextNeedDirectCount > 0 ? $currentDirectCount/$nextNeedDirectCount*100 : 0; 
        empty($nextNeedDirectCount) && $directContributionRate = 100;
        $directContributionRate > 100 && $directContributionRate = 100;

        /**
         * [$teamContributionRate 团队贡献比率]
         */
        $teamContributionRate =  $currentTeamCount > 0  && $nextNeedTeamCount > 0 ? $currentTeamCount/$nextNeedTeamCount*100 : 0;
        empty($nextNeedTeamCount) && $teamContributionRate = 100;
        $teamContributionRate > 100 && $teamContributionRate = 100;

       
        $checkGift = [
            'give_user_level' => (int)$this->user['level'],
            'give_user_id' => $this->user_id,
            'status' => ['neq',-1],
        ];

        $giftCount = $giveGiftMdl->getCount($checkGift);

        /**
         * [$isSendGift 是否已经送礼]
         * @var [type]
         */
        $isSendGift = $giftCount == 1 ? true : false;
        $isSendGift = $isNeedCroos == true && $giftCount == 2 ? true : false;

        // 是否有默认地址
        $addressMdl = new UserAddress();
        $flag = $addressMdl->hasDefaultAddress($this->user_id);

        //判断是否审核中 
        $checkStatus = [
            'status'=>0,
            'give_user_id' => $this->user_id,
            'give_user_level' => (int)$this->user['level'],
        ];       
        $giftStatusPending = $giveGiftMdl->getCount($checkStatus);
               
        $data = [
            'currentLevelName' => $currentLevelName,
            'currentDirectCount' =>$currentDirectCount,
            'currentTeamCount' => $currentTeamCount,
            'nextLevelName' => $nextLevelName,
            'nextNeedDirectCount'=> $nextNeedDirectCount,
            'nextNeedTeamCount' => $nextNeedTeamCount,
            'isFulfilUpgrade' => $isFulfilUpgrade,
            'isNeedCroos' => $isNeedCroos,
            'directContributionRate' => $directContributionRate,
            'teamContributionRate' => $teamContributionRate,
            'nextLevelRight' => $nextLevelRight,
            'isSendGift' => $isSendGift,
            'isDefaultAddress' => $flag,
            'giftStatusPending' =>$giftStatusPending,
            'nextNeedDirectLevelName'=>$nextNeedDirectLevelName,
        ];

        I('debug/d') == 1 && p($data,1);

        $this->assign($data);

        return $this->fetch(); 
    }

}