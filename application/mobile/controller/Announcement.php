<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: 当燃 2016-01-09
 */
namespace app\mobile\controller;
use think\Page;

class Announcement extends MobileBase {

    /**
     * [index 公告首页列表]
     */
    public function index(){
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);

        $cat_id = 1; //公告id
        $cat_ids =  M('ArticleCat')->where(['parent_id'=>1])->column('cat_id'); //获取公告分类下的子分类ids
        array_push($cat_ids,$cat_id);
        $cond = [
            'cat_id' => ['in',$cat_ids], //cat_id=1 //是公告
            'is_open' => 1,
        ];
        // $cat_ids && $cond['cat_id'] = ['in',$cat_ids];
        $articleModel = M('article');
        $currPage=I('currPage',1);
        $notice_list = $articleModel->where($cond)->order('add_time desc')->page($currPage,10)->select();
        $this->assign('lists', $notice_list);
        if ($_GET['is_ajax']) {
            return $this->fetch('ajaxIndex');
            exit;
        }
        return $this->fetch();
    }

    /**
     * [detail 公告详情]
     */
    public function detail()
    {
        $id=I('get.id');
        $article=M('article')->where(['article_id'=>$id])->find();
        $this->assign('article', $article);
        return $this->fetch();
    }

}