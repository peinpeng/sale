<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: Jaywoo 2019-5-5
 */
namespace app\mobile\controller;

use Think\Db;

class BusinessCollege extends MobileBase
{

    public function _initialize() {
        parent::_initialize();
    }

    public function index(){
        //页面引导
      $is_guide = isGuide($this->user_id);
      $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
      $this->assign('guide_imgs',$guide_imgs);
        return $this->fetch();
    }

    public function detail(){
        return $this->fetch();
    }


    protected function _deal_content($content=""){
        //针对图集七牛路径作处理
        $content = htmlspecialchars_decode($content);
        preg_match_all("/src=[\"|'| ]{0,}([^>]*\.(gif|jpg|bmp|png))/isU",$content,$matchs);
        $qiniuClient = \app\common\logic\QiniuLogic::instance();
        $qiniuContent = '';
        foreach ($matchs[1] as $k => $v) {
            $newUrl = $qiniuClient::getDomain().str_replace(['"/public/upload','/public/upload'], '', $v);
            $content = str_replace($v.'"', $newUrl, $content);
        }       
        return $content;
    }

}