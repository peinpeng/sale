<?php
/**
 * tpshop
 * ============================================================================
 * * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * $Author: 当燃 2016-01-09
 */

namespace app\mobile\controller;


class Complaint extends MobileBase
{
    /*
       * 初始化操作
       */
    public function _initialize()
    {
        parent::_initialize();
        if (session('?user')) {
            $session_user = session('user');
            $user = M('users')->where("user_id", $session_user['user_id'])->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
        } else {
            header("Location:" . U('Mobile/User/login'));
        }

    }

    /**
     * [index 我要投诉]
     */
    public function index()
    {
        //页面引导
        $is_guide = isGuide($this->user_id);
        $guide_imgs = $is_guide['status'] ==1? $is_guide['data']['guide_imgs']:[]; 
        $this->assign('guide_imgs',$guide_imgs);
        
        $give_gift_id = I('give_gift_id');
        if(!$give_gift_id) $this->error('闯关id为空');

        if (IS_POST) {
            $data['give_gift_id'] = $give_gift_id;
            $data['respondent_user_id'] = model('GiveGift')->where(['id'=>$give_gift_id])->value('collect_user_id');
            $data['title'] = I('title');
            $data['content'] = I('content');
            $data['user_id'] = $this->user_id;
            $data['add_time'] = time();
            if ($_FILES['imgList']['tmp_name']) {
                $files = $this->request->file('imgList');
                foreach ($files as $k => $file) {
                    $image_upload_limit_size = config('image_upload_limit_size');
                    $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
                    $dir = UPLOAD_PATH . 'complaint_pic/';
                    if (!($_exists = file_exists($dir))) {
                        $isMk = mkdir($dir);
                    }
                    $info = $file->validate($validate)->move($dir, true);
                    $parentDir = date('Ymd');
                    if ($info) {
                        $data['pics'][$k] = '/' . $dir . $parentDir . '/' . $info->getFilename();
                    } else {
                        $this->error($file->getError()); //上传错误提示错误信息
                    }
                }
                $data['pics'] = implode(',', $data['pics']); //上传的所有图片
            }

            $res = M('complaint')->add($data);
            if (!$res) $this->error('投诉失败');
            $this->success('投诉成功',U('complaintList'));
        }
        $config = tpCache('shop_info');
        $this->assign('give_gift_id', $give_gift_id);
        $this->assign('config', $config);
        return $this->fetch();
    }


    /**
     * [detail 投诉信息详情]
     */
    public function detail()
    {
         $id = I('get.id');
         $model = model('Common/Complaint');
         $complaint = $model->with(['user','respondent_user','give_gift'])->where(['complaint_id' => $id, 'user_id|respondent_user_id' => $this->user_id])->find();
         if (!$complaint) $this->error('无权查看改详情');
         $complaint['pics'] = explode(',', $complaint['pics']);
         $this->assign('complaint', $complaint);
         $this->assign('user_id', $this->user_id);
        return $this->fetch();
    }

    /**
     * [complaintList 投诉列表]
     */
    function complaintList()
    {
        $model = model('Common/Complaint');
        $p = I('p', '1');
        $type = I('get.type',0);// 0 我的投诉 1 投诉我的  

        $where = [];
        $type ? $where['respondent_user_id']=$this->user_id : $where['user_id']=$this->user_id;
     
        $lists = $model->with(['user','respondent_user','give_gift'])->where($where)->order('complaint_id desc')->page($p , '10')->select();
        // ps($model);

        if (IS_AJAX) {
            $this->assign('lists', $lists);
            return $this->fetch('ajaxList');
            exit;
        }
        return $this->fetch();
    }

    //发起申诉
    public function appeal()
    {
        $model = model('Common/Complaint');
        $id = I('id', 0);
        
        if(IS_POST && $id){
            if ($_FILES['appeal_pics']['tmp_name']) {
                $files = $this->request->file('appeal_pics');
                foreach ($files as $k => $file) {
                    $image_upload_limit_size = config('image_upload_limit_size');
                    $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
                    $dir = UPLOAD_PATH . 'complaint_pic/';
                    if (!($_exists = file_exists($dir))) {
                        $isMk = mkdir($dir);
                    }
                    $info = $file->validate($validate)->move($dir, true);
                    $parentDir = date('Ymd');
                    if ($info) {
                        $data['pics'][$k] = '/' . $dir . $parentDir . '/' . $info->getFilename();
                    } else {
                        $this->error($file->getError()); //上传错误提示错误信息
                    }
                }
                $appeal_pics = implode(',', $data['pics']); //上传的所有图片
            }
            $appeal = I('appeal','');
            // $appeal_pics = I('appeal_pics','');
            $res = $model->appeal($id,$appeal,$appeal_pics);
            if(!$res) $this->error($model->getError());
            $this->success('添加申诉成功',U('complaintList'));
        }

        $info = $model->get($id);
        if(!$info){
            return $this->error('投诉不存在');
        }

        $this->assign([
            'info'  => $info,
        ]);
        return $this->fetch();
    }

    //撤销
    public function revoke()
    {
        $model = model('Common/Complaint');
        $id = I('id', 0);
        $uid = I('uid', 0);

        $res = $model->revoke($id,$uid);
        if(!$res) ajaxReturn(['status'=>0,'msg'=>$model->getError()]);
        ajaxReturn(['status'=>0,'msg'=>'撤销成功','url'=>U('complaintList')]);
    }




}