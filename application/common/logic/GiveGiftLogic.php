<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/23
 * Time: 11:52
 */
namespace app\common\logic;

class GiveGiftLogic
{

    /**
     * User: 聂风 Function:更新订单状态
     * @param $collectUserId 收礼人id
     * @param $giveUserId 送礼人id
     * @param $status 状态
     */
    public function updateOrderStatus($collectUserId, $giveUserId, $status, $remark ='子商城购买记录')
    {
        $giveGiftModel = model('Common/GiveGift');
        $id = $giveGiftModel->where(
                    ['give_user_id'=>$giveUserId, 'collect_user_id'=>$collectUserId, 'status'=>0]
                )->value('id');
        if(empty($id)){
            return api_ajaxReturn(2, '记录不存在');
        }
        $res = $giveGiftModel->changeStatusByMobile($id,$status,$remark,$collectUserId,$giveUserId);
        if($res){
            return api_ajaxReturn(10000, '审核成功',['giveGiftId'=>$id]);
        }else{
            return api_ajaxReturn(2, '审核失败');
        }
    }
}