<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用TP5助手函数可实现单字母函数M D U等,也可db::name方式,可双向兼容
 * ============================================================================
 * Author: dyr
 * Date: 2017-12-04
 */

namespace app\common\logic;

use app\common\util\TpshopException;
use think\Model;
use think\Db;

/**
 * 用户类
 * Class CatsLogic
 * @package Home\Logic
 */
class CompressedImageLogic extends BaseLogic
{
    private $user;
    public $coupon = [];


    /**
     * [img description]
     * @param  [type] $img_path  [图片路径]
     * @param  [type] $save_path [保存路径]
     * @param  [type] $thumb_w   [压缩比]
     * @return [type]            [description]
     */
    public function img($img_path,$save_path,$thumb_w){
        $img_path = 'D:\phpStudy\PHPTutorial\WWW\sale\public\upload\identityCard\20190215\90a99e319a5ca849708080f8c9548d1f.jpg';
        $save_path = 'D:\phpStudy\PHPTutorial\WWW\sale\public\upload\identityCard\20190215\90a99e319a5ca849708080f8c9548d1f.jpg';
        $thumb_w = 800;
        $res = $this->compressedImage($img_path,$save_path,$thumb_w);
       return $res;

    }

    /**
   * desription 压缩图片
   * @param sting $imgsrc 图片路径
   * @param  [type] $thumb_w   [压缩比]
   * @param string $imgdst 压缩后保存路径
   */
  public function compressedImage($imgsrc, $imgdst ,$thumb_w) {
    list($width, $height, $type) = getimagesize($imgsrc);
     
    $new_width = $width;//压缩后的图片宽
    $new_height = $height;//压缩后的图片高
         
    if($width >= $thumb_w){
      $per = $thumb_w / $width;//计算比例
      $new_width = $width * $per;
      $new_height = $height * $per;
    }
     
    switch ($type) {
      case 1:
        $giftype = check_gifcartoon($imgsrc);
        if ($giftype) {
          header('Content-Type:image/gif');
          $image_wp = imagecreatetruecolor($new_width, $new_height);
          $image = imagecreatefromgif($imgsrc);
          imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
          //90代表的是质量、压缩图片容量大小
          imagejpeg($image_wp, $imgdst, 90);
          imagedestroy($image_wp);
          imagedestroy($image);
        }
        break;
      case 2:
        header('Content-Type:image/jpeg');
        $image_wp = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($imgsrc);
        // p($image);
        imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        //90代表的是质量、压缩图片容量大小
        imagejpeg($image_wp, $imgdst, 90);
        imagedestroy($image_wp);
        imagedestroy($image);
        break;
      case 3:
        header('Content-Type:image/png');
        $image_wp = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefrompng($imgsrc);
        imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        //90代表的是质量、压缩图片容量大小
        imagejpeg($image_wp, $imgdst, 90);
        imagedestroy($image_wp);
        imagedestroy($image);
        break;
    }
  }


}