<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\logic;

use think\Loader;
use think\Model;
use think\Page;
use think\Db;

/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class UserLevelLogic extends BaseLogic
{

    /**
     * User: Simony
     * Function:是否能升级 canLevel + levelInfo
     * @param $startUserId
     * @param $endUserId
     * @param bool $isReturnLevelInfo 是否返回等级信息
     * @return array
     */
    public function isCanUpgradeAndLevelInfo($userId, $limit, $isReturnLevelInfo = false)
    {
        $usersModel = model('common/Users');
        $currentUserInfo = $usersModel->where(['user_id' => $userId])->find();
        $startLevelId = $currentUserInfo['level'];  //开启等级id
        $endLevelId = $currentUserInfo['level'] + $limit; //结束等级id

        $userLevelModel = model('common/UserLevel');
        $getPartLevelInfo = $userLevelModel->getPartLevelInfo($startLevelId, $endLevelId);

        $data = [];
        foreach ($getPartLevelInfo as $v) {
            $data[$v['level_id']] = $v;
        }
        //下一个用户等级信息
        $nextUserLevelInfo = $data[$startLevelId + 1];
        $first_leader = $usersModel->getFisrtLeaderNumAttr($userId); //直推人数
        $teamnum = model('common/distribut/DistributTeam')->getUserTeamsAllCount($userId);//团队人数
        if ($isReturnLevelInfo === true)
            $res = $data;
        $res['canLevel'] = false;
        // 直推 和 审核人数达标
        if ($first_leader >= $nextUserLevelInfo['tjnum'] &&
            $teamnum >= $nextUserLevelInfo['team_check_num']
        ) {
            $res['canLevel'] = true;
        }
        return $res;
    }


}