<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: Jaywoo
 * Date: 2019-05-18
 */
namespace app\common\logic\place;
use app\common\model\Users;
use app\common\model\UserLevelLog;
use app\common\logic\BaseLogic;


/**
 * 占位匹配补偿类
 * 
 */
class PlaceMatchCompensateLogic extends BaseLogic
{

    protected $userMdl,$waitingMatchingMdl,$lossCountMdl,$lossCompensateConfigMdl,$userLevelLogMdl,$lossLogMdl,$matchingLogMdl,$temporaryLockingMdl;
    /**
     * [initialize 初始化]
     * @return [type] [description]
     */
    public function initialize(){
        $this->userMdl = model('Common/Users');
        $this->temporaryLockingMdl = model('Common/place/PlaceTemporaryLocking');           //上下级临时锁定记录表 DistributTeam
        $this->waitingMatchingMdl = model('Common/place/PlaceWaitingMatching');             //占位表
        $this->matchingLogMdl = model('Common/place/PlaceMatchingLog');                     //占位匹配补偿记录表
        $this->lossCountMdl = model('Common/place/PlaceLossCount');                         //流失统计表
        $this->lossLogMdl = model('Common/place/PlaceLossLog');                             //流失记录表
        $this->lossCompensateConfigMdl = model('Common/place/PlaceLossCompensateConfig');   //流失补偿等级配置表
        $this->userLevelLogMdl = model('Common/UserLevelLog');                              //升级记录表
        $this->distributTeamMdl = model('Common/distribut/DistributTeam');                  //团队直推链表
        $this->giveGiftMdl = model('Common/GiveGift');                                      //送礼管理表
        
    }

    /****************************/
    /*           占位           */                         
    /****************************/


    /**
     * Author: Jaywoo
     * 升级占位排队判断
     *
    */
    public function savePlace($user_id)
    {
        $res = $this->saveInWaitingMatching($user_id);
        return $res;
    }

    /**
     * Author: Jaywoo
     * 加入占位表
     *
    */
    public function saveInWaitingMatching($user_id,$desc='')
    {
        $row = $this->waitingMatchingMdl->where(['user_id'=>$user_id])->find();

        $data = [];
        $data['user_id']        = $user_id;
        $data['desc']           = $desc;
        $data['compensate_num'] = $this->getCompensateNum($user_id);//补偿
        $data['lineup_time']    = $this->userLevelLogMdl->getLineupTime($user_id);
        $data['loss_num']       = $this->lossCountMdl->getLossNum($user_id)?:0;
        $data['loss_time']      = $this->lossLogMdl->getEarliestLossTime($user_id)?:0;
        $data['match_num']      = $this->getMatchNum($user_id);//匹配数

        if($row){
            //编辑
            $data['id']             = $row['id'];
            $data['update_time']    = time();
        }else{
            //新增
            $data['add_time']      = time();
            $res = $this->checkAddPlace($user_id);//新增占位记录check条件
            if($res['status'] != 1) return $res;
        }
        $res = $this->waitingMatchingMdl->saveAll([$data]);
        // p($res);
        $this->changePlaceState($user_id);//更新占位人状态

        if(!$res)  return ['status'=>0, 'msg'=>'添加数据失败'];
        return ['status'=>1, 'msg'=>'ok'];
    }

    //新增占位记录check条件
    public function checkAddPlace($user_id)
    {
        //用户等级变动是否通过一星节点
        $res = $this->userLevelLogMdl->isPassLevel1($user_id);
        // p($res);
        if(!$res)   return ['status'=>0, 'msg'=>'用户等级变动没有通过一星节点'];
        return ['status'=>1, 'msg'=>'ok'];
    }


    //Author: Jaywoo  确认闯关后，更新占位人信息
    public function updatePlaceInfo($firstleader_id,$user_id)
    {
        $res = $this->getSourceAndTypeForMatchingLog($user_id,$firstleader_id);
        $placeUser = $this->waitingMatchingMdl->where(['user_id'=>$firstleader_id])->find();
        $data = [];

        //占位
        if($res['source_type'] == 2){
            //优先匹配名额
            if($placeUser['match_num'] > $placeUser['matched_num']){
                //有匹配名额
                $data['matched_num'] = $placeUser['matched_num']+1;
            }elseif($placeUser['compensate_num'] > $placeUser['compensated_num']){
                //有补偿名额
                $data['compensated_num'] = $placeUser['compensated_num']+1;
            }
        }

        //团队
        if($res['source_type'] == 1){
            //优先补偿名额
            if($placeUser['compensate_num'] > $placeUser['compensated_num']){
                //有补偿名额
                $data['compensated_num'] = $placeUser['compensated_num']+1;
            }elseif($placeUser['match_num'] > $placeUser['matched_num']){
                //有匹配名额
                $data['matched_num'] = $placeUser['matched_num']+1;
            }
        }
        $data['update_time'] = time();
        $res = $this->waitingMatchingMdl->where(['user_id'=>$firstleader_id])->save($data);  //更新匹配和补偿数量
        if(!$res)   return ['status'=>0, 'msg'=>'更新占位人数据失败'];

        $this->changePlaceState($firstleader_id);       //更新状态及下位流程

        return ['status'=>1, 'msg'=>'ok'];
        
    }
  
    //Author: Jaywoo  
    public function getMatchNum($user_id)
    {
        return 1;
    }

    //Author: Jaywoo  获取补偿数 取等级设置补偿数和流失总数的小值
    public function getCompensateNum($user_id)
    {
        $num = 0;

        //等级配置的可补偿数
        $user_level = $this->userMdl->getUserLevel($user_id);
        $config_compensate_num = $this->lossCompensateConfigMdl->getCompensateNum($user_id,$user_level);
        //流失总数
        $loss_num = $this->lossCountMdl->getLossNum($user_id);
        if(!$loss_num){
            return $num;
        }

        //当前等级补偿总数
        $compensated_num = $this->waitingMatchingMdl->where(['user_id'=>$user_id])->value('compensated_num'); //已补偿数
        $compensated_num = $compensated_num?$compensated_num:0;
        $surplus_num = $loss_num-$compensated_num ;//剩余补偿数

        $num = $surplus_num-$config_compensate_num>0?$config_compensate_num:$surplus_num;
        return $num;
    }

    /****************************/
    /*     闯关匹配补偿规则      */                         
    /****************************/

    //checkToChangeFristLeader 获取团队配置补偿人或者占位人
    public function getPlaceFirstLeader($user_id)
    {
        //用户上级小于1星时 执行补偿/匹配
        $user  = $this->userMdl->getUserInfo($user_id);
        $firstleader_level_id = $this->userMdl->where(['user_id'=>$user['first_leader']])->value('level');
        if($firstleader_level_id >=2){
            return false;
        }

        //check团队匹配补偿规则
        $uid = $this->getOptimalUserIdForTeam($user_id);

        if(!$uid){
            //check全服占位规则
            $uid = $this->getLeaderForPlace();

            //如果都没有则拿平台账号
            if(!$uid){
                $uid = model('config')->where(['inc_type'=>'other','name'=>'system_user_id'])->value('value');
            }
        }

        return $uid;
    }


    //团队匹配补偿规则 返回user_id
    public function getOptimalUserIdForTeam($user_id)
    {
        return $this->waitingMatchingMdl->getOptimalUserIdForTeam($user_id);
    }

    /**
     * Author: Jaywoo
     * 全服寻找匹配或补偿上级 
    */
    public function getLeaderForPlace()
    {
        //获取占位表排第一的用户
        return $this->waitingMatchingMdl->getPlaceTopUserId();
    }

    /**
     * Author: Jaywoo
     * 判断来源和是否补偿
    */
    public function getSourceAndTypeForMatchingLog($user_id,$firstleader_id)
    {
        //判断是否团队补偿/匹配
        $teamIds = $this->distributTeamMdl->getUserParentIds($user_id);
        $source_type = in_array($firstleader_id,$teamIds)?1:2;

        //判断是补偿/匹配
        $placeUser = $this->waitingMatchingMdl->where(['user_id'=>$firstleader_id])->find();
        if($source_type == 1){
            $type = $placeUser['compensate_num']-$placeUser['compensated_num'] > 0 ? 2 : 1;
        }else{
            $type = $placeUser['match_num']-$placeUser['matched_num'] > 0 ? 1 : 2;
        }
        return ['source_type'=>$source_type,'type'=>$type];
        
    }

    /**
     * Author: Jaywoo
     * 确实闯关检查
    */
    public function checkLeaderIsChange($collect_user_id,$give_user_id)
    {

        //用户是否在占位表中
        $placeUser = $this->waitingMatchingMdl->where(['user_id'=>$collect_user_id,'enable'=>1])->find();
        if(!$placeUser) return false;
        if($placeUser && in_array($placeUser['state'], [2,3]) ||($placeUser['match_num'] <= $placeUser['matched_num'] && $placeUser['compensate_num'] <= $placeUser['compensated_num']))  return false;
        return true;
    }

     /**
     * Author: Jaywoo
     * 闯关添加匹配/补偿记录
     *
    */
    public function addPlaceMatchingLog($firstleader_id,$user_id)
    {

        trans();
        //闯关检查上级用户是否匹配或补偿名额已用完
        $res = $this->checkLeaderIsChange($firstleader_id,$user_id);
        if($res === false){
            trans('rollback');
            return ['status'=>0, 'msg'=>'当前闯关上级匹配和补偿名额已用完！'];
        }

        //获取来源  补偿/匹配类型
        $res = $this->getSourceAndTypeForMatchingLog($user_id,$firstleader_id); 
        

        $data =[
            'user_id'         => $firstleader_id,
            'compensate_id'   => $user_id,
            'source_type'     => $res['source_type'],
            'type'            => $res['type'],
            'give_gift_id'    => $this->getGiftGive($firstleader_id,$user_id),
        ];
        $res = $this->matchingLogMdl->addData($data);
        if(!$res){
            trans('rollback');
            return ['status'=>0, 'msg'=>'添加匹配/补偿记录失败'];
        }

        /**
         * [修改上级占位表信息]
         */
        $res = $this->updatePlaceInfo($firstleader_id,$user_id);
        if($res['status'] == 0 ){
            trans('rollback');
            return ['status'=>0, 'msg'=>'修改上级占位表信息失败'];
        }

        trans('commit');

        if($res == false)  return ['status'=>0, 'msg'=>'添加匹配补偿记录失败'];
        return ['status'=>1, 'msg'=>'ok'];
    }

    /**
     * Author: Jaywoo
     * 获取送礼表id
    */
    public function getGiftGive($firstleader_id,$user_id)
    {
        $cond =[
            'give_user_id'=>$user_id,
            'collect_user_id'=>$firstleader_id,
            'status'=>0,
            'give_type'=>1,

        ];
        return $this->giveGiftMdl->where($cond)->order('id desc')->value('id');
    }
     /****************************/
    /*     闯关审核              */                         
    /****************************/
    /**
     * Author: Jaywoo
     * 审核成功操作
     *
    */


    /***************************************************************************************************************************************************************/
    //闯关是否是占位匹配
    public function checkIsPlace($give_gift_id,$status=null)
    {
        $where = ['give_gift_id'=>$give_gift_id];
        $status !== null && $where['status'] = $status;
        return $this->matchingLogMdl->where($where)->count()?true:false;
    }


    //闯关成功 占位匹配规则 回调
    public function giveToSuccess($give_gift_id)
    {
        $give_gift = model('GiveGift')->getZhiTuiGiveGift($give_gift_id);
        if(!$give_gift_id){
            return ['status'=>0, 'msg'=>'闯关数据不存在'];
        }
        $give_gift_id = $give_gift['id'];

        // p($this->checkIsPlace($give_gift_id));
        //不是占位规则的闯关
        if(!$this->checkIsPlace($give_gift_id,1)){
            return ['status'=>1, 'msg'=>'ojbk'];
        }

        $user_id = $give_gift['give_user_id'];//闯关人
        $collect_user_id = $give_gift['collect_user_id'];//闯关审核人

        trans();

        //添加流失记录 并流失总数加一
        $before_firstleader = M('Users')->where(['user_id'=>$user_id])->value('first_leader');
        $res = $this->addLossLog($user_id,$before_firstleader,$collect_user_id);
        if($res['status'] != 1){
            trans('rollback');
            return $res;
        }

        //解除临时锁定
        $result = $this->temporaryLockingMdl->where(['temporary_firstleader_id'=>$before_firstleader,'temporary_user_id'=>$user_id,'status'=>1])->update(['status'=>2]);

        //更新会员上级
        $res = $this->changeFirstLeader($user_id,$collect_user_id);
        if($res['status'] != 1){
            trans('rollback');
            return $res;
        }

        //更新占位状态
        $result = $this->changePlaceState($collect_user_id);
        
        //修改占位匹配补偿记录表状态
        $result = $this->matchingLogMdl->changeStatusSuccess($give_gift_id);
        if(!$result){
            trans('rollback');
            return ['status'=>0, 'msg'=>'更新占位匹配补偿记录表失败'];
        }

        trans('commit');       

        $this->changePlaceState($collect_user_id);//更新占位人状态 
        return ['status'=>1, 'msg'=>'ojbk'];
    }

    //闯关失败
    public function giveToFail($give_gift_id)
    {
        

        $give_gift = model('GiveGift')->getZhiTuiGiveGift($give_gift_id);
        if(!$give_gift_id){
            return ['status'=>0, 'msg'=>'闯关数据不存在'];
        }
        $give_gift_id = $give_gift['id'];

        //不是占位规则的闯关
        if(!$this->checkIsPlace($give_gift_id,1)){
            return ['status'=>1, 'msg'=>'ojbk'];
        }

        $user_id = $give_gift['give_user_id'];//闯关人
        $collect_user_id = $give_gift['collect_user_id'];//闯关审核人

        trans();

        //修改占位匹配补偿记录表状态 (同时返还审核人的匹配/补偿数量)
        $result = $this->matchingLogMdl->changeStatusFail($give_gift_id);
        // p($result);
        if(!$result){
            trans('rollback');
            return ['status'=>0, 'msg'=>$this->matchingLogMdl->getError()];
        }

        trans('commit');

        $this->changePlaceState($collect_user_id);//更新占位人状态

        return ['status'=>1, 'msg'=>'ojbk'];
    }

    //更新
    public function changeFirstLeader($user_id,$p_user_id)
    {
        //添加会员变更上级记录
        $user_info          = D('users')->where(['user_id'=>$user_id])->find();    //
        $first_leader_info  = D('users')->where(['user_id'=>$user_info['first_leader']])->find();    //原上级
        $first_leader       = D('users')->where(['user_id'=>$p_user_id])->find();    //新上级

        $before_first_leader_id = $first_leader_info['user_id'];   //原上级ID         
        $after_first_leader_id = $first_leader['user_id'];         //要更改的上级ID     

        $changeFirstLeaderLogMdl = model('Common/ChangeFirstLeaderLog');
        $result = $changeFirstLeaderLogMdl->addData($uid,$before_first_leader_id,$after_first_leader_id,1);  //添加会员修改上级记录

        $dtMdl = model('Common/distribut/DistributTeam');
        $res = $dtMdl->updateUserParent($user_info['mobile'],$first_leader['mobile']);//变更上级，更换上级链
        if(!$res){
            return ['status'=>0,'msg'=>$dtMdl->getError()];
        }
        return ['status'=>1,'msg'=>'ok'];
    }

    //获得用户应该处于的占位状态
    public function getPlaceState($user_id)
    {
        // $this->toPlace();//占位程序

        $row = $this->waitingMatchingMdl->where(['user_id'=>$user_id])->find();
        if(!$row) return 0;

        //名额已用完
        if($this->getPlaceNum($user_id)<=0){
            $give_gift_check = $this->matchingLogMdl->where(['user_id'=>$user_id,'status'=>1])->count();//用户是否在闯关审核中 是 true 否 false
            if($give_gift_check) return 2;//闯关审核中

            return 3;//名额已用完
        } 
        
        $row2 = $this->waitingMatchingMdl->where(['state'=>0])->order($this->waitingMatchingMdl->getOrderByStr())->find();
        if($row['user_id'] == $row2['user_id']) return 1;//应该上位

        return 0;//排队
    }

    //更新用户应该处于的占位状态
    public function changePlaceState($user_id)
    {
        $state = $this->getPlaceState($user_id);
        $res = $this->waitingMatchingMdl->where(['user_id'=>$user_id])->update(['state'=>$state,'update_time'=>time()]);

        $this->toPlace();

        return $res;
    }

    //确认占位人员是否还有剩余名额
    public function getPlaceNum($user_id)
    {
        $row = $this->waitingMatchingMdl->where(['user_id'=>$user_id])->find();
        return ($row['match_num'] - $row['matched_num']) + ($row['compensate_num'] - $row['compensated_num']);
    }

    //上位程序 占位
    public function toPlace()
    {
        $count = $this->waitingMatchingMdl->where(['state'=>1])->count();
        if($count > 1){
            $row = $this->waitingMatchingMdl->where(['state'=>1])->order($this->waitingMatchingMdl->getOrderByStr())->find();
            $this->waitingMatchingMdl->where(['state'=>1,'id'=>['<>',$row['id']]])->update(['state'=>0]);
            return $row['user_id'];
        }elseif($count == 1){
            return $this->waitingMatchingMdl->where(['state'=>1])->value('user_id');
        }

        //无人在位，开始上位了（登基流程）
        $row2 = $this->waitingMatchingMdl->where(['state'=>0])->order($this->waitingMatchingMdl->getOrderByStr())->find();

        //登基
        $this->waitingMatchingMdl->where(['user_id'=>$row2['user_id']])->update(['state'=>1,'update_time'=>time()]);

        return $row['user_id'];
    }

    /**
     * Author: Jaywoo
     * 闯关成功后，添加流失记录，更新流失总数
     *
    */
    public function addLossLog($user_id,$before_firstleader,$after_firstleader)
    {
        trans();
        //添加记录
        $result=$this->lossLogMdl->add([
            'user_id'            => $user_id,
            'before_firstleader' => $before_firstleader,
            'after_firstleader'  => $after_firstleader,
            'add_time'           => time(),
        ]); 
        
        if(!$result ){
            trans('rollback');
            return ['status'=>0, 'msg'=>'添加流失记录失败'];
        }

        //更新流失总数
        $update = [
            'loss_num'    => ['exp','loss_num+1'],
            'update_time' => time()
        ];
        $result = $this->lossCountMdl->where(['user_id'=>$before_firstleader])->count();
        if(!$result) {
            $res = $this->lossCountMdl->add([
                'user_id'  => $before_firstleader,
                'loss_num' => 1,
                'add_time' => time(),
            ]);
            if(!$res ){
                trans('rollback');
                return ['status'=>0, 'msg'=>'添加流失总数失败'];
            }
        }else{
            $res = $this->lossCountMdl->where(['user_id'=>$before_firstleader])->update($update);
            if(!$res ){
                trans('rollback');
                return ['status'=>0, 'msg'=>'更新流失总数失败'];
            }
        }
        trans('commit');
       
       return ['status'=>1, 'msg'=>'ok'];

    }






}
