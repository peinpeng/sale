<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/22
 * Time: 19:25
 */
namespace app\common\logic;

class HallLogic
{
    public function teamUserData($userId)
    {
        $user = model('users')->get($userId);

        $first_leader = model('Common/Users')->getFisrtLeaderNumAttr($userId,0);//大于等于0星
        $teamNum      = model('Common/distribut/DistributTeam')->teamNum($user['user_id']);
        $teamNum1     = model('Common/distribut/DistributTeam')->teamNum($user['user_id'],2);

        $slippingUsersLogic = new SlippingUsersLogic;
        $connecterNum = $slippingUsersLogic->getConnectNum($userId);
        return $this->teamUserDataFormat($user,$first_leader,$teamNum,$teamNum1,$connecterNum);
    }

    protected function teamUserDataFormat($user,$first_leader,$teamNum,$teamNum1,$connecterNum)
    {
        return [
            'headPic' => image_handle($user['head_pic']),
            'mobile' => $user['mobile'],
            'levelName'=>$user['level_name'],
            'connecterNum'=>$connecterNum,
            'firstLeader'=>$first_leader?$first_leader:0,
            'teamNum'=>$teamNum,
            'aboveNum'=>$teamNum1
        ];
    }

    public function teamSearch($userId, $scene, $keywords, $currPage, $pageSize)
    {
        $orderBy = 'reg_time DESC';

        if($keywords!='') {
            $where['mobile|nickname'] = ['like',"$keywords%"];
        }
        $user = model('users')->get($userId);
        $memberCenterLogicMdl = model('common/user/MemberCenterLogic','logic');

        $team = $scene == 1 ? $memberCenterLogicMdl->myTeam($where,$orderBy,$currPage,$userId,$pageSize):$memberCenterLogicMdl->myTeamForFirstLeader($where,$orderBy,$currPage,$userId,$pageSize);
        return $this->teamSearchFormat($team);
    }

    protected function teamSearchFormat($list)
    {
        $data = [];
        if(empty($list)){
            return $data;
        }
        foreach ($list as $k => $v) {
            $title = '';
            $jiedian = '';
            $tuijian = '';
            if($v['is_slipping'] && $v['is_slipping'] == 1){
                $title = '滑落';
                $jiedian = $v['connecter_mobile'];
            }
            if($v['is_connect'] && $v['is_connect'] == 1){
                $title = '接点';
                $tuijian = $v['referrer_mobile'];
            }
            $data[] = [
                'headPic'=> image_handle($v['head_pic']),
                'mobile'=>$v['mobile'],
                'title' =>$title,
                'jiedian' =>$jiedian,
                'tuijian' =>$tuijian,
                'levelName' =>$v['level_name'],
                'addTime' => date('Y.m.d', $v['reg_time']),
                'devote' => model('Common/Users')->getFisrtLeaderNumAttr($v['user_id'],0)?:0,
            ];
        }
        return $data;

    }
}