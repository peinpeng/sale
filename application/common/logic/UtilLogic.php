<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/18
 * Time: 15:22
 */
namespace app\common\logic;

use think\Cache;

class UtilLogic
{
    //获取城市数据
    public function cityAreas()
    {
        $data = [];
        $cacheKey = 'cityAreas';
        $list = Cache::get($cacheKey);
        if ($list) {
            $data['list'] = unserialize($list);
        } else {
            $list = [];
            $region = M('region')->where('id>0')->field('id,name,parent_id')->select();
            $k0 = 0;
            foreach ($region as $k => $v) {
                if ($v['parent_id'] == 0) {
                    $list[$k0]['province'] = $v['name'];
                    $list[$k0]['provinceId'] = $v['id'];
                    //unset($region[$k]);
                    $i = 0;
                    foreach ($region as $k1 => $v1) {
                        if ($v1['parent_id'] == $v['id']) {
                            $list[$k0]['list'][$i]['city'] = $v1['name'] ? $v1['name'] : '';
                            $list[$k0]['list'][$i]['cityId'] = $v1['id'] ? $v1['id'] : '';
                            // unset($region[$k1]);

                            $j = 0;
                            foreach ($region as $k2 => $v2) {
                                if ($v2['parent_id'] == $v1['id']) {
                                    $list[$k0]['list'][$i]['areaList'][$j]['area'] = $v2['name'] ? $v2['name'] : '';
                                    $list[$k0]['list'][$i]['areaList'][$j]['areaId'] = $v2['id'] ? $v2['id'] : '';
                                    // unset($region[$k2]);
                                    $j++;
                                }
                            }
                            $i++;
                        }
                    }
                    $k0++;
                }
            }

            if (!empty($list)) {
                $data['list'] = $list;
                Cache::set($cacheKey, serialize($list));
            }
        }

        if (!empty($list)) {
            $msg = '获取成功';
            $code = 10000;
        } else {
            $msg = '获取失败';
            $code = -1;
        }
        return [
            'code'=>$code,
            'message'=>$msg,
            'result'=>$data,
        ];
    }

    public function batchesCity($parentId)
    {
        $data = M('region')->where('parent_id', $parentId)->field('id,name')->select();
        // $data = M('regionAll')->where('parent_id', $parentId)->field('id,name')->select();
        return $data;
    }
}