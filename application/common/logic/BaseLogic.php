<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\logic;
use think\Model;
class BaseLogic extends Model {
    

    /**
     * [$error 错误信息]
     * @var string
     */
    protected $error = '';

    /**
     * [initialize 初始化]
     * @return [type] [description]
     */
    public function initialize(){
        parent::initialize();
    }

    /**
     * [getError 获取出错信息]
     * @return [type] [description]
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * [setError 设置逻辑错误信息]
     * @param string $error [description]
     */
    public function setError($error = '')
    {
       $this->error = $error;
       return false;
    }



}
