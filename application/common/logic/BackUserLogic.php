<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/21
 * Time: 17:30
 */
namespace app\common\logic;

use app\common\model\Users;

class BackUserLogic
{
    public function selectUser($mobile, $userIdArr, $currPage, $pageSize, $isAccurate= 0)
    {
        if(empty($mobile) && empty($userIdArr)){
            return api_ajaxReturn(2, '查询条件不能为空');
        }
        $where = [];
        if($isAccurate == 0 && $mobile){
            $where['a.mobile'] = ['like', "%{$mobile}%"];
        }
        if($isAccurate == 1 && $mobile){
            $where['a.mobile'] = $mobile;
        }
        if($userIdArr){
            $where['a.user_id'] = ['in', $userIdArr];
        }
        $count = 0;
        $listObj = Users::alias('a')
                ->field("a.*,b.true_name,c.show_name,d.mobile as first_leader_mobile")
                ->join('credit_identity_card b', 'b.user_id=a.user_id', 'left')
                ->join('user_level c', 'c.level_id=a.level', 'left')
                ->join('users d', 'd.user_id=a.first_leader','left')
                ->where($where);
        if($currPage > 0){
            $listObj = $listObj->page($currPage, $pageSize);
            $count = Users::alias('a')->where($where)->count();
        }
        $list = $listObj->select();

        $list = $this->selectUserFormat($list);
        $data = [
            'list'=>$list,
            'totalCount'=>$count,
            'totalPage'=>ceil($count/$pageSize),
            'currPage'=>$currPage,
        ];
        return api_ajaxReturn(10000, '请求成功', $data);
    }

    //转换
    protected function selectUserFormat($list)
    {
        $data = [];
        if(empty($list)){
            return $data;
        }

        foreach ($list as $k => $v) {
            $data[$v['user_id']] = [
                'userId' => $v['user_id'],
                'trueName' => $v['true_name']?$v['true_name']:'',
                'mobile' => $v['mobile'],
                'nickname' => $v['nickname'],
                'levelName' => $v['show_name'],
                'firstLeaderMobile' => $v['first_leader_mobile'],
                'underlingNumber' => $v['underling_number'],
                'loginNum' => $v['login_num'],
                'lastLogin' => date('Y-m-d H:i:s', $v['last_login']),
                'regTime' => date('Y-m-d H:i:s', $v['reg_time']),
                'headPic' => image_handle($v['head_pic']),
                'isLock' => $v['is_lock'],
            ];
        }
        return $data;
    }

    public function selectUserByNickname($nickname, $currPage, $pageSize, $isAccurate= 0)
    {
        if(empty($nickname)){
            return api_ajaxReturn(2, '查询条件不能为空');
        }
        $where = [];
        if($isAccurate == 0 && $nickname){
            $where['nickname'] = ['like', "%{$nickname}%"];
        }
        if($isAccurate == 1 && $nickname){
            $where['nickname'] = $nickname;
        }
        $count = 0;
        $list = Users::where($where)->column('user_id');
        if($currPage > 0){
            $listObj = $listObj->page($currPage, $pageSize);
            $count = Users::alias('a')->where($where)->count();
        }
        // $list = $listObj->column('user_id');
        $data = [
            'list'=>$list?:'',
            'totalCount'=>$count,
            'totalPage'=>ceil($count/$pageSize),
            'currPage'=>$currPage,
        ];
        return api_ajaxReturn(10000, '请求成功', $data);
    }
}