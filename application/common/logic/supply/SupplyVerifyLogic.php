<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/9/12
 * Time: 14:29
 */
namespace app\common\logic\supply;

class SupplyVerifyLogic
{
    private $signName = 'sign';

    private $timeLimit = 60;

    //验签
    public function verify()
    {
        $apiKey = input('apiKey');
        $time = input('time');
        if(empty($apiKey)){
            api_response('90001','apiKey不能为空');
        }
        if(empty($time)){
            api_response('90002','time不能为空');
        }
        if((time() - $time > $this->timeLimit)){
            api_response('90005','该请求超时');
        }
        $apiSecret = model('Supply')->getApiSecret($apiKey);
        if(empty($apiSecret)){
            api_response('90003','apiKey不正确');
        }
        session('supply_api_secret', $apiSecret);
        $params = $_POST?$_POST:$_GET;
        $key = input($this->signName, '', 'trim');
        $isUserful=$this->checkInterfaceUseful($params,$key,$apiSecret);
        if(!$isUserful){
            api_response('90004','密钥不正确');
        }
    }

    /**
     * 检查接口是否合法
     * @return [type] [description]
     */
    private function checkInterfaceUseful($params,$key,$apiSecret){
        //暂时不需要验签
        if(request()->ip()=='127.0.0.1') return true;

        $privateKey=$this->getKey($params,$apiSecret);
        if(!$this->checkKey($key,$privateKey)){
            return false;
        }
        return true;
    }

    protected function getKey($params="",$apiSecret){
        if(isset($params[$this->signName])){
            unset($params[$this->signName]);
        }
        ksort($params);
        // dump($params);
        $str='';
        if(is_array($params)){
            foreach ($params as $k=>$v){
                $str.="$k=$v&";
            }
        }
        $str=trim($str,'&').$apiSecret;
        // dump($str);

        return md5($str);
    }

    /**
     * 检查密钥正确性
     * @param  [type] $paramKey  [前端传过来的密钥]
     * @param  [type] $resultKey [后台计算得到的密钥]
     * @return [type]     bollean       [description]
     */
    private function checkKey($paramKey,$resultKey){
        if($paramKey!=$resultKey){
            return false;
        }else{
            return true;
        }
    }
}