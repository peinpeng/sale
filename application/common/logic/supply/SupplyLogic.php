<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/9/12
 * Time: 15:27
 */
namespace app\common\logic\supply;

class SupplyLogic extends SupplyVerifyLogic
{
    private $redirectUrlName = 'supply_redirect_url';

    //进入登录页面
    public function beginLogin()
    {
        if(input('apiKey')){
            //验签
            $this->verify();
            //跳转地址
            $redirectUrl = urldecode(input('redirectUrl'));
            $this->setRedirectUrl($redirectUrl);
        }else{
            if($this->getRedirectUrl()){
                $this->deleteRedirectUrl();
            }
        }
    }

    //登录后跳转
    public function loginRedirect($userId, $function = null)
    {
        $redirectUrl = $this->getRedirectUrl();
        if(!$redirectUrl){
            return;
        }
        $this->deleteRedirectUrl();
        $data = model('Users')->supplyGetUserData($userId);
        $data['time'] = time();
        $this->responseRedirect($redirectUrl,$data, $function);
    }

    //获取跳转链接
    private function getRedirectUrl()
    {
        return session($this->redirectUrlName);
    }

    //设置跳转地址
    public function setRedirectUrl($redirectUrl)
    {
        session($this->redirectUrlName, $redirectUrl);
    }

    //删除跳转地址
    private function deleteRedirectUrl()
    {
        session($this->redirectUrlName,null);
    }

    //输入页面进行跳转
    private function responseRedirect($url,$params=[],$function,$method = 'post')
    {
        $str =  '<form name="form2" id="submit" action="'.$url.'" method="'.$method.'">';
        if($params){
            foreach ($params as $k => $v) {
                $str .= '<input type="hidden" name="'.$k.'" value="'.$v.'">';
            }
        }
        $str .= '<input type="hidden" name="sign" value="'.$this->getKey($params,session('supply_api_secret')).'">';
        $str .= '</form>';
        $str .= '<script>document.getElementById("submit").submit();</script>';
        if($function instanceof \Closure){
            $function();
        }
        echo $str;exit;
    }

}