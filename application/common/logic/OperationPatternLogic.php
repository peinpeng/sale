<?php
//发送短信
namespace app\common\logic;


class OperationPatternLogic extends BaseLogic
{	

	static $distributConfig = [];
	protected $distributType = ['vip','trade'];
	protected $userMdl,$orderMdl,$userLevelMdl,$distributLevelConfigMdl,$distributMoneyIncomeLogMdl,$distributTeamMdl;

	public $_prizeType = [
		'direct_prize' => 1,  //直推
		'role_prize' => 2,    //角色补贴
		'weight_prize' => 3,  //加权分红 
		'operation_prize' => 4 //运营分红
	];	

	public $_prizeTypeDesc = [
		'direct_prize' => '直推奖',  //直推
		'role_prize' => '角色补贴',    //角色补贴
		'weight_prize' => '加权分红',  //加权分红 
		'operation_prize' => '运营分红' //运营分红
	];	

	/**
	 * [getDistributType 获得允许的分销类型]
	 * @param  string $distributType [如果不为空则检查]
	 * @return [type]                [description]
	 */
	public function getDistributType($distributType = '')
	{
		if(empty($distributType)) return $this->distributType;
		return in_array($distributType, $this->distributType);
	}

	/**
	 * [initialize 初始化]
	 * @return [type] [description]
	 */
	public function initialize(){
		
		$this->userMdl = model('Common/Users');
		$this->orderMdl = model('Common/Order');
		$this->distributLevelConfigMdl = model('Common/distribut/DistributLevelConfig');
		$this->distributMoneyIncomeLogMdl = model('Common/distribut/DistributMoneyIncomeLog');
		$this->userLevelMdl = model('Common/users/UserLevel');
		$this->distributTeamMdl = model('Common/distribut/distributTeam');

		foreach($this->distributType as $k=>$v)
		{
			self::$distributConfig[$v] = $this->distributLevelConfigMdl->getDistributConfig($v);
		}
	}


	/**
	 * [getDistributConfigByDistributType 根据分销类型、等级、奖项类型获取分红等级配置信息]
	 * @param  string  $distributType [description]
	 * @param  integer $level         [description]
	 * @param  string  $prizeType     [description]
	 * @return [type]                 [description]
	 */
	public function getDistributConfigByDistributType($distributType = '',$level = 0,$prizeType = '')
	{

		if(empty($level)) return (array)self::$distributConfig[$distributType];
		if(empty($prizeType)) return (array)self::$distributConfig[$distributType][$level];
		return  (string)self::$distributConfig[$distributType][$level][$prizeType];
	}


	/**
	 * [grantDirectPrize 发放直推]
	 * @param  integer $userId      [用户编号]
	 * @param  integer $orderId     [订单编号]
	 * @param  integer $totalAmount [订单总金额]
	 * @return [type]               [description]
	 */
	public function grantDirectPrize($userId,$orderId,$totalAmount,$distributType)
	{
		$user       = $this->userMdl->getByPk($userId,'user_id,level,first_leader');
		$directUser = $this->userMdl->getByPk($user['first_leader'],'user_id,level');
		if(empty($directUser)) return ;
		/**
		 * 获取直推奖分红比率
		 */
	
		$directRate = $this->getDistributConfigByDistributType($distributType,$directUser['level'],'direct_prize');

		$directPrize = number_format($totalAmount*$directRate,3,'.','');

		/**
		 * 发放预计奖金
		 */
		if(empty($directRate)) return ;

		$this->_makeGrantData($userId,$orderId,$user['first_leader'],$directPrize,'direct_prize');

		if(!empty($this->error)) return false;

		return ;
	}

	/**
	 * [grantWeightPrize 发放加权分红]
	 * @param  [type] $userId        [description]
	 * @param  [type] $orderId       [description]
	 * @param  [type] $totalAmount   [description]
	 * @param  [type] $distributType [description]
	 * @return [type]                [description]
	 */
	public function grantWeightPrize($userId,$orderId,$totalAmount,$distributType)
	{
		/**
		 * 发放三种加权分红，目前trade(购物区)无加权分红
		 */
		if(empty($distributType) || $distributType == 'trade') return ;

		/**
		 * 查询出加权分红不为0的数据
		 */
		$distributConfig = $this->getDistributConfigByDistributType($distributType);
		
		$weightList = [];
		
		foreach ($distributConfig as $k => $v) {
			if(!empty($v['weight_prize'])) $weightList[] = $v;
		}
		
		/**
		 * [如数据量大，需要使用队列]
		 * @var [type]
		 */
		if(!empty($weightList)) foreach ($weightList as $k => $v) {
			$grantData = [];
			$userIds = $this->userMdl->getCol('user_id',['level'=>$v['level_id']]);
			/**
			 * 如果不存在$v['level_id']的用户那就直接跳过
			 */
			if(empty($userIds)) continue;

			$weightRate  = $this->getDistributConfigByDistributType($distributType,$v['level_id'],'weight_prize');
			/**
			 * [均分加权分红]
			 */
			$weightPrize = number_format($totalAmount*$weightRate/count($userIds),3,'.','');
			
			/**
			 * [$chunkUserIds 数据处理优化等]
			 * @var array
			 */
			$chunkUserIds = array_chunk($userIds, 500);
			foreach ($chunkUserIds as $kk => $vv) {
				foreach ($vv as $kkk => $vvv) {
					$this->_makeGrantData($userId,$orderId,$vvv,$weightPrize,'weight_prize');
					if(!empty($this->error)) return false;
				}
			}
		}

		return ;
	}


	/**
	 * [grantOperationPrize 发放运营分红]
	 * @param  [type] $userId        [description]
	 * @param  [type] $orderId       [description]
	 * @param  [type] $totalAmount   [description]
	 * @param  [type] $distributType [description]
	 * @return [type]                [description]
	 */
	public function grantOperationPrize($userId,$orderId,$totalAmount,$distributType)
	{
		/**
		 * 如果订单金额少于五元，不发放运营分红 （临时）
		 */
		if($totalAmount < 5) return ;

		/**
		 * 查询出运营分红 (目前写死)
		 */
		$operationPrize = $this->getDistributConfigByDistributType($distributType,6,'operation_prize');
		
		/**
		 * 运营分红：首先按照订单地址所属区域的运营中心分红
		 *  如果该地区没有运营中心则该笔运营分红分为该会员直线上级的第一个运营中心会员
		 */
		$orderAddressDistrict = $this->orderMdl->getBy('order_id',$orderId,'district');
		if(!empty($orderAddressDistrict)) 
		{
			$operationUser = $this->userMdl->getBy('district',$orderAddressDistrict,'user_id');
			/**
			 * 如果订单的地区运营中心不存在，查找用户最近的一个运营中心
			 */
			if(empty($operationUser))
			{
				$parentIds = $this->distributTeamMdl->getUserParentIds($userId);
				$nearestUser = $this->userMdl->getParentInfos($parentIds,'user_id,level',$this->userLevelMdl->getMaxLevel());
				/**
				 * 如果没有最近的运营中心,不返分红
				 */
				if(empty($nearestUser)) return ;

				$operationUser = $nearestUser['user_id'];		
			}

			$this->_makeGrantData($userId,$orderId,$operationUser,'5.000','operation_prize');
			if(!empty($this->error)) return false;

			return ;
		}

		return ;
	}


	/**
	 * [grantRolePrize 发放角色补贴]
	 * @param  [type] $userId        [description]
	 * @param  [type] $orderId       [description]
	 * @param  [type] $totalAmount   [description]
	 * @param  [type] $distributType [description]
	 * @return [type]                [description]
	 */
	public function grantRolePrize($userId,$orderId,$totalAmount,$distributType)
	{

		/**
		 * 根据分红类型获取角色补贴奖最高值，目前的以运营中心的最高值为基础
		 */
		$rolePrizeRate = $this->getDistributConfigByDistributType($distributType,$this->userLevelMdl->getMaxLevel(),'role_prize');
		
		if(empty($rolePrizeRate)) return ;

		/**
		 * 获取当前购买用户的直推链
		 */
		$parentIds = $this->distributTeamMdl->getUserParentIds($userId);
		$parentUserInfos = $this->userMdl->getParentInfos($parentIds,'user_id,level');
		
		/**
		 * 获取最近的  目前角色补贴发放等级（不为0的）
		 */
		
		$distributConfig = $this->getDistributConfigByDistributType($distributType);
		$rolePrizeList = [];
		if(!empty($distributConfig)) foreach ($distributConfig as $k => $v) {
			if(!empty($v['role_prize'])){
				$rolePrizeList[$v['level_id']] = [
					'level' => $v['level_id'],
					'role_prize' => $v['role_prize']
				];
			} 
		}
		
		/**
		 * 用户直推链
		 * [parentUserInfos  $k 为userId  $v 为 levelId]
		 */
		$userRolePrizes = []; //需要发放角色补贴的用户
		if(!empty($parentUserInfos) && !empty($rolePrizeList)) foreach ($rolePrizeList as $k => $v) {
			
			$userRolePrize = $this->userMdl->getNearestUser($parentUserInfos,$v['level']);
			if(!empty($userRolePrize)) $userRolePrizes[] = $userRolePrize;
		}

		/**
		 * 如果存在发放角色补贴的用户
		 */
		if(!empty($userRolePrizes)){
			$userRolePrizes['rolePrizeRate'] = $rolePrizeRate;
			$userRolePrizes['hasDividedRate'] = 0;
			foreach ($userRolePrizes as $k => $v) {
				$grantData = [];
				$rolePrize = '';
				if($k == 0 && isset($v['level'])){   //第一次分 分满
					$userRolePrizes['rolePrizeRate'] -= $rolePrizeList[$v['level']]['role_prize'];
					$userRolePrizes['hasDividedRate'] += $rolePrizeList[$v['level']]['role_prize'];
					$rolePrize = number_format($totalAmount*$rolePrizeList[$v['level']]['role_prize'],3,'.','');
					if($userRolePrizes['rolePrizeRate'] > 0)
					{
						$this->_makeGrantData($userId,$orderId,$v['user_id'],$rolePrize,'role_prize');
						if(!empty($this->error)) return false;
					}
				}elseif ($userRolePrizes['rolePrizeRate'] > 0 && isset($v['level'])) { //第二次以及之后的分配
					$rolePrizeRate = $rolePrizeList[$v['level']]['role_prize'] - $userRolePrizes['hasDividedRate'];
					$userRolePrizes['hasDividedRate'] += $rolePrizeRate;
					$userRolePrizes['rolePrizeRate'] -= $rolePrizeRate;
					$rolePrize = number_format($totalAmount*$rolePrizeRate,3,'.','');
					$this->_makeGrantData($userId,$orderId,$v['user_id'],$rolePrize,'role_prize');
					if(!empty($this->error)) return false;
				}
				
			}
		}
	    return;
		
	}


	/**
	 * [_makeGrantData 运营模式发放奖金组装数组]
	 * @param  [type] $userId        [description]
	 * @param  [type] $orderId       [description]
	 * @param  [type] $benefitUserId [description]
	 * @param  [type] $money         [description]
	 * @param  [type] $prizeType     [description]
	 * @return [type]                [description]
	 */
	protected function _makeGrantData($userId,$orderId,$benefitUserId,$money,$prizeType){

		$grantData[] = [
			'buy_user_id'        =>  $userId,
			'benefit_user_id'    =>  $benefitUserId,
			'money'              =>  $money,
			'type'               =>  $this->_prizeType[$prizeType],
			'source_table'       =>  'order',
			'source_id'          =>  $orderId,
			'des'                =>  $this->_prizeTypeDesc[$prizeType]
		];	

		$res = $this->distributMoneyIncomeLogMdl->addIncomeLog($grantData);	
		
		if(empty($res)) {
			$this->error = '发放'.$this->_prizeTypeDesc[$prizeType].'出现错误';
			return false;
		}
		return ;

	}	


	/**
	 * [operationFlow 商业模式主程序] 
	 * 如果存在一个购物车有两个模式的商品，结算时根据订单商品表做操作
	 * @param  integer $userId        [用户编号]
	 * @param  integer $orderId       [订单编号]
	 * @param  integer $totalAmount   [订单金额]
	 * @param  string  $distributType [分红类型]
	 * @return [type]                 [description]
	 */
	public function operationFlow($userId = 0,$orderId = 0 ,$totalAmount = 0,$distributType = '')
	{
		$res = $this->_checkOperationFlow($userId,$orderId,$totalAmount,$distributType);
		
		if($res === false) return false;
		
		trans();

		//直推奖
		$res = $this->grantDirectPrize($userId,$orderId,$totalAmount,$distributType);

		if($res === false) return false;
		
		//加权分红
		$res = $this->grantWeightPrize($userId,$orderId,$totalAmount,$distributType);
		if($res === false) return false;

		//运营分红
		$res = $this->grantOperationPrize($userId,$orderId,$totalAmount,$distributType);
		if($res === false) return false;
		
		//角色补贴
		$res = $this->grantRolePrize($userId,$orderId,$totalAmount,$distributType);
		if($res === false) return false;

		trans('commit');
		return ;	
	}

	/**
	 * [_checkOperationFlow description]
	 * @param  [type] $userId        [description]
	 * @param  [type] $orderId       [description]
	 * @param  [type] $totalAmount   [description]
	 * @param  [type] $distributType [description]
	 * @return [type]                [description]
	 */
	protected function _checkOperationFlow($userId,$orderId,$totalAmount,$distributType)
	{
		if(empty($userId)) $this->error = '用户编号有误';
		if(empty($orderId)) $this->error = '订单编号有误';
		if($totalAmount <= 0) $this->error = '订单金额出现错误';
		if(!in_array($distributType,$this->distributType)) $this->error = '分销类型出现错误';
		if(!empty($this->error))  return false;	
		return ;
	}

	
}