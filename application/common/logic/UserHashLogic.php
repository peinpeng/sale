<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/18
 * Time: 16:43
 */
namespace app\common\logic;

use app\common\model\credit\CreditIdentityCard;
use app\common\model\GiveGift;
use app\common\model\UserLevel;
use app\common\model\Users;
use app\common\model\UserToken;

class UserHashLogic
{
    protected $redis;

    protected $hashTable = 'users_hash';

    protected $levelData = [];

    public function __construct()
    {
        $this->redis = new \Redis();
        $this->redis->connect(config('cache.host'),config('cache.port'));
        $this->redis->auth(config('cache.password')); //密码验证
        $this->levelData();
    }

    //把user表里的全部数据的部分字段刷进redis
    public function allTable()
    {
        //$this->redis->delete($this->hashTable);
        $this->selectUserAndSet();
    }

    //刷新单个用户数据
    public function refreshOne($userId)
    {
        $user = Users::field('user_id,nickname,head_pic,first_leader,sex,birthday,mobile,level,paypwd,password,province,city,district')
                ->where('user_id', $userId)
                ->find();
        $card = CreditIdentityCard::where('user_id', $userId)->column('process,true_name','user_id');
        $token = UserToken::where('user_id', $userId)->column('user_id,token');
        $firstLeader = Users::where('user_id', $user['first_leader'])->column('user_id,mobile');
        $shop = GiveGift::where([
                'give_user_id'=>$userId,
                'status'=>0,
            ]
        )->field('collect_user_id,id,give_user_id')
        ->select();
        $arr = [];
        foreach ($shop as $k => $v) {
            $arr[$v['give_user_id']][$v['collect_user_id']] = $v['id'];
        }

        $data = $this->dataFormat($user, $card, $token, $firstLeader, $arr);
        $this->hset($userId, $data);
        return $data;
    }

    //等级
    protected function levelData()
    {
        $this->levelData = (new UserLevel())->column('level_id,show_name');
    }

    protected function selectUserAndSet($where=[])
    {
        Users::field('user_id,nickname,head_pic,first_leader,sex,birthday,mobile,level,paypwd,password,province,city,district')
        ->where($where)
        ->chunk(1000, function($list) {
            $userIdArr = array_column($list, 'user_id');
            $leaderIdArr = array_column($list, 'first_leader');

            $card = CreditIdentityCard::where('user_id', 'in', $userIdArr)->column('process,true_name','user_id');
            $token = UserToken::where('user_id', 'in', $userIdArr)->column('user_id,token');
            $firstLeader = Users::where('user_id', 'in', $leaderIdArr)->column('user_id,mobile');
            $shop = GiveGift::where([
                'give_user_id'=>['in', $userIdArr],
                'status'=>0,
                ]
            )->field('collect_user_id,id,give_user_id')
            ->select();
            $arr = [];
            foreach ($shop as $k => $v) {
                $arr[$v['give_user_id']][$v['collect_user_id']] = $v['id'];
            }

            foreach ($list as $k => $v) {
                $this->hset($v['user_id'], $this->dataFormat($v, $card, $token, $firstLeader, $arr));
            }
        });
        return true;
    }

    protected function hset($key, $value)
    {
        $this->redis->hSet($this->hashTable, $key, json_encode($value));
    }

    protected function dataFormat($data, $card, $token, $firstLeader, $shop)
    {
        $process = 1;
        if(isset($card[$data['user_id']])){
            $process = model('common/credit/CreditIdentityCard')->switchStatus($card[$data['user_id']]['process']);
        }
        $trueName = '';
        if(isset($card[$data['user_id']])){
            $trueName = $card[$data['user_id']]['true_name'];
        }

        $loginToken = '';
        if(isset($token[$data['user_id']])){
            $loginToken = $token[$data['user_id']];
        }

        $birthday = '';
        if($data['birthday']){
            $birthday = date('Y-m-d', $data['birthday']);
        }

        $firstLeaderMobile = '';
        if(isset($firstLeader[$data['first_leader']])){
            $firstLeaderMobile = $firstLeader[$data['first_leader']];
        }

        //商店闯关记录
        $shopArr = [];
        if(isset($shop[$data['user_id']])){
            $shopArr = $shop[$data['user_id']];
        }


        return [
            'user_id' => $data['user_id'],
            'mobile'  => $data['mobile'],
            'nickname' => $data['nickname'],
            'login_token' => $loginToken,
            'head_pic' => $this->imageHandle($data['head_pic']),
            'first_leader' => $data['first_leader'],
            'first_leader_mobile' => $firstLeaderMobile,
            'real_status' => $process,
            'true_name' => $trueName,
            'sex' => $data['sex'],
            'birthday' => $birthday,
            'levelName' => $this->levelData[$data['level']],
            'level'=>$data['level'],
            'paypwd'=>$data['paypwd'],
            'password'=>$data['password'],
            'shop_arr'=>$shopArr,
            'province' => $data['province'],
            'city' => $data['city'],
            'district' => $data['district'],
        ];
    }

    protected function imageHandle($url)
    {
        if(empty($url)){
            return $url;
        }
        if(preg_match('/^(?:http[s]?:)?\/\//i',$url)){
            return $url;
        }
        return $url;
    }

    //查询用户信息
    public function findUser($userId)
    {
        $result = $this->redis->hget($this->hashTable, $userId);
        if($result){
            $data = json_decode($result, true);
        }else{
            $data = $this->refreshOne($userId);
        }
        return $data;
    }

    public function test2()
    {
        $a = $this->redis->hget('users_hash', 153459159);
        var_dump(json_decode($a, true));
    }
}