<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\logic;
use think\Model;
class IndexLogic extends Model {
    
   /**
    *首页 
    */
   public function index(){
       $indexInfo['indexNav']=model('common/nav/Navigation')->getNavigationByPosition(11);
       $vipWhere=[
           'is_on_sale'=>1,
           'is_recommend'=>1,
           'distribut_type'=>'vip',
       ];
       $indexInfo['vipGoods']=model('Goods')->getList($vipWhere,[],false,[],'sort desc',6);
       $tradeWhere=[
           'is_on_sale'=>1,
           'is_recommend'=>1,
           'distribut_type'=>'trade',
       ];
       $indexInfo['tradeGoods']=model('Goods')->getList($tradeWhere,[],false,[],'sort desc',6);
       return $indexInfo;
   }



}
