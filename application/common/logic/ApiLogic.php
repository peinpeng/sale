<?php
namespace app\common\logic;
//传过去的参数目前都为string 类型
use think\Model;
use think\Page;
use think\db;
use think\Session;
use think\Verify;

class ApiLogic extends Model
{

    /**
     * 生成密钥
     * @param  [type] $params [description]
     * @param  [type] $time   [description]
     * @return [type]         [description]
     */
    public function getKey($params=""){

        unset($params['privateSign']);
        $keyNameTail = "_PRIVATE_KEY";
        $key2NameTail = "_PRIVATE_SECRET";
        $keyName = trim(strtoupper($params['client'])).$keyNameTail;
        $keyName2 = trim(strtoupper($params['client'])).$key2NameTail;
        $key=C($keyName);
        $key2=C($keyName2);
        $params['privateKey']=$key;
        $params['privateSecret']=$key2;
        ksort($params);
        // dump($params);
        $str='';
        if(is_array($params)){
            foreach ($params as $k=>$v){
                $str.="$k=$v&";
            }
        }
        $str=trim($str,'&');
        // dump($str);

        return substr(md5($str),10,10);
    }

    /**
     * 生成密钥
     * @param  [type] $params [description]
     * @param  [type] $time   [description]
     * @return [type]         [description]
     */
    public function getKeyCloud($params=""){
        unset($params['privateSign']);
        $key='wGlNNi48HFXxRFpCDn1ZLfGFtqCktKVH';
        $key2='jhs4vEhjc3WzxrBlefaNGcal1XXv7S7Q';
        $params['privateKey']=$key;
        $params['privateSecret']=$key2;

        ksort($params);
        // dump($params);
        $str='';
        if(is_array($params)){

            foreach ($params as $k=>$v){
                $str.="$k=$v&";
            }
        }
        $str=trim($str,'&');

        return substr(md5($str),10,10);
    }

    /**
     * 生成密钥
     * @param  [type] $params [description]
     * @param  [type] $time   [description]
     * @return [type]         [description]
     */
    public function getKeyOld($params=""){
        unset($params['privateSign']);
        $key=C('PRIVATE_KEY');
        $key2=C('PRIVATE_SECRET');
        $params['privateKey']=$key;
        $params['privateSecret']=$key2;

        ksort($params);
        // dump($params);
        $str='';
        if(is_array($params)){

            foreach ($params as $k=>$v){
                $str.="$k=$v&";
            }
        }
        $str=trim($str,'&');

        return substr(md5($str),10,10);
    }

    /**
     * 检查密钥正确性
     * @param  [type] $paramKey  [前端传过来的密钥]
     * @param  [type] $resultKey [后台计算得到的密钥]
     * @return [type]     bollean       [description]
     */
    public function checkKey($paramKey,$resultKey){
        if($paramKey!=$resultKey){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 检查有效时间
     * @param  [type] $cTime [description]
     * @return [type] bollean [description]
     */
    public function checkUsefulTime($cTime){
        $stayTime=C("STAY_TIME");
        $now=time();
        if($now>$cTime+$stayTime){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 将参数变成数组
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function convertUrlQuery($query){
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1];
        }
        return $params;
    }

    /**
     * 检查接口是否合法
     * @return [type] [description]
     */
    public function checkInterfaceUseful($params,$key){
        //暂时不需要印签
        if(request()->ip()=='127.0.0.1') return array("status"=>1);

        $privateKey=$this->getKey($params);
        // return array("status"=>0,"info"=>$privateKey.'-'.$key,'data'=>'');exit;

        if(!$this->checkKey($key,$privateKey)){
            return array("status"=>0,"info"=>"密钥不正确！",'data'=>'');exit;
        }

        return array("status"=>1);
    }


    /**
     * 根据token值查询到用户的id
     */
    public function getUserByToken($token=""){
        if(!$token){
            return array('status'=>0,'msg'=>"token有误");exit;
        }
        $user_token=Db::name('user_token')->order('add_time desc')->where('token',$token)->find();
        if(time()-$user_token['add_time']>7*24*60*60){
            Db::name('user_token')->where('token',$token)->delete();
            return array('status'=>0,'msg'=>"token已经失效");exit;
        }
        return $user_token['user_id'];
    }

    /**
     * 验证码验证
     * $id 验证码标示
     */
    public function verifyHandle($id)
    {
        $verifyCode = input('verifyCode');
        $sessionId = input('sessionId');
        if($sessionId){
            session_id($sessionId);
            Session::start();
        }
        if(empty($verifyCode)){
            return false;
        }
        $verify = new Verify();
        if (!$verify->check($verifyCode, $id ? $id : 'user_reg')) {
            return false;
        }
        return true;
    }

    /**
     * 检查用户是已注册
     * @param  [type] $phone [description]
     * @return [type]        [description]
     */
    public function checkUser($phone){
        if(Db::name('users')->where(array('mobile'=>$phone))->find()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 模拟手机验证
     * @param  [type] $scene      [description]
     * @param  [type] $mobile     [description]
     * @param  [type] $code       [description]
     * @param  [type] $session_id [description]
     * @return [type]             [description]
     */
    public function sendSmsTest($scene , $mobile , $code, $session_id){
        //Db::name('sms_log')
        $scenes = C('SEND_SCENE');
        $msg = $scenes[$scene][1];
        M('sms_log')->insertGetId(array('mobile' => $mobile, 'code' => $code, 'add_time' => time(), 'session_id' => $session_id, 'status' => 0, 'scene' => $scene, 'msg' => $msg));
        return array('status'=>1);
    }
}