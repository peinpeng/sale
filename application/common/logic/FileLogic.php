<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/22
 * Time: 17:46
 */
namespace app\common\logic;

class FileLogic
{
    //文件不存在的返回关键字
    const NOT_EXISTS = 'file_not_exists';

    /**
     * User: 聂风 Function:保存远程文件
     * @param $dir 文件夹
     * @param $fileUrl 地址
     * @param string $fileName 文件名
     * @return bool
     */
    public function longRangeSave($dir, $fileUrl, $fileName = '')
    {
        $isLocal = $this->isLocalImages($fileUrl);
        if($isLocal !== false){
            return $isLocal;
        }
        $dir = $this->createDir($dir);
        if(empty($fileUrl)){
            return false;
        }
        if(empty($fileName)){
            $fileName = $this->createFileName($fileUrl);
        }
        $isExits = @fopen($fileUrl, 'r');
        if(!$isExits){
            return self::NOT_EXISTS;
        }
        $content = @file_get_contents($fileUrl);
        if(empty($content)){
            return false;
        }
        $result = file_put_contents($dir . $fileName, $content);
        if($result){
            return '/'.ltrim($dir, '/').$fileName;
        }
        return false;
    }

    /**
     * User: 聂风 Function:是否是本网站原有图片
     * @param $fileUrl
     * @return bool|string
     */
    protected function isLocalImages($fileUrl)
    {
        $url = str_replace(SITE_URL, '.', $fileUrl);
        if(file_exists($url)){
            return ltrim($url, '.');
        }else{
            return false;
        }
    }

    protected function createDir($dir)
    {
        if(!is_dir($dir)){
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($dir, 0700, true);
        }
        return rtrim($dir, '/'). '/';
    }

    protected function createFileName($fileUrl)
    {
        $fileName = basename($fileUrl);
        $type = explode('.', $fileName);
        return md5(time().mt_rand(1,999999999)).'.'.$type[(count($type)-1)];
    }
}