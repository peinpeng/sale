<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/17
 * Time: 18:45
 */
namespace app\common\logic;

use app\common\logic\SlippingUsersLogic;
use app\common\logic\UsersLogic;
use app\common\model\Users;
use app\common\logic\CompressedImageLogic;
use app\supply\logic\UserRegLogic;
use think\Verify;

class UserLogic
{
    //不用验证验证码的客户端
    protected $notVerifyCodeClient = ['PHP'];

    /**
     * User: 聂风 Function:用户注册
     * @param $postData 提交的数据
     * @param $type 0本机1第三方
     * @return array
     */
    public function userReg($postData,$type=0)
    {
        $regType = isset($postData['regType'])?$postData['regType']:'1';
        $postData['passwordConfirm'] = $postData['password'];
        // p($postData);
        /*if (!(new ApiLogic())->verifyHandle('user_reg')) {
            api_response(-1, '图像验证码错误');
        }*/
        $reg_sms_enable = tpCache('sms.regis_sms_enable');
        $reg_smtp_enable = tpCache('sms.regis_smtp_enable');
        //检测用户是否已存在
        if(model('Common/Users')->where(['mobile'=>$postData['phone']])->find()){
            api_response(2, '用户已存在');
        }
        $logic = new UsersLogic();
        $session_id = session_id();

        //是否开启注册验证码机制
        // if (check_mobile_all($postData['phone'])) {
        //     $this->smsValidate($reg_sms_enable, $postData['code'], $postData['phone'], $session_id, $postData['scene'], $postData['client']);
        // }
        // //是否开启注册邮箱验证码机制
        // if (check_email($postData['phone'])) {
        //     $this->emailValidate($reg_smtp_enable, $postData['code'], $postData['phone']);
        // }

        $invite = $inviter = $invitePhone = isset($postData['invitePhone'])?$postData['invitePhone']:'';
        //邀请人
        $invite = $this->checkInvite($invite);
        //如果是港澳台地区 不验证区id
        // if( in_array($postData['province'], getProvinceForHMT()) ) {
        //     if(!$postData['city'])  api_response(2, '请选择所在区域');
        // }else{
        //     if(!isset($postData['province']) || !isset($postData['city']) || !isset($postData['district']) || !$postData['province'] || !$postData['city'] || !$postData['district']){
        //         api_response(2, '请选择所在区域');
        //     }
        // }
        cookie('first_leader', $invite['user_id']);
        if ($is_bind_account && session("third_oauth")) { //绑定第三方账号
            $thirdUser = session("third_oauth");
            $head_pic = $thirdUser['head_pic'];
            $data = $logic->reg($postData['phone'], encrypt($postData['password']), encrypt($postData['passwordConfirm']), 0, $invite, $postData['nickname'], $head_pic, $postData['province'], $postData['city'], $postData['district']);
            //用户注册成功后, 绑定第三方账号
            $userLogic = new UsersLogic();
            $data = $userLogic->oauth_bind_new($data['result']);
        } else {
            $data = $logic->reg($postData['phone'], encrypt($postData['password']), encrypt($postData['passwordConfirm']), 0, $invite,$postData['nickname'], '',$postData['province'], $postData['city'], $postData['district']);
            //添加用户支付宝跟微信
            if ($data && ($postData['paytypeZfb'] || $postData['paytypeWx'])){
                $paytypeModel = model('common/Paytype');
                $resZfb = !empty($postData['paytypeZfb']) ? $paytypeModel->addOrSave($data['result']['user_id'], 1, ['account' => $postData['paytypeZfb'], 'type' => 1]) : '';
                $resWx  = !empty($postData['paytypeWx']) ? $paytypeModel->addOrSave($data['result']['user_id'], 2, ['account' => $postData['paytypeWx'], 'type' => 2]) : '';

            }
        }

        if ($data['status'] != 1){
            api_response($data['status'], $data['msg']);
        }

        // //注册成功后 如果注册是滑落人添加滑落人记录
        // $inviter = get_user_info($inviter, 2);//根据手机号查找邀请人
        // if ( $data['result']['first_leader'] != $inviter['user_id']){
        //     $res = (new SlippingUsersLogic())->slippingRule($data['result']['user_id'],'connecter','',$inviter['user_id'],$data['result']['first_leader']);
        //     if($res['status'] ==0){
        //         api_response(-1, '注册成功,添加滑落人记录失败');
        //     }
        // }

        //绑定临时关系
        if(!isset($postData['source']) || empty($postData['source'])){
            $postData['source'] = 'default';
        }
        model('common/place/PlaceTemporaryLocking')->temporaryBind($invitePhone, $data['result']['user_id'], $postData['source']);
        
        //解除未注册临时绑定关系
        model('common/place/PlaceTemporaryLocking')->delTemporaryBindRelation($postData['phone']);

        //获取公众号openid,并保持到session的user中
        $oauth_users = M('OauthUsers')->where(['user_id' => $data['result']['user_id'], 'oauth' => 'weixin', 'oauth_child' => 'mp'])->find();
        $oauth_users && $data['result']['open_id'] = $oauth_users['open_id'];
        //正常注册才生成session
        if($regType == '1'){
            
            session('user', $data['result']);
            setcookie('shareId', $res['result']['user_id'], null, '/');
        }else{
            //为他人注册
            cookie('first_leader', null);
        }

        //注册第三方s---------------------
        if($type == 0){
            (new UserRegLogic())->reg($postData['phone'], $inviter);
        }else{
            model('users')->where('mobile', $postData['phone'])->update(['invite_code'=>$postData['invite_code']]);
        }
        //注册第三方e---------------------

        return [
            'userId' => $data['result']['user_id'],
            'sessionId' => $session_id,
            'loginToken' => $data['result']['login_token'],
        ];
    }

    /**
     * User: 聂风 Function:验证短信验证码
     * @param $status 是否开启 1开启
     * @param $code 验证码
     * @param $phone 手机号码
     * @param $sessionId session
     * @param $scene 类型
     */
    public function smsValidate($status, $code, $phone, $sessionId, $scene, $client)
    {
        if (!in_array(strtoupper($client), $this->notVerifyCodeClient) && $status) {
            if(empty($code)){
                api_response(2, '验证码不能为空');
            }
            $logic = new UsersLogic();
            //手机功能没关闭
            $check_code = $logic->check_validate_code($code, $phone, 'phone', $sessionId, $scene);
            if ($check_code['status'] != 1) {
                api_response($check_code['status'], $check_code['msg']);
            }
        }
    }

    /**
     * User: 聂风 Function:邮箱验证码
     * @param $status
     * @param $code
     * @param $email
     */
    public function emailValidate($status, $code, $email)
    {
        if ($status) {
            $logic = new UsersLogic();
            //邮件功能未关闭
            $check_code = $logic->check_validate_code($code, $email);
            if ($check_code['status'] != 1) {
                api_response($check_code['status'], $check_code['msg']);
            }
        } else {
            if (!(new ApiLogic())->verifyHandle('user_reg')) {
                api_response(-1, '图像验证码错误');
            }
        }
    }

    /**
     * User: 聂风 Function:获取邀请人信息
     * @param $invite 邀请人号码
     */
    public function checkInvite($invite)
    {
        if (!empty($invite)) {
            $invite = get_user_info($invite, 2);//根据手机号查找邀请人

            if (empty($invite)) {
                api_response(-1, '推荐人不存在');
            }
            if ($invite['level'] <= 1) {
                //api_response(-1, '该账号暂无推荐权限');
            }
            //判断推荐人账号是否被冻结
            if ($invite['is_lock'] == 1) {
                api_response(-1, '该账号暂时被冻结无推荐权限');
            }
            // //判断推荐人是否为引荐人 即被推荐人是否为滑落用户
            // $slippingUsersLogic = new SlippingUsersLogic();
            // $res = $slippingUsersLogic->slippingRule($invite['user_id'],'referrer');

            // if($res['status'] ==1){
            //     unset($invite);
            //     $invite = get_user_info($res['data'], 0);//根据接点人ID查找接点人
            // }
        } else {
            $invite = array();
        }
        return $invite;
    }

    public function userLogin($phone, $password, $verifyCode,$loginType='2',$mobileCode='',$sessionId='')
    {
        //验证码验证
        if ($verifyCode) {
            if (!(new ApiLogic())->verifyHandle('user_login')) {
                api_response(-1, '图像验证码错误');
            }
        }
        $logic = new UsersLogic();
        if($loginType == 1){
            $res = $logic->login_mobile($phone, $mobileCode,$sessionId);
        }elseif ($loginType == 2) {
            $res = $logic->login($phone, $password);
        }
        if ($res['status'] != 1) {
            if($res['status'] == -1 && $loginType ==1) api_response(999, '请完善注册资料！');
            if($res['status'] == -1 && $loginType ==2) api_response(2, '账号不存在,请切换至手机号验证码注册完成注册');
            api_response(2, $res['msg'],$res['result']);
        }
        session('user', $res['result']);
        return [
            'userId'=>$res['result']['user_id'],
            'sessionId'=>session_id(),
            'shareId'=>base64_encode($res['result']['user_id']),
            'loginToken'=>$res['result']['login_token'],
        ];
    }

    //修改登录密码
    public function changePassword($userId, $phone, $code, $password, $sessionId, $client)
    {
        $enable = tpCache('sms.password_sms_enable');
        $result = $this->passwordVerify($userId, $phone, $code, $password, $password, $sessionId, $client, 2, $enable);
        if($result !== true){
            return $result;
        }
        Users::where('user_id', $userId)->save(['password'=>encrypt($password)]);
        return api_ajaxReturn(10000, '修改密码成功');
    }

    //修改支付密码
    public function changePayPassword($userId, $phone, $code, $password, $passwordConfirm, $sessionId, $client)
    {
        $enable = tpCache('sms.paypwd_sms_enable');
        $result = $this->passwordVerify($userId, $phone, $code, $password, $passwordConfirm, $sessionId, $client, 3, $enable);
        if($result !== true){
            return $result;
        }
        Users::where('user_id', $userId)->save(['paypwd'=>encrypt($password)]);
        (new UserHashLogic())->refreshOne($userId);
        return api_ajaxReturn(10000, '修改支付密码成功');
    }
    //总商城后台修改支付密码
    public function changePayPasswordForMall($userId, $phone, $password, $passwordConfirm)
    {
        $enable = tpCache('sms.paypwd_sms_enable');
        $result = $this->passwordVerifyForMall($userId, $phone, $password, $passwordConfirm);
        if($result !== true){
            return $result;
        }
        Users::where('user_id', $userId)->save(['paypwd'=>encrypt($password)]);
        (new UserHashLogic())->refreshOne($userId);
        return api_ajaxReturn(10000, '修改支付密码成功');
    }

    //修改支付密码
    public function forgetPwd($phone, $code, $password, $passwordConfirm, $sessionId, $client)
    {
        $enable = tpCache('sms.forget_sms_enable');
        if(empty($phone)){
            return api_ajaxReturn(2, '手机号码不能为空');
        }
        $userId = Users::where('mobile', $phone)->value('user_id');
        if(empty($userId)){
            return api_ajaxReturn(2, '用户不存在');
        }
        $result = $this->passwordVerify($userId, $phone, $code, $password, $passwordConfirm, $sessionId, $client, 3, $enable);
        if($result !== true){
            return $result;
        }
        Users::where('user_id', $userId)->save(['password'=>encrypt($password)]);
        return api_ajaxReturn(10000, '修改密码成功');
    }

    //总商城后台修改支付密码的验证
    protected function passwordVerifyForMall($userId, $phone, $password, $passwordConfirm)
    {
        if(!check_mobile($phone)){
            return api_ajaxReturn(2, '手机号码不正确');
        }
        if(empty($password)){
            return api_ajaxReturn(2, '密码不能为空');
        }
        if($password != $passwordConfirm)
        {
            return api_ajaxReturn(2, '两次密码不一致');
        }
        return true;
    }

    //修改登录密码，支付密码的公共验证
    protected function passwordVerify($userId, $phone, $code, $password, $passwordConfirm, $sessionId, $client, $scene, $enable)
    {
        if(!check_mobile($phone)){
            return api_ajaxReturn(2, '手机号码不正确');
        }
        if(empty($password)){
            return api_ajaxReturn(2, '密码不能为空');
        }
        if($password != $passwordConfirm)
        {
            return api_ajaxReturn(2, '两次密码不一致');
        }
        //验证短信验证码
        $this->smsValidate($enable, $code, $phone, $sessionId, $scene, $client);
        return true;
    }

    //修改绑定手机号，修改备用手机号
    public function changeMobile($userId, $phone, $code, $password, $sessionId, $client, $type)
    {
        switch ($type) {
            case 1:
                //修改绑定手机号
                $scene = 4;
                $enable = tpCache('sms.mobile_sms_enable');
                break;
            case 2:
                //修改备用手机号
                $scene = 5;
                $enable = tpCache('sms.mobilebak_sms_enable');
                break;
        }
        if(!check_mobile($phone)){
            return api_ajaxReturn(2, '手机号码不正确');
        }
        $this->smsValidate($enable, $code, $phone, $sessionId, $scene, $client);

        $userPassword = Users::where('user_id', $userId)->value('password');
        if(encrypt($password) != $userPassword){
            return api_ajaxReturn(2, '密码不正确');
        }
        $userField = 'mobile_bak';
        if($type == 1){
            $isHas = Users::where(['mobile'=>$phone, 'user_id'=>['<>', $userId]])->count();
            if($isHas){
                return api_ajaxReturn(2, '手机已被使用');
            }
            $userField = 'mobile';
        }
        $res = (new Users())->where(['user_id'=>$userId])->save([$userField=>$phone]);
        (new UserHashLogic())->refreshOne($userId);
        return api_ajaxReturn(10000, '绑定成功');
    }

    /**
     * User: 聂风 Function:获取用户信息
     * @param $userId 用户id
     */
    public function userInfo($userId)
    {
        $userData = Users::where('user_id', $userId)->find();
        $userLogic = model('Common/UsersLogic', 'logic');
        $first_leader = $userLogic->get_info($userData['first_leader']);

        $level_name = M('user_level')->where("level_id", $userData['level'])->cache(true)->getField('level_name'); // 等级名称
        $usersModel = model('common/Users');
        $ztjnum = $usersModel->getFisrtLeaderNumAttr($userData['user_id']);//直推人数
        $teamnum = model('common/distribut/DistributTeam')->getUserTeamsAllCount($userData['user_id']);//团队人数

        //实名认证
        $cardModel = model('common/credit/CreditIdentityCard');
        $cardData = $cardModel->where('user_id', $userId)->find();

        //支付宝微信
        $paytype = model('common/Paytype');
        $paytypeData = $paytype->where('user_id', $userId)->column('type,account');

        //其他用户信息
        $userContribution = (string)model('Common/Users')->getFisrtLeaderNumAttr($userId,0);
        $teamContribution = (string)model('Common/distribut/DistributTeam')->teamNum($userId);
        $compensateNum = (string)model('Common/place/PlaceWaitingMatching')->getBy('user_id',$userId,'compensate_num');

        $temporaryLockingNum = (string)M('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$userId])->count();

        // $temporaryLockingNum = M('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$userId])->count();

        // $temporaryLockingNum = (string)M('PlaceTemporaryLocking')->where(['temporary_firstleader_id'=>$userId])->count();


        $slippingUser = model('Common/slippingUsers')->where(['slipping_id'=>$userId])->find();
        if($slippingUser){
            $connecter    = model('Common/Users')->getBy('user_id',$slippingUser['connecter_id']);

            $slippingUser['connecter_mobile'] = $connecter && !empty($connecter['nickname'])?$connecter['nickname']:$connecter['mobile'];
        }
        //接点人
        $connecterMobile = $slippingUser['connecter_mobile']?$slippingUser['connecter_mobile']:'';


        //格式化
        $data = [
            'userId' => $userId,
            'mobile' => $userData['mobile'],
            'mobileBak' => $userData['mobile_bak']?$userData['mobile_bak']:'',
            'email' => $userData['email'],
            'sex' => $userData['sex'],
            'birthday' => $userData['birthday']?date('Y-m-d', $userData['birthday']):'',
            'userMoney' => $userData['user_money'],
            'frozenMoney' => $userData['frozen_money'],
            'headPic'=> image_handle($userData['head_pic']),
            'level' => $userData['level'],
            'levelName' => $level_name,
            'ztjNum' => $ztjnum?$ztjnum:'0',
            'teamNum' => $teamnum?:'0',
            'nickname' => $userData['nickname'],
            'firstLeaderMobile' => $first_leader['result']?$first_leader['result']['mobile']:'',
            'realStatus'=>isset($cardModel['process'])?$cardModel->switchStatus($cardModel['process']):1,
            'trueName' => isset($cardData['true_name'])?$cardData['true_name']:'',
            'cardNumber'=>isset($cardData['card_number'])?$cardData['card_number']:'',
            'cardNumberStar'=>isset($cardData['card_number'])?$cardModel->showIdCard($cardData['card_number']):'',
            'cardFrontPhoto'=>isset($cardData['card_front_photo'])?image_handle($cardData['card_front_photo']):'',
            'cardBackPhoto'=>isset($cardData['card_back_photo'])?image_handle($cardData['card_back_photo']):'',
            'zfbAccount' => isset($paytypeData[1])?$paytypeData[1]:'',
            'wxAccount' => isset($paytypeData[2])?$paytypeData[2]:'',
            'userContribution'=>$userContribution,
            'teamContribution'=>$teamContribution,
            'compensateNum'=>$compensateNum?:'0',
            'connecterMobile'=>$connecterMobile,
            'temporaryLockingNum'=>$temporaryLockingNum?:'0',
        ];
        return $data;
    }

    /**
     * User: 聂风 Function:实名认证
     * @param $userId 用户id
     * @param $idCardData post提交的信息
     * @return string
     */
    public function realName($userId, $idCardData)
    {
        //文件上传
        if($_FILES) {

            $frontImg = $this->newUploadImage('cardFrontPhoto', 'identityCard');
            $backImg = $this->newUploadImage('cardBackPhoto', 'identityCard');
            if ($frontImg['code'] != 1 || $backImg['code'] != 1) {
                $msg = $frontImg['msg'] ?: $backImg['msg'];
                return api_ajaxReturn(2, $msg);
            }

            $idCardData['card_front_photo'] = $frontImg['data'];
            $idCardData['card_back_photo'] = $backImg['data'];

            //图片压缩 并上传阿里云
            $ossClient = new \app\common\logic\OssLogic;
            $uploadImg = $_SERVER['DOCUMENT_ROOT'];
            /*$CompressedImageMdl = new CompressedImageLogic();
            $CompressedImageMdl->compressedImage($uploadImg.$idCardData['card_front_photo'],$uploadImg.$idCardData['card_front_photo'],800);//压缩图片
            $CompressedImageMdl->compressedImage($uploadImg.$idCardData['card_back_photo'],$uploadImg.$idCardData['card_back_photo'],800);//压缩图片*/

            $fileFront = $_FILES['cardFrontPhoto'];
            $fileBack = $_FILES['cardBackPhoto'];

            list($image, $type) = explode('/', $fileFront['type']);
            $objectFront = uniqid('sale_front_') . time() . '.' . $type;
            $objectBack = uniqid('sale_back_') . time() . '.' . $type;

            $objectFront = 'identityCard/' . $objectFront;
            $objectBack = 'identityCard/' . $objectBack;

            $card_front_photo = $ossClient->uploadFile($uploadImg . $idCardData['card_front_photo'], $objectFront);
            $card_back_photo = $ossClient->uploadFile($uploadImg . $idCardData['card_back_photo'], $objectBack);

            $idCardData['card_front_photo'] = $card_front_photo['info']['url'];//获取图片路径
            $idCardData['card_back_photo'] = $card_back_photo['info']['url'];//获取图片路径

            unlink($uploadImg . $frontImg['data']);//删除文件
            unlink($uploadImg . $backImg['data']);//删除文件
        }else{

            //图片链接获取文件
            $fileDir = UPLOAD_PATH . 'identityCard/' .date('Ymd') . '/';
            if(!isset($idCardData['cardFrontPhoto']) || empty($idCardData['cardFrontPhoto'])){
                return api_ajaxReturn(2, '请上传身份证正面照片');
            }
            if(!isset($idCardData['cardBackPhoto']) || empty($idCardData['cardBackPhoto'])){
                return api_ajaxReturn(2, '请上传身份证反面照片');
            }
            $fileLogic = new FileLogic();
            $idCardData['card_front_photo'] = $fileLogic->longRangeSave($fileDir, $idCardData['cardFrontPhoto']);
            if(!$idCardData['card_front_photo'] || $idCardData['card_front_photo'] == FileLogic::NOT_EXISTS){
                return api_ajaxReturn(2, '上传身份证正面照片出错');
            }
            $idCardData['card_back_photo'] = $fileLogic->longRangeSave($fileDir, $idCardData['cardBackPhoto']);
            if(!$idCardData['card_back_photo'] || $idCardData['card_back_photo'] == FileLogic::NOT_EXISTS){
                return api_ajaxReturn(2, '上传身份证反面照片出错');
            }
        }

        $cardMdl  = model('Common/credit/CreditIdentityCard');
        $is_idNum = $cardMdl->checkIdCardNum($idCardData['cardNumber']);
        if ($is_idNum) {
            $result = [
                // 'code' => $code,
                'msg'  => '验证失败,该身份证号已被使用,请使用本人身份证验证',
                // 'data' => $data,
                // 'url'  => $url,
                // 'wait' => $wait,
            ];
            flashMessage($result);
            return api_ajaxReturn(2, $result['msg']);
        }
        //地区归属
        $id_area_type = $cardMdl->regionalValidation($idCardData['cardNumber']);
                
        if($id_area_type['code']==-1)  return api_ajaxReturn(2, '身份证号有误');
        
        $addData = [
            'user_id' => $userId,
            'true_name' => $idCardData['trueName'],
            'card_number'=>$idCardData['cardNumber'],
            'card_front_photo'=>$idCardData['card_front_photo'],
            'card_back_photo'=>$idCardData['card_back_photo'],
            'area_type'=>$id_area_type['area_type'],
        ];
        $res = $cardMdl->addData($addData);
        if ($res == false) {
            $result = [
                // 'code' => $code,
                'msg'  => $cardMdl->getError(),
                // 'data' => $data,
                // 'url'  => $url,
                // 'wait' => $wait,
            ];
            flashMessage($result);
            return api_ajaxReturn(2, $result['msg']);
        }
        return api_ajaxReturn(10000, '上传成功');
    }

    public function newUploadImage($name,$path="images"){
        if ($_FILES[$name]['tmp_name']) {
            $file = request()->file($name);
            $image_upload_limit_size = config('image_upload_limit_size');
            $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
            $dir = UPLOAD_PATH . $path.'/';
            // p($dir);
            if (!($_exists = file_exists(UPLOAD_PATH))) {
                $isMk = mkdir(UPLOAD_PATH);
            }
            if (!($_exists = file_exists($dir))) {
                $isMk = mkdir($dir);
            }
            $parentDir = date('Ymd');
            $info = $file->validate($validate)->move($dir, true);
            if ($info) {
                $url = '/' . $dir . $parentDir . '/' . $info->getFilename();
                return ['code'=>1,'data'=>$url];
                // return $url;
            } else {
                return ['code'=>0,'msg'=>$file->getError()];
                // $this->error($file->getError());//上传错误提示错误信息
            }
        }
        return ['code'=>0,'msg'=>'请上传图片'];
    }

    /**
     * User: 聂风 Function:修改资料
     * @param $userId 用户id
     * @param $post 提交的信息
     * @param $file 头像
     * @return string
     */
    public function editUser($userId, $post, $file)
    {
        $dir = UPLOAD_PATH . 'head_pic/';
        $fileDir = $dir.date('Ymd') . '/';

        if($file['tmp_name']){
            $file = request()->file('headPic');
            $image_upload_limit_size = config('image_upload_limit_size');
            $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
            if (!($_exists = file_exists($dir))) {
                // p(111);
                $isMk = mkdir($dir,0775,true);
            }
            $info = $file->validate($validate)->move($dir, true);

            if ($info) {
                $post['head_pic'] = '/' . $fileDir . $info->getFilename();
            } else {
                return api_ajaxReturn(3, $file->getError());
            }
        }elseif(!empty($post['head_pic'])){
            $post['head_pic'] = (new FileLogic())->longRangeSave($fileDir, $post['head_pic']);
            if($post['head_pic'] === false){
                return api_ajaxReturn(3, '上传文件失败');
            }
            if($post['head_pic'] == FileLogic::NOT_EXISTS){
                unset($post['head_pic']);
            }
        }

        if (!empty($post['email'])) {
            $c = M('users')->where(['email' => $post['email'], 'user_id' => ['<>', $userId]])->count();
            if($c){
                return api_ajaxReturn(2, '邮箱已被使用');
            }
        }
        $userLogic = new UsersLogic();
        if (!$userLogic->update_info($userId, $post)){
            return api_ajaxReturn(2, '保存失败');
        }
        return api_ajaxReturn(10000, '保存成功');
    }

    //分享二维码
    public function shareCode($userId)
    {
        $bg = model("Common/Qrcode")->where(['enabled'=>1,'title'=>['notin',['活动页','分享活动页']]])->order('sort DESC')->select();
        $userInfo = model('Common/Users')->where(['user_id'=>$userId])->find();

        $backgroundList = [];
        foreach ($bg as $k => $v) {
            $typeArr = explode(',', $v['tk_type']);
            $backgroundList[] = [
                'name' => $v['title'],
                'images' => image_handle($v['pic']),
                'contact' => $v['contact'],
                'mobile' => in_array('1', $typeArr)?$userInfo['mobile']:'',
                'wxAccount' => in_array('2', $typeArr)?$userInfo['wx_account']:'',
                'inviteCode' => $userInfo['mobile'],
                // 'qrCodeUrl'=>U('Mobile/user/share',['shareId'=>base64_encode($userId),'id'=>$v['id']],true,true),
                'qrCodeUrl'=>U('Mobile/user/activityPage',['tk_id'=>base64_encode($userId),'id'=>$v['id']],true,true),
            ];
        }
        return $backgroundList;
    }

}