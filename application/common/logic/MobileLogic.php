<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\logic;
use think\Model;
class MobileLogic extends Model {
    
   /**
    * 
    *公共控制器对应的逻辑层   
    */
   public function publicAssign(){
       $switch = \think\Lang::detect();
       $publicInfo['buttonNav']=model('common/nav/Navigation')->getNavigationByPosition(2);
       $publicInfo['IndexNav']=model('common/nav/Navigation')->getNavigationByPosition(11);    
       $publicInfo['lang']= $switch;
       //dump($publicInfo);exit;
       return $publicInfo;
   }

   //app底部接口
   public function navFormat($list)
   {
       $data = [];
       foreach ($list as $k => $v) {
           $data[] = [
               'name' => $v['name'],
               'url' => $v['url'],
               'image' => image_handle($v['image']),
               'imageUnselect' => image_handle($v['image_unselect']),
               'nameEnglish' => $v['name_english'],
           ];
       }
       return $data;
   }

 
  //获取首页背景图
  public function getBackground($scene = 0)
  {
      $backgroundImg = '';
      switch ($scene) {
          //获取登录页背景图
          case '1':
           $backgroundImg = tpCache('shop_info.store_login_background');
              break;
          //获取首页背景图
          case '2':
           $backgroundImg = tpCache('shop_info.store_index_background');
              break;
          default:
              break;
      }
      $data['backgroundImg'] = !empty($backgroundImg)?SITE_URL.$backgroundImg:tpCache('shop_info.store_background');
      $data['bgColor'] = '';
      return $data;
  }

}
