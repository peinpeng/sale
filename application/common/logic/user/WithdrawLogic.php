<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: lhb
 * Date: 2017-05-15
 */

namespace app\common\logic\user;

use app\common\model\Coupon;
use think\Model;
use think\Db;

/**
 * 活动逻辑类
 */
class WithdrawLogic extends Model
{
    public function addWithdrawAccount($data=[]){
       if(!$data['bank_card']){
           $this->error='账号不能为空';
           return false;
       }
       if($data['type']==3 ){
           if(!$data['bank_name']){
               $this->error='请填写银行信息';
               return false;
           }
           if(!$data['sub_branch']){
               $this->error='请填写支行信息';
               return false;
           }
       }
       //是否开启注册验证码机制
       if(check_mobile($data['mobile'])){
           if(tpCache('sms.bind_mobile_sms_enable')){
               //手机功能没关闭
               $check_code = model('UsersLogic','logic')->check_validate_code($data['sms_code'], $data['mobile'], 'phone', session_id(), 6);
               if($check_code['status'] != 1){
                   $this->error=$check_code['msg'];
                   return false;
               }
           }
       }
       switch ($data['type']){
           case 1:$data['bank_name']='支付宝';break;
           case 2:$data['bank_name']='微信';break;
       }
       $res= model('common/withdrawals/WithdrawalAccount')->addData($data);
       if(!$res){
           return false;
       }
       return true;
       
    }
    public function withdraw($user_id=0,$data=[]){
        $data['user_id'] = $user_id;
        $user = model('users')->get($user_id);
        $cash = tpCache('cash');
        if(encrypt($data['paypwd']) != $user['paypwd']){
            $this->error='支付密码错误';
            return false;
        }
        if ($data['money'] > $user['wallet_money']) {
            $this->error='本次提现余额不足';
            return false;
        }
        if ($data['money'] <= 0) {
            $this->error='提现额度必须大于0';
            return false;
        }
        
        if ($cash['cash_open'] == 1) {
            $taxfee =  round($data['money'] * $cash['service_ratio'] / 100, 2);
            // 限手续费
            if ($cash['max_service_money'] > 0 && $taxfee > $cash['max_service_money']) {
                $taxfee = $cash['max_service_money'];
            }
            if ($cash['min_service_money'] > 0 && $taxfee < $cash['min_service_money']) {
                $taxfee = $cash['min_service_money'];
            }
            if ($taxfee >= $data['money']) {
                $this->error='手续费超过提现额度了！';
                return false;
                $this->ajaxReturn(['status'=>0, 'msg'=>'']);
            }
            $data['taxfee'] = $taxfee;
        
            // 每次限最多提现额度
            if ($cash['min_cash'] > 0 && $data['money'] < $cash['min_cash']) {
                $this->error='每次最少提现额度' . $cash['min_cash'];
                return false;
            }
            if ($cash['max_cash'] > 0 && $data['money'] > $cash['max_cash']) {
                $this->error='每次最多提现额度' . $cash['max_cash'];
                return false;
            }
        
            // 今天限总额度
            if ($cash['count_cash'] > 0) {
                $status = ['in','0,1,2,3'];
                $create_time = ['gt',strtotime(date("Y-m-d"))];
                $total_money2 = Db::name('withdrawals')->where(array('user_id' =>$user_id, 'status' => $status, 'create_time' => $create_time))->sum('money');
                if (($total_money2 + $data['money'] > $cash['count_cash'])) {
                    $total_money = $cash['count_cash'] - $total_money2;
                    if ($total_money <= 0) {
                        $this->error="你今天累计提现额为{$total_money2},金额已超过可提现金额.";
                        return false;
                    } else {
                        $this->error="你今天累计提现额为{$total_money2}，最多可提现{$total_money}账户余额.";
                        return false;
                    }
                }
            }
            // 今天限申请次数
            if ($cash['cash_times'] > 0) {
                $total_times = Db::name('withdrawals')->where(array('user_id' => $user_id, 'status' => $status, 'create_time' => $create_time))->count();
                if ($total_times >= $cash['cash_times']) {
                    $this->error="今天申请提现的次数已用完.";
                    return false;
                }
            }
        }else{
            $data['taxfee'] = 0;
        }
        trans();
        $withdrawals=model('common/withdrawals/withdrawals')->addData($data);
        if($withdrawals===false){
            $this->error='提交失败,联系客服!';
            trans('rollback');
            return false;
        }
        
        $walletRes=model('common/users/UserWallet')->opreateUserWallet($user_id,-$data['money']);
        if($walletRes===false){
            $this->error='增加日志失败';
            trans('rollback');
            return false;
        }
        
        $res=model('common/users/UserMoneyLog')->addMoneyLog($user_id,-$data['money'], $user['wallet_money']-$data['money'], 2, 'withdrawals', $withdrawals,'用户申请提现');
        if(res) {
            trans('commit');
            return true;
        } else {
            $this->error='提交失败,联系客服!';
            trans('rollback');
            return false;
        }
    }
    public function checkWithdraw($id,$status,$remark,$admin_id,$file,$isRefuse=2){
        
        $withdrawalsMl=model('common/withdrawals/Withdrawals');
        if(in_array($status, [2,3]) && !$file){
            $this->error='请上传凭证';
            return false;
        }
        $withdrawals = $withdrawalsMl->get($id);
       
        if (!$withdrawals) {
             $this->error='提现记录不存在';
            return false;
        }
        if(!$status){
            $this->error='请选择审核状态';
            return false;
        }
        
        trans();
        $res=$withdrawalsMl->updateData($id,$status,$file);
        if($res===false){
            $this->error='更改状态失败';
            trans('rollback');
            return false;
        }
      
        //操作记录
        $res=model('common/withdrawals/WithdrawalsRecord')->addData($id,$status,$admin_id,$remark,$file,$isRefuse);
        if($res===false){
            $this->error='更改状态失败';
            trans('rollback');
            return false;
        }
  
        //-1审核失败0申请中1审核通过2付款成功3付款失败'
        if($status==-1 || $status==3){
            if($isRefuse==1){
               $res=model('common/users/UserWallet')->opreateUserWallet($withdrawals['user_id'],$withdrawals['money']);
               if($res===false){
                   $this->error='更改用户余额失败';
                   trans('rollback');
                   return false;
               }
               
               $userMoney = model('common/users/UserWallet')->getWallByUser($withdrawals['user_id']);
               $res=model('common/users/UserMoneyLog')->addMoneyLog($withdrawals['user_id'], $withdrawals['money'], $userMoney+$withdrawals['money'], 4, 'withdrawals', $withdrawals['id'],'提现失败返回');
               if($res===false){
                   $this->error='增加日志失败';
                   trans('rollback');
                   return false;
               }
            }
        }
        trans('commit');
        return true;
    
    }
  
    
}