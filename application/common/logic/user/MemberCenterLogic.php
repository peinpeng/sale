<?php
/**
 * 
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: wq
 * Date: 2018-12-22
 */

namespace app\common\logic\user;

use app\common\model\Coupon;
use think\Model;
use think\Db;

/**
 * 会员中心类
 */
class MemberCenterLogic extends Model
{
    public function myTeam($where,$orderBy,$p,$user_id, $pageSize=15){
        $UserMdl = model('common/Users');
        $slippingModel     = model('common/SlippingUsers');

        //获取user表的下级(可能有滑落人)
        $ids = $UserMdl->where(['first_leader'=>$user_id])->field('user_id')->select();
        $ids = $UserMdl->toArrayOne($ids,'user_id');


        //查询下级是否有滑落人
        $slippingIds = $slippingModel->where(['slipping_id'=>['in',$ids],'is_slipping'=>1])->field('slipping_id')->select();
        $slippingIds = $slippingIds ? $UserMdl->toArrayOne($slippingIds,'slipping_id') : [];

        //获取直推队员人数 (排除滑落人)
        
        $connectIds = $slippingIds? array_diff($ids,$slippingIds):$ids;

        //用户推荐的滑落人id
        $slippingByUser = $slippingModel->where(['referrer_id'=>$user_id,'is_slipping'=>1])->field('slipping_id')->select();
        $slippingByUser = $slippingByUser ? $UserMdl->toArrayOne($slippingByUser,'slipping_id') : [];
        // p($slippingByUser);

        //用户总的直推下级ids
        $totalIds = $slippingByUser?array_merge($connectIds,$slippingByUser):$connectIds;
        $con['user_id'] = ['in',$totalIds];


        $team = $UserMdl->where($where)->where($con)->order($orderBy)->page($p,$pageSize)->select();
        foreach ($team as $k => $v) {
          if(in_array($v['user_id'], $slippingByUser)){
            $team[$k]['is_slipping'] = 1;
            $connecter_id = $slippingModel->where(['slipping_id'=>$v['user_id']])->value('connecter_id');
            $team[$k]['connecter_mobile'] = $UserMdl->where(['user_id'=>$connecter_id])->value('mobile');
          }else{
            $team[$k]['is_slipping'] = 0;
            $team[$k]['connecter_mobile'] = 0;

          }
          $team[$k]['is_connect'] = 0;
        }
        // p(collection($team)->toArray());
        return $team;
    }
    public function myTeamForFirstLeader($where,$orderBy,$p,$user_id,$pageSize=15){
        $UserMdl = model('common/Users');
        $slippingModel     = model('common/SlippingUsers');
        //获取user表的下级(可能有滑落人)
        $ids = $UserMdl->where(['first_leader'=>$user_id])->field('user_id')->select();
        $ids = $UserMdl->toArrayOne($ids,'user_id');
        //查询下级是否有滑落人
        $slippingIds = $slippingModel->where(['slipping_id'=>['in',$ids],'is_slipping'=>1])->field('slipping_id')->select();
        $slippingIds = $slippingIds ? $UserMdl->toArrayOne($slippingIds,'slipping_id') : [];

        
        $con = ['first_leader'=>$user_id];


        $team = $UserMdl->where($where)->where($con)->order($orderBy)->page($p,$pageSize)->select();
        foreach ($team as $k => $v) {
          if(in_array($v['user_id'], $slippingIds)){
            $team[$k]['is_connect'] = 1;
            $referrer_id = $slippingModel->where(['slipping_id'=>$v['user_id']])->value('referrer_id');
            $team[$k]['referrer_mobile'] = $UserMdl->where(['user_id'=>$referrer_id])->value('mobile');
          }else{
            $team[$k]['is_connect'] = 0;
            $team[$k]['referrer_mobile'] = 0;
          }
            $team[$k]['is_slipping'] = 0;
        }
        return $team;
    }
    /**
     * 我的收益
     * @param number $user_id  
     * 
     */
    public function getUserProfit($user_id=0){
       if(!$user_id){
           $this->error='用户不存在';
           return false;
       }
       $logMl=model('common/distribut/DistributMoneyIncomeLog');
       $user['money']=model('common/users/UserWallet')->getBy('user_id',$user_id,'money');
       
       //总收益
       $user['total']  = $logMl->getUserProfit($user_id); //总收益
       
       $begin_month    = mktime(0,0,0,date('m'),1,date('Y'));
       $end_month      = mktime(23,59,59,date('m'),date('t'),date('Y'));
       $user['month_bill']  = $logMl->getUserProfit($user_id,['create_time'=>['between',[$begin_month,$end_month]]]); 
       
       $begin_month    = mktime(0,0,0,date('m'),date('d'),date('Y'));
       $end_month      = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
       $user['today_bill']  = $logMl->getUserProfit($user_id,['create_time'=>['between',[$begin_month,$end_month]]]); 
       return $user;
   }
   /**
    * 钱包流水
    * @param number $user_id
    * @param string $type
    * @param number $startTime
    * @param number $endTime
    * @param number $p  
    * 
    */
   public function myBill($user_id=0,$type='',$startTime=0,$endTime=0,$p=1){
       $where=[];
       $user_id && $where['user_id'] = $user_id;
       $type && $where['type'] = ['in',$type];
       $startTime && $where['create_time'] = ['between',[$startTime,$endTime]];
      
       $page['page'] = $p;
       $page['pageSize'] = 15;
       return model('common/users/UserMoneyLog')->getList($where,$page,1);
       //dump($where);exit;
   }
   
    
}