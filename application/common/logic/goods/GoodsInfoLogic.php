<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 */

namespace app\common\logic\goods;

use app\common\model\Goods;
use think\Model;
use think\Db;
/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package common\Logic
 */
class GoodsInfoLogic extends Model
{
    /**
     * 获取商品推广有奖
     * @param type $goods_id
     */
    public function goodsCommission($goods=[],$user_id=0){
        if(!$user_id){
            return 0;
        }else{
            if(!$user_id){
                return 0;
            }
            $level=model('Users')->getBy('user_id',$user_id,'distribut_level');
            $where=['level_id'=>$level,'distribut_type'=>$goods['distribut_type']];
            $persent=model('common/distribut/DistributLevelConfig')->getCol('direct_prize',$where);
            return $goods['shop_price']*$persent[0];
        }
    }
    /**
     * 获取指定商品用户商品价格
     * @param type $goods_id
     */
    public function goodsVipPrice($goods=[],$user_id=0,$spec=[]){
     
        if(!$user_id){
            return $goods['shop_price'];
        }
        if(!$this->haveVipPrice($goods)){
            return $goods['shop_price'];
        }
        $user=model('users')->get($user_id);
        if(empty($spec)){
            return $user['level']==2 ?$goods['vip_price']:$goods['shop_price'];
        }
        //dump($spec);exit;
        return $user['level']==2 ?$spec['vip_price']:$spec['price'];
    }
    /**
     * 获取指定商品用户规格价格
     * @param unknown $goods
     * @param number $user_id  */
    public function specVipPrice($goods=[],$user_id=0){
        $specGoodsPrice  = M('SpecGoodsPrice')->where(['goods_id'=> $goods['goods_id']])->getField("key,price,vip_price,store_count,item_id"); // 规格 对应 价格 库存

        if(!$user_id  )        return $specGoodsPrice;
        if(!$this->haveVipPrice($goods))return $specGoodsPrice;
        $user=model('users')->get($user_id);
        if($user['level']!=2) return $specGoodsPrice;
//         dump(array_map(function($value){$value['vip_price']>0 && $value['price']=$value['vip_price'];return $value;}, $specGoodsPrice));exit;
        return array_map(function($value){
            $value['vip_price']>0 && $value['price']=$value['vip_price'];
            return $value;
        }, $specGoodsPrice);
    }
    public function haveVipPrice($goods=[]){
        return in_array($goods['distribut_type'],model('goods')->haveVipPrivce);
    }
    /**
     * 通过商品区域获取商品列表
     * @param unknown $distribut_type
     * @param unknown $p
     * @param unknown $where  
     */
    public function getGoodsInfoByDisType($distribut_type,$user_id=0,$p,$where=[]){
        $where['distribut_type']=$distribut_type;
        $where['is_on_sale']=1;
        $page['page']=$p;
        $page['pageSize']=15;
        $goodsInfo=model('goods')->getList($where,$page,false,[],'sort desc');
        
        foreach ($goodsInfo as $k=>$v){
            $goodsInfo[$k]['promotion']=$this->goodsCommission($v,$user_id);
            $goodsInfo[$k]['price']=$this->goodsVipPrice($v,$user_id);
        }
        //$goods=$goodsInfo->toArray();
        unset($where['cat_id']);
        $catIds=model('goods')->getCol('cat_id',$where);
        $topCats=model('GoodsCategory')->getTopCategoryHaveGoods(['id'=>['in',$catIds]]);
        return compact('topCats','goodsInfo');
        //dump($topCats);exit;
    }
}