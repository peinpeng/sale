<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 采用最新Thinkphp5助手函数特性实现单字母函数M D U等简写方式
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\logic;

use think\Loader;
use think\Model;
use think\Page;
use think\Db;

/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class SlippingUsersLogic extends BaseLogic
{

    private $_desc=[
        1 => '操作pc端后台修改',
        2 => '操作手机端后台修改',

    ];
    /**
     * 变更上下级 
     * $firstLeaderUid 更改后的上级ID
     * $changeUid  被更改上级的用户ID
     * $firstLeaderUidBefore 原上级用户ID 
     */
    public function changeFirstLeader($firstLeaderUid,$firstLeaderUidBefore,$changeUid,$desc='')
    {
        $countModel         = model('common/SlippingCount');
        $usersModel         = model('common/Users');
        $slippingUsersModel = model('common/SlippingUsers');
        $slippingConfigMdl  = model('common/SlippingConfig');
        $slippingConfig     = $slippingConfigMdl->where(['enable'=>1])->find(); //滑落规则配置
        $userInfo           = $usersModel->where(['user_id'=>$changeUid])->find();    //用户资料


        //判断用户是否为滑落人
       $slippingUsersInfo =  $slippingUsersModel->where(['slipping_id'=>$changeUid,'is_slipping'=>1])->find();

       if($slippingUsersInfo){
            $time = time();
            //更改用户为非滑落人
            $res = $slippingUsersModel->where(['slipping_id'=>$changeUid])->update(['is_slipping'=>0,'update_time'=>$time,'desc'=>$this->_desc[$desc]]);

            if($res === false)   return array('status'=>0 , 'msg'=>'更改用户为非滑落人失败!');

            //滑落用户等级>=2 引荐人的直推人数修改
            if($userInfo['level'] >= $slippingConfig['team_member_level']){

              //引荐人直推人数减一 
              $referrerIsConnecter = $countModel->where(['connecter_id'=>$slippingUsersInfo['referrer_id']])->find();
              if($referrerIsConnecter){
                  //滑落人的引荐人如果是接点人则直推人数减一
                  $direct_num = $referrerIsConnecter['direct_num']-1;
                  $result = $referrerIsConnecter['enable'] == 0 ? $countModel->where(['connecter_id'=>$slippingUsersInfo['referrer_id']])->update(['direct_num'=>$direct_num,'update_time'=>time()]) : '';

                  if($result === false)   return array('status'=>0 , 'msg'=>'ID为:'.$slippingUsersInfo['referrer_id'].'直推人数修改失败!');

                  //是否还可以接点
                  if($direct_num >= $slippingConfig['team']){
                      $configDirectNum = $direct_num >= $slippingConfig['direct_num'] ? $slippingConfig['high_num'] : $slippingConfig['low_num'];
                      $surplus_num = $configDirectNum > $referrerIsConnecter['slipping_num'] ? $configDirectNum - $referrerIsConnecter['slipping_num'] : 0;
                      //修改引荐人的剩余接点人数
                      $res = $surplus_num > 0 ?  $countModel->where(['connecter_id'=>$slippingUsersInfo['referrer_id']])->update(['direct_num'=>$direct_num,'update_time'=>time(),'surplus_num'=>$surplus_num]) : $countModel->where(['connecter_id'=>$slippingUsersInfo['referrer_id']])->update(['direct_num'=>$direct_num,'update_time'=>time(),'surplus_num'=>$surplus_num,'enable'=>0]);

                      if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$slippingUsersInfo['referrer_id'].'剩余接点人数修改失败!');

                  }
              }

              //更改后的上级直推人数加一
              $res = $slippingUsersModel->changeUserToConnecter($firstLeaderUid,'changeFirstLeader');  
              if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$firstLeaderUid.'直推人数更改数据失败');

            }
            //接点人的滑落人数量减一
            $connecter = $countModel->where(['connecter_id'=>$slippingUsersInfo['connecter_id']])->find();
            $slipping_num = $connecter['slipping_num']-1;
            // p($slipping_num);
            if($connecter['direct_num']>=$slippingConfig['team']){
                $configDirectNum = $connecter['direct_num'] >= $slippingConfig['direct_num'] ? $slippingConfig['high_num'] : $slippingConfig['low_num'];
                $surplus_num = $slipping_num>=$configDirectNum?0:$configDirectNum-$slipping_num;
                $data = [
                    'slipping_num'=>$slipping_num,
                    'surplus_num'=>$surplus_num,
                    'update_time'=>time(),
                    'enable'=>1
                ];

                $res = $surplus_num > 0 ? $countModel->where(['connecter_id'=>$slippingUsersInfo['connecter_id']])->update($data) :$countModel->where(['connecter_id'=>$slippingUsersInfo['connecter_id']])->update(['slipping_num'=>$slipping_num,'surplus_num'=>$surplus_num,'update_time'=>time(),'enable'=>0]);

                if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$slippingUsersInfo['connecter_id'].'更新数据失败!');

            }
            return true;
            
       }
       if($userInfo['level'] >= $slippingConfig['team_member_level']){
         //原上级直推人数更改
         $res = $this->changeBeforeFirstLeaderDirectNum($firstLeaderUidBefore);
         if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$firstLeaderUidBefore.'直推人数更改数据失败');
         

         //更改后的上级直推人数加一
         $res = $slippingUsersModel->changeUserToConnecter($firstLeaderUid,'changeFirstLeader');  
         if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$firstLeaderUid.'直推人数更改数据失败');
         return true;
       }
       return array('status'=>1 , 'msg'=>'等级少于一星不作修改');
    }

    /**
    * 更改上下级 用户为非滑落人时更改原来上级直推数量
    */
   public  function changeBeforeFirstLeaderDirectNum($firstLeaderUidBefore)
   {
        $slippingUsersModel = model('common/SlippingUsers');
        $res = $slippingUsersModel->getDirectNum($firstLeaderUidBefore);
        return $res;
   }
   // /**
   //  * 更改用户等级 判断是否符合接点人条件
   //  */
   // public  function changeLevelForConnecter($uid)
   // {    
   //      $usersModel         = model('common/Users');
   //      $slippingUsersModel = model('common/SlippingUsers');
   //      $userInfo = $usersModel->where(['user_id'=>$uid])->find();
        
   //      $res = $slippingUsersModel->getDirectNum($firstLeaderUidBefore);  
   // }
    /**
     * 变更上下级 
     * $firstLeaderUid 更改后的上级ID
     * $changeUid  被更改上级的用户ID
     * $firstLeaderUidBefore 原上级用户ID 
     */
    public function changeFirstLeaderForExcel($firstLeaderUid)
    {

      $slippingUsersModel = model('common/SlippingUsers');

      //更改后的上级直推人数加一
      $res = $slippingUsersModel->changeUserToConnecter($firstLeaderUid,'changeFirstLeader');  
      if($res === false)   return array('status'=>0 , 'msg'=>'ID为:'.$firstLeaderUid.'直推人数更改数据失败');
      return true;

    }

/************************************************* 2019-04-15 销售任务游戏项目V1.2版修改滑落规则及商业模式 start *********************************/

    /**
    * 滑落规则
    * $type changeFirstLeader 更改上下级  changeLevel 更改等级 upgradeLevel  闯关升级
    * firstLeaderId 
    */
   public  function slippingRule($uid,$type='',$desc='',$referrer_id='',$connecter_id='')
   {    
      if(empty($type)) return ['status'=>0,'msg'=>'参数有误']; 
      switch ($type) { 

        //等级变更
        case 'changeLevel':
          $res = $this->changUserLevel($uid);
          break;

        //上下级变更
        case 'changeFirstLeader':
          $res = $this->changeUserFirstLeader($uid,$desc);
          break;

        //是否成为引荐人 是则获取接点人uid
        case 'referrer':
          $res = $this->isDirecter($uid);
          break;

        //添加滑落人记录 并更改接点人数据
        case 'connecter':
          $res = $this->toConnect($uid,$connecter_id,$referrer_id);
          break;
        
        default:
          return ['status'=>0,'msg'=>'参数有误']; 
          break;
      }
      if($res['status'] ==0){
        return ['status'=>0,'msg'=>$res['msg']]; 
      }else{
        return ['status'=>1,'msg'=>$res['msg'],'data'=>$res['data']]; 
      }

   }
  /**
    * 等级变更时判断该等级是否符合接点人等级配置条件 
    *  
    */
  public  function getConnectNum($uid)
  {   
    $usersModel         = model('common/Users');
    $countModel         = model('common/SlippingCount');
    $slippingConfigMdl  = model('common/SlippingConfig');
    
    $connecterInfo =  $countModel->where(['connecter_id'=>$uid])->find(); 
    $userLevel     = $usersModel->where(['user_id'=>$uid])->value('level');    //用户等级

    $configMaxLevel = $slippingConfigMdl->where(['enable'=>1])->order('level DESC')->find();
    if($userLevel > $configMaxLevel['level']){
      $num = $slippingConfigMdl->where(['enable'=>1,'level'=>$configMaxLevel['level']])->value('num'); 
    }else{
      $num = $slippingConfigMdl->where(['enable'=>1,'level'=>$userLevel])->value('num')?:0;

    }
    //如果后台设置了改用户的可接点人数 则用已设置的数量 没有就获取配置表的等级对应可接点数量
    /**2019-5-5 接点规则修改 5星及以上用户不能接点
     * @author Jaywoo
     */
    if($userLevel >= 6) {
      $num = 0;
    }else{
      $num = ($connecterInfo && $connecterInfo['total_num']>0 )? $connecterInfo['total_num'] : $num;
    }
    return $num;
  }
  /**
    * 添加接点人
    *  
    */
  public  function addConnecter($uid,$surplus_num)
  {   
    $usersModel         = model('common/Users');
    $countModel         = model('common/SlippingCount');
    $reg_time = $usersModel->where(['user_id'=>$uid])->value('reg_time');
  
    //非接点用户数据
    $data = [
        'connecter_id' => $uid,
        'surplus_num'  => $surplus_num,
        'enable'       => 1,
        'reg_time'     => $reg_time,

    ];
    $res = $countModel->addData1($data);
    return $res;
  }
  
  /**
  *判断用户是否可以成为接点人 是则添加接点人数据
  * $from 判断是从注册 还是帮他人注册 还是更改上下级
  */
  public function changUserLevel($uid)
  {
      $countModel         = model('common/SlippingCount');
      $usersModel         = model('common/Users');

      $userInfo = $usersModel->where(['user_id'=>$uid])->find();        //用户资料
      $num      = $this->getConnectNum($uid);                           //查询配置表与等级对应的可接点数量                
      //查询用户是否已经是接点人
      $connecterInfo   =  $countModel->where(['connecter_id'=>$uid])->find(); 

      if($connecterInfo) {

        //剩余可接点人数
        $surplus_num = $num - $connecterInfo['slipping_num'];   

        if($surplus_num>0){
          $res = $connecterInfo['enable']==0? $countModel->where(['connecter_id' => $uid])->update(['surplus_num' => $surplus_num,'update_time' => time(),'enable'=>1]):$countModel->where(['connecter_id' => $uid])->update(['surplus_num' => $surplus_num,'update_time' => time()]);

        }else{

          $res = $countModel->where(['connecter_id' => $uid])->update(['surplus_num' => 0,'update_time' => time(),'enable'=>0]);

        }

        if($res ==false){
          return ['status'=>0,'msg'=>'更新接点人数据失败','data'=>''];
          }
        return ['status'=>1,'msg'=>'成功','data'=>''];

      }else{
          if($num>0) {
            $res = $this->addConnecter($uid,$num);
            if($res ==false){
              return ['status'=>0,'msg'=>' 添加接点人数据失败','data'=>''];
            }
            return ['status'=>1,'msg'=>'成功','data'=>''];
          }
          return ['status'=>1,'msg'=>'可接人数为0','data'=>''];
      }
     
  }

  /**
  *变更上下级  滑落人改为非滑落人
  * 
  */
  public function changeUserFirstLeader($uid,$desc)
  {
      $slippingUsersModel = model('common/SlippingUsers');
      $countModel         = model('common/SlippingCount');


      //判断用户是否为滑落人
      $slippingUsersInfo =  $slippingUsersModel->where(['slipping_id'=>$uid,'is_slipping'=>1])->find();

      //更改用户为非滑落人
      if($slippingUsersInfo) {
        trans();
        $res = $slippingUsersModel->where(['slipping_id'=>$uid])->update(['is_slipping'=>0,'update_time'=>time(),'desc'=>$this->_desc[$desc]]);
        if($res == false){
          trans('rollback');
          return ['status'=>0,'msg'=>'更改用户为非滑落人失败','data'=>''];
        }

        //原上级滑落人数减一
        $connecter    = $countModel->where(['connecter_id'=>$slippingUsersInfo['connecter_id']])->find();
        $slipping_num = $connecter['slipping_num']-1;

        $num = $this->getConnectNum($slippingUsersInfo['connecter_id']); 
        $surplus_num  = $num-$slipping_num;

        $data = [
            'slipping_num'=>$slipping_num,
            'surplus_num'=>$surplus_num>0?$surplus_num:0,
            'update_time'=>time(),
            'enable'=>$surplus_num>0?1:0
        ];

        $res = $countModel->where(['connecter_id'=>$slippingUsersInfo['connecter_id']])->update($data);

        if($res == false){
          trans('rollback');
          return array('status'=>0 , 'msg'=>'ID为:'.$slippingUsersInfo['connecter_id'].'更新数据失败!','data'=>'');
        }
        trans('commit');
        return ['status'=>1,'msg'=>'成功','data'=>''];
      }
      return ['status'=>1,'msg'=>'用户为非滑落人，不做更改','data'=>''];
  }


  /**
  * isDirecter 判断该用户是否可以成为引荐人
  * 
  */
  public function isDirecter($referrer_id)
  {
      $usersModel        = model('common/Users');
      $countModel        = model('common/SlippingCount');
      $slippingConfigMdl = model('common/SlippingConfig');
      $slippingUsersModel = model('common/SlippingUsers');

      $slippingConfig    = $slippingConfigMdl->where(['enable'=>1])->find();

      //查询下级是否有接点人(2019-4-22 滑落规则修改,等级对应可接点数量为 1星=>0 2星=>1 3星=>2 4星=>3 5星=>3)上限为3人
      $ids = $usersModel->where(['first_leader'=>$referrer_id,'level'=>['>',2]])->field('user_id')->select();
      $ids = $slippingUsersModel->toArrayOne($ids,'user_id');
      if($ids){
        $connecterId = $countModel->alias('c')
                        ->join('users u','c.connecter_id = u.user_id')
                        ->where(['c.connecter_id'=>['in',$ids],'c.enable'=>1])
                        ->order('c.slipping_num asc,u.level desc,c.reg_time asc')
                        ->value('c.connecter_id');
        if(empty($connecterId)) return ['status'=>0,'msg'=>'没有符合条件的接点人'];
        return ['status'=>1,'msg'=>'成功','data'=>$connecterId];
      }else{
        return ['status'=>0,'msg'=>'没有符合条件的接点人','data'=>''];
      }
  }

    //添加滑落人记录
    public function addSlippingUser($slipping_id,$connecter_id,$referrer_id)
    {

      $slippingUsersModel = model('common/SlippingUsers');

      $data = [
              'connecter_id' => $connecter_id,   //接点人
              'slipping_id'  => $slipping_id,    //滑落人
              'referrer_id'  => $referrer_id,    //引荐人
              'is_slipping'  => 1,               //是否滑落人
      ];

      $res = $slippingUsersModel->addData($data);

      if($res === false){
          return  ['status'=>0,'msg'=>'添加滑落人记录失败','data'=>''];
      }
      return  ['status'=>1,'msg'=>'成功','data'=>''];
    }

    /**
    *添加滑落人并修改相关数据 
    * 
    */
    public function toConnect($slipping_id,$connecter_id,$referrer_id){

      //添加滑落人记录
      trans();
      $res = $this->addSlippingUser($slipping_id,$connecter_id,$referrer_id);
      if($res['status'] == 0){
          trans('rollback');
          return ['status'=>0,'msg'=>$res['msg'],'data'=>''];
      }

      //更新接点人数据统计表  
      $countModel          = model('common/SlippingCount');
      $slippingConfigMdl   = model('common/SlippingConfig');

      $counnectCountDetail = $countModel->where(['connecter_id'=>$connecter_id])->find();
      $num                 = $this->getConnectNum($connecter_id);           

      $slipping_num = $counnectCountDetail['slipping_num'] + 1;  //接点人的滑落人数量加一
      $surplus_num  = $num  - $slipping_num; 

      $counnectCount = [
          'slipping_num' => $slipping_num,
          'surplus_num'  => $surplus_num > 0 ? $surplus_num : 0,
          'enable'       => $surplus_num > 0 ? 1 : 0,
          'update_time'  => time(),
      ];

      $result = $countModel->where(['connecter_id'=>$connecter_id])->update($counnectCount);
      if($result === false){
          trans('rollback');
          return ['status'=>0,'msg'=>'更新接点人数据失败','data'=>''];
      }

      trans('commit');
      return ['status'=>1,'msg'=>'成功','data'=>''];
   }

/************************************************* 2019-04-15 销售任务游戏项目V1.2版修改滑落规则及商业模式 end *********************************/
  

}