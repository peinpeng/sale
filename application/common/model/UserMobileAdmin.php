<?php
namespace app\common\model;

use think\Db;

class UserMobileAdmin extends Base
{   
    protected $_role = [
        1 => '手机后台管理员',
       
    ];
     //状态描述
    public function getRoleNameAttr($value, $data)
    {
        return $this->_role[$data['role']];
    }
    public function getRoleList()
    {
        return $this->_role;
    }
    //判断用户是否已存在
     public function checkUserMobileAdmin($user_id)
    {
        return $this->where(['user_id'=>$user_id])->count();
    }
     public function Users()
    {
        return $this->hasOne('Users','user_id','user_id')->field('user_id,nickname,mobile');
    }
      public function lists($where=[],$p=1,$listRows=10,$order='id DESC',$with=[])
    {
        return $this->where($where)->with($with)->order($order)->page($p,$listRows)->select();
    }

    /**
     * [addData 添加手机端后台管理员]
     *
     */
    public function addAdmin($row)
    {
        
        $insertData = [
           'user_id'  => $row['user_id'],
           'enable'   => $row['enable'],
           'add_time' => time(),
           'role'     => $row['role'],
        ];
        adminLog('添加手机端后台管理员');

        return $this->add($insertData);
    }
    /**
     * [delAdmin 删除手机端后台管理员]
     *
     */
    public function delAdmin($id)
    {
        $r = $this->where(['id'=>$id])->delete();

        return $r;
    }

    //是否是手机后台管理员
    public function isMobileAdminUser($user_id)
    {
        $info = $this->where(['user_id'=>$user_id,'enable'=>1,'role'=>1])->find();
        if($info) return true;
        return false;
    }



}
