<?php
namespace app\common\model\place;
use think\Db;
use app\common\model\Base;
class PlaceWaitingMatching extends Base
{

    // const  = 'text';
    
    public function user()
    {
        return $this->hasOne('app\common\model\Users','user_id','user_id');
    }

    public function getRankingAttr($value,$row)
    {
        if($row['state']==1) return 1;
        if($row['state']==0){
            return $this->where(['enable'=>1,'state'=>0,'lineup_time'=>['<=',$row['lineup_time']]])->order($this->getOrderByStr())->count();
        }
        return '-';
    }

    public function getOrderByStr()
    {
        return 'if(state=1,1,0) ASC,lineup_time ASC,matched_num ASC,loss_time ASC';
    }
    
    /**
     * Author: Jaywoo
     * 是否在占位表排队或正在占位中
     */
    public function isPlaceUser($user_id){
        $cond = [
            'state'=>['in',[0,1]],
            'user_id'=>$user_id,
            // 'enable'=>1,

        ];
        return $this->where($cond)->find();
    }


     //获取已补偿数量
    public function getCompensatedNum($user_id){
        $compensated_num = $this->where(['user_id'=>$user_id])->value('compensated_num');
        return $compensated_num?$compensated_num:0;
        
    }

    //当前获取匹配总数
    public function getMatchNum($user_id){
        $match_num = $this->where(['user_id'=>$user_id])->value('match_num');
        return $match_num?$match_num:0;
        
    }

    //获取当前补偿总数
    public function getCompensateNum($user_id){
        $num = $this->where(['user_id'=>$user_id])->value('compensate');
        return $num?$num:0;
    }

     //获取占位排队状态
    public function getState($user_id){
        return $this->where(['user_id'=>$user_id])->value('state');
    }

    //获取占位人信息
    public function getPlaceTop(){
        $cond = [
            'enable' => 1,
            'state'  =>['in',[0,1]],
        ];
        return $this->where($cond)->order('lineup_time asc')->find();
    }

    /**
     *  团队补偿匹配规则
     * [getOptimalUser description]
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function getOptimalUserIdForTeam($user_id){

        $teamIds = model('Common/distribut/DistributTeam')->getFirstLeaderTeam($user_id);
        if(empty($teamIds)) return false;

        $cond = [
            'user_id' => ['in',$teamIds],
            'state'   =>['in',[0,1]],
            'enable'  =>1,
        ];

        $cond1 = array_merge($cond,['compensate_num - compensated_num'=>['>',0]]);
        $loss_time =  $this->where($cond1)->min('loss_time');
        // ps($this);
        // dump($teamIds);
        // dump($cond1);
        // dump($loss_time);
        if($loss_time){
            //补偿
            $cond1['loss_time'] = $loss_time;
            $getUserids = $this->where($cond1)->column('user_id');
            // p($getUserids);
        }else{
            //匹配
             if(empty($loss_time)) {
                $cond2 = array_merge($cond,['match_num - matched_num'=>['>',0]]);
                $lineup_time =  $this->where($cond2)->min('lineup_time');
                
                if(empty($lineup_time)) return false;

                $cond2['lineup_time'] = $lineup_time;
                $getUserids = $this->where($cond2)->column('user_id');
            }
        }
            // p($getUserids);
        if(count($getUserids) == 1){
            return $getUserids[0];
        }else{
            foreach ($teamIds as $key => $value) {
                if(in_array($value,$getUserids)) return $value;
            }
        }

        return false;
    }

    //获取占位人user_id
    public function getPlaceTopUserId(){
        $order =$this->getOrderByStr();
        $cond = [
            'enable' => 1,
            'state'  =>1,
        ];
        $placeUser = $this->where($cond)->order($order)->find();
        if(!$placeUser) return false;
        return $placeUser['user_id'];
    }

    /**
     * Author: Jaywoo
     * addData  添加数据
     */
    public function addData($data){
        if( !$data['user_id'] || !$data['match_num'] || !$data['lineup_time']){
            return false;
        }
        $result=$this->add([
            'user_id'         => $data['user_id'],
            'match_num'       => $data['match_num']?$data['match_num']:1,
            // 'compensate_num'  => $data['compensate_num']?$data['compensate_num']:1,
            // 
            'compensate_num'  => 0,
            'state'           => 0,
            'matched_num'     => 0,
            'compensated_num' => 0,
            'add_time'        => time(),
            'enable'          => 1,
            'desc'            => $data['desc']?$data['desc']:'',
            'lineup_time'     => $data['lineup_time'],
            'loss_num'        => $data['loss_num']?$data['loss_num']:0,
            'loss_time'       => $data['loss_time']?$data['loss_time']:0,
        ]);
        if(!$result){
            return false;
        }
       
       return $result;
       
    }

}
