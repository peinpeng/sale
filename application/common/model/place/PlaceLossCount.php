<?php
namespace app\common\model\place;
use think\Db;
use app\common\model\Base;
use app\common\logic\UsersLogic;
class PlaceLossCount extends Base
{
    
    
   
    public function levelConfig(){
        return $this->hasOne('user_level','level_id','level');
    }

    //获取总流失数
    public function getLossNum($user_id){
        return $this->where(['user_id'=>$user_id])->value('loss_num');
    }

    
}
