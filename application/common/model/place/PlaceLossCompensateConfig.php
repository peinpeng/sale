<?php
namespace app\common\model\place;
use think\Db;
use app\common\model\Base;
class PlaceLossCompensateConfig extends Base
{
    
    
    
    public function levelConfig(){
        return $this->hasOne('app\common\model\UserLevel','level_id','level_id');
    }

    /**
     * Author: Jaywoo
     * 获取等级配置的可补偿数
     *
    */
    public function getCompensateNum($user_id,$level){
        $lossCountMdl = model('Common/place/PlaceLossCount');

        $compensate_num = $this->where(['level_id'=>$level,'enable'=>1])->value('compensate_num');
        if (!$compensate_num) {
            $compensate_num = $this->where(['enable'=>1])->order('id desc')->value('compensate_num');
        }
        if($compensate_num == '*') {
            $compensate_num = $lossCountMdl->getLossNum($user_id);
        }
        return $compensate_num;
    }


}
