<?php
namespace app\common\model\place;
use think\Db;
use app\common\model\Base;
use app\common\logic\UsersLogic;
class PlaceMatchingLog extends Base
{
    

    public function user()
    {
        return $this->hasOne('app\common\model\Users','user_id','user_id');
    }
    
    
    public function levelConfig(){
        return $this->hasOne('user_level','level_id','level');
    }

    /**
     * Author: Jaywoo
     * addData  添加数据
     */
    public function addData($data){
        if( !$data['user_id'] || !$data['compensate_id'] || !$data['source_type'] || !$data['type']){
            return false;
        }
        $result=$this->add([
            'user_id'         => $data['user_id'],
            'compensate_id'   => $data['compensate_id'],
            'source_type'     => $data['source_type'],
            'type'            => $data['type'],
            'status'          => 1,
            'add_time'        => time(),
            'give_gift_id'    => $data['give_gift_id'],
        ]);
        if(!$result){
            return false;
        }
       
       return $result;
       
    }

    //闯关成功
    public function changeStatusSuccess($give_gift_id)
    {
        $res = $this->where(['give_gift_id'=>$give_gift_id])->update(['status'=>2,'update_time'=>time()]);
        return $res;
    }

    //闯关失败 返还数量
    public function changeStatusFail($give_gift_id)
    {
        $row = $this->where(['give_gift_id'=>$give_gift_id])->find();
        if(!$row){
            $this->error = '没有记录';
        }

        trans();
        $res = $this->where(['id'=>$row['id']])->update(['status'=>3,'update_time'=>time()]);
        if(!$res){
            trans('rollback');
            $this->error = 'PlaceMatchingLog->update:fail';
            return false;
        }

        //返还数量
        $field = $row['type'] == 1 ? 'matched_num' : 'compensated_num';
        $res = model('Common/place/PlaceWaitingMatching')->where(['user_id'=>$row['user_id']])->setDec($field);
        if(!$res){
            trans('rollback');
            $this->error = 'PlaceWaitingMatching->setInc:fail';
            return false;
        }

        trans('commit');
        return true;
    }

}
