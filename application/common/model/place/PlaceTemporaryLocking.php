<?php
namespace app\common\model\place;
use app\common\model\Users;
use think\Db;
use app\common\model\Base;
class PlaceTemporaryLocking extends Base
{
    public function user()
    {
        return $this->hasOne('app\common\model\Users','user_id','temporary_firstleader_id');
    }
    public $_source = [
        'default'=>'正常',
        'share'=>'分享',
        'notReg'=>'分享获取短信验证码未注册',
        // 'clickLock'=>'分享点击',
    ];

    public $_status = [
        '1'=>'临时锁定上下级关系',
        '2'=>'已解除锁定上下级关系',
        '3'=>'锁定分享第一个推荐人',
        // '4'=>'分享点击',
        '5'=>'已解除锁定分享第一个推荐人',
    ];
    
    /**
     * Author: Jaywoo
     * 分享注册添加暂时锁定上下级关系记录
     *
     */
    public function addData($firstleader_id='',$user_id='',$status='0',$source='',$mobile='')
    {
        if( !$firstleader_id  ||!$source ||(!$user_id && !$mobile)){
            return false;
        }
        $result=$this->add([
            'temporary_firstleader_id'     => $firstleader_id,
            'temporary_user_id'            => $user_id,
            'add_time'                     => time(),
            'status'                       => $status?$status:0,
            'source'                       => $source,
            'mobile'                       => $mobile,
        ]);
        if(!$result){
            $this->error = 'PlaceTemporaryLocking->add:fail';
            return false;
        }


        return $result;
    }

    /**
     * User: 聂风 Function:临时绑定
     * @param $firstleaderMobile 推荐人手机号
     * @param $userId 用户id
     * @param string $source 注册来源
     */
    public function temporaryBind($firstleaderMobile, $userId, $source='default')
    {
        $inviteLevel = Users::field('level,user_id')->where('mobile', $firstleaderMobile)->find();
        if($inviteLevel && $inviteLevel['level'] <= 1){
            $this->addData($inviteLevel['user_id'],$userId,1, $source);
        }
    }

    /**
     * User: Jaywoo Function:不注册，临时绑定用户手机号
     * @param $firstleaderMobile 推荐人手机号
     * @param $userId 用户id
     * @param string $source 注册来源
     */
    public function temporaryBindForMobile1($firstleaderMobile, $mobile,$tk_id='')
    {  
        if(!check_mobile_all($mobile)) return ['code'=>10000,'msg'=>'手机号码不正确','data'=>''];

        $info = $this->where(['mobile'=>$mobile])->find();
        $userInfo = Users::where(['mobile'=>$mobile])->count();
        $tk_mobile = Users::where(['user_id'=>$info['temporary_firstleader_id']])->value('mobile');
        if($info || $userInfo) return ['code'=>10000,'msg'=>'已注册或已绑定','data'=>$tk_mobile];

        $inviteLevel = Users::field('level,user_id')->where('mobile', $firstleaderMobile)->find();
        if(!$inviteLevel && $firstleaderMobile) return ['code'=>10000,'msg'=>'推荐人不存在','data'=>''];
        if(!$inviteLevel) return ['code'=>10000,'msg'=>'','data'=>''];

        $res = $this->addData($inviteLevel['user_id'],'','3','notReg',$mobile);

        if($res)return ['code'=>10000,'msg'=>'绑定成功','data'=>''];
        return ['code'=>10000,'msg'=>'绑定失败','data'=>''];
    }

    /**
     * User: Jaywoo Function:不注册，临时绑定用户手机号
     * @param $firstleaderMobile 推荐人手机号
     * @param $userId 用户id
     * @param string $source 注册来源
     */
    public function temporaryBindForMobile($mobile,$tk_id='')
    {   
        if(!$tk_id) return ['code'=>10000,'msg'=>'','data'=>''];

        $info = $this->where(['mobile'=>$mobile])->find();
        $tk_mobile = Users::where(['user_id'=>$info['temporary_firstleader_id']])->value('mobile');

        if($info) return ['code'=>10000,'msg'=>'已绑定','data'=>$tk_mobile];

        $this->addData($tk_id,'','3','notReg',$mobile);

        $tk_mobile = Users::where(['user_id'=>$tk_id])->value('mobile');
        return ['code'=>10000,'msg'=>'已绑定','data'=>$tk_mobile];

    }

    /**
     * User: Jaywoo Function:用户注册后，解除临时绑定
     * @param $mobile 注册手机号
     */
    public function delTemporaryBindRelation($mobile)
    {   

        $info = $this->where(['mobile'=>$mobile,'source'=>'notReg'])->find();
        if(!$info) return true;

        //改变状态 status = 5 解除锁定状态
        $res = $this->where(['mobile'=>$mobile,'source'=>'notReg'])->update(['status'=>5]);
        if($res) return true;
        return false;
    }

    
   
}
