<?php
namespace app\common\model\place;
use app\common\model\Base;
use app\common\logic\UsersLogic;
class PlaceLossLog extends Base
{
    
    
    //获取最早流失时间
    public function getEarliestLossTime($user_id){
        return $this->where(['before_firstleader'=>$user_id])->min('add_time');
    }

}
