<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/5/20
 * Time: 9:12
 */
namespace app\common\model;

use app\common\logic\UserHashLogic;

class UserToken extends Base
{
    //自定义初始化
    protected static function init()
    {
        //有修改就添加进redis hash表里做缓存
        UserToken::event('after_insert', function ($userToken) {
            (new UserHashLogic())->refreshOne($userToken->user_id);
        });
    }

    /**
     * User: 聂风 Function:添加token
     * @param $userId
     */
    public function addData($userId)
    {
        $tokenModel = self::all(['user_id' => $userId]);
        if($tokenModel){
            self::where(['user_id' => $userId])->delete();
        }
        $token = create_token();
        $data = [
            'token' => $token,
            'session_id' => session_id(),
            'user_id' => $userId,
            'add_time' => time(),
        ];
        $this->save($data);
        return $token;
    }

    public function getSessionId($userId)
    {
        $sessionId = self::where('user_id', $userId)->value('session_id');
        return $sessionId?$sessionId:'';
    }
}