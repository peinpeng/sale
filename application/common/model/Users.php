<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */
namespace app\common\model;

use app\common\logic\UserHashLogic;
use think\Db;

class Users extends Base
{
    protected $pk = 'user_id';
    protected $_is_lock = [
        0 => '正常',
        1 => '冻结',
    ];

    //自定义初始化
    protected static function init()
    {
        //有修改就添加进redis hash表里做缓存
        /*Users::event('after_write', function ($user) {
            (new UserHashLogic())->refreshOne($user->user_id);
        });*/
    }
    /**
     * 获取用户信息
     */
    public function getUserInfo($user_id){
        return $this->where(['user_id'=>$user_id])->find();
    }
    /**
     * 根据手机号获取用户id
     */
    public function getUserIdByMobile($mobile){
        return $this->where(['mobile'=>$mobile])->value('user_id');
    }
     /**
     * 根据手机号获取用户id
     */
    public function getUserIdByNickname($nickname){
        return $this->where(['nickname'=>$nickname])->column('user_id');
    }
     /**
     * 根据手机号或昵称获取用户id
     */
    public function getUserIdByMobileOrNickname($keywords){
        $where = [];
        // $where['mobile|nickname'] = ['like','%'.$keywords.'%'];
        $where['mobile|nickname'] = ['like',"%$keywords%"];
        $give_user_ids = $this->where($where)->field('user_id')->select();
        foreach ($give_user_ids as $k => $v) {
                $give_user_ids[$k] = $v['user_id'];
        }
        return $give_user_ids;    
    }


    /**
     * 冻结状态
     */
    public function getIsLockStateAttr($value, $data){
        return $this->_is_lock[$data['is_lock']];
    }

    /**
     * 用户的微信号
     */
    public function getWxAccountAttr($value, $data){
        return M('Paytype')->where(['type'=>2,'user_id'=>$data['user_id']])->value('account')?:'';
    }

    /**
     * 用户的支付宝号
     */
    public function getZfbAccountAttr($value, $data){
        return M('Paytype')->where(['type'=>1,'user_id'=>$data['user_id']])->value('account')?:'';
    }

    /**
     * 用户的微信号
     */
    public function getPlaceTemporaryLockingAttr($value, $data){
        return M('place_temporary_locking')->where(['temporary_firstleader_id'=>$data['user_id']])->count()?:0;
    }

    /**
     * 用户下线分销金额
     * @param $value
     * @param $data
     * @return float|int
     */
    public function getRebateMoneyAttr($value, $data){
        $sum_money = DB::name('rebate_log')->where(['status' => 3,'user_id'=>$data['user_id']])->sum('money');
        $rebate_money = empty($sum_money) ? (float)0 : $sum_money;
        return  $rebate_money;
    }

    // /**
    //  * [getFisrtLeaderNumAttr 统计直推（大于等于某个等级）人数]
    //  * @param  [type]  $userId [上级用户编号]
    //  * @param  integer $level  [用户等级]
    //  * @return [type]          [description]
    //  */
    // public function getFisrtLeaderNumAttr($userId,$level = 0){
    //     if(empty($level))
    //     {
    //         $fisrt_leader = Users::where(['first_leader'=>$userId])->count();
    //     }else{
    //         $fisrt_leader = Users::where(['first_leader'=>$userId,'level'=>['egt',$level]])->count(); 
    //     } 
    //     return  $fisrt_leader;
    // }
    /**
     * [getFisrtLeaderNumAttr 统计直推（大于等于某个等级）人数]
     * @param  [type]  $userId [上级用户编号]
     * @param  integer $level  [用户等级]
     * @return [type]          [description]
     */
    public function getFisrtLeaderNumAttr($userId,$level = 0)
    {
        $slippingModel     = model('common/SlippingUsers');

        if(empty($level))
        {
            //查询用户users表的下级 
            $ids = Users::where(['first_leader'=>$userId])->field('user_id')->select();
        }else{
            //查询等级$level星或以上的下级
            $ids = Users::where(['first_leader'=>$userId,'level'=>['egt',$level]])->field('user_id')->select();
        } 

        $ids = $this->toArrayOne($ids,'user_id');

        if($ids){
            //查询下级是否有滑落人
            $slippingIds = $slippingModel->where(['slipping_id'=>['in',$ids],'is_slipping'=>1])->field('slipping_id')->select();
            $slippingIds = $slippingIds ? $this->toArrayOne($slippingIds,'slipping_id') : [];

            //获取直推队员人数 (排除滑落人)
            $connectIds = array_diff($ids,$slippingIds);
            $fisrt_leader = count($connectIds);

            //用户推荐的滑落人数
            $slippingCount = $slippingModel
            ->alias('s')
            ->join('users u', 's.slipping_id=u.user_id')
            ->where(['s.referrer_id'=>$userId,'u.level'=>['egt',$level]])
            ->count();

            $totalNum = $fisrt_leader+$slippingCount;
        }
         return  $totalNum;
  
    }
     /**
     * [toArrayOne 转为一位数组]
     * $field 要转为一维数组的字段
     * @return boolean [description]
     */
    public function toArrayOne($obj,$field){
        foreach ($obj as $k => $v) {
            $data[] = $v[$field];
        }
       
        return $data;
    }

    /**
     * [getFisrtLeaderNumAttr 上级手机号]
     * @return [type]          [description]
     */
    public function getFisrtLeaderMobileAttr($value, $row){
        $mobile = Users::where(['user_id'=>$row['first_leader']])->value('mobile');      
        return  $mobile;
    }
    
    /**
     * [getFisrtLeaderNumAttr 根据上级手机号获取上级id]
     * @return [type]          [description]
     */
    public function getFisrtLeaderIdByMobile($mobile){
        $user_id =$this->where(['mobile'=>$mobile])->value('user_id');      
        return  $user_id;
    }

    /**
     * 用户二级下线数
     * @param $value
     * @param $data
     * @return mixed
     */
    public function getSecondLeaderNumAttr($value, $data){
        $second_leader = Users::where(['second_leader'=>$data['user_id']])->count();
        return  $second_leader;
    }

    /**
     * 用户二级下线数
     * @param $value
     * @param $data
     * @return mixed
     */
    public function getThirdLeaderNumAttr($value, $data){
        $third_leader = Users::where(['third_leader'=>$data['user_id']])->count();
        return  $third_leader;
    }

    /**
     * [getParentInfos 获取用户直推链用户信息,如果输入了等级，则获取用户直推链最近的一个用户]
     * @param  [type] $parentIds [description]
     * @return [type]            [description]
     */
    public function getParentInfos($parentIds = [],$field = '*', $level = 0)
    {

        if(empty($parentIds)) return [];


        $parentInfos = $this->whereIn('user_id',array_values($parentIds))->order('field(user_id,'.implode(',',$parentIds).')')->column($field);

        // ps($this,0);

        // p($parentIds,1);
        // p($parentInfos,0);

        if(empty($level))  return $parentInfos;
        
        /**
         * [$nearestUser 最近符合等级的用户]
         * @var string
         */

        $nearestUser = $this->getNearestUser($parentInfos,$level);
       
        return $nearestUser;
    }   

    /**
     * [getNearestUser 获取最近的]
     * @param  [type]  $parentInfos [description]
     * @param  integer $level       [description]
     * @return [type]               [description]
     */
    public function getNearestUser($parentInfos = [],$level = 0)
    {
        // p($parentInfos,1);

        $nearestUser = [];

        if(!empty($parentInfos) && !empty($level)) foreach ($parentInfos as $k => $v) {

            if($v == $level){
                $nearestUser = $k;
                break;
            } 
        }

        return $nearestUser;

    }


    /**
     * [creditUser 获取用户信用分信息]
     * 
     */
     public function creditUser(){
        return $this->hasOne('app\common\model\credit\CreditUser','user_id','user_id');
    }


    /**
     * [creditUser 获取用户信用分信息]
     * 
     */
     public function slippingUsers(){
        return $this->hasOne('app\common\model\SlippingUsers','slipping_id','user_id');
    }
    /**
     * [slippingCount 获取用户接点信息]
     * 
     */
     public function slippingCount(){
        return $this->hasOne('app\common\model\SlippingCount','connecter_id','user_id');
    }
    /**
     * [slippingCount 获取用户占位信息]
     * 
     */
     public function placeWaitingMatching(){
        return $this->hasOne('app\common\model\place\PlaceWaitingMatching','user_id','user_id');
    }
    /**
     * [getUserLevelNameAttr 获取用户等级名称]
     */
    public function getLevelNameAttr($value,$row)
    {
       return model('common/users/UserLevel')->getBy('level_id',$row['level'],'show_name');
    
    }
    
    /**
     * [getWalletMoneyAttr 获取用户等级名称]
     */
    public function getWalletMoneyAttr($value,$row)
    {
        return model('common/users/UserWallet')->getBy('user_id',$row['user_id'],'money')?:0;
    
    }

    /**
     * [updateUserLevel 升级用户等级]
     * @param  [type] $userId [description]
     * @param  [type] $level  [description]
     * @return [type]         [description]
     */
    public function updateUserLevel($userId, $level, $uplevel_source="", $description="")
    {
        // $res = $this->where(['user_id'=>$userId])->update(['level'=>$level]);
        $userLevelLogMdl = model('Common/UserLevelLog');
        return $userLevelLogMdl->addData($userId,$level,$uplevel_source,$description);
        // return $this->where(['user_id'=>$user_id])->update(['level'=>$level]);
    }


    /**
     * [getLockUser 获取锁定的用户]
     * @param  string $field [description]
     * @return [type]        [description]
     */
    public function getLockUser($field = '*')
    {
        return $this->getCol($field,['is_lock'=>1]);
    }


    /**
     * [getUserLevel 获取用户等级]
     * @param  string $user_id [description]
     * @return [type]        [description]
     */
    public function getUserLevel($user_id)
    {
        return $this->where(['user_id'=>$user_id])->value('level');
    }
    //刷新token
    public function refreshToken($userId)
    {
        $token = create_token();
        $this->save(['token'=>$token], ['user_id'=>$userId]);
        return $token;
    }

    //单个获取用户信息给第三方
    public function supplyGetUserData($userId)
    {
        $returnData = [];
        $data = $this->where('user_id', $userId)->find();
        $address = [];
        $data['province'] && $address[] = $data['province'];
        $data['city'] && $address[] = $data['city'];
        $data['district'] && $address[] = $data['district'];
        if($address){
            $region = model('Region')->where('id','in', $address)->column('id,name');
            $returnData['province'] = isset($region[$data['province']])?$region[$data['province']]:'';
            $returnData['city'] = isset($region[$data['city']])?$region[$data['city']]:'';
            $returnData['district'] = isset($region[$data['district']])?$region[$data['district']]:'';
        }else{
            $returnData['province'] = '';
            $returnData['city'] = '';
            $returnData['district'] = '';
        }
        $paytype = model('Paytype')->where(['type'=>['in',[1,2]], 'user_id'=>$userId])->column('type,account');
        $returnData['alipayAccount'] = isset($paytype[1])?$paytype[1]:'';
        $returnData['weChatAccount'] = isset($paytype[2])?$paytype[2]:'';
        //获取所有下级
        //$teamsAll = model('common/distribut/DistributTeam')->getUserTeamsAll($userId);
        $returnData['mobile'] = $data['mobile'];
        $returnData['recommendMobile'] = $data['first_leader']?$this->where('user_id', $data['first_leader'])->value('mobile'):'';
        $returnData['nickname'] = $data['nickname'];
        $returnData['underlingNumber'] = $data['underling_number'];
        return $returnData;
    }

    //批量获取用户信息给第三方
    public function supplyGetUserDataList($userIdArr)
    {
        $list = $this->alias('a')
                ->join('region b','b.id=a.province','left')
                ->join('region c','c.id=a.city','left')
                ->join('region d','d.id=a.district','left')
                ->field('b.name as province,c.name as city,d.name as district,a.mobile,a.first_leader,a.nickname,a.underling_number,a.user_id')
                ->where('a.user_id','in', $userIdArr)
                ->select();

        $listFormat = [];
        if($list){
            $firstLeaderArr = get_arr_column($list,'first_leader');
            $firstLeaderList = [];
            if($firstLeaderArr){
                $firstLeaderList = $this->where(['user_id'=>['in', $firstLeaderArr]])->column('user_id,mobile');
            }

            foreach ($list as $user) {
                $returnData = [];
                $paytype = model('Paytype')->where(['type'=>['in',[1,2]], 'user_id'=>$user['user_id']])->column('type,account');
                $returnData['alipayAccount'] = isset($paytype[1])?$paytype[1]:'';
                $returnData['weChatAccount'] = isset($paytype[2])?$paytype[2]:'';
                $returnData['province'] = $user['province'];
                $returnData['city'] = $user['city'];
                $returnData['district'] = $user['district'];
                $returnData['mobile'] = $user['mobile'];
                $returnData['recommendMobile'] = isset($firstLeaderList[$user['first_leader']])?$firstLeaderList[$user['first_leader']]:'';
                $returnData['nickname'] = $user['nickname'];
                $returnData['underlingNumber'] = $user['underling_number'];
                $listFormat[] = $returnData;
            }
        }
        return $listFormat;
    }


}
