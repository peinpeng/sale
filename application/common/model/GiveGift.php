<?php
namespace app\common\model;

use app\common\logic\UserHashLogic;
use think\Db;
use app\common\logic\SlippingUsersLogic;


class GiveGift extends Base
{
    protected $pk = 'id';

    //0：待确认 1已确认 -1已拒绝
    public $_status = [
        0 => '待确认',
        1 => '已确认',
        -1 => '已拒绝'
    ];

    //1:支付宝 2 微信
    public $_paytype = [
        1 => '支付宝',
        2 => '微信',
    ];
    //1自提2快递
    public $_collectType = [];
    public $_deliveryType = [];
    /**
	 * [initialize 初始化]
	 * @return [type] [description]
	 */
	public function initialize(){
		parent::initialize();
		$this->_collectType = [
            1 => lang('Order_order_detail_ziti'),
            2 => lang('Order_order_detail_kuaidi'),
        ];
        $this->_deliveryType = [
            0 => lang('Order_order_detail_weifahuo'),
            1 => lang('Order_order_detail_yifahuo'),
        ];
	}



    public function getStatusNameAttr($value,$row)
    {
        return array_key_exists($row['status'],$this->_status)?$this->_status[$row['status']]:'未知';
    }

    public function getPaytypeNameAttr($value,$row)
    {
        return array_key_exists($row['paytype'],$this->_paytype)?$this->_paytype[$row['paytype']]:'未知';
    }

    public function giveUser(){
        return $this->hasOne('users','user_id','give_user_id');
    }

    public function userAddress(){
        return $this->hasOne('UserAddress','address_id','address_id');
    }

    public function collectUser(){
        return $this->hasOne('users','user_id','collect_user_id');
    }

    public function getMainDesAttr($value,$row)
    {
        $show_name = M('user_level')->where(['level_id'=>$row['give_user_level']+1])->value('show_name');
        return "升级{$show_name}会员申请记录";
    }

    //评论信用记录
    public function getCommentAttr($value,$row)
    {
        return model('Common/credit/CreditUserLog')->logComment($row['id']);
    }

    //complaint_id
    public function getComplaintAttr($value,$row)
    {
        return model('Common/Complaint')->getInfoByGiveGiftId($row['id'],'give_gift');
    }

    public function getCollectTypeChAttr($value,$row)
    {
        return $this->_collectType[$row['collect_type']];
    }

    //后台修改审核状态 status 0 失败 1 成功 
    public function changeStatus($id,$status,$remark='',$uplevel_source='' )    
    {

        //判断
        $chach_id = $this->getZhiTuiGiveGift($id)['id'];
        $key = 'give_gift_'.$chach_id;
        if(cache($key) === false){
            cache($key,1,5);
        }else{
            $this->error = '另一条闯关记录正在审核中，请稍候！';
            return false;
        } 

        $data = [
           'status' => $status ==1?1:-1,
           'confirmtime' => time(),
           'remark' => $remark,
        ];
        // p($data);
        trans();
        $res = $this->where(['id'=>$id])->save($data);
        if(!$res){
            $this->error = 'GiveGift->save:fail';
            trans('rollback');
            cache($key,null);
            return false;
        }

        $order = $this->get($id);
        // $nextUserInfo = model('Common/users/UserLevel')->getLevelInfo($order['give_user_level'] + 1);//获取下个等级信息

        if($status == 1){
            //请求商城生成分红记录
            $this->requestMallBarrier($id);

            $where = [
                'give_user_level' => $order['give_user_level'],
                'give_user_id' => $order['give_user_id'],
                // 'status' => 1,
                'give_type' => $order['give_type']==1?2:1,
            ];
            $order2 = $this->where($where)->order('id desc')->find();

            // p($order2);
            if(empty($order2) || $order2['status'] ==1){
                //更新送礼人等级
                $give_user = M('users')->where(['user_id' => $order['give_user_id']])->find();
                // $level_info = M('user_level')->where('level_id', 'GT', $give_user['level'])->limit(1)->select(); //用户等级信息
                $level_info = M('user_level')->where('level_id', 'GT', $order['give_user_level'])->limit(1)->select(); //用户等级信息

                $next_user_level = $level_info[0]; //下一个等级信息
                if ( $give_user['level'] >= $next_user_level['level_id'] ){
                    $this->error = '当前用户等级已大于或等于闯关等级';
                    trans('rollback');
                    cache($key,null);
                    return false;
                }
                $userLevelLogMdl = model('Common/UserLevelLog');
                $res = $userLevelLogMdl->addData($order['give_user_id'], $next_user_level['level_id'],$uplevel_source);
                if(!$res){
                    $this->error = 'UserLevelLog->addData:fail';
                    trans('rollback');
                    cache($key,null);
                    return false;
                }else{

                    // //等级变更后 用户跟用户的直推人  接点人条件判断
                    // $SlippingUsersLogic = new SlippingUsersLogic;
                    // $res  = $SlippingUsersLogic->slippingRule($order['give_user_id'],'changeLevel');

                    //闯关成功占位规则
                    $placeLogic = model('Common/place/PlaceMatchCompensateLogic','logic');
                    $res = $placeLogic->giveToSuccess($id);
                    if($res['status'] != 1){
                        $this->error = $res['msg'];
                        trans('rollback');
                        cache($key,null);
                        return false;
                    }
                    //成功，请求商城
                    $this->requestMall($order, $status);

                }
            }
        }else{
            //闯关失败占位规则
            $placeLogic = model('Common/place/PlaceMatchCompensateLogic','logic');
            $res = $placeLogic->giveToFail($id);
            if($res['status'] != 1){
                $this->error = $res['msg'];
                trans('rollback');
                cache($key,null);
                return false;
            }
            //失败，请求商城
            $this->requestMall($order, $status);
        }

        (new UserHashLogic())->refreshOne($order['give_user_id']);
        trans('commit');
        cache($key,null);
        return true;    
    }

    /**
     * User: 聂风 Function:请求商城逻辑
     * @param $order 闯关记录
     * @param $status 是否成功状态
     */
    public function requestMall($order, $status)
    {
        $where = [
            'give_user_level' => $order['give_user_level'],
            'give_user_id' => $order['give_user_id'],
        ];
        $order2 = $this->where($where)->order('id desc')->limit(2)->select();
        if(count($order2) == 2){
            if(abs($order2[0]['addtime'] - $order2[1]['addtime']) > 360){
                unset($order2[1]);
            }
        }
        $arr = array_column($order2, 'id');

        if($status == 1){
            $urlSuffix = 'bonusFormal';
        }else{
            $urlSuffix = 'bonusRollback';
        }
        $url = tpCache('retail.api_link').'/api/Barrier/'.$urlSuffix;
        //$url = 'http://shopone.kitnote.com/api/Barrier/'.$urlSuffix;
        $result = httpRequest($url,'POST',['giveGiftId'=>implode(',', $arr)]);
        //var_dump($result);die;
    }

    //请求商城记录
    public function requestMallBarrier($id)
    {
        $url = tpCache('retail.api_link').'/api/Barrier/barrierBonusWalletLog';
        //$url = 'http://shopone.kitnote.com/api/Barrier/barrierBonusWalletLog';
        $result = httpRequest($url,'POST',['giveGiftId'=>$id]);
    }

    //前端修改审核状态
    public function changeStatusByMobile($id,$status,$remark,$collect_user_id=0,$handle_user_id=0)
    {
        $cond  = ['id'=>$id];
        if(!model('Common/UserMobileAdmin')->isMobileAdminUser($handle_user_id)){
            $cond['collect_user_id'] = $collect_user_id;//是否是手机后台管理员 不是加上筛选条件
        }
        $order = $this->where($cond)->find();
        if(!$order){
            $this->error = '没有权限';
            return false;
        }
        return $this->changeStatus($id,$status,$remark,\app\common\model\UserLevelLog::UPLEVEL_SOURCE_APPLY_PASS);//后台修改审核状态 status 0 失败 1 成功 
    }

    public function getDeliveryTypeChAttr($value,$row)
    {
        if ($row['collect_type'] == 2) {
            $ch =  $row['delivery_type'] == 0 ? '未发货' : '已发货';
        }else{
             $ch = '上门自提';
        } 
        return $ch;
        // return $this->_deliveryType[$row['delivery_type']];
    }

    //是否可以 0 不能评论 1评论 2 重新评论
    public function getIscommentAttr($value,$row)
    {
        $credit_config  = tpCache('credit_config');
        $auto_comment = $credit_config['auto_comment'];//超过多少天不能评论
        $off_comment = $credit_config['off_comment'];//超过多少天不能重新评论
        if(($row['collect_type'] == 1 || $row['delivery_type']==1 )&&($row['addtime']+$auto_comment*24*60*60 >= time())){
            $comment = model('Common/credit/CreditUserLog')->logComment($row['id']);
            if($comment){
                //重新评论
                if(!$comment['comment_again'] && ($comment['add_time']+$off_comment*24*60*60 >= time())) return 2;
            }else{
                //评论
                 return 1;
            }
        }
        return 0;
    }

    /**
     * [addData 添加礼品]
     * @param integer $user_id         [description]
     * @param integer $collect_user_id [description]
     * @param integer $isNeedCroos     [description]
     * @param integer $addressId       [description]
     * @param integer $paytype         [description]
     */
    public function addData($user_id = 0,$collect_user_id = 0,$isNeedCroos = 0,$addressId = 0, $paytype = 2)
    {
        $userMdl = model('Common/Users');
        $userLevelMdl = model('Common/users/UserLevel');
        $userInfo = $userMdl->getByPk($user_id);
        $collectUserInfo = $userMdl->getByPk($collect_user_id);
        if(empty($userInfo) || empty($collectUserInfo)) return false;
        $nextLevelInfo = $userLevelMdl->getLevelInfo($userInfo['level'] + 1);

        $money = $nextLevelInfo['gift_amount'];
        $give_type = !empty($isNeedCroos) ? 2 : 1;
        !empty($isNeedCroos) && $money = $nextLevelInfo['need_cross_amount'];
       
        $insertData = [
           'give_user_level' => $userInfo['level'],
           'give_user_id' => $userInfo['user_id'],
           'money' => $money,
           'paytype' => $paytype,
           'collect_user_id' => $collect_user_id,
           'collect_user_level' => $collectUserInfo['level'],
           'status' => 0,
           'addtime' => time(),
           'give_type' => $give_type,
           'collect_type' => 1,
        ];

        if(!empty($addressId)){
            $insertData['collect_type'] = 2;
            $insertData['address_id'] = $addressId;
        }
        // return $result;
        return $this->add($insertData);
    }
    //  /**
    //  * 删除
    //  * @param $id
    //  * @return string
    //  */
    // public function delData($id)
    // {
    //     $res = $this->where(['id'=>$id])->delete();
    //     return $res;
    // }
   

    /**
     * [checkIsGift 是否已经送礼]
     * @param  integer $user_id [description]
     * @return [type]           [description]
     */
    public function checkIsGift($user_id = 0)
    {
        $userInfo = model('Common/Users')->getByPk($user_id);

        if(empty($userInfo)) return false;

        $checkGift = [
            'give_user_level' => (int)$userInfo['level'],
            'give_user_id' => $userInfo['user_id'],
            'status' => ['neq',-1]
        ];

        $nextLevelInfo = model('Common/users/UserLevel')->getLevelInfo($userInfo['level'] + 1);
        
        $isNeedCroos = !empty($nextLevelInfo['need_cross_level']) ? true : false;


        $giftList = $this->getList($checkGift,[],true,[],'addtime DESC',2);

        $status = get_arr_column($giftList['list'],'status');

        $statusSum = array_sum($status);
       
        /**
         * [$isAllAudit 是否都已审核]
         */
        $isAllAudit = $statusSum == 1 ? true : false; 
        
        $isNeedCroos == true && $isAllAudit = ($statusSum == 2) ? true : false; 
       
        /**
         * [$isSendGift 是否申请信息]
         */
        $isSendGift = $giftList['count'] > 0 ? true : false;

        // 闯关  无数据

        /**
         * [$isNeedAudit 是否需要继续闯关]
         */
        $isNeedAudit =  $statusSum = 0 ? true : false;
    
        $isNeedCroos == true && $isNeedAudit = (count($giftList['list']) < 2) ? true : false;
        

        return ['isSendGift'=>$isSendGift,'isAllAudit'=>$isAllAudit,'isNeedAudit'=>$isNeedAudit];

    }




    // //发货凭证上传时间 评分
    // public function checkVoucherPic()
    // {
    //     $day1 =3;
    //     $day2 =7;
    //     $time1 = $day1 * 24 * 60 * 60;
    //     $time2 = $day2 * 24 * 60 * 60;
    //     // dump($time2);
    //     // p(time()-1546845029);
    //     $row1 = $this->where(['confirmtime - addtime'=>['<=',$time1]])->select();
    //     // ps($this,0);
    //     $row2 = $this->where(['confirmtime'=>['eq',0],'addtime'=>['<',time()-$time2]])->select();

        
    //     p(collection($row2)->toArray());

    // }
    
    //主动闯关 id
    public function getZhiTuiGiveGift($id)
    {
        $g = $this->get($id);
        if($g['give_type'] != 1){
            $g = $this->getOrder2($id);
        }
        return $g;
    }
    

    //获取是否有第二闯关申请
    public function getOrder2($id)
    {
        $order = $this->get($id);
        $where = [
            'give_user_level' => $order['give_user_level'],
            'give_user_id' => $order['give_user_id'],
            // 'status' => 1,
            'give_type' => $order['give_type']==1?2:1,
        ];
        $order2 = $this->where($where)->order('id desc')->find();
        return $order2;
    }

    //闯关是否成功
    public function isSuccess($id)
    {
        $g = $this->get($id);
        if($g['status'] != 1) return false;
        $g2 = $this->getOrder2($id);
        if($g2['status'] != 1) return false;
        return true;
    }

    //用户是否在闯关审核中 是 true 否 false
    public function getUserShenHeState($user_id)
    {
        //自己的审核
        $give_gift_count = $this->where(['collect_user_id'=>$user_id,'status'=>0])->count();//闯关待确认数据
        if($give_gift_count>0) return true;

        //同一个闯关，另一个人的审核
        $ids = $this->where(['collect_user_id'=>$user_id])->column('id');
        foreach ($ids as $k => $id) {
            $order2 = $this->getOrder2($id);
            if($order2['status'] != 1) return true;
        }
        return false;
    }


}
