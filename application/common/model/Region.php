<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */
namespace app\common\model;

use think\Db;
use think\Model;

class Region extends Base
{

    /**
     * [getDetailAddress description]
     * @param  integer $addressIds [description]
     * @param  integer $address  [详细地址]
     * @return [type]            [description]
     */
    public function getDetailAddress($addressId = 0)
    {
        $addressInfo = model('Common/UserAddress')->getByPk($addressId);
        $addressArr = model('Common/Region')->getCol('name',['id'=>['in',$addressInfo['province'],$addressInfo['city'],$addressInfo['district'],$addressInfo['twon']]]);
        $detailAddress = !empty($addressArr)?implode('',$addressArr).$addressInfo['address']:$addressInfo['address'];
       
        return $detailAddress;
    }

}
