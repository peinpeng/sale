<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */
namespace app\common\model;

use think\Db;
use think\Model;

class Ad extends Base{
  

    //获取首页弹窗广告
    public function getIndexAd($is_app='')
    {
        $positionId = M('AdPosition')->where(['position_name'=>'首页弹窗广告','is_open'=>1])->value('position_id'); 
        $cond = [
            'pid'=>$positionId,
            'enabled'=>1,
            'start_time'=>['<',time()],
            'end_time'=>['>',time()],
        ];
        //app 格式化
        if($is_app == 1) {
            $adList = M('ad')->where($cond)->field("ad_name as adName,ad_link as adLink,ad_code as adCode")->find();
            if($adList){
                $adList['adName'] = $adList['adName']?:'';
                $adList['adLink'] = $adList['adLink']?:'';
                $adList['adCode'] = $adList['adCode']?SITE_URL.$adList['adCode']:'';
            }
        }else{
            $adList = M('ad')->where($cond)->field("ad_name,ad_link,ad_code")->select();
        }
        $adList = $adList?:(object)null;
        return $adList;
    }


}
