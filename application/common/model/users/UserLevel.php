<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\users;
use app\common\model\Base;
class UserLevel extends Base {

    static $maxLevel = 0;

    static $levelList = [];
    protected $_levelType = [
        1 => '默默无闻',
        2 => '第1关声名不显',
        3 => '第2关小有名气',
        4 => '第3关声明鹊起',
        5 => '第4关名噪一时',
        6 => '第5关赫赫有名',
        7 => '第6关闻名遐迩',
        8 => '第7关大名鼎鼎',
        9 => '第8关威名远播',
        10 => '第9关名震四海',
        11 => '第10关震古烁今',
        12 => '第11关笑看风云',
        13 => '第12关傲视天下',
        14 => '第13关独孤求败',
    ]; 


    public function initialize()
    {
        parent::initialize();
        self::$maxLevel = $this->max('level_id');
        self::$levelList = $this->getCol('*,level_id');
    }
     public function getLevelType()
    {
        return $this->_levelType;
    }
    public function getLevelTypeNameAttr($value,$row)
    {
        return $this->_levelType[$row['level']];
    }

    /**
     * [getMaxLevel 获取当前用户等级表最高等级]
     * @return [type] [description]
     */
    public function getMaxLevel()
    {
        return self::$maxLevel;
    }

    /**
     * [getLevelList 获取用户等级列表数据]
     * @return [type] [description]
     */
    public function getLevelList()
    {
        return self::$levelList;
    }


    /**
     * [getLevelInfo 获取用户等级信息]
     * @param  integer $levelId [description]
     * @return [type]           [description]
     */
    public function getLevelInfo($levelId = 0)
    {
        
        if(empty(self::$levelList)) return [];
        return self::$levelList[$levelId];
    }
     /**
     * 获取升下一级要邀请的等级名称
     */
    public function nextNeedDirectLevelName($level_id){
       return $this->where(['level_id'=>$level_id])->value('show_name'); 
        
    }

}
