<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\users;
use app\common\model\Base;
class UserMoneyLog extends Base {

    public $_type = [
        1   =>   '消费',
        2   =>   '提现',
        3   =>   '返佣',
        4   =>   '驳回提现',
        5   =>   '后台管理员操作',
        6   =>   '充值余额'
    ];
    
    public $_seachType=[
       ['id'=>'1','value'=>'消费'],
      ['id'=>'2,4','value'=>'提现'],
      ['id'=>'3','value'=>'奖金']
    ];
    public function getTypeChAttr($value,$row){
        return $this->_type[$row['type']];
    }
   
    //关联用户模型
    public function users()
    {
        return $this->hasOne('users','user_id', 'user_id')->field('id, money, mobile, nickname, userface');
    }

    /**
     * 录入记录
     * @param $user_id      用户id
     * @param $money        变动金额（负数代表减少金额）
     * @param $after_money  变动后金额
     * @param $type         金额变动类型 1消费 2提现 3返佣 4 '驳回提现'
     * @param $source_table 来源表
     * @param $source_id    来源id
     * @param string $des   变动说明
     * @return int|string
     */
    public function addMoneyLog($user_id, $money, $after_money, $type, $source_table, $source_id, $des = '')
    {
        return $this->insert([
            'user_id'       =>  $user_id,
            'money'         =>  $money,
            'after_money'   =>  $after_money,
            'type'          =>  $type,
            'source_table'  =>  $source_table,
            'source_id'     =>  $source_id,
            'des'           =>  $des,
            'create_time'   =>  time()
        ]);
    }

    // 批量拨款并插入余额记录，用于股东商业模式收益
    public function addMutipleLog($data)
    {
        $memberMdl = model('Common/Member');
        // $memberMoney = $memberMdl->getMemberMoney($data);
        foreach ($data as $key => $value) {
            $user = $memberMdl->getRow($value['benefit_user_id']);
            $after_money = $value['money']+$user['money'];
            $Mupdate['money'] = $after_money;
            $res = M('member')->where('id', $value['benefit_user_id'])->update($Mupdate);
            if ($res) {
                $insertData[]  = [
                    'user_id'       =>  $value['benefit_user_id'],
                    'money'         =>  $value['money'],
                    'after_money'   =>  $after_money,
                    'type'          =>  3,
                    'source_table'  =>  $value['source_table'],
                    'source_id'     =>  $value['source_id'],
                    'des'           =>  '返佣',
                    'create_time'   =>  time()
                ] ;
            }
        }
        if(isset($insertData)){
            return $this->insertAll($insertData);
        }else{
            return;
        }

    }
    
}
