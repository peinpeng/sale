<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\users;
use app\common\model\Base;
class UserWallet extends Base {
        /**
         * 操作用户余额
         * @param string $user_id
         * @param string $money   负数为减去
         * 
         */
        public function opreateUserWallet($user_id='',$money=''){
            $data=[
                'money'=>['exp','`money`+'.$money],
                'user_id'=>$user_id,
                'update_time'=>time(),
            ];
            $userWallet=$this->getBy('user_id',$user_id);
            if($userWallet){
                return $this->where(['user_id'=>$user_id])->save($data);
            }else{
                return $this->add($data);
            }
        }
        public function getWallByUser($user_id){
            return $this->getBy('user_id',$user_id,'money') ? : 0;
        }
    
}
