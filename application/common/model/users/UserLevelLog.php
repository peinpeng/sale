<?php
namespace app\common\model\users;
use think\Db;
use app\common\model\Base;
class UserLevelLog extends Base
{
    
    private $_statusList=[1=>'升级成功'];
    private $_source=[
        'default' => '默认',
    ];

    public function getStatusNameAttr($value,$row){
        return isset($this->_statusList[$row['status']])?$this->_statusList[$row['status']]:'未定义:'.$row['status'];
    }

    public function level(){
        return $this->hasOne('user_level','level_id','level_id');
    }

    public function beforelevel(){
        return $this->hasOne('user_level','level_id','before_level_id');
    }

    public function user(){
        return $this->hasOne('users','user_id','user_id');
    }

     /**
      * 增加升级记录
      * $user_id 用户id
      * $level_id 升级等级id
      * $source   升级来源
      */
    public function addData($user_id="", $level_id="", $uplevel_source="", $description=''){
        // P($uplevel_source);
        if( !$user_id || !isset($level_id) ){
            return false;
        }
        $userMdl = model('Common/Users');
        $user = $userMdl->get($user_id);
        if(!$user){
            $this->error = '用户user_id：'.$user_id.'不存在';
            return false;
        }

        $before_level_id = $user['level'];
        if($level_id<=$before_level_id){
            $this->error = '升级等级不能小于或等于当前等级';
            return false;
        }

        trans();
        $result=$this->add([
            'user_id'         => $user_id,
            'level_id'        => $level_id,
            'before_level_id' => $before_level_id,
            // 'source'          => $source,
            'status'          => 1,
            'add_time'        => time(),
            'uplevel_source'  => (string)$uplevel_source,
            'desc'            => $description,
        ]);
        if(!$result){
            trans('rollback');
            $this->error = 'UserLevelLog->add:fail';
            return false;
        }
        $res = $userMdl->where(['user_id'=>$user_id])->update(['level'=>$level_id]);
        if(!$result){
            trans('rollback');
            $this->error = 'Users->update:fail';
            return false;
        }
        trans('commit');
       return $result;
       
    }

    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }
}
