<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class SlippingUsers extends Base
{

   


    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }

    public function user(){
        return $this->hasOne('users','slipping_id','user_id');
    }

     /**
      * 增加滑落用户记录
      * $user_id 用户id
      * $referrer_id 引荐人id
      * $connecter_id   接点人id
      */
    public function addData($data){
        if( !$data['slipping_id'] || !$data['referrer_id'] || !$data['connecter_id'] ){
            return false;
        }
        $result=$this->add([
            'slipping_id'  => $data['slipping_id'],
            'referrer_id'  => $data['referrer_id'],
            'connecter_id' => $data['connecter_id'],
            'is_slipping'  => $data['is_slipping']==0?0:1,
            'add_time'     => time(),
            'desc'         => $data['desc']?$data['desc']:'',
        ]);
        if(!$result){
            $this->error = 'SlippingUsers->add:fail';
            return false;
        }
       
       return $result;
       
    }
    /**
     * [isSlipping 判断是否为滑落人]
     * @return boolean [description]
     */
    public function isSlipping($user_id){
        $result =$this->where(['slipping_id'=>$user_id,'is_slipping'=>1])->find();
       
        return $result;
    }
     /**
     * [toArrayOne 转为一位数组]
     * $field 要转为一维数组的字段
     * @return boolean [description]
     */
    public function toArrayOne($obj,$field){
        foreach ($obj as $k => $v) {
            $data[] = $v[$field];
        }
       
        return $data;
    }
    /**
     * [isSlipping 获取用户下面的滑落人ID一维数组]
     * @return array[一维数组ID集]
     */
    public function getSlippingUserIds($user_id){
        $data =[];
        $result =$this->where(['connecter_id'=>$user_id,'is_slipping'=>1])->field('slipping_id')->select();
        if($result){
            $result = collection($result)->toArray();
            $data = $this->toArrayOne($result,'slipping_id');
        }
        return $data;
    }

    /**
    * isDirecter 判断该用户是否可以成为引荐人
    * 
    */
    public function isDirecter($referrer_id)
    {
        $usersModel        = model('common/Users');
        $countModel        = model('common/SlippingCount');
        $slippingModel        = model('common/SlippingUsers');
        $slippingConfigMdl = model('common/SlippingConfig');
        $slippingConfig    = $slippingConfigMdl->where(['enable'=>1])->find();
  
        //判断推荐人是否有等级2星或以上的下级队员 
        $ids = $usersModel->where(['first_leader'=>$referrer_id,'level'=>['egt',$slippingConfig['level']]])->field('user_id')->select();
        $ids = $this->toArrayOne($ids,'user_id');
        // p($ids);
        if($ids){
            // //查询下级是否有滑落人
            // $slippingIds = $slippingModel->where(['slipping_id'=>['in',$ids],'is_slipping'=>1])->field('slipping_id')->select();
            // $slippingIds = $slippingIds ? $this->toArrayOne($slippingIds,'slipping_id') : [];

            // //获取直推队员ID (排除滑落人)
            // $connectIds = array_diff($ids,$slippingIds);
            // if(empty($connectIds)) return false;


            // $connecterId = $countModel->where(['connecter_id'=>['in',$connectIds],'enable'=>1])->order('slipping_num asc,direct_num desc,reg_time asc')->value('connecter_id');
            $connecterId = $countModel->where(['connecter_id'=>['in',$ids],'enable'=>1])->order('slipping_num asc,direct_num desc,reg_time asc')->value('connecter_id');
            if(empty($connecterId)) return false;
            return $connecterId;
        }else{
            return false;
        }
    }

    /**
    *添加滑落人并修改相关数据 
    * 
    */
    public function toConnect($connecter_id,$slipping_id,$referrer_id){

        //添加滑落人记录
        $data = [
                'connecter_id' => $connecter_id,   //接点人
                'slipping_id'  => $slipping_id,    //滑落人
                'referrer_id'  => $referrer_id,    //引荐人
                'is_slipping'  => 1,               //是否滑落人
        ];
        trans();
        $res = $this->addData($data);
        if($res === false){
            $this->error='添加滑落人记录失败!';
            trans('rollback');
            return false;
        }

        //更新接点人数据统计表  
        $countModel          = model('common/SlippingCount');
        $slippingConfigMdl   = model('common/SlippingConfig');
        $slippingConfig      = $slippingConfigMdl->where(['enable'=>1])->find();
        $counnectCountDetail = $countModel->where(['connecter_id'=>$connecter_id])->find();

        $slipping_num = $counnectCountDetail['slipping_num'] + 1;  //滑落人数量加一
        $surplus_num  = $counnectCountDetail['surplus_num']  - 1;  //剩余可接点人数减一
        $num          = ($counnectCountDetail['direct_num'] >= $slippingConfig['direct_num']) ? $slippingConfig['high_num'] : $slippingConfig['low_num']; //根据直推人数判断接点人可接数量

        $counnectCount = [
            'slipping_num' => $slipping_num,
            'surplus_num'  => $surplus_num,
            'enable'       => $slipping_num < $num ? 1 : 0,
            'update_time'  => time(),
        ];

        $result = $countModel->where(['connecter_id'=>$connecter_id])->update($counnectCount);
        if($result === false){
            $this->error='更新接点人数据失败!';
            trans('rollback');
            return false;
        }

        trans('commit');
        return true;
   }



   //  /**
   //  *添加滑落人并修改相关数据 
   //  * 
   //  */
   //  public function toConnect1($connecter_id,$slipping_id,$referrer_id){

   //      //添加滑落人记录
   //      $data = [
   //              'connecter_id' => $connecter_id,        //接点人
   //              'slipping_id'  => $slipping_id,         //滑落人
   //              'referrer_id'  => $referrer_id,         //引荐人
   //              'is_slipping'  => 1,                    //是否滑落人
   //              'desc'         => '后台批量插入滑落人',  //是否滑落人
   //      ];
   //      trans();
   //      $res = $this->addData($data);
   //      if($res === false){
   //          $this->error='添加滑落人:'.$slipping_id.'记录失败!';
   //          trans('rollback');
   //          return false;
   //      }

   //      //更新接点人数据统计表  
   //      $countModel          = model('common/SlippingCount');
   //      $slippingConfigMdl   = model('common/SlippingConfig');
   //      $slippingConfig      = $slippingConfigMdl->where(['enable'=>1])->find();
   //      $counnectCountDetail = $countModel->where(['connecter_id'=>$connecter_id])->find();

   //      $slipping_num = $counnectCountDetail['slipping_num'] + 1;  //滑落人数量加一
   //      $surplus_num  = ($counnectCountDetail['surplus_num']  - 1)>0?$counnectCountDetail['surplus_num']  - 1:0;  //剩余可接点人数减一
   //      $num          = ($counnectCountDetail['direct_num'] >= $slippingConfig['direct_num']) ? $slippingConfig['high_num'] : $slippingConfig['low_num']; //根据直推人数判断接点人可接数量

   //      $counnectCount = [
   //          'slipping_num' => $slipping_num,
   //          'surplus_num'  => $surplus_num,
   //          'enable'       => $slipping_num < $num ? 1 : 0,
   //          'update_time'  => time(),
   //      ];

   //      $result = $countModel->where(['connecter_id'=>$connecter_id])->update($counnectCount);
   //      if($result === false){
   //          $this->error='更新接点人数据失败!';
   //          trans('rollback');
   //          return false;
   //      }

   //      trans('commit');
   //      return true;
   // }
   /**
    *判断用户是否可以成为接点人 是则添加接点人数据
    * $from 判断是从注册 还是帮他人注册 还是更改上下级
    */
    public function changeUserToConnecter($uid,$from='')
    {
        $slippingUsersModel = model('common/SlippingUsers');
        $countModel         = model('common/SlippingCount');
        $usersModel         = model('common/Users');
        $slippingConfigMdl  = model('common/SlippingConfig');

        $slippingConfig  = $slippingConfigMdl->where(['enable'=>1])->find();
        $userInfo        = $usersModel->where(['user_id'=>$uid])->find();           //用户资料


        //查询用户是否已经是接点人
        $connecterInfo   =  $countModel->where(['connecter_id'=>$uid])->find(); 

        if($connecterInfo && ($from == 'regForOther' || $from == 'changeFirstLeader' || $form == 'reg')) {
            //增加直推人数
            $num = $connecterInfo['direct_num']+1;
            //直推人数增加是否达到直推人数可接滑落人数限制条件 达到则增加可接点数量
            $totalNum = $connecterInfo['slipping_num']+$connecterInfo['surplus_num'];
            
            if (($totalNum < $slippingConfig['high_num']) && ($num >= $slippingConfig['direct_num'])){
                //获取到达直推人数限制条件的可接点数量
                $surplus_num   = $slippingConfig['high_num'];
                //更新剩余可接点人数
                $newSurplusNum = $surplus_num - $connecterInfo['slipping_num'];
                $time   = time();
                $result = $connecterInfo['enable']==0? $countModel->where(['connecter_id' => $uid])->update(['direct_num' => $num,'surplus_num' => $newSurplusNum,'update_time' => $time,'enable'=>1]):$countModel->where(['connecter_id' => $uid])->update(['direct_num' => $num,'surplus_num' => $newSurplusNum,'update_time' => $time]);
            }else{
                $result = $countModel->where(['connecter_id' => $uid])->update(['direct_num' => $num,'update_time' => $time]);
            }
            return $result;
        }

        //后台修改接点用户等级
        if($connecterInfo && ($from == 'changeLevelForPC' || $from == 'changeLevelForMobile')){

            if($userInfo['level'] < $slippingConfig['level']){
                $res = $countModel->where(['connecter_id' => $uid])->update(['enable' => 0,'update_time' => time()]);
            }else{
                if($connecterInfo['direct_num'] >= $slippingConfig['team']){
                    $surplus_num = $connecterInfo['direct_num'] >= $slippingConfig['direct_num'] ? $slippingConfig['high_num'] : $slippingConfig['low_num'];
                    $newSurplusNum = $surplus_num - $connecterInfo['slipping_num'];
                    //剩余可接点数大于0则更改enable为1
                    $res = $newSurplusNum>0? $countModel->where(['connecter_id' => $uid])->update(['enable'=>1,'update_time' => time()]):'';
                }
            }

            if(!$res) return false;
            //用户的直推上级 是否符合接点人条件操作
            $result = $this->firstLeaderIsConnecter($uid);
            if(!$result){
            //     return true;
            // }else{
                $this->error='修改数据失败,或上级不符合接点人条件!';
                return false;
            }
            return true;
        }

        //用户是否成为接点人
        $res = $this->isConnecter($uid);
        if($res ===false){
            $this->error='修改数据失败,或用户不符合接点人条件!';
            return false;
        }

        //用户的上级是否可以成为接点人  
        $result = $this->firstLeaderIsConnecter($uid);
        if($result){
            return true;
        }else{
            $this->error='修改数据失败,或上级不符合接点人条件!';
            return false;
        }

        return false;
    }

    /**
    *用户直推数量变少后的可接点数量更改
    * 
    */
    public function getDirectNum($uid)
    {
        $countModel         = model('common/SlippingCount');
        $slippingConfigMdl  = model('common/SlippingConfig');

        $slippingConfig    = $slippingConfigMdl->where(['enable'=>1])->find();

        //查询用户是否已经是接点人
        $connecterInfo     =  $countModel->where(['connecter_id'=>$uid])->find(); 
        // p($connecterInfo);
        if($connecterInfo) {

            //减少直推人数
            $num = $connecterInfo['direct_num']-1;

            //直推人数减少是否达到直推人数可接滑落人数限制条件 未达到则减少可接点数量
            if($num<$slippingConfig['team']){

                $result = $countModel->where(['connecter_id' => $uid])->update(['direct_num' => $num,'surplus_num' => 0,'update_time' => time(),'enable'=>0]);
            }

            if(($num < $slippingConfig['direct_num']) && ($connecterInfo['direct_num'] >= $slippingConfig['direct_num'])){

                $surplus_num = $slippingConfig['low_num']-$connecterInfo['slipping_num'];

                $res = $connecterInfo['slipping_num']>=$slippingConfig['low_num'] ? $countModel->where(['connecter_id'=>$uid])->update(['direct_num'=>$num,'surplus_num'=>0,'update_time'=>time(),'enable'=>0]):$countModel->where(['connecter_id' => $uid])->update(['direct_num' => $num,'surplus_num' => $surplus_num,'update_time' => time()]);
                if($res){
                    return true;
                }else{
                    $this->error='修改数据失败!';
                    return false;
                }
            }
        }
       
    }
     /**
    *用户升级后 接点人资格判断
    * 
    */
    public function isConnecter($uid)
    {
        $countModel         = model('common/SlippingCount');
        $usersModel         = model('common/Users');
        $slippingConfigMdl  = model('common/SlippingConfig');
        $slippingUsersModel = model('common/SlippingUsers');

        $slippingConfig  = $slippingConfigMdl->where(['enable'=>1])->find();
        $userInfo        = $usersModel->where(['user_id'=>$uid])->find();           //用户资料
        $connecterInfo   =  $countModel->where(['connecter_id'=>$uid])->find(); 


        //判断用户的直推下级数量是否达到接点人直推人数要求                                   
        $firstLeaderNum    = $usersModel->where(['first_leader'=>$uid,'level'=>['egt',$slippingConfig['team_member_level']]])->count();  //用户users表等级大于等于1星的下级数量
        $referrerNum       = $this->getNumForReferrer($uid);

        if ( ($firstLeaderNum +$referrerNum)>= $slippingConfig['team'] && $userInfo['level'] >= $slippingConfig['level'] ){   

            //根据直推数量限制获取用户可接点数量
            $surplus_num = ($firstLeaderNum +$referrerNum) >= $slippingConfig['direct_num'] ? $slippingConfig['high_num'] : $slippingConfig['low_num'];
            $newSurplusNum = $surplus_num - $connecterInfo['slipping_num'];

            //接点用户数据
            $connecterData = [
                'surplus_num'  => $newSurplusNum,
                'direct_num'   => $firstLeaderNum+$referrerNum,
                'enable'       => $newSurplusNum >0 ? 1 : 0,
                'update_time'  => time(),

            ];

            //非接点用户数据
            $data = [
                'connecter_id' => $uid,
                'surplus_num'  => $surplus_num,
                'direct_num'   => $firstLeaderNum+$referrerNum,
                'enable'       => 1,
                'reg_time'     => $userInfo['reg_time'],

            ];

            $res = $connecterInfo ? $countModel->where(['connecter_id'=>$uid])->update($connecterData) : $countModel->addData($data);
            if($res){
                return true;
            }else{
                $this->error='修改数据失败!';
                return false;
            }
        }
       
    }

    /**
     * 用户的直推上级接点人判断
     */
    public function firstLeaderIsConnecter($uid)
    {
        $slippingUsersModel = model('common/SlippingUsers');
        $usersModel         = model('common/Users');

        $userInfo = $usersModel->where(['user_id'=>$uid])->find();   //用户资料

        //用户的直推上级 是否符合接点人条件操作
        $slippingUserInfo = $slippingUsersModel->where(['slipping_id'=>$uid,'is_slipping'=>1])->find();
        $result = $slippingUserInfo['referrer_id'] ? $this->isConnecter($slippingUserInfo['referrer_id']) : $this->isConnecter($userInfo['first_leader']);
        return $result;

    }

     /**
     * 用户等级改变 获取引荐人直推等级>=2的滑落人数
     */
    public function getNumForReferrer($uid)
    {
        $slippingUsersModel = model('common/SlippingUsers');
        // $usersModel         = model('common/Users');
        $slippingConfigMdl  = model('common/SlippingConfig');

        $slippingConfig  = $slippingConfigMdl->where(['enable'=>1])->find();


        $num = $slippingUsersModel
            ->alias('c')
            ->join('users u', 'c.slipping_id=u.user_id')
            ->where(['c.referrer_id' => $uid,'u.level'=>['egt',$slippingConfig['team_member_level']]])
            ->count();

        return $num;
    }


    
}
