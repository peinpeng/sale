<?php
namespace app\common\model;
use app\common\model\Users;
use think\Db;
use app\common\model\Base;
class TemporaryClickLocking extends Base
{
    public function user()
    {
        return $this->hasOne('app\common\model\Users','user_id','user_id');
    }
    //获取次数
    public function getTimes($user_id)
    {
        $times = $this->where(['user_id'=>$user_id])->count();
        $times = $times>0?$times:0;
        return $times;
    }


    
    public function addData($user_id,$session_id)
    {
        if( !$user_id  ||!$session_id ){
            return false;
        }
        $result=$this->add([
            'user_id'     => $user_id,
            'add_time'    => time(),
            'session_id'    => $session_id,
        ]);
        if(!$result){
            $this->error = 'TemporaryClickLocking->add:fail';
            return false;
        }


        return $result;
    }

    //添加
    public function isAddLockNum($user_id,$session_id)
    {
        if( !$user_id  ||!$session_id ){
            return false;
        }
        $info = $this->where(['user_id'=>$user_id,'session_id'=>$session_id])->find();
        if($info) return true;
        $res = $this->addData($user_id,$session_id);
        return $res;
    }
    
}
