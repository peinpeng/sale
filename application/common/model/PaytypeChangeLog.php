<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */
namespace app\common\model;

use think\Model;

class PaytypeChangeLog extends Model
{
	public function addLog($uid,$type,$beforeAccount='',$afterAccount,$desc='')
	{
		if( !$uid || !in_array($type, [1,2]) || !$afterAccount ){
            return false;
        }
        $result=$this->add([
            'user_id'        => $uid,
            'before_account' => $beforeAccount?:'',
            'after_account'  => $afterAccount,
            'desc'           => $desc,
            'add_time'      => time(),
            'type'          => $type,
        ]);
        if(!$result){
            $this->error = 'PaytypeChangeLog->add:fail';
            return false;
        }
       
       return $result;



	}
}
