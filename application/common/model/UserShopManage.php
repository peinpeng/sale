<?php
/**
 * ============================================================================
 * 个人商城管理  
 * ============================================================================
 */
namespace app\common\model;
use think\Model;
class UserShopManage extends Model {

    public function getInfo($user_id)
    {
        $info = $this->where(['user_id'=>$user_id])->find();
        if(!$info){
            $this->add(['user_id'=>$user_id,'add_time'=>time()]);
            $info = $this->where(['user_id'=>$user_id])->find();
        }
        return $info;
    }

    //单条
    public function getFormAndEshop($user_id,$scene)
    {
        $url = tpCache('retail.api_link').'/child/Thirdapi/getFormAndEshop';
        // p(httpRequest($url,'POST',['uid'=>$user_id,'scene'=>$scene]));
        $res = json_decode(httpRequest($url,'POST',['uid'=>$user_id,'scene'=>$scene]),true);
        return $res;
    }

    //批量获取用户展示链接
    public function getChildShopLink($user_ids)
    {
        $data = [];

        //键名必须存在
        $param = [
            0 => [],
            1 => [],
            2 => [],
        ];
        foreach($user_ids as $v){
            if(!$v) continue;
            $info = $this->getInfo($v);
            if($info['scene']==0 OR $info['scene']==1 OR $info['scene']==2){
                $param[$info['scene']][] = $v;
            }
            if($info['scene'] == 3){
                $data[$v] = ['link'=>$info['url'],'title'=>'第三方链接'];
            }
        }
        if(!empty($param)){
            $url = tpCache('retail.api_link').'/child/Thirdapi/getChildShopLink';
            $res = json_decode(httpRequest($url,'POST',['param'=>json_encode($param)]),true);
            if($res['code'] == 10000){
                foreach ($res['result'] as $user_id => $value) {
                    $data[$user_id]['link'] = jump_stitch($value['link']);
                    $data[$user_id]['title'] = $value['name']?:$value['title'];
                    if($data[$user_id]['title'] == '进入子商城'){
                        unset($data[$user_id]);
                    }
                }
            }
        }
        return $data;
    }
}
