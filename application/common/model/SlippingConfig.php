<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class SlippingConfig extends Base
{
    
    
    private $_change_sourse=[
        1 => '操作后台会员详情页修改',
        2 => '操作手机端后台修改',

    ];

    public function user(){
        return $this->hasOne('users','user_id','user_id');
    }
    public function levelConfig(){
        return $this->hasOne('user_level','level_id','level');
    }

     /**
      * 增加滑落用户记录
      * 
      */
    public function addData(){
        if( !$user_id || !isset($after_first_leader_id) ){
            return false;
        }
        $result=$this->add([
            'user_id'                => $user_id,
            'after_first_leader_id'  => $after_first_leader_id,
            'before_first_leader_id' => $before_first_leader_id,
            'change_sourse'          => $change_sourse,
            'status'                 => 1,
            'change_time'            => time(),
            'desc'                   => $description?:$this->_change_sourse[$change_sourse],
        ]);
        if(!$result){
            $this->error = 'ChangeFirstLeaderLog->add:fail';
            return false;
        }
       
       return $result;
       
    }

    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }
}
