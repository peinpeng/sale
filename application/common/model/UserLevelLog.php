<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class UserLevelLog extends Base
{
    
    private $_statusList=[1=>'升级成功'];
    // private $_source=[
    //     'default' => '默认',
    // ];
    private $_uplevel_source=[
        1 => '申请闯关审核通过修改',
        2 => '操作后台会员详情页修改',
        3 => '操作手机端后台修改',

    ];
    const UPLEVEL_SOURCE_APPLY_PASS  = 1;
    const UPLEVEL_SOURCE_ADMIN_PASS  = 2;
    const UPLEVEL_SOURCE_MOBILE_PASS = 3;

    public function getStatusNameAttr($value,$row){
        return isset($this->_statusList[$row['status']])?$this->_statusList[$row['status']]:'未定义:'.$row['status'];
    }

    public function getUplevelSourceAttr($value,$row){
        return $this->_uplevel_source[$row['uplevel_source']];
    }
    public function getUplevelSource(){
        return $this->_uplevel_source;
    }

    public function level(){
        return $this->hasOne('user_level','level_id','level_id');
    }

    public function beforelevel(){
        return $this->hasOne('user_level','level_id','before_level_id');
    }

    public function user(){
        return $this->hasOne('users','user_id','user_id');
    }

     /**
      * 增加升级记录
      * $user_id 用户id
      * $level_id 升级等级id
      * $source   升级来源
      */
    public function addData($user_id="", $level_id="", $uplevel_source = "", $description=''){
        // P($uplevel_source);
        if( !$user_id || !isset($level_id) ){
            return false;
        }
        $userMdl = model('Common/Users');
        $user = $userMdl->get($user_id);
        if(!$user){
            $this->error = '用户user_id：'.$user_id.'不存在';
            return false;
        }

        $before_level_id = $user['level'];
        if ( $level_id<=$before_level_id && $uplevel_source == 1){
            $this->error = '升级等级不能小于或等于当前等级';
            return false;
        }

        trans();
        $result=$this->add([
            'user_id'         => $user_id,
            'level_id'        => $level_id,
            'before_level_id' => $before_level_id,
            'uplevel_source'  => $uplevel_source,
            'status'          => 1,
            'add_time'        => time(),
            // 'uplevel_source'  => (string)$uplevel_source,
            'desc'            => $description?:$this->_uplevel_source[$uplevel_source],
        ]);
        if(!$result){
            trans('rollback');
            $this->error = 'UserLevelLog->add:fail';
            return false;
        }
        $res = $userMdl->where(['user_id'=>$user_id])->update(['level'=>$level_id]);
        if($res === false){
            trans('rollback');
            $this->error = 'Users->update:fail';
            return false;
        }
       
        trans('commit');

        //等级改变 占位
        $logic = model('Common/place/PlaceMatchCompensateLogic','logic');
        $res = $logic->savePlace($user_id);
       


       return $result;
       
    }

    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }

    //用户等级变动是否通过一星节点
    public function isPassLevel1($user_id){
        $row = $this->where(['user_id'=>$user_id])->order('id desc')->find();
        if($row['level_id'] >= 2 && $row['before_level_id'] < 2){
            return true;
        }
        return false;
    } 


    //第一次升星时间
    public function getLineupTime($user_id){
        $cond = [];
        $cond =[
            'user_id' => $user_id,
            'before_level_id' => 1,
            'status' => 1,
        ];
        $add_time = $this->where($cond)->order('add_time ASC')->value('add_time');
        return $add_time;
    }

}
