<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\model;

use think\Db;
use think\Model;

class Paytype extends Base
{
    /**
     * User: Simony
     * Function:添加 or 修改
     * @param $user_id
     * @param $data
     */
    public function addOrSave($user_id,$type, $data)
    {
        $paytype = $this->where(['user_id' => $user_id,'type'=>$type])->find();
        $data['bind_time']=time();
        if (!empty($paytype)){
            return $this->where(['user_id' => $user_id,'type'=>$type])->save($data);
        }else{
            $data['user_id']=$user_id;
            return $this->add($data);
        }
    }
     /**
     * User: Simony
     * Function:添加 or 修改
     * @param $user_id
     * @param $data
     */
    public function addDataReg($user_id,$type, $data)
    {
        $paytype = $this->where(['user_id' => $user_id,'type'=>$type])->find();
        if ($paytype && ($paytype['account'] != $data['account']) ) {
            return false;
        }
        $data['bind_time']=time();        
        $data['user_id']=$user_id;
        return $this->add($data);
        
    }
}
