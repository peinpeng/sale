<?php
/**
 * 
 * ============================================================================
 * 版权所有  圣火科技，并保留所有权利。
 * 网站地址: https://www.tkkkc.com/
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author:wq
 * Date:2018-12-19
 */
namespace app\common\model\nav;
use think\Model;
use app\common\model\Base;
class NavPosition extends Base {
    public function getAllPosition(){
        return $this->where(['is_del'=>0])->order('sort')->column('id,name');
    }
}
