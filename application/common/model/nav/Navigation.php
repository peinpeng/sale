<?php
/**
 * 
 * ============================================================================
 * 版权所有  圣火科技，并保留所有权利。
 * 网站地址: https://www.tkkkc.com/
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author:wq
 * Date:2018-12-19
 */
namespace app\common\model\nav;
use think\Model;
use app\common\model\Base;
class Navigation extends Base {
       public function navPosition(){
           return $this->hasOne('NavPosition','id','position');
       }
       public function getNavigationByPosition($positionIds=0){
           $where=['position'=>$positionIds,'is_show'=>1];
           $list = $this->getList($where,[],false,[],'sort asc');
           foreach ($list as $k => $v) {
               if(session('user') && preg_match('/^(?:http[s]?:)?\/\//i',$v['url'])){
                   $list[$k]['url'] = jump_stitch($v['url']);
               }
           }
           return $list;
       }
      public function getAdLinkWithToken(){
        $loginToken = trim(I('loginToken')); 
        $adList = M('ad')->where(['pid' => 1,'enabled'=>1])->select(); //轮播广告 pid是1
          foreach ($adList as $k => $v) {
              if($loginToken && preg_match('/^(?:http[s]?:)?\/\//i',$v['ad_link'])){
                  $adList[$k]['ad_link'] = jump_stitch_app($v['ad_link']);
              }
          }
        return $adList;
       }
       
    /**
     * 获取指定一条数据
     * @param array   $where    条件
     * @return obj
     * @author Jaywoo
     */
    public function getInfo($where=[])
    {
        $info = $this->where($where)->find();
        if($info)  $info['guide_imgs'] = explode(',', $info['guide_imgs']);
        return $info;
    }

     /**
     * app获取单条导航的引导图
     * @param array   $where    条件
     * @return obj
     * @author Jaywoo
     */
    public function getNavGuideImg($guide_id)
    {   
        $where = [];
        $list  = [];
        $where['is_guide'] = 1;
        switch ($guide_id) {
          //首页导航引导
          case '1':
            $where['id'] = 32;
            break;
          //分享拓客导航引导
          case '2':
            $where['id'] = 35;
            break;
          //个人中心导航引导
          case '3':
            $where['id'] = 31;
            break;
          //拓客社交导航引导
          case '4':
            $where['id'] = 30;
            break;
          //拓客直播导航引导
          case '5':
            $where['id'] = 29;
            break;
          //拓客商学院导航引导
          case '6':
            $where['id'] = 28;
            break;
          //全网拓客导航引导
          case '7':
            $where['id'] = 33;
            break;
          //申请闯关导航引导
          case '8':
            $where['id'] = 25;
            break;
          //审核闯关导航引导
          case '9':
            $where['id'] = 26;
            break;
          //行会大厅导航引导
          case '10':
            $where['id'] = 27;
            break;
          //全国优品导航引导
          case '11':
            $where['id'] = 34;
            break;
          
          default:
            return $list;
            break;
        }
        $guide_imgs = $this->where($where)->value('guide_imgs');
        if($guide_imgs){
          $guide_imgs_list = explode(',', $guide_imgs);
          foreach ($guide_imgs_list as $k => $v) {
            $list[$k] =SITE_URL.$v;
          }
        }
        return $list;
    }


}
