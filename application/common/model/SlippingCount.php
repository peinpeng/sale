<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class SlippingCount extends Base
{
    
   
    public function user(){
        return $this->hasOne('users','user_id','connecter_id');
    }

     /**
      * 增加用户的已接点人数和可接点人数及引荐人数
      * $connecter_id 用户id
      * $slipping_num 接点人数
      * $surplus_num  可接点人数
      * $referrer_num 引荐人数
      */
    public function addData($data){
        if( !$data['connecter_id'] ){
            return false;
        }
        $result=$this->add([
            'connecter_id'  => $data['connecter_id'],
            'slipping_num'  => 0,
            'surplus_num'   => $data['surplus_num'],
            'direct_num'    => $data['direct_num'],
            'add_time'      => time(),
            'enable'        => $data['enable'],
            'reg_time'      => $data['reg_time'],
        ]);
        if(!$result){
            $this->error = 'SlippingCount->add:fail';
            return false;
        }
       
       return $result;
       
    }




     /**
      * 增加用户的已接点人数和可接点人数及引荐人数
      * $connecter_id 用户id
      * $slipping_num 接点人数
      * $surplus_num  可接点人数
      * $referrer_num 引荐人数
      */
    public function addData1($data){
        if( !$data['connecter_id'] ){
            return false;
        }
        $result=$this->add([
            'connecter_id'  => $data['connecter_id'],
            // 'slipping_num'  => 0,
            'surplus_num'   => $data['surplus_num'],
            // 'direct_num'    => $data['direct_num'],
            'add_time'      => time(),
            'enable'        => $data['enable'],
            'reg_time'      => $data['reg_time'],
        ]);
        if(!$result){
            $this->error = 'SlippingCount->add:fail';
            return false;
        }
       
       return $result;
       
    }

    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }
}
