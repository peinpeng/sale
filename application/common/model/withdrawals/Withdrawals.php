<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\withdrawals;
use app\common\model\Base;
class Withdrawals extends Base {
    public function addData($data=[]){
        $userAccount = model('common/Withdrawals/WithdrawalAccount')->getBy('user_id',$data['user_id']);
        $data['bank_name'] = $userAccount['bank_name'] . ' ' . $userAccount['sub_branch'];
        $data['bank_card'] = $userAccount['bank_card'];
        $data['realname'] = $userAccount['name'];
        $data['create_time'] = time();
        return $this->add($data);
    }
    /**
     * 更新审核状态
     * @param unknown $id
     * @param unknown $status  
     */
    public function updateData($id,$status,$uplodePic=""){
      
        $updateData=['status'=>$status,'check_time'=>time()];
        if($status==-1 || $status==3){
            $updateData['refuse_time']=time();
        }
        $uplodePic &&  $updateData['upload_pic']=$uplodePic;
       return M('withdrawals')->where(['id'=>$id])->save($updateData);
    }
}
