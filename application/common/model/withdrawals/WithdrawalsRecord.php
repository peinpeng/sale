<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\withdrawals;
use app\common\model\Base;
class WithdrawalsRecord extends Base {
    public function addData($w_id,$status,$admin_id,$remark,$upload_pic,$isRefuse=2){
        $data['w_id'] = $w_id;
        $data['status'] = $status;
        $data['remark'] = $remark;
        $data['admin_id'] = $admin_id;
        $data['upload_pic'] = $upload_pic;
        $data['add_time'] = time();
        $data['is_refuse'] = $isRefuse;
        return $this->insert($data);
    }
 
}
