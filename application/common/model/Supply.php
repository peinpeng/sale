<?php
/**
 * Created by PhpStorm.
 * User: liangjianbiao
 * Date: 2019/9/12
 * Time: 14:39
 */
namespace app\common\model;

use think\Model;

class Supply extends Model
{
    //获取api_secret
    public function getApiSecret($apiKey)
    {
        return $this->where('api_key', $apiKey)->value('api_secret');
    }

}