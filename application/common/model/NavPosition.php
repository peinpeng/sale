<?php
/**
 * Author: xLiang
 * Date: 2018-06-01
 */
namespace app\common\model;
use think\Model;
class NavPosition extends Model {
    /**
     * 列表
     * @param array   $where    条件
     * @param int     $p        页数
     * @param int     $listRows 每页数量
     * @param string  $order    排序
     * @param array   $with     关联表
     * @return obj
     * @author xLiang
     */
    public function lists($where=[],$p=1,$listRows=10,$order='id DESC',$with=[])
    {
        return $this->where($where)->with($with)->order($order)->page($p,$listRows)->select();
    }

    /**
     * 获取个别字段的数组
     * @param array         $where    条件
     * @param string        $column   字段
     * @param string        $order    排序
     * @return obj
     * @author xLiang
     */
    public function getNavPositionRow($where=[],$column='id,name,create_time',$order='sort ASC,id DESC')
    {
        if(!isset($where['is_show'])) $where['is_show'] = 1; //默认 显示
        if(!isset($where['is_del']))  $where['is_del']  = 0; //默认 非删除

        return $this->where($where)->order($order)->column($column);
    }

    /**
     * 获取指定一条数据
     * @param array   $where    条件
     * @return obj
     * @author xLiang
     */
    public function getInfo($where=[])
    {
        return $this->where($where)->find();
    }

}
