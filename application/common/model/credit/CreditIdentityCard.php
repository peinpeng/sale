<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\model\credit;

use think\Model;
use app\common\model\Base;
use app\common\logic\UserHashLogic;

class CreditIdentityCard extends Base {
    //自定义初始化
    protected static function init()
    {
    }
     protected $_processes=[
        // 'none'    => '未认证',
        'pending' => '待审核',
        'invalid' => '无效',
        'success' => '已认证',
    ];
    protected $_verifyStatus=[
        'valid'      => '有效证件',
        'invalid'    => '无效证件',
        'unverified' => '未验证',
    ];
    protected $area_type=[
        '1'  => '大陆',
        '2'  => '台湾',
        '3'  => '香港',
        '4'  => '澳门'
    ];
    public function processList(){
        return $this->_processes;
    }
    public function getProcessChAttr($value,$row){
        return $this->_processes[$row['process']];
    }
    public function getAreaTypeNameAttr($value,$row){
        return $this->area_type[$row['area_type']];
    }

    /**
     * [getCreditIdentityCardList 获取实名认证列表]
     * @return [type] [description]
     */
    public function getCreditIdentityCardList($cond='',$Page,$field='*',$order=''){
    	
    	$rows = $this->alias('c')->where($cond)->join('Users u', 'c.user_id = u.user_id')->field($field)->limit($Page->firstRow,$Page->listRows)->order($order)->select();
        foreach ($rows as $key => $value) {
            //审核状态转中文
            $value['process'] = $this->_processes[$value['process']]; 
        }
    	return $rows;
    }

    /**
     * [getCreditIdentityCardList 获取实名认证列表count]
     * @return [type] [description]
     */
    public function getCreditIdentityCardCount($cond=''){
        
        $count = $this->alias('c')->where($cond)->join('Users u', 'c.user_id = u.user_id')->count();
       
        return $count;
    }
     /**
     * 根据用户ID获取用户身份认证信息
     *
     * @param integer $userId
     */
    public function getUserIdentityDetail($userId){
        $info = $this->where(['user_id'=>$userId])->find();
        return $info;
    }

     /**
     * 身份证是否被其他人认证过
     * @Author   Henson
     * @DateTime 2018-11-30
     */
    function certified_status($true_name = '', $card_number = '', $user_id = 0) {
        $where['true_name'] = $true_name;
        $where['card_number'] = $card_number;
        $where['user_id'] = ['neq', $user_id];
        $where['process'] = ['exp',"in('success','pending')"];
        $cert_count = $this->where($where)->count();
        if ($cert_count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据用户ID设置身份认证状态
     *
     * @param integer $userId
     * @param string $process
     * @return boolean
     */
    public function setProcess($userId,$process){
        if(!isset($this->_processes[$process])){
            $this->error='状态值“'.$process.'”无效！仅仅支持：'.implode('、',array_keys($this->_processes));
            return false;
        }
        if(!$userId){
            $this->error='user_id为空';
            return false;
        }
        $data=[];
        $data['process'] = $process;
        $data['process_time']=time();

        $result=$this->where(['user_id'=>$userId])->save($data);
        if($result===false){
            $this->error = 'CreditIdentityCard->save:fail';
            return false;
        }
        (new UserHashLogic())->refreshOne($userId);
        return $result;
    }



    /**
     * 身份证前端展示（隐藏后面6位）
     * @param $id_card
     * @return string
     */
    public function showIdCard($id_card)
    {
        $newStr = substr($id_card, 0, -6);
        return str_repeat('*', strlen($newStr)) . substr($id_card, -6, 6);
    }

    /**
     * 添加
     *
     * @param array   $row 数据
     * @return boolean|
     */
    public function addData($row){
        if(!isset($row['process'])||!isset($this->_processes[$row['process']])) $row['process']='pending';
        $row['user_id']=$row['user_id']*1;
        $data=[
            'user_id'             => $row['user_id'],
            'true_name'           => trim($row['true_name']),
            'card_number'         => trim($row['card_number']),
            'card_front_photo'    => trim($row['card_front_photo']),
            'card_back_photo'     => trim($row['card_back_photo']),
            'card_handheld_photo' => isset($row['card_handheld_photo'])?trim($row['card_handheld_photo']):"",
            'process'             => trim($row['process']),
            'verify_status'       => isset($row['verify_status'])?trim($row['verify_status']):"unverified",
            'create_time'         => time(),
            'update_time'         => time(),
            'process_time'        => time(),
            'area_type'           => $row['area_type']
        ];
        $oldData=$this->where(['user_id'=>$data['user_id']])->find();
        if($oldData){
            if($oldData['process']=='success' && !empty($oldData['card_front_photo'])){
                $this->error='您已经认证过了';
                return false;
            }
        }
        if($data['user_id']<1){
            $this->error='登录信息无效，user_id获取失败，请重新登录';
            return false;
        }
        if(empty($data['true_name'])){
            $this->error='请输入真实姓名';
            return false;
        }
        if(empty($data['card_number'])){
            $this->error='请输入身份证号码';
            return false;
        }
        // if(!is_idcard($data['card_number'])){
        //     $this->error='请输入有效的身份证号码';
        //     return false;
        // }
        if(empty($data['card_front_photo'])){
            $this->error='请上传身份证正面照片';
            return false;
        }
        if(empty($data['card_back_photo'])){
            $this->error='请上传身份证背面照片';
            return false;
        }
        // if($row['scene'] == 1 && empty($data['card_handheld_photo'])){
        //     $this->error='请上传手持身份证的半身照';
        //     return false;
        // }

        //如果身份证号码认证过了，再用其他的账号认证直接给通过
        if($this->where(['card_number'=>$data['card_number'],'process'=>'success'])->count()>0){
            $data['process'] = 'success';
            $data['process_time'] = time();
        }
        $userId = $data['user_id'];
        if($oldData){
            $data['update_time']=$data['create_time'];
            if($oldData['create_time']>0) unset($data['create_time']);//已经设置过申请时间的情况下，不用再次设置
            unset($data['user_id'],$data['process_time']);
            $result=$this->where(['id'=>$oldData['id']])->save($data);
        }else{
            $result=$this->add($data);
        }
        (new UserHashLogic())->refreshOne($userId);
        if($result===false) return false;
        

        return $result;
    }

    public function test(){
        // p(23);
    }

    /**
     * 身份证唯一性验证
     * @param $id_card
     * @return string
     */
    public function checkIdCardNum($card_number)
    {
        $res = $this->where(['card_number'=>$card_number,'process'=>['in',['success','pending']]])->find();
        return $res;
    }

    /**
     * 身份证地区验证
     * @param $id_card
     * @return string
     */
     public function regionalValidation($id){
        $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
        $rex1= "/[A-Z]{1,2}[0-9]{6}([0-9A])/" ;  //香港身份证
        $rex2=  "/^[1|5|7][0-9]{6}([0-9Aa])/"; //澳门身份证
        $rex3= "/[A-Z][0-9]{9}/";         //台湾身份证
        $code = -1;
        $area_type = 0;
        
        // p(preg_match($rex2, $id));
        if(preg_match($regx, $id) && is_idcard($id))
        {
          $area_type = 1;
          $code = 1;
        }
        elseif(preg_match($rex1, $id) && strlen($id)==8)
        {
          $area_type = 3;
          $code = 1;
        }
        elseif(preg_match($rex2, $id) && strlen($id)==8)
        {
          $area_type = 4;
          $code = 1;
        }
        elseif(preg_match($rex3, $id) && is_idcard_tw($id))
        {
            // p(2222);
          $area_type = 2;
          $code = 1;
        }
        return ['code'=>$code,'area_type'=>$area_type];
   
      }


    /**
     * 删除
     * @param $id
     * @return string
     */
    public function delData($id)
    {
        $res = $this->where(['id'=>$id])->delete();
        return $res;
    }

    /**
     * User: 聂风 Function:实名认证状态
     * @param $userId 用户id
     */
    public function realStatus($userId)
    {
        $process = self::where('user_id', $userId)->value('process');
        return $this->switchStatus($process);
    }

    //转换状态
    public function switchStatus($process)
    {
        if(empty($process) || $process == 'none'){
            //未验证
            $realStatus = 1;
        }elseif($process == 'pending'){
            //待审核
            $realStatus = 2;
        }elseif($process == 'invalid'){
            //验证不通过
            $realStatus = 3;
        }elseif($process == 'success'){
            //实名认证成功
            $realStatus = 4;
        }
        return $realStatus;
    }
}
