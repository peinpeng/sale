<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\model\credit;

use think\Model;

class CreditUserLog extends Model {

	// action_type 类型
    const GOOD_COMMENT             = 1;  //1 好评
    const MIDDLE_COMMENT           = 2;  //2 中评
    const BAD_COMMENT              = 3;  //3 差评
    const COMPLAIN                 = 4;  //4 投诉
    const THAW_ADMIN               = 5;  //5 后台解冻
    const ADDCREDITSCORE_ADMIN     = 6;  //6 后台添加信用分
    const VOUCHER_PIC_INTHREEDAYS  = 7;  //7 商家3天内上传发货凭证
    const VOUCHER_PIC_OUTSEVENDAYS = 8;  //8 商家7天内未上传发货凭证
    const LOCK_ADMIN               = 9;  //9 后台主动冻结用户（永久）

    //1.好评 2中评 3差评  4投诉
    // protected $_actionType = [
    //     'good_comment'      => '好评',
    //     'middle_comment'    => '中评',
    //     'bad_comment'       => '差评',
    //     'complain'          => '投诉',
    // ]; 
	
	//1.好评 2中评 3差评  4投诉
	protected $_actionType = [
        1 => '好评',
        2 => '中评',
        3 => '差评',
        4 => '投诉',
		5 => '后台解冻',
        6 => '后台添加信用分',
        7 => '商家3天内上传发货凭证',
        8 => '商家7天内未上传发货凭证',
        9 => '后台主动冻结用户',
	]; 

    protected $_creditUserActionType = [
        1 => 'good_comment_count',
        2 => 'middle_comment_count',
        3 => 'bad_comment_count',
        4 => 'complain_count',
    ]; 

    protected $_sourceTable = [
        1 => 'give_gift',
        2 => 'give_gift',
        3 => 'give_gift',
        4 => 'complaint',
        5 => 'credit_user_log',
        6 => 'credit_user_log',
        7 => 'give_gift',
        8 => 'give_gift',
        9 => 'credit_user_log',
    ];     

	//自定义初始化
    protected static function init()
    {
        //TODO:自定义的初始化
    }
    public function user()
    {
        return $this->hasOne('app\Common\model\Users','user_id','user_id')->field('user_id,nickname,mobile');
    }
    public function getActionTypeNameAttr($value,$row)
    {
        return $this->_actionType[$row['action_type']];
    }
    public function getActionType()
    {
        return $this->_actionType;
    }

    /**
     * 用户信用评分记录表 并同步用户信用评分表
     */
    public function addData($user_id,$action_type,$point = 0,$source_id = 0, $des = '')
    {
        $creditUserMdl = model('Common/credit/CreditUser');
        $credit_user = $creditUserMdl->getInfo($user_id);
        if(!array_key_exists($action_type,$this->_actionType)){
            $this->error = '类型有误';
            return false;
        }

        $point = $this->getPointByActionType($action_type,$point);//类型获取积分

        $term = array_key_exists($action_type,$this->_actionType)?$this->_creditUserActionType[$action_type]:'';

        $after_point  = $creditUserMdl->format_point($credit_user['total_point'] + $point);//操作后积分数据 0 -100
        $source_table = array_key_exists($action_type,$this->_sourceTable)?$this->_sourceTable[$action_type]:'';//来源表

        $data = [
            'user_id'       => $user_id,
            'action_type'   => $action_type,
            'point'         => $point,
            'after_point'   => $after_point,
            'source_id'     => $source_id,
            'source_table'  => $source_table,
            'des'           => $des,
            'add_time'      => time(),
        ];

        trans();
        //更新CreditUser
        $cuData = [
            'total_point'   => $after_point,
            'update_time'   => time(),
        ];
        
        $res = $creditUserMdl->where(['user_id'=>$user_id])->update($cuData);
        if(!$res){
            $this->error = 'CreditUser->update:fail';
            trans('rollback');
            return false;
        }

        $log_id = $this->add($data);
        if(!$log_id){
            $this->error = 'CreditUserLog->add:fail';
            trans('rollback');
            return false;
        }

        //判断积分冻结用户

        trans('commit');
        $this->checkLock($user_id,$log_id);//冻结状态判断 与操作
        $this->updateCreditCount($user_id);//更新数量
        return $log_id;
    }

    //类型获取积分
    public function getPointByActionType($action_type,$point = 0)
    {
        $credit_config  = tpCache('credit_config');
        switch ($action_type) {
            case '1':
                $term = 'good_comment';
                break;
            case '2':
                $term = 'middle_comment';
                break;
            case '3':
                $term = 'bad_comment';
                break;
            case '7':
                $term = 'deliver3_inter';
                break;
            case '8':
                $term = 'deliver7_outer';
                break;        
            default:
                return $point;
                break;
        }
        return $credit_config[$term];
    }

    //冻结状态判断 与操作 
    public function checkLock($user_id,$log_id)
    {
        $credit_config  = tpCache('credit_config');
        // dump($credit_config);

        $forever_time   = 2133999048;//永久冻结时间
        $lock_end_time  = 0;//冻结时间
        $lock_des       = '';//锁定描述

        //当前信用分
        $creditUserMdl  = model('Common/credit/CreditUser');
        $credit_user    = $creditUserMdl->getInfo($user_id);
        $point          = $credit_user['total_point'];
        $log            = $this->where(['id'=>$log_id])->order('id desc')->find();

        //三段积分临界点判断
        if($point < $credit_config['frozen_forever'] && $point-$log['point'] >= $credit_config['frozen_forever']){
            $lock_end_time  = $forever_time;
            $lock_des       = "由于该账号信用分低于{$credit_config['frozen_forever']}分，账号被永久冻结，解冻请联系客服！";
        }elseif($point < $credit_config['frozen_one_month'] && $point-$log['point'] >= $credit_config['frozen_one_month']){
            $days = 30;
            $lock_end_time = $days * 60 * 60 * 24 + time();
            $thaw_date = date('Y年m月d日 H时i分s秒',$lock_end_time);
            $lock_des       = "由于该账号信用分低于{$credit_config['frozen_one_month']}分，账号被冻结{$days}天，预计{$thaw_date}解除冻结，在此期间将不能收到闯关审核！";
        }elseif($point < $credit_config['frozen_one_week'] && $point-$log['point'] >= $credit_config['frozen_one_week']){
            $days = 7;
            $lock_end_time = $days * 60 * 60 * 24 + time();
            $thaw_date = date('Y年m月d日 H时i分s秒',$lock_end_time);
            $lock_des       = "由于该账号信用分低于{$credit_config['frozen_one_week']}分，账号被冻结{$days}天，预计{$thaw_date}解除冻结，在此期间将不能收到闯关审核！";
        }

        //被投诉次数（不包括以前触发冻结的投诉）
        $complaint_num = $credit_config['complaint_num'];
        if($complaint_num>0){
            $cond1 = ['user_id'=>$user_id,'action_type'=>self::COMPLAIN,'to_use'=>0];
            $complain_count = $this->where($cond1)->count();
            if($complain_count >= $complaint_num){
                $lock_end_time = $forever_time;
                $lock_des = "由于该账号被投诉成功{$complain_count}次，账号被永久冻结，解冻请联系客服！";             
            }
        }

        //后台主动冻结
        if($log['action_type'] == self::LOCK_ADMIN){
            $lock_end_time = $forever_time;
            $lock_des = "账号已被系统管理员永久冻结，【来自后台操作】。";  
        }

        return $this->lockUser($log_id,$user_id,$lock_end_time,$lock_des);
    }

    //冻结操作
    public function lockUser($log_id,$user_id,$lock_end_time,$lock_des)
    {
        if($lock_end_time <= 0) return ['status'=>0,'msg'=>'锁定时间为0'];

        if($this->isLockState($user_id)){
            if($lock_end_time <= $this->lock_end_time($user_id)){
                return ['status'=>1,'msg'=>'已存在锁定时间更长的冻结'];
            }
        }

        trans();
        $res = $this->where(['id'=>$log_id,'user_id'=>$user_id])->update(['is_lock'=>1,'lock_end_time'=>$lock_end_time,'update_time'=>time(),'lock_des'=>$lock_des]);
        if(!$res){
            trans('rollback');
            return ['status'=>0,'msg'=>'CreditUserLog->update:fail'];
        }
        $res = model('Common/Users')->where(['user_id'=>$user_id])->update(['is_lock'=>1,'update_time'=>time()]);
        if(!$res){
            trans('rollback');
            return ['status'=>0,'msg'=>'Users->update:fail'];
        }
        $cond1 = ['user_id'=>$user_id,'action_type'=>self::COMPLAIN,'to_use'=>0];
        $this->where($cond1)->update(['to_use'=>1]);
        trans('commit');
        return ['status'=>1,'msg'=>'ok'];
    }

    //冻结结束时间
    public function lock_end_time($user_id)
    {
        return $this->where(['user_id'=>$user_id,'is_lock'=>1])->order('lock_end_time desc')->value('lock_end_time');
    }

    //冻结描述
    public function lock_des($user_id)
    {
        return $this->where(['user_id'=>$user_id,'is_lock'=>1])->order('lock_end_time desc')->value('lock_des');
    }


    //冻结状态判断 (会根据冻结时间更新冻结状态)
    public function isLockState($user_id)
    {
        $is_lock_user = model('Common/Users')->where(['user_id'=>$user_id])->value('is_lock');
        if(!$is_lock_user) return false;
        $lock_end_time = $this->lock_end_time($user_id);
        if(!$lock_end_time) return false;
        if($lock_end_time < time()){
            //冻结时间结束，开始解冻
            $res = model('Common/Users')->where(['user_id'=>$user_id])->update(['is_lock'=>0,'update_time'=>time()]);
            return false;
        }
        return true;
    }

    //后台解冻
    public function thawAdmin($user_id)
    {
        if(!$this->isLockState($user_id)) return true;//是否冻结
        $log_id = $this->where(['user_id'=>$user_id,'is_lock'=>1])->order('lock_end_time desc')->value('id');
        trans();
        $res = $this->addData($user_id,self::THAW_ADMIN,0,$log_id,'后台解冻');
        if($res === false){
            trans('rollback');
            return ['status'=>0,'msg'=>'CreditUserLog->add:fail'];
        }
        $res = $this->where(['id'=>$log_id,'user_id'=>$user_id])->update(['is_lock'=>2,'update_time'=>time()]);
        if($res === false){
            trans('rollback');
            return ['status'=>0,'msg'=>'CreditUserLog->update:fail'];
        }
        $res = model('Common/Users')->where(['user_id'=>$user_id])->update(['is_lock'=>0,'update_time'=>time()]);
        if($res === false){
            trans('rollback');
            return ['status'=>0,'msg'=>'Users->update:fail'];
        }
        trans('commit');
        return $this->thawAdmin($user_id);
    }

   
    //获取数据类型名称 by 来源
    public function logBySource($source_id,$source_table)
    {
        return $this->where(['source_id'=>$source_id,'source_table'=>$source_table])->order('id desc')->find();
    }

    //
    public function logComment($source_id)
    {
        return $this->where(['source_id'=>$source_id,'action_type'=>['in',[1,2,3]]])->order('id desc')->find();
    }

    //重新评论
    public function commentAgain($log_id,$user_id,$action_type)
    {
        $log = $this->where(['id'=>$log_id,'action_type'=>['in',[1,2,3]]])->find();
        if(!$log) return ['status'=>0,'msg'=>'评论不存在'];

        $creditUserMdl = model('Common/credit/CreditUser');
        $credit_user = $creditUserMdl->getInfo($user_id);

        $old_term       = $this->_creditUserActionType[$log['action_type']];
        $new_term       = $this->_creditUserActionType[$action_type];
        $new_point      = $this->getPointByActionType($action_type);
        $old_point      = $log['point'];
        $total_point    = $creditUserMdl->format_point($credit_user['total_point']+$new_point-$old_point);//类型获取积分


        trans();

        $res = $this->where(['id'=>$log_id])->update(['point'=>$new_point,'action_type'=>$action_type,'update_time'=>time(),'des'=>'重新评论','comment_again'=>1,'after_point'=>$total_point]);
        if($res === false){
            trans('rollback');
            return ['status'=>0,'msg'=>'CreditUserLog->update:fail'];
        }

        $res = $creditUserMdl->where(['user_id'=>$user_id])->update(['total_point'=>$total_point,'update_time'=>time()]);
        if(!$res){
            trans('rollback');
            return ['status'=>0,'msg'=>'CreditUser->update:fail'];
        }

        trans('commit');
        $this->checkLock($user_id,$log_id);//冻结状态判断 与操作
        $this->updateCreditCount($user_id);//更新数量
        return ['status'=>1,'msg'=>'评价成功'];

    }

    //根据发货凭证上传时间 操作信用评分
    public function checkVoucherPic()
    {
        $model = model('GiveGift');

        $day1 =3;
        $day2 =7;
        $time1 = $day1 * 24 * 60 * 60;
        $time2 = $day2 * 24 * 60 * 60;
        $time3 = time() - $time2;
        //3天内发货的
        $row1 = $model->where(['voucher_credit_state'=>0,'collect_type'=>2])->where(['voucher_time'=>['>',0],'voucher_time - addtime'=>['<=',$time1]])->page('1,100')->select();//3天内发货的
        //7天未发货的
        $row2 = $model->where(['voucher_credit_state'=>0,'collect_type'=>2])->where("(`voucher_time` = 0  AND `addtime` < {$time3}) OR voucher_time - addtime >= {$time2}")->page('1,100')->select();
        
        $ids1 = get_arr_column($row1, 'id');
        $ids2 = get_arr_column($row2, 'id');
        $res = $model->where(['id'=>['in',array_merge($ids1,$ids2)]])->update(['voucher_credit_state'=>1]);
        if($res){
            //3天内发货的
            foreach ($row1 as $k1 => $v1) {
                $res = $this->addData($v1['collect_user_id'],self::VOUCHER_PIC_INTHREEDAYS,0,$v1['id'],'商家3天内上传发货凭证');
            }
            //7天未发货的
            foreach ($row2 as $k2 => $v2) {
                $res = $this->addData($v2['collect_user_id'],self::VOUCHER_PIC_OUTSEVENDAYS,0,$v2['id'],'商家7天内未上传发货凭证');
            }
        }
        return true;
    }

    //发货后xx天未评价的自动好评
    public function autoComment()
    {
        $model = model('GiveGift');
        $credit_config  = tpCache('credit_config');
        $auto_comment = $credit_config['auto_comment'];//发货后xx天未评价的自动好评

        //发货后xx天未评价的
        $time = time() - $auto_comment * 24 * 60 * 60;
        $row = $model->where(['comment_state'=>0])->where("`voucher_time` > 0 AND `voucher_time` <= {$time}")->page('1,100')->select();
        $ids = get_arr_column($row, 'id');
        $res = $model->where(['id'=>['in',$ids]])->update(['comment_state'=>1]);
        if($res){
           foreach ($row as $k => $v) {
                $res = $this->addData($v['collect_user_id'],self::GOOD_COMMENT,0,$v['id'],"发货后{$auto_comment}天自动好评");
                $ids = $v['id'];
            } 
        }
        return true;
    }

    //更新数量
    public function updateCreditCount($user_id)
    {
        $creditUserMdl = model('Common/credit/CreditUser');
        $credit_user = $creditUserMdl->getInfo($user_id);
        $data = ['update_time'=>time()];
        $tmp = $this->_creditUserActionType;
        foreach ($tmp as $k => $v) {
            $data[$v] = $this->where(['user_id'=>$user_id,'action_type'=>$k])->count();
        }
        return $creditUserMdl->where(['user_id'=>$user_id])->update($data);
    }

    /**
     * 批量更新函数
     * @param $data array 待更新的数据，二维数组格式
     * @param array $params array 值相同的条件，键值对应的一维数组
     * @param string $field string 值不同的条件，默认为id
     * @return bool|string
     */
    public function batchUpdate($data, $field, $params = [])
    {
       if (!is_array($data) || !$field || !is_array($params)) {
          return false;
       }

        $updates = parseUpdate($data, $field);
        $where = parseParams($params);

        // 获取所有键名为$field列的值，值两边加上单引号，保存在$fields数组中
        // array_column()函数需要PHP5.5.0+，如果小于这个版本，可以自己实现，
        // 参考地址：http://php.net/manual/zh/function.array-column.php#118831
        $fields = array_column($data, $field);
        $fields = implode(',', array_map(function($value) {
            return "'".$value."'";
        }, $fields));

        $sql = sprintf("UPDATE `%s` SET %s WHERE `%s` IN (%s) %s", 'post', $updates, $field, $fields, $where);

       return $sql;
    }
    /**
     * 将二维数组转换成CASE WHEN THEN的批量更新条件
     * @param $data array 二维数组
     * @param $field string 列名
     * @return string sql语句
     */
    public function parseUpdate($data, $field)
    {
        $sql = '';
        $keys = array_keys(current($data));
        foreach ($keys as $column) {

            $sql .= sprintf("`%s` = CASE `%s` \n", $column, $field);
            foreach ($data as $line) {
                $sql .= sprintf("WHEN '%s' THEN '%s' \n", $line[$field], $line[$column]);
            }
            $sql .= "END,";
        }

        return rtrim($sql, ',');
    }

    /**
     * 解析where条件
     * @param $params
     * @return array|string
     */
    public function parseParams($params)
    {
       $where = [];
       foreach ($params as $key => $value) {
          $where[] = sprintf("`%s` = '%s'", $key, $value);
       }
       
       return $where ? ' AND ' . implode(' AND ', $where) : '';
    }





}
