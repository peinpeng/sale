<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */

namespace app\common\model\credit;

use app\common\model\Base;

class CreditUser extends Base {
    
    protected $pk = 'id';

    //自定义初始化
    protected static function init()
    {
        //TODO:自定义的初始化

    }

    /**
     * 初始化 wallet_all 表数据
     */
    public function initData($user_id)
    {
        $data = $this->where('user_id', $user_id)->value('id');
        if ($data) return true;
        $data = [
            'user_id' => $user_id,
            'add_time' => time()
        ];
        $this->insert($data);
    }

    //获取数据
    public function getInfo($user_id)
    {
    	$info = $this->where(['user_id'=>$user_id])->find();
    	if(!$info){
    		$this->initData($user_id);
    		return $this->where(['user_id'=>$user_id])->find();
    	}
    	return $info;
    }

    //冻结状态判断
    public function isLockState($user_id)
    {
    	
    }

    public function format_point($point)
    {
        if($point>100) return 100;
        if($point<0) return 0;
        return $point;
    }

}
