<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\distribut;
use app\common\model\Base;
class DistributMoneyIncomeLog extends Base {

    protected $prizeType = [
        1   =>   '直推奖',
        2   =>   '补贴',
        3   =>   '加权分红',
        4   =>   '运营分红',
    ];

   /**
    * [getPrizeType 获取奖金类型]
    * @return [type] [description]
    */
   public function getPrizeType()
   {
       return $this->prizeType;
   } 

   /**
    * [getPrizeTypeName 获取奖金显示名]
    * @param  integer $prizeType [description]
    * @return [type]             [description]
    */
   public function getPrizeTypeName($prizeType = 0)
   {
        return (string)$this->prizeType['prizeType'];
   }

   //关联受益用户模型
   public function benefitUsers()
   {
       return $this->hasOne('app\common\model\Users','user_id', 'benefit_user_id')->field('id, money, mobile, nickname, userface');
   }

   // 关联订单模型
   public function order(){
       return $this->hasOne('app\common\model\Order', 'order_id', 'source_id');
   }

   /**
    * 预计收益录入记录
    * @param $user_id      用户id
    * @param $money        变动金额（负数代表减少金额）
    * @param $after_money  变动后金额
    * @param $type         收益类型: 1直推,2绩效补贴,3培育,4绩效分红,5运营分红,6股东分红,
    * @param $source_table 来源表
    * @param $source_id    来源id
    * @param string $des   变动说明
    * @return int|string
    */
   public function addIncomeLog($data)
   {
       foreach ($data as $key => $value) {
            $insertData[]  = [
               'buy_user_id'        =>  $value['buy_user_id'],
               'benefit_user_id'    =>  $value['benefit_user_id'],
               'money'              =>  $value['money'],
               'state'              =>  isset($value['state'])?$value['state']:1,      
               'type'               =>  $value['type'],
               'source_table'       =>  $value['source_table'],
               'source_id'          =>  $value['source_id'],
               'des'                =>  $value['des'],
               'create_time'        =>  time()
           ] ;
       }
       return $this->insertAll($insertData);
   }

   // 查询到账收益已超过15天，并修改预计收益表状态并拨款到余额
   public function updateState(){
       $where['state']         = 2;
       $where['update_time']   = ['<', time()-15*24*60*60];
       $data = $this->where($where)->select();
       if (!empty($data)) {
           //拨款到余额并生成余额记录
           $res = model('Common/distribut/DistributMoneyLog')->addMutipleLog($data);
           if ($res) {
               $ids = get_arr_column($data, 'id');
               $this->where(['id'=>['IN',$ids]])->update(['state' => 3]);
           }
       }
   }

   // 查询收益金额
   public function getIncome($where, $size = 0, $page = 0){
       $limit = "";
       if ($size > 0) {
           $limit = $size * $page . ',' . $size;
       }
       return $this->where($where)->limit($limit)->sum('money')?:'0.00';
   }

   // 由预计状态改为收益到账状态
   public function changeState($orderIds){
       if ($orderIds) {
           $where['state']      =  1;
           $where['source_id']  =  ['IN',$orderIds];
           if ($this->where($where)->select()) {
               return $this->where($where)->setField(['update_time' => time(), 'state' =>2]);
           }else{
               return false;
           }
       }
   }


   public function getIncomeDetail($where){
       $detail = $this->where($where)->find();
       switch ($detail['state']) {
           case 1:
               $detail['state'] = '预计收益';
               unset($detail['update_time']);
               break;
           case 2:
               $detail['state'] = '到账收益';
               break;
           case 3:
               $detail['state'] = '已拨款';
               break;
       }
       $buyUser = model('Common/Member')->getRow($detail['buy_user_id'])['nickname'];
       $order = model('Common/Order')->get_ynwt_order($detail['source_id'], $detail['buy_user_id']);
       $source = $order['order_type']==1?'课程':'资源';
       $detail['explain'] = '用户【'.$buyUser.'】购买'.$source.'消费'.$order['total_amount'].'元';
       return $detail;
   }
   /**
    * 获取用户指定条件内的收益
    * @param number $user_id
    * @param unknown $where  */
   public function getUserProfit($user_id=0,$where=[]){
       $where['benefit_user_id']=$user_id;
       return $this->where($where)->sum('money');
   }
 

}
