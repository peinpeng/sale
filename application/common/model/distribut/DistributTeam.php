<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\distribut;
use app\common\model\Base;

use think\Db;
use think\Model;

class DistributTeam extends Base {
     public function users()
    {
        return $this->hasOne('app\common\model\Users', 'user_id', 'user_id');
    }

    //根据user_id表获取用户的上级链id串 用（）包围id 没有则更新
    public function getParentIdStr($user_id){

        $userTeam = $this->where(['user_id'=>$user_id])->find();
        if(!empty($userTeam)) return $userTeam['parent_id_str'];
        
        $parent_id = model('Common/Users')->where(['user_id'=>$user_id])->value('first_leader');
        if(empty($parent_id)) return 'team';
        
        $parent_team = $this->where(['user_id'=>$parent_id])->find();
        
        if($parent_team){
            $p_parent_id_str = $parent_team['parent_id_str'];
        }else{
            $p_parent_id_str = $this->getParentIdStr($parent_id);
        }
       
        $parent_id_str = $p_parent_id_str.'_('.$parent_id.')';
        
        $this->insertGetId(['user_id'=>$user_id,'parent_id_str'=>$parent_id_str,'parent_id'=>$parent_id,'add_time'=>time()]);
        
        return $parent_id_str;
    }

    //获取上级链user_id数组 最近的直推链放前面
    public function getFirstLeaderTeam($user_id)
    {
        $userTeam = $this->where(['user_id'=>$user_id])->find();
        if(empty($userTeam)) return false;
        $ids = substr($userTeam['parent_id_str'],5);
        $ids = str_ireplace(['(',')'],'',$ids);
        $ids = explode('_', $ids);
        $ids = array_reverse($ids);
        return $ids;
    }

    //获取会员全网体团队成员userIds 包括下级的下级的下级的...
    public function getUserTeamsAll($user_id)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $res = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->column('user_id');
        return $res;
    }

    //分页获取会员全网体团队成员userIds 包括下级的下级的下级的...
    public function getUserTeamsAllForPage($user_id,$page,$pageSize)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $res = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->page($page,$pageSize)->column('user_id');
        //echo \think\Db::getLastSql();die;
        $count = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->count();
        return ['list'=>$res, 'count'=>$count];
    }

    //获取会员全网体团队成员userIds 包括下级的下级的下级的...
    public function getUserTeamsAllForCJ($user_id,$p=0)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        // $res = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->column('user_id');
        $res = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->page($p,10)->column('user_id');
        return $res;
    }

    //获取会员全网体团队成员userIds 包括下级的下级的下级的...
    public function getUserTeamsAllForMobile($user_id)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $res = $this->alias('a')
        ->join('Users u','a.user_id=u.user_id','LEFT')
        ->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])
        ->column('a.parent_id,a.user_id,u.mobile','a.user_id');
        return $res;
    }

    //层级显示
    public function getUserLevelUpListOne($user_id)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $res = $this
        ->with('users')
        ->where(['parent_id'=>$user_id])
        ->select();
        $res = collection($res)->toArray();
        $data = [];
        foreach ($res as $k => $v) {
            $v['is_lower'] = $this->where(['parent_id'=>$v['user_id']])->count()?1:0;
            $v['is_lower'] ?array_unshift($data, $v): array_push($data, $v);
        }
        return $data;
    }

    //获取会员全网体团队成员的信息 包括下级的下级的下级的...
    public function getUserTeamsAllInfo($user_id)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $res = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->select();
        return $res;
    }


    /**
     * [teamNum 获取会员全网体团队人数（或者大于某个等级）]
     * @param  [type]  $user_id [用户编号]
     * @param  integer $level   [用户等级]
     * @return [type]           [description]
     */
    public function teamNum($user_id,$level = 0)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $number = 0;
        if(empty($level))
        {
            $number = $this->alias('d')->join('users u','u.user_id = d.user_id','LEFT')->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->count();
        }else{
            // $userIds = $this->getUserTeamsAll($user_id);
            // $number  = model('Common/Users')->where(['level'=>['egt',$level]])->whereIn('user_id',array_filter(array_values($userIds)))->count();
            $number = $this->alias('d')->join('users u','u.user_id = d.user_id','LEFT')->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%'],'level'=>['egt',$level]])->count();
        }
        return $number;
    }



    /**
     * [getUserParentIds 获取用户直推链用户信息]
     * @param  [type] $user_id [description]
     * @return [type] $type    判断是否获取流失后的上级直推链 'loss'为流失
     */
    public function getUserParentIds($user_id,$type='')
    {
        $parent_id_str = $this->getParentIdStr($user_id);
        if($type == 'loss') $parent_id_str = $parent_id_str.'_('.$user_id.')';
        preg_match_all("/\(([0-9]+)\)/s", $parent_id_str, $match);
        return $match[1];
    }

    /**
     * User: Simony
     * Function:统计用户团体人数
     * @param $user_id
     * @return int|string
     */
    public function getUserTeamsAllCount($user_id)
    {
        $praent_id_str = $this->getBy('user_id',$user_id,'parent_id_str');
        $count = $this->where(['parent_id_str'=>['like',$praent_id_str.'_('.$user_id.')%']])->count();
        return $count;
    }

    /**
     * 变更上级，更换上级链.
     * /mobile/Test/updateUserParent/mobile/18520591050/pmobile/18520591015
     * @param number $mobile
     * @param number $pmobile
     * @return type
     */
    public function updateUserParent($mobile, $pmobile)
    {

        if($mobile == $pmobile){
            $this->error = '不能将上级设置成自己！';
            return false;
        }
        $userInfo = model('Common/Users')->getBy('mobile',$mobile);
        $pUserInfo = model('Common/Users')->getBy('mobile',$pmobile);
        if(empty($userInfo) || empty($pUserInfo)){
            $this->error = '权限不足,或者账号有问题';
            return false;
        }
        if($pUserInfo['level'] < 2){
            $this->error = '上级必须为VIP1及以上等级';
            return false;
        }
        $userId = $userInfo['user_id'];
        $parentId = $pUserInfo['user_id'];

        $teamAll = $this->getUserTeamsAll($userId);
        if(in_array($parentId, $teamAll)){
            $this->error = '不能把你下级团队的成员修改成你的上级';
            return false;
        }

        $userTeam = $this->where(['user_id' => $userId])->find();
        // p($userTeam);
        $parentTeam = $this->where(['user_id' => $parentId])->find();

        $this->where(['user_id' => $userId])->update(['parent_id' => $parentId]);
        //新上级链
        $parentIdsNew = $parentTeam ? $parentTeam[parent_id_str].'_('.$parentId.')' : 'team_('.$parentId.')';
        //更新所有$userId的子孙及他自己的上级链
        $sql = "update saas_distribut_team set `parent_id_str` = REPLACE (parent_id_str,'$userTeam[parent_id_str]','$parentIdsNew')
        where  (`parent_id_str` like '$userTeam[parent_id_str]_($userId)%' ) or `id`='$userTeam[id]'";
         
        $res = Db::execute($sql);
        if($res) {
            model('Common/Users')->updateBy(['user_id'=>$userId],['first_leader'=>$parentId]);
            return true;
        }else{
            $this->error = '更新失败';
            return false;
        }
    }
    


}
