<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model\distribut;
use app\common\model\Base;
class DistributLevelConfig extends Base {

    const DISTRIBUT_VIP = 'vip';
    const DISTRIBUT_TRADE = 'trade';

    /**
     * [getDistributConfig 根据分销类型获取分红等级配置信息]
     * @param  string $distributType [description]
     * @return [type]                [description]
     */
    public function getDistributConfig($distributType = '')
    {
        if(empty($distributType)){
            $this->error = '分销等级配置信息出现错误!';
            return false;
        }

        $distributConfig = $this->where(['distribut_type'=>$distributType])->column('direct_prize,role_prize,weight_prize,operation_prize','level_id');
        if(!empty($distributConfig)) return $distributConfig;
        $this->error = '分销等级配置信息为空';
        return false;
    }


}
