<?php
/**
 * ============================================================================
 * 二维码  
 * ============================================================================
 */
namespace app\common\model;
use think\Model;
class Qrcode extends Model {

    protected $tk_type_arr = [
        1 => '手机',
        2 => '微信'
    ];

    public function getTkTypeChAttr($value,$row)
    {
        $ch = '';
        $tk_types = explode(',', $row['tk_type']);
        foreach ($tk_types as $v) {
            empty($this->tk_type_arr[$v]) || $ch .= $this->tk_type_arr[$v].',';
        }
        return trim($ch,',');
    }

    public function getTkTypeArrAttr($value,$row)
    {
        $tk_types = explode(',', $row['tk_type']);
        return $tk_types;
    }

    public function getContactTypeArr()
    {
        return $this->contact_type_arr;
    }



}
