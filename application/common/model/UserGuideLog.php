<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class UserGuideLog extends Base
{
    
   
    public function user(){
        return $this->hasOne('users','user_id','user_id');
    }

     /**
      * 增加用户的已接点人数和可接点人数及引荐人数
      * $connecter_id 用户id
      * $slipping_num 接点人数
      * $surplus_num  可接点人数
      * $referrer_num 引荐人数
      */
    public function addData($data){
        if( !$data['user_id'] ){
            return false;
        }
        $result=$this->add([
            'user_id'  => $data['user_id'],
            'guide_ids'  => $data['guide_ids'],
            'add_time' => time(),
        ]);
        if(!$result){
            $this->error = 'UserGuideLog->add:fail';
            return false;
        }
       
       return $result;
       
    }
     
    public function lists($size,$cond,$with=[],$orderBy='add_time desc'){
        return paging($this->where($cond)->count(),$size,$this->with($with)->where($cond)->order($orderBy));
    }
}
