<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: pb
 * Date: 2018-10-13
 */
namespace app\common\model;
use think\Model;
class Base extends Model {
    
    protected $error = '';
    /**
     * [getError 获取出错信息]
     * @return [type] [description]
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * [setError 设置模型错误信息]
     * @param string $error [description]
     */
    public function setError($error = '')
    {
       return $this->error = $error;
    }

    /**
     * [getList 获取列表]
     * @param  [type]  $cond    [description]
     * @param  [type]  $page    [第几页，]
     * @param  boolean $isTotal [description]
     * @param  [type]  $with    [description]
     * @return [type]           [description]
     */
    public function getList($cond=[],$page=[],$isTotal=false,$with=[],$orderBy='',$limit=false)
    {
        if(!empty($isTotal) && !empty($page) && !empty($page['pageSize'])){
            
            $data['list']  = $this->with($with)->where($cond)->page((int)$page['page'],$page['pageSize'])->order($orderBy)->select();
            $data['count'] = $this->where($cond)->count();
        }elseif(!empty($isTotal)){
            $data['list']  = $this->with($with)->where($cond)->order($orderBy)->select();
            $data['count'] = $this->where($cond)->count();
        }
        else{
            $data   =  $this->with($with)->where($cond)->limit($limit)->order($orderBy)->select();
        }
        return $data;
    }

    public function getCol($field = '*',$where = [])
    {
        return $this->where($where)->column($field);
    }


    /**
     * [getByPk 获取模型信息]
     * @param  integer $pkId [模型主键编号]
     * @param  string $field [查询字段]
     * @return [type]        [description]
     */
    public function getByPk($pkId = 0,$field = '*')
    {
        if(empty($pkId)) return '';
        return $this->where([$this->pk=>$pkId])->field($field)->find();
    }


    /**
     * [saveData 添加编辑数据]
     * @param  [type]  $data [description]
     * @param  integer $pkId [description]
     * @return [type]        [description]
     */
    public function saveData($data = [],$pkId = 0)
    {
        if(empty($data)) {
            $this->error = '传递过来的数据为空';
            return false;
        }
        
        if(!empty($this->field)) foreach ($this->field as $k => $v) {
            if(!isset($data[$v])) unset($data[$v]);
        } 

        if($pkId > 0) {
            $result =  $this->where([$this->pk=>$pkId])->update($data);  
            if(!$result) $this->error = '保存出现错误';
        }else{
            $result =  $this->save($data);
            if(!$result) $this->error = '添加出现错误';
        }
        return !empty($this->error) ? false : $result;  
    }

    /**
     * [getBy 根据表的一个属性获取值]
     * @param  string $attr  [description]
     * @param  string $value [description]
     * @param  string $field [description]
     * @return [type]        [description]
     */
    public function getBy($attr = '',$value = '',$field = '*')
    {   
        if(empty($attr) || empty($value)) return '';
        if($field == '*')  return $this->where([$attr=>$value])->field($field)->find();
        return $this->where([$attr=>$value])->value($field);
    }

    /**
     * [getCount 获取数据总量]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function getCount($where = [])
    {
        return $this->where($where)->count();
    }


    //获取单条信息
    public function getRow($id, $field = true)
    {
        return $this->where('id', $id)->field($field)->find();
    }


    /**
     * [updateBy 根据条件更新]
     * @param  [type] $where [description]
     * @param  [type] $data  [description]
     * @return [type]        [description]
     */
    public function updateBy($where = [],$data = [])
    {
        if( empty($where) || empty($data) ) return false;
        return $this->where($where)->update($data);
    }

}
