<?php
/**
 * ============================================================================
 * 公众号  
 * ============================================================================
 */
namespace app\common\model;
use think\Model;
class WechatOfficialAccount extends Model {

    public function getWechatImage()
    {
        $pic = self::where(['enabled'=>1])->value('pic');
        return image_handle($pic);
    }
}
