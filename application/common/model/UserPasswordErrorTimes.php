<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class UserPasswordErrorTimes extends Base
{
    //增加错误次数
    public function setErrorTimes($user_id){
        if($user_id<1) return false;
        $info = $this->where(['user_id'=>$user_id])->find();
        $login_config  = tpCache('login_config');
        if(!$info){
            $data = [
                'user_id'=>$user_id,
                'times'=>1,
            ];
            $res = $this->add($data);
            if($res){
                if(1 == $login_config['password_error_times']){
                     $this->setLock($user_id);
                }
                return true;
            }
            return false;
        }
        //错误次数已满 冻结
        if($info['times']+1 >= $login_config['password_error_times']){
            if($info['end_time']>time()) return true;
             $this->setLock($user_id);
        }
        $res = $this->where(['user_id'=>$user_id])->setInc('times'); //错误次数+1
        if($res){
            return true;
        }
        return false;
    }
    //冻结用户
    public function setLock($user_id){
        //添加冻结时间和解冻时间
        $login_config  = tpCache('login_config');
        $data = [
            'start_time'=>time(),
            'is_lock'=>1,
            'end_time'=>time()+$login_config['password_error_time']*24*60*60,
        ];
        $res = $this->where(['user_id'=>$user_id])->update($data);
        $result = model('common/Users')->where(['user_id'=>$user_id])->update(['is_lock'=>1,'update_time'=>time()]);
        if(!$result) return false;
        return ture;
    }
    //时间到解冻用户
    public function setUnlock($user_id){
        //清除冻结时间 解冻时间 次数
        $data = [
            'start_time'=>0,
            'end_time'=>0,
            'times'=>0,
            'is_lock'=>0,
        ];
        $res = $this->where(['user_id'=>$user_id])->update($data);
        if(model('Common/credit/CreditUserLog')->isLockState($user['user_id']) == false){
            model('common/Users')->where(['user_id'=>$user_id])->update(['is_lock'=>0,'update_time'=>time()]);
        }
        if(!$res) return false;
        return ture;
    }
    //判断是否密码错误次数上限而冻结得用户
    public function isErrorTimesLockUser($user_id){
        
        $info = $this->where(['user_id'=>$user_id])->find();
        if($info && $info['end_time']){
            if($info['end_time']<time()) {
                $this->setUnlock($user_id);
                return false;
            }
            if($info['end_time']>time()) return ture;
        }
        return false;
    }
    //获取用户剩余错误次数
    public function getCanErrorTimes($user_id){
        
        $login_config  = tpCache('login_config');
        $times = $this->where(['user_id'=>$user_id])->value('times');
        $can_err_times = $login_config['password_error_times']-$times;
        return $can_err_times;
    }
}
