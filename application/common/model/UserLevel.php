<?php
namespace app\common\model;
use think\Db;
use app\common\logic\UsersLogic;
class UserLevel extends Base
{
    /**
     * User: Simony
     * Function: 获取从A-B等级的内容
     * @param $startLevel
     * @param $endLevel
     */
    public function getPartLevelInfo($startLevelId, $endUserId){
        $level_info = M('user_level')->where("level_id>={$startLevelId} and level_id<{$endUserId}")->select(); //等级信息
        return $level_info;
    }
}
