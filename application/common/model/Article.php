<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: IT宇宙人
 * Date: 2015-09-09
 */
namespace app\common\model;

use think\Db;
use think\Model;

class Article extends Base{
    private $_showType=[
        1 => '单图展示',
        2 => '三图展示',

    ];
    private $_articleType=[
        1 => '视频',
        2 => '图文',
        3 => '音频',

    ];
    public function getShowTypeNameAttr($value,$row){
        return $this->_showType[$row['show_type']];
    }
    public function getShowType(){
        return $this->_showType;
    }
    public function getShowTypeName($type){
        if(empty($type)) return '';
        return $this->_showType[$type];
    }
    /**
     * 文章类型转换
     */
    public function getArticleTypeNameAttr($value,$row){
        return $this->_articleType[$row['article_type']];
    }
    public function getArticleTypeName($type){
        if(empty($type)) return '';
        return $this->_articleType[$type];
    }

    /**
     * User: Simony
     * Function: getArticleList  获取文章列表
     * @param int $cat_id   文章分类id
     * @param null $title   文章筛选标题
     * @param string $order 排序字段
     * @param int $limit    限制
     * @return array
     */
    public function getArticleList($cat_id=1, $title=null,$order='article_id',$limit=5)
    {
        $Article = M('Article');
        $list = array();

        $where = " 1 = 1 ";
        $keywords = $title;
        $keywords && $where .= " and title like '%$keywords%' ";
        $cat_id && $where .= " and cat_id = $cat_id and is_open=1 ";
        $list = $Article->where($where)->order($order.' desc')->limit($limit)->select();
        return $list;
    }
     // 处理上传资源
    public function checkExtensions($type, $url)
    {
        // if ($upload['error'] == 1) return ['code' => false, 'msg' => '上传文件过大，请重新上传'];
        $extensions = [
            'music' => ['m4a', 'wav', 'mp3', 'wma', 'aac'],//录音文件扩展名
            'video' => ['3gp', 'rmvb', 'wmv', 'avi', 'mkv', 'mp4', 'mov', 'MOV'] //视频文件扩展名
        ];
        // $tmp = $upload['tmp_name'];
        // if (!empty($tmp)) {
        //     $maxSize = getRealSize('60mb'); //$maxSize=getRealSize('10kb');
        //     $systemMaxSize = uploadMaxSize();
        //     $maxUploadSize = min($maxSize, $systemMaxSize);
            // if ($upload['size'] > $maxUploadSize) return ['code' => false, 'msg' => '上传文件不得超过' . $maxUploadSize . '字节'];
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        if (!in_array($ext, $extensions[$type])) {
            $data['code'] = FALSE;
            $data['msg'] = '不支持此类型文件的上传！(' . $ext . ')';
            return $data;
        }
        return ['code'=>1,'msg'=>'ok'];
            // $path = '/public/cloudcard/material/' . $type . '/' . time() . '.' . $ext;
            // if (!is_dir(ROOT_PATH . '/public/cloudcard')) mkdir(ROOT_PATH . '/public/cloudcard', 0777, true);
            // if (!is_dir(ROOT_PATH . '/public/cloudcard/material')) mkdir(ROOT_PATH . '/public/cloudcard/material', 0777, true);
            // if (!is_dir(ROOT_PATH . '/public/cloudcard/material/' . $type)) mkdir(ROOT_PATH . '/public/cloudcard/material/' . $type, 0777, true);
            // if (!move_uploaded_file($tmp, ROOT_PATH . $path)) return ['code' => false, 'msg' => '上传失败'];
            // return ['code' => true, 'file' => $path];
        // }
    }


    //app获取用户注册协议链接
    public function getUserRegAgreementUrl(){
        $Article = M('Article');
        $name = '拓客新零售用户注册协议';
        $userRegAgreementId = $Article->where(['title'=>$name])->value('article_id');
        if($userRegAgreementId){
            $url = 'Mobile/article/detail/article_id/'.$userRegAgreementId.'.html';
        }else{
            $url = '';
        }
        return $url;
    }

    //获取协议 1:全国优品购买协议
    public function getAgreement($type){
        $Article = M('Article');
        $data = [];
        $cond = [];
        switch ($type) {
            case '1':
                $cond['title'] = '全国优品购买协议';
                $cond['is_open'] = 1;
                break;
            
            default:
                break;
        }
        $agreement = empty($cond)?[]:$Article->where($cond)->find();
        if($agreement){
            $data['title']   = $agreement['title'];
            $data['content'] = $agreement['content'];
        }
        return $data;
    }
}
