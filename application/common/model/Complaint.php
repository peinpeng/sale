<?php
/**
 * tpshop
 * ============================================================================
 * 版权所有 2015-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * Author: yaodian
 * Date: 2019-01-09
 */
namespace app\common\model;

use think\Db;
use think\Model;

class Complaint extends Base{
    
    protected $_status = [
        0 => '审核中',
        1 => '投诉成功',
        2 => '投诉失败',
        3 => '已撤销',
    ];

    public function user()
    {
        return $this->hasOne('Users','user_id','user_id')->field('user_id,head_pic,nickname,mobile');
    }

    public function respondentUser()
    {
        return $this->hasOne('Users','user_id','respondent_user_id')->field('user_id,head_pic,nickname,mobile');
    }

    public function giveGift()
    {
        return $this->hasOne('GiveGift','id','give_gift_id');
    }
    public function getStatusList()
    {
        return $this->_status;
    }
    
    public function getStatusChAttr($value,$row)
    {
        return $this->_status[$row['status']];
    }

    // public function getPicsDataAttr($value,$row)
    // {
    //     if($row['pics']) return explode(',', $row['pics']);
    //     return [];
    // }


    public function getAppealPicsDataAttr($value,$row)
    {
        if($row['appeal_pics']) return explode(',', $row['appeal_pics']);
        return [];
    }

    //审核  $reply 回复  $pass 0 审核失败 1 成功
    public function check($id, $reply = '', $pass = 1)
    {
        $data = [
            'check_time'    => time(),
            'status'        => $pass==1?1:2,
            'reply_time'    => time(),
            'reply_content' => $reply,
        ];

        trans();
        $res = $this->where(['complaint_id'=>$id])->update($data);
        if(!$res){
            $this->error = 'Complaint->update:fail';
            trans('rollback');
            return false;
        }

        //添加投诉成功信用分变记录 (同时更新被投诉成功次数)
        if($pass==1){
            $respondent_user_id = $this->where(['complaint_id'=>$id])->value('respondent_user_id');
            $model = model('Common/credit/CreditUserLog');
            $res = $model->addData($respondent_user_id,\app\common\model\credit\CreditUserLog::COMPLAIN,0,$id,'投诉成功');
            if(!$res){
                $this->error = 'CreditUserLog->addData:fail';
                trans('rollback');
                return false;
            }
        }

        trans('commit');
        return true;
    }

    //撤销投诉
    public function revoke($id,$user_id)
    {
        $data = [
            'revoke_time'   => time(),
            'status'        => 3,
        ];
        return $this->where(['complaint_id'=>$id,'user_id'=>$user_id])->update($data);
    }

    //发起申诉
    public function appeal($id, $appeal,$appeal_pics)
    {
        $data = [
            'appeal_time'   => time(),
            'appeal'        => $appeal,
            'appeal_pics'   => is_array($appeal_pics)?implode(',', $appeal_pics):$appeal_pics,
        ];
        return $this->where(['complaint_id'=>$id])->update($data);
    }

    //获取数据类型名称 by 来源
    public function getInfoByGiveGiftId($id)
    {
        return $this->where(['give_gift_id'=>$id])->find();
    }
}
