<?php
/**
 * ============================================================================
 * 二维码  
 * ============================================================================
 */
namespace app\common\model;
use think\Model;
class ShareView extends Model {

    public function qrcode()
    {
        return $this->hasOne('Qrcode','id','qrcode_id');
    }

    public function getQrcodeTitleAttr($value,$row)
    {
        return M('Qrcode')->where(['id'=>$row['qrcode_id']])->value('title')?:'无';
    }
    public function getThumbArrAttr($value,$row)
    {
        return explode(',', $row['thumb']);
    }
    public function getHtmlContentAttr($value,$row)
    {
        return htmlspecialchars_decode($row['content']);
    }


}
